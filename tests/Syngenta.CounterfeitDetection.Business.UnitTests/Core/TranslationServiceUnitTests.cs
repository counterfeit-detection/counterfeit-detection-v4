﻿using Microsoft.EntityFrameworkCore.Query;
using Moq;
using Syngenta.CounterfeitDetection.Business.Core.Implementations;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.Business.Core.Models;
using Syngenta.CounterfeitDetection.DataAccess.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Models;
using System.Linq.Expressions;

namespace Syngenta.CounterfeitDetection.Business.UnitTests.Core;

[TestFixture]
internal class TranslationServiceUnitTests {
    private ITranslationService? _translationService;
    
    private IGenericRepository<Language>? _languageRepo;
    private IGenericRepository<TranslationMaster>? _translationMasterRepo;
    private Mock<IGenericRepository<TranslationMaster>>? _translationMasterRepoMock;
    private Mock<IGenericRepository<Language>>? _languageRepoMock;

    [SetUp]
    public void SetUp(){
        _translationMasterRepo = Mock.Of<IGenericRepository<TranslationMaster>>();
        _languageRepo = Mock.Of<IGenericRepository<Language>>();

        _translationService = new TranslationService(
        _translationMasterRepo,
        _languageRepo);

        _translationMasterRepoMock = Mock.Get(_translationMasterRepo);
        _languageRepoMock = Mock.Get(_languageRepo);

        _translationMasterRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<TranslationMaster, bool>>?>(),
            It.IsAny<Func<IQueryable<TranslationMaster>, IOrderedQueryable<TranslationMaster>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<TranslationMaster>, IIncludableQueryable<TranslationMaster, object>>?>()
        )).Returns<Expression<Func<TranslationMaster, bool>>?,
        Func<IQueryable<TranslationMaster>, IOrderedQueryable<TranslationMaster>>?, int?, int?, 
        Func<IQueryable<TranslationMaster>, IIncludableQueryable<TranslationMaster, object>>?>((exp, order, skip, take, include) 
        => Task.FromResult(DummyTranslationMasters.AsQueryable().Where(exp!).AsEnumerable()));

        _languageRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Language, bool>>?>(),
            It.IsAny<Func<IQueryable<Language>, IOrderedQueryable<Language>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Language>, IIncludableQueryable<Language, object>>?>()
        )).Returns<Expression<Func<Language, bool>>?,
        Func<IQueryable<Language>, IOrderedQueryable<Language>>?, int?, int?, 
        Func<IQueryable<Language>, IIncludableQueryable<Language, object>>?>((exp, order, skip, take, include) 
        => Task.FromResult(DummyLanguages.AsQueryable().Where(exp!).AsEnumerable()));
    }

    [TestCaseSource(nameof(InvalidKeyCases))]
    public void GetTranslationByLangugeId_PassedInvalidKeys_ThrowsArgumentNullException(string key) 
        => Assert.ThrowsAsync<ArgumentNullException>(async () => await _translationService!.GetTranslation(key, (int?)null));

    private static IEnumerable<TestCaseData> InvalidKeyCases() {
        yield return new TestCaseData(
            null
        );
        
        yield return new TestCaseData(
            String.Empty
        );
        
        yield return new TestCaseData(
            "      "
        );
    }

    [Test]
    public async Task GetTranslationByLangugeId_ValidKey_ReturnsDefaultValue() {
        //Arrange
        const int languageId = 1;

        //Act
        var result = await _translationService!.GetTranslation(DummyTranslationMasterWithoutTranslations.Key!, languageId);

        //Assert
        Assert.That(result, Is.EqualTo(DummyTranslationMasterWithoutTranslations.DefaultValue));
    }
    
    [Test]
    public async Task GetTranslationByLangugeId_ValidKey_ReturnsTransaltedValue() {
        //Arrange
        const int languageId = 1;

        //Act
        var result = await _translationService!.GetTranslation(DummyTranslationMasterWithTranslations.Key!, languageId);

        //Assert
        Assert.That(result, Is.EqualTo(DummyTranslationLanguage.Value));
    }
    
    [Test]
    public async Task GetTranslationByLangugeId_UndefinedLanguageId_ReturnsDefaultValue() {
        //Arrange
        const int languageId = 3;

        //Act
        var result = await _translationService!.GetTranslation(DummyTranslationMasterWithoutTranslations.Key!, languageId);

        //Assert
        Assert.That(result, Is.EqualTo(DummyTranslationMasterWithoutTranslations.DefaultValue));
    }

    [TestCaseSource(nameof(InvalidKeyCases))]
    public void GetTranslationByLangugeCode_InvalidLanguageCode_Throws(string languageCode) 
        => Assert.ThrowsAsync<ArgumentNullException>(async () => await _translationService!.GetTranslation(DummyTranslationMasterWithoutTranslations.Key!, languageCode));
        
    [Test]
    public void GetTranslationByLangugeCode_NotExistedLanguageCode_Throws(){
        //Arrange
        const string notExistedLanguageCode = "NL";

        //Act & Assert
        Assert.ThrowsAsync<KeyNotFoundException>(async () => await _translationService!.GetTranslation(DummyTranslationMasterWithoutTranslations.Key!, notExistedLanguageCode));
    }

    private static IEnumerable<TranslationMaster> DummyTranslationMasters => new List<TranslationMaster> { 
        DummyTranslationMasterWithTranslations,
        DummyTranslationMasterWithoutTranslations,
    }.AsEnumerable();

    private static IEnumerable<TranslationLanguage> DummyLanguageTranslations => new List<TranslationLanguage> { 
        DummyTranslationLanguage,
    }.AsEnumerable();

    private static TranslationLanguage DummyTranslationLanguage => new TranslationLanguage { 
       Id = 1,
       MasterId = 1,
       LanguageId = 1,
       Value = "translated.value"
    };

    private static TranslationMaster DummyTranslationMasterWithoutTranslations => new TranslationMaster { 
        Id = 2,
        Key = "test.key.without.translations",
        DefaultValue = "test.value.without.translations"
    };
    
    private static TranslationMaster DummyTranslationMasterWithTranslations => new TranslationMaster { 
        Id = 1,
        Key = "test.key.with.translations",
        DefaultValue = "test.value.with.translations",
        LanguageTranslations = DummyLanguageTranslations
    };
    
    private static IEnumerable<Language> DummyLanguages => new List<Language> { 
        new Language{
            Code = "TL",
            Id = 1
        }
    }.AsEnumerable();
}
