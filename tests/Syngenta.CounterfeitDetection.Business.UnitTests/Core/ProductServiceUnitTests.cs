﻿using AutoMapper;
using Microsoft.EntityFrameworkCore.Query;
using Moq;
using Newtonsoft.Json;
using Syngenta.CounterfeitDetection.Business.Core.Implementations;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.Business.Core.Models;
using Syngenta.CounterfeitDetection.Business.Integrations.Interfaces;
using Syngenta.CounterfeitDetection.Business.Integrations.Models;
using Syngenta.CounterfeitDetection.Business.Mappings;
using Syngenta.CounterfeitDetection.DataAccess.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Models;
using System.Linq.Expressions;

namespace Syngenta.CounterfeitDetection.Business.UnitTests.Core;

[TestFixture]
internal class ProductServiceUnitTests {
    private IGenericRepository<ProductBrand> _productBrandRepo;
    private IGenericRepository<Country> _countryRepo;
    private IGenericRepository<ProductPackSizeGTIN> _productPackSizeGTINRepo;
    private IProductService _productService;
    private ISAPIntegrationService _sapIntegrationService;
    private readonly IMapper _mapper = new Mapper(
        new MapperConfiguration(cfg => cfg.AddProfile<MappingProfile>()));


    private Mock<IGenericRepository<ProductBrand>>? _productBrandRepoMock;
    private Mock<IGenericRepository<Country>>? _countryRepoMock;
    private Mock<ISAPIntegrationService>? _sapIntegrationServiceMock;
    private Mock<IGenericRepository<ProductPackSizeGTIN>>? _productPackSizeGTINRepoMock;

    [SetUp]
    public void SetUp() {
        _productBrandRepo = Mock.Of<IGenericRepository<ProductBrand>>();
        _countryRepo = Mock.Of<IGenericRepository<Country>>();
        _sapIntegrationService = Mock.Of<ISAPIntegrationService>();
        _productPackSizeGTINRepo = Mock.Of<IGenericRepository<ProductPackSizeGTIN>>();

        _productService = new ProductService(_productBrandRepo, _countryRepo, _sapIntegrationService, _productPackSizeGTINRepo, _mapper);

        _productBrandRepoMock = Mock.Get(_productBrandRepo);
        _countryRepoMock = Mock.Get(_countryRepo);
        _sapIntegrationServiceMock = Mock.Get(_sapIntegrationService);
        _productPackSizeGTINRepoMock = Mock.Get(_productPackSizeGTINRepo);

        _productBrandRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<ProductBrand, bool>>?>(),
            It.IsAny<Func<IQueryable<ProductBrand>, IOrderedQueryable<ProductBrand>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<ProductBrand>, IIncludableQueryable<ProductBrand, object>>?>()
        )).Returns<Expression<Func<ProductBrand, bool>>?,
        Func<IQueryable<ProductBrand>, IOrderedQueryable<ProductBrand>>?, int?, int?,
        Func<IQueryable<ProductBrand>, IIncludableQueryable<ProductBrand, object>>?>((exp, order, skip, take, include)
        => Task.FromResult(DummyProductCatalogue.AsQueryable().Where(exp!).AsEnumerable()));

        _countryRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Country, bool>>?>(),
            It.IsAny<Func<IQueryable<Country>, IOrderedQueryable<Country>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Country>, IIncludableQueryable<Country, object>>?>()
        )).Returns<Expression<Func<Country, bool>>?,
        Func<IQueryable<Country>, IOrderedQueryable<Country>>?, int?, int?,
        Func<IQueryable<Country>, IIncludableQueryable<Country, object>>?>((exp, order, skip, take, include)
        => Task.FromResult(DummyCountry.AsQueryable().Where(exp!).AsEnumerable()));

        _productPackSizeGTINRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<ProductPackSizeGTIN, bool>>?>(),
            It.IsAny<Func<IQueryable<ProductPackSizeGTIN>, IOrderedQueryable<ProductPackSizeGTIN>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<ProductPackSizeGTIN>, IIncludableQueryable<ProductPackSizeGTIN, object>>?>()
        )).Returns<Expression<Func<ProductPackSizeGTIN, bool>>?,
        Func<IQueryable<ProductPackSizeGTIN>, IOrderedQueryable<ProductPackSizeGTIN>>?, int?, int?,
        Func<IQueryable<ProductPackSizeGTIN>, IIncludableQueryable<ProductPackSizeGTIN, object>>?>((exp, order, skip, take, include)
        => Task.FromResult(DummyProductPackSizeGTIN.AsQueryable().Where(exp!).AsEnumerable()));

        _sapIntegrationServiceMock.Setup(x => x.GetProductJourney(It.IsAny<string>()))
        .ReturnsAsync(DummyProductJourney);
    }

    [Test]
    public void GetProductCatalogueTest_InvalidCountryCode_ThrowsKeyNotFoundException() {
        const string countryCode = "WC";

        Assert.ThrowsAsync<KeyNotFoundException>(async () => await _productService.GetProductCatalogue(countryCode));
    }

    [Test]
    public async Task GetProductCatalogueTest_WithoutCountryCode_ReturnsAllProducts() {
        var expectedModel = JsonConvert.SerializeObject(DummyExpectedProductData);

        var result = JsonConvert.SerializeObject(await _productService.GetProductCatalogue(null));

        Assert.That(result, Is.EqualTo(expectedModel));
    }

    [Test]
    public async Task GetProductCatalogueTest_WithCountryCode_ReturnsFilteredProductCatalogue() {
        const string countryCode = "TC";
        const int expectedId = 1;
        var expectedModel = JsonConvert.SerializeObject(DummyExpectedProductData.Where(x => x.Id == expectedId));

        var result = JsonConvert.SerializeObject(await _productService.GetProductCatalogue(countryCode));

        Assert.That(result, Is.EqualTo(expectedModel));
    }
    
    [Test]
    public void GetProductDetailsTest_UnknownGTIN_ThrowsKeyNotFoundException() {
        const string gtin = "12345678902222";

        Assert.ThrowsAsync<KeyNotFoundException>(async () => await _productService.GetProductDetails(gtin));
    }

    [Test]
    public async Task GetProductDetailsTest_WithExistingGTIN_ReturnsProductDetails() {
        const string gtin = "12345678901234";
        const int expectedId = 1;
        var expectedModel = JsonConvert.SerializeObject(DummyExpectedProductData.FirstOrDefault(x => x.Id == expectedId));

        var result = JsonConvert.SerializeObject(await _productService.GetProductDetails(gtin));

        Assert.That(result, Is.EqualTo(expectedModel));
    }

    [TestCaseSource(nameof(GetProductJourneyInvalidCases))]
    public void GetProductJourneyTest_InvalidArguments_ThrowsArgumentNullException(string gtin, string serialNumber) 
        => Assert.ThrowsAsync<ArgumentNullException>(async () => await _productService.GetProductJourney(gtin, serialNumber));

    [Test]
    public async Task GetProductJourneyTest_WithGTINAndSerialNumber_ReturnsProductJourneyList() {
        const string gtin = "12345678";
        const string serialNumber = "SN1234";
        var expectedModel = JsonConvert.SerializeObject(DummyExpectedProductJourney);

        var result = JsonConvert.SerializeObject(await _productService.GetProductJourney(gtin, serialNumber));

        Assert.That(result, Is.EqualTo(expectedModel));
    }

    private static IEnumerable<TestCaseData> GetProductJourneyInvalidCases() {
        yield return new TestCaseData(
            "12345678",
            String.Empty
        );

        yield return new TestCaseData(
            "12345678",
            "    "
        );

        yield return new TestCaseData(
            "12345678",
            null
        );

        yield return new TestCaseData(
            String.Empty,
            "SN1234"
        );

        yield return new TestCaseData(
            "   ",
            "SN1234"
        );

        yield return new TestCaseData(
            null,
            "SN1234"
        );
    }

    private static IEnumerable<ProductJourneyStep> DummyExpectedProductJourney => new List<ProductJourneyStep>{
        new ProductJourneyStep(){
            ShippingId = "1",
            GTIN = "12345678",
            SN = "SN1234",
            UserName = "test user",
            SalesRep = "test user",
            Location = new Infrastructure.Models.Geolocation(12.34f, 56.78f),
            AuthenticationEventTime = DateTime.MinValue,
            ShippingEventTime = DateTime.MinValue
        },
        new ProductJourneyStep(){
            ShippingId = "2",
            GTIN = "12345678",
            SN = "SN1234",
            UserName = "test user",
            SalesRep = "test user",
            Location = new Infrastructure.Models.Geolocation(34.12f, 78.56f),
            AuthenticationEventTime = DateTime.MinValue,
            ShippingEventTime = DateTime.MinValue
        },
        new ProductJourneyStep(){
            ShippingId = "4",
            GTIN = "12345678",
            SN = "SN1234",
            UserName = "test user",
            SalesRep = "test user",
            Location = null,
            AuthenticationEventTime = DateTime.MinValue,
            ShippingEventTime = DateTime.MinValue
        },
    };

    private static IEnumerable<ProductJourneyEntry> DummyProductJourney => new List<ProductJourneyEntry>{
        new ProductJourneyEntry(){
            Evttype= "authentication",
            Evtid = "1",
            Userid = "test user",
            Latitude = "12.34",
            Longitude = "56.78",
            Eventdatetime = DateTime.MinValue
        },
        new ProductJourneyEntry(){
            Evttype= "authentication",
            Evtid = "2",
            Userid = "test user",
            Latitude = "34.12-",
            Longitude = "78.56-",
            Eventdatetime = DateTime.MinValue
        },
        new ProductJourneyEntry(){
            Evttype= "validation",
            Evtid = "3",
            Userid = "test user",
            Eventdatetime = DateTime.MinValue
        },
        new ProductJourneyEntry(){
            Evttype= "authentication",
            Userid = "test user",
            Evtid = "4",
            Eventdatetime = DateTime.MinValue
        },
    };

    private static IEnumerable<ProductBrand> DummyProductCatalogue => new List<ProductBrand>{
        new ProductBrand(){
            Id = 1,
            Name = "test name",
            ProductPackSizes = new List<ProductPackSize>(){
                new ProductPackSize(){
                    MeasureUnit = DummyMeasureUnit,
                    Size = "100",
                    ProductPackSizeGTINs = new List<ProductPackSizeGTIN>(){
                        new ProductPackSizeGTIN(){
                            GTIN = "12345678901234",
                            IsGPD = true,
                        },
                        new ProductPackSizeGTIN(){
                            GTIN = "12345678909876",
                            IsGPD = true,
                        },
                    }
                }
            },
            ProductCountry = new ProductCountry(){
                CountryId = 1,
                ProductId = 1
            }
        },
        new ProductBrand(){
            Id = 2,
            Name = "another test name",
            ProductPackSizes = new List<ProductPackSize>(){
                new ProductPackSize(){
                    MeasureUnit = DummyMeasureUnit,
                    Size = "10",
                    ProductPackSizeGTINs = new List<ProductPackSizeGTIN>(){
                        new ProductPackSizeGTIN(){
                            GTIN = "12345678905678",
                            IsGPD = false,
                        }
                    }
                },
                new ProductPackSize(){
                    MeasureUnit = DummyMeasureUnit,
                    Size = "1000",
                    ProductPackSizeGTINs = new List<ProductPackSizeGTIN>(){
                        new ProductPackSizeGTIN(){
                            GTIN = "12345678905678",
                            IsGPD = false,
                        }
                    }
                }
            },
            ProductCountry = new ProductCountry(){
                CountryId = 2,
                ProductId = 2
            }
        },
    }.AsEnumerable();

    private static MeasureUnit DummyMeasureUnit => new MeasureUnit() {
        Unit = "ml"
    };

    private static IEnumerable<Country> DummyCountry => new List<Country>{
        new Country(){
            Id = 1,
            Name = "test country",
            Code = "TC"
        },
        new Country(){
            Id = 2,
            Name = "India",
            Code = "IN",
        },
    }.AsEnumerable();

    private static IEnumerable<ProductData> DummyExpectedProductData => new List<ProductData>{
        new ProductData(){
            Id = 1,
            Name = "test name",
            GTIN = "12345678901234",
            IsUnderGPD = true,
            Capacity = "100 ml"
        },
        new ProductData(){
            Id = 1,
            Name = "test name",
            GTIN = "12345678909876",
            IsUnderGPD = true,
            Capacity = "100 ml"
        },
        new ProductData(){
            Id = 2,
            Name = "another test name",
            GTIN = "12345678905678",
            IsUnderGPD = false,
            Capacity = "10 ml"
        },
        new ProductData(){
            Id = 2,
            Name = "another test name",
            GTIN = "12345678905678",
            IsUnderGPD = false,
            Capacity = "1000 ml"
        },
    }.AsEnumerable();

    private static IEnumerable<ProductPackSizeGTIN> DummyProductPackSizeGTIN => new List<ProductPackSizeGTIN>{
        new ProductPackSizeGTIN(){
            Id = 1,
            GTIN = "12345678901234",
            IsGPD = true,
            PackSize = new ProductPackSize(){
                MeasureUnit = new MeasureUnit(){
                    Unit = "ml"
                },
                Size = "100",
                ProductCatalogue = new ProductBrand(){
                    Id = 1,
                    Name = "test name"
                }
            }
        }
    }.AsEnumerable();
}
