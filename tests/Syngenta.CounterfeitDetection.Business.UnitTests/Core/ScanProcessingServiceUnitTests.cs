// unset:silent

using AutoMapper;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Logging;
using Moq;
using Syngenta.CounterfeitDetection.Business.Core.Implementations;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.Business.Core.Models;
using Syngenta.CounterfeitDetection.Business.Integrations.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Models;
using Syngenta.CounterfeitDetection.Infrastructure.Exceptions;
using Syngenta.CounterfeitDetection.Infrastructure.Models;
using Syngenta.CounterfeitDetection.Business.UnitTests.DummyMapper;
using System.Linq.Expressions;

namespace Syngenta.CounterfeitDetection.Business.UnitTests.Core;

[TestFixture]
internal class ScanProcessingServiceUnitTests {
    #region Setup
    private readonly IMapper _mapper = new Mapper(
        new MapperConfiguration(cfg => cfg.AddProfile<DummyMappingProfile>()));
    private IGenericRepository<Transaction>? _transactionsRepo;
    private IGenericRepository<TransactionStatusType>? _transactionStatusTypeRepo;
    private IGenericRepository<User>? _usersRepo;
    private IGenericRepository<Country>? _countryRepo;
    private IGenericRepository<Role>? _roleRepo;
    private IGenericRepository<ClientAppType>? _clientsRepo;
    private IGenericRepository<BarcodeType>? _codeTypeRepo;
    private IGenericRepository<ScanningPointSetting>? _scanningPointSettingRepo;
    private IGenericRepository<ReportType>? _reportTypeRepo;
    private IGenericRepository<QRDomainCheckRule>? _qrDomainCheckRuleRepo;
    private IGenericRepository<CodeTypeCheckRule>? _codeTypeCheckRuleRepo;
    private IGenericRepository<TransactionResult>? _transactionResultRepo;
    private IGenericRepository<TransactionReview>? _transactionReviewRepo;
    private IGenericRepository<FollowUpAction>? _followUpActionRepo;
    private IGenericRepository<FollowUpActionResultRule>? _followUpRulesRepo;
    private IBlobStorageIntegrationService? _blobService;

    private IScanProcessingService? _service;

    private Mock<IGenericRepository<Transaction>>? _transactionsRepoMock;
    private Mock<IGenericRepository<TransactionStatusType>>? _transactionStatusTypeRepoMock;
    private Mock<IGenericRepository<User>>? _usersRepoMock;
    private Mock<IGenericRepository<Country>>? _countryRepoMock;
    private Mock<IGenericRepository<Role>>? _roleRepoMock;
    private Mock<IGenericRepository<ClientAppType>>? _clientsRepoMock;
    private Mock<IGenericRepository<BarcodeType>>? _codeTypeRepoMock;
    private Mock<IGenericRepository<ScanningPointSetting>>? _scanningPointSettingRepoMock;
    private Mock<IGenericRepository<ReportType>>? _reportTypeRepoMock;
    private Mock<IGenericRepository<QRDomainCheckRule>>? _qrDomainCheckRuleRepoMock;
    private Mock<IGenericRepository<CodeTypeCheckRule>>? _codeTypeCheckRuleRepoMock;
    private Mock<IGenericRepository<TransactionResult>>? _transactionResultRepoMock;
    private Mock<IGenericRepository<TransactionReview>>? _transactionReviewRepoMock;
    private Mock<IGenericRepository<FollowUpAction>>? _followUpActionRepoMock;
    private Mock<IGenericRepository<FollowUpActionResultRule>>? _followUpRulesRepoMock;

    private Mock<IBlobStorageIntegrationService>? _blobServiceMock;

    [SetUp]
    public void Setup() {
        _transactionsRepo = Mock.Of<IGenericRepository<Transaction>>();
        _transactionStatusTypeRepo = Mock.Of<IGenericRepository<TransactionStatusType>>();
        _usersRepo = Mock.Of<IGenericRepository<User>>();
        _countryRepo = Mock.Of<IGenericRepository<Country>>();
        _roleRepo = Mock.Of<IGenericRepository<Role>>();
        _clientsRepo = Mock.Of<IGenericRepository<ClientAppType>>();
        _codeTypeRepo = Mock.Of<IGenericRepository<BarcodeType>>();
        _scanningPointSettingRepo = Mock.Of<IGenericRepository<ScanningPointSetting>>();
        _reportTypeRepo = Mock.Of<IGenericRepository<ReportType>>();
        _qrDomainCheckRuleRepo = Mock.Of<IGenericRepository<QRDomainCheckRule>>();
        _codeTypeCheckRuleRepo = Mock.Of<IGenericRepository<CodeTypeCheckRule>>();
        _transactionResultRepo = Mock.Of<IGenericRepository<TransactionResult>>();
        _transactionReviewRepo = Mock.Of<IGenericRepository<TransactionReview>>();
        _followUpActionRepo = Mock.Of<IGenericRepository<FollowUpAction>>();
        _followUpRulesRepo = Mock.Of<IGenericRepository<FollowUpActionResultRule>>();
        _blobService = Mock.Of<IBlobStorageIntegrationService>();

        _service = new ScanProcessingService(
            _transactionsRepo,
            _transactionStatusTypeRepo,
            _usersRepo,
            _countryRepo,
            _roleRepo,
            _clientsRepo,
            _codeTypeRepo,
            _scanningPointSettingRepo,
            _reportTypeRepo,
            _codeTypeCheckRuleRepo,
            _qrDomainCheckRuleRepo,
            _transactionResultRepo,
            _transactionReviewRepo,
            _followUpActionRepo,
            _followUpRulesRepo,
            _blobService,
            _mapper,
            Mock.Of<ILogger<ScanProcessingService>>());

        _transactionsRepoMock = Mock.Get(_transactionsRepo);
        _transactionStatusTypeRepoMock = Mock.Get(_transactionStatusTypeRepo);
        _usersRepoMock = Mock.Get(_usersRepo);
        _countryRepoMock = Mock.Get(_countryRepo);
        _roleRepoMock = Mock.Get(_roleRepo);
        _clientsRepoMock = Mock.Get(_clientsRepo);
        _codeTypeRepoMock = Mock.Get(_codeTypeRepo);
        _scanningPointSettingRepoMock = Mock.Get(_scanningPointSettingRepo);
        _reportTypeRepoMock = Mock.Get(_reportTypeRepo);
        _qrDomainCheckRuleRepoMock = Mock.Get(_qrDomainCheckRuleRepo);
        _codeTypeCheckRuleRepoMock = Mock.Get(_codeTypeCheckRuleRepo);
        _transactionResultRepoMock = Mock.Get(_transactionResultRepo);
        _transactionReviewRepoMock = Mock.Get(_transactionReviewRepo);
        _followUpActionRepoMock = Mock.Get(_followUpActionRepo);
        _followUpRulesRepoMock = Mock.Get(_followUpRulesRepo);
        _blobServiceMock = Mock.Get(_blobService);

        _usersRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<User, bool>>?>(),
            It.IsAny<Func<IQueryable<User>, IOrderedQueryable<User>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<User>, IIncludableQueryable<User, object>>?>()
        )).Returns(Task.FromResult(new List<User> { DummyUser, DummyUser_T2 }.AsEnumerable()));

        _clientsRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<ClientAppType, bool>>?>(),
            It.IsAny<Func<IQueryable<ClientAppType>, IOrderedQueryable<ClientAppType>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<ClientAppType>, IIncludableQueryable<ClientAppType, object>>?>()
        )).Returns(Task.FromResult(new List<ClientAppType> { DummyClientAppType }.AsEnumerable()));

        _countryRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Country, bool>>?>(),
            It.IsAny<Func<IQueryable<Country>, IOrderedQueryable<Country>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Country>, IIncludableQueryable<Country, object>>?>()
        )).Returns(Task.FromResult(new List<Country> { DummyCountry, DummyCountry_T2 }.AsEnumerable()));

        _roleRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Role, bool>>?>(),
            It.IsAny<Func<IQueryable<Role>, IOrderedQueryable<Role>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Role>, IIncludableQueryable<Role, object>>?>()
        )).Returns(Task.FromResult(new List<Role> { DummyRole }.AsEnumerable()));

        _clientsRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<ClientAppType, bool>>?>(),
            It.IsAny<Func<IQueryable<ClientAppType>, IOrderedQueryable<ClientAppType>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<ClientAppType>, IIncludableQueryable<ClientAppType, object>>?>()
        )).Returns(Task.FromResult(new List<ClientAppType> { DummyClientAppType }.AsEnumerable()));

        _codeTypeRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<BarcodeType, bool>>?>(),
            It.IsAny<Func<IQueryable<BarcodeType>, IOrderedQueryable<BarcodeType>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<BarcodeType>, IIncludableQueryable<BarcodeType, object>>?>()
        )).Returns(Task.FromResult(new List<BarcodeType> { DummyCodeType_DataMatrix, DummyCodeType_QR }.AsEnumerable()));

        _transactionsRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Transaction, bool>>?>(),
            It.IsAny<Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Transaction>, IIncludableQueryable<Transaction, object>>?>()
        )).Returns(Task.FromResult(DummyCreatedTransactions));

        _transactionStatusTypeRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<TransactionStatusType, bool>>?>(),
            It.IsAny<Func<IQueryable<TransactionStatusType>, IOrderedQueryable<TransactionStatusType>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<TransactionStatusType>, IIncludableQueryable<TransactionStatusType, object>>?>()
        )).Returns(Task.FromResult(new List<TransactionStatusType> {
            DummyTransactionStatusType,
        }.AsEnumerable()));

        _scanningPointSettingRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<ScanningPointSetting, bool>>?>(),
            It.IsAny<Func<IQueryable<ScanningPointSetting>, IOrderedQueryable<ScanningPointSetting>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<ScanningPointSetting>, IIncludableQueryable<ScanningPointSetting, object>>?>()
        )).Returns(Task.FromResult(new List<ScanningPointSetting> { DummyScanningPointSetting }.AsEnumerable()));

        _reportTypeRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<ReportType, bool>>?>(),
            It.IsAny<Func<IQueryable<ReportType>, IOrderedQueryable<ReportType>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<ReportType>, IIncludableQueryable<ReportType, object>>?>()
        )).Returns(Task.FromResult(DummyReportTypes));

        _codeTypeCheckRuleRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<CodeTypeCheckRule, bool>>?>(),
            It.IsAny<Func<IQueryable<CodeTypeCheckRule>, IOrderedQueryable<CodeTypeCheckRule>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<CodeTypeCheckRule>, IIncludableQueryable<CodeTypeCheckRule, object>>?>()
        )).Returns(Task.FromResult(new List<CodeTypeCheckRule>() { DummyCodeTypeCheckRule_DataMatrix }.AsEnumerable()));

        _qrDomainCheckRuleRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<QRDomainCheckRule, bool>>?>(),
            It.IsAny<Func<IQueryable<QRDomainCheckRule>, IOrderedQueryable<QRDomainCheckRule>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<QRDomainCheckRule>, IIncludableQueryable<QRDomainCheckRule, object>>?>()
        )).Returns(Task.FromResult(new List<QRDomainCheckRule>() { DummyQRDomainCheckRule }.AsEnumerable()));

        _followUpActionRepoMock.Setup(r => r.UpdateAsync(It.IsAny<FollowUpAction>()))
            .Returns(Task.FromResult(new FollowUpAction()));
        _followUpActionRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<FollowUpAction, bool>>?>(),
            It.IsAny<Func<IQueryable<FollowUpAction>, IOrderedQueryable<FollowUpAction>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<FollowUpAction>, IIncludableQueryable<FollowUpAction, object>>?>()
        )).Returns(Task.FromResult(DummyFollowUpActions));

        _followUpRulesRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<FollowUpActionResultRule, bool>>?>(),
            It.IsAny<Func<IQueryable<FollowUpActionResultRule>, IOrderedQueryable<FollowUpActionResultRule>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<
                IQueryable<FollowUpActionResultRule>,
                IIncludableQueryable<FollowUpActionResultRule, object>>?
            >()
        )).Returns(Task.FromResult(new List<FollowUpActionResultRule> {
            DummyFollowUpRule(false, 0)
        }.AsEnumerable()));

        _blobServiceMock.Setup(s => s.SaveImage(It.IsAny<string>()))
            .Returns(Task.FromResult(new Uri("http://localhost/img/1")));

        _transactionResultRepoMock.Setup(r => r.CreateAsync(It.IsAny<TransactionResult>()))
            .Returns(Task.FromResult(new TransactionResult()));
    }
    #endregion

    #region Tests
    #region CreateScans tests
    #region Algorithm tests
    [Test]
    public async Task CreateScansTest_ReceivesEmptyScansArray_DoesNotCallRepositories() {
        await _service!.CreateScans(DummyUserData, DummyClientData, new List<ScanData>());

        Assert.DoesNotThrow(() => {
            _transactionsRepoMock!.VerifyNoOtherCalls();
            _usersRepoMock!.VerifyNoOtherCalls();
            _clientsRepoMock!.VerifyNoOtherCalls();
        });
    }

    [Test]
    public async Task CreateScansTest_ReceivesEmptyScansArray_ReturnsEmptyResultModel() {
        var actual = await _service!.CreateScans(DummyUserData, DummyClientData, new List<ScanData>());

        Assert.That(actual.Submitted?.Count() == 0 && actual.Failed?.Count() == 0);
    }

    [Test]
    public async Task CreateScansTest_ReceivesScansArray_CallsGetManyAsyncOnClientAppRepo() {
        var actual = await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanData);

        Assert.DoesNotThrow(
            () => _clientsRepoMock!.Verify(
                r => r!.GetManyAsync(
                    It.IsAny<Expression<Func<ClientAppType, bool>>?>(),
                    It.IsAny<Func<IQueryable<ClientAppType>, IOrderedQueryable<ClientAppType>>?>(),
                    It.IsAny<int?>(),
                    It.IsAny<int?>(),
                    It.IsAny<Func<IQueryable<ClientAppType>, IIncludableQueryable<ClientAppType, object>>?>()
                )
            )
        );
    }

    [Test]
    public void CreateScansTest_ReceivesNothingFromClientAppRepo_ThrowsArgumentException() {
        _clientsRepoMock!.Setup(r => r!.GetManyAsync(
            It.IsAny<Expression<Func<ClientAppType, bool>>?>(),
            It.IsAny<Func<IQueryable<ClientAppType>, IOrderedQueryable<ClientAppType>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<ClientAppType>, IIncludableQueryable<ClientAppType, object>>?>()
        )).Returns(Task.FromResult(new List<ClientAppType>().AsEnumerable()));

        Assert.ThrowsAsync<ArgumentException>(
            async () => await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanData));
    }

    [Test]
    public async Task CreateScansTest_ReceivesNothingFromClientAppRepo_DoesNotCallUsersRepo() {
        _clientsRepoMock!.Setup(r => r!.GetManyAsync(
            It.IsAny<Expression<Func<ClientAppType, bool>>?>(),
            It.IsAny<Func<IQueryable<ClientAppType>, IOrderedQueryable<ClientAppType>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<ClientAppType>, IIncludableQueryable<ClientAppType, object>>?>()
        )).Returns(Task.FromResult(new List<ClientAppType>().AsEnumerable()));

        try {
            await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanData);
        }
        catch { }

        Assert.DoesNotThrow(() => _usersRepoMock!.VerifyNoOtherCalls());
    }

    [Test]
    public async Task CreateScansTest_GetsClientInfo_CallsGetManyAsyncOnUsersRepo() {
        var actual = await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanData);

        Assert.DoesNotThrow(
            () => _usersRepoMock!.Verify(
                r => r!.GetManyAsync(
                    It.IsAny<Expression<Func<User, bool>>?>(),
                    It.IsAny<Func<IQueryable<User>, IOrderedQueryable<User>>?>(),
                    It.IsAny<int?>(),
                    It.IsAny<int?>(),
                    It.IsAny<Func<IQueryable<User>, IIncludableQueryable<User, object>>?>()
                )
            )
        );
    }

    [Test]
    public async Task CreateScansTest_GetsNothingFromUserRepo_CallsGetManyOnCountryRepo() {
        _usersRepoMock!.Setup(r => r!.GetManyAsync(
            It.IsAny<Expression<Func<User, bool>>?>(),
            It.IsAny<Func<IQueryable<User>, IOrderedQueryable<User>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<User>, IIncludableQueryable<User, object>>?>()
        )).Returns(Task.FromResult(new List<User>().AsEnumerable()));

        var actual = await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanData);

        Assert.DoesNotThrow(
            () => _countryRepoMock!.Verify(r => r!.GetManyAsync(
                It.IsAny<Expression<Func<Country, bool>>?>(),
                It.IsAny<Func<IQueryable<Country>, IOrderedQueryable<Country>>?>(),
                It.IsAny<int?>(),
                It.IsAny<int?>(),
                It.IsAny<Func<IQueryable<Country>, IIncludableQueryable<Country, object>>?>()))
        );
    }

    [Test]
    public void CreateScansTest_GetsNothingFromUserRepoAndCountryDoesNotExist_ThrowsArgumentException() {
        _usersRepoMock!.Setup(r => r!.GetManyAsync(
            It.IsAny<Expression<Func<User, bool>>?>(),
            It.IsAny<Func<IQueryable<User>, IOrderedQueryable<User>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<User>, IIncludableQueryable<User, object>>?>()
        )).Returns(Task.FromResult(new List<User>().AsEnumerable()));
        _countryRepoMock!.Setup(r => r!.GetManyAsync(
            It.IsAny<Expression<Func<Country, bool>>?>(),
            It.IsAny<Func<IQueryable<Country>, IOrderedQueryable<Country>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Country>, IIncludableQueryable<Country, object>>?>()
        )).Returns(Task.FromResult(new List<Country>().AsEnumerable()));

        Assert.ThrowsAsync<ArgumentException>(
            async () => await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanData)
        );
    }

    [Test]
    public async Task CreateScansTest_GetsNothingFromUserRepoAndCountryDoesNotExist_DoesNotCallCreateAsyncUserRepo() {
        _usersRepoMock!.Setup(r => r!.GetManyAsync(
            It.IsAny<Expression<Func<User, bool>>?>(),
            It.IsAny<Func<IQueryable<User>, IOrderedQueryable<User>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<User>, IIncludableQueryable<User, object>>?>()
        )).Returns(Task.FromResult(new List<User>().AsEnumerable()));
        _countryRepoMock!.Setup(r => r!.GetManyAsync(
            It.IsAny<Expression<Func<Country, bool>>?>(),
            It.IsAny<Func<IQueryable<Country>, IOrderedQueryable<Country>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Country>, IIncludableQueryable<Country, object>>?>()
        )).Returns(Task.FromResult(new List<Country>().AsEnumerable()));

        try {
            await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanData);
        }
        catch { }

        Assert.DoesNotThrow(
            () => _usersRepoMock!.Verify(r => r.CreateAsync(It.IsAny<User>()), Times.Never));
    }

    [Test]
    public async Task CreateScansTest_GetsNothingFromUserRepo_CallsGetManyOnRoleRepo() {
        _usersRepoMock!.Setup(r => r!.GetManyAsync(
            It.IsAny<Expression<Func<User, bool>>?>(),
            It.IsAny<Func<IQueryable<User>, IOrderedQueryable<User>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<User>, IIncludableQueryable<User, object>>?>()
        )).Returns(Task.FromResult(new List<User>().AsEnumerable()));

        var actual = await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanData);

        Assert.DoesNotThrow(
            () => _roleRepoMock!.Verify(r => r!.GetManyAsync(
                It.IsAny<Expression<Func<Role, bool>>?>(),
                It.IsAny<Func<IQueryable<Role>, IOrderedQueryable<Role>>?>(),
                It.IsAny<int?>(),
                It.IsAny<int?>(),
                It.IsAny<Func<IQueryable<Role>, IIncludableQueryable<Role, object>>?>()))
        );
    }

    [Test]
    public void CreateScansTest_GetsNothingFromUserRepoAndRoleDoesNotExist_ThrowsArgumentException() {
        _usersRepoMock!.Setup(r => r!.GetManyAsync(
            It.IsAny<Expression<Func<User, bool>>?>(),
            It.IsAny<Func<IQueryable<User>, IOrderedQueryable<User>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<User>, IIncludableQueryable<User, object>>?>()
        )).Returns(Task.FromResult(new List<User>().AsEnumerable()));
        _roleRepoMock!.Setup(r => r!.GetManyAsync(
            It.IsAny<Expression<Func<Role, bool>>?>(),
            It.IsAny<Func<IQueryable<Role>, IOrderedQueryable<Role>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Role>, IIncludableQueryable<Role, object>>?>()
        )).Returns(Task.FromResult(new List<Role>().AsEnumerable()));

        Assert.ThrowsAsync<ArgumentException>(
            async () => await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanData)
        );
    }

    [Test]
    public async Task CreateScansTest_GetsNothingFromUserRepoAndRoleDoesNotExist_DoesNotCallCreateAsyncUserRepo() {
        _usersRepoMock!.Setup(r => r!.GetManyAsync(
            It.IsAny<Expression<Func<User, bool>>?>(),
            It.IsAny<Func<IQueryable<User>, IOrderedQueryable<User>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<User>, IIncludableQueryable<User, object>>?>()
        )).Returns(Task.FromResult(new List<User>().AsEnumerable()));
        _roleRepoMock!.Setup(r => r!.GetManyAsync(
            It.IsAny<Expression<Func<Role, bool>>?>(),
            It.IsAny<Func<IQueryable<Role>, IOrderedQueryable<Role>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Role>, IIncludableQueryable<Role, object>>?>()
        )).Returns(Task.FromResult(new List<Role>().AsEnumerable()));

        try {
            await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanData);
        }
        catch { }

        Assert.DoesNotThrow(
            () => _usersRepoMock!.Verify(r => r.CreateAsync(It.IsAny<User>()), Times.Never));
    }

    [Test]
    public async Task CreateScansTest_GetsNothingFromUserRepo_CallsCreateAsyncOnUsersRepo() {
        _usersRepoMock!.Setup(r => r!.GetManyAsync(
            It.IsAny<Expression<Func<User, bool>>?>(),
            It.IsAny<Func<IQueryable<User>, IOrderedQueryable<User>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<User>, IIncludableQueryable<User, object>>?>()
        )).Returns(Task.FromResult(new List<User>().AsEnumerable()));

        var actual = await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanData);

        Assert.DoesNotThrow(
            () => _usersRepoMock!.Verify(r => r!.CreateAsync(It.IsAny<User>()))
        );
    }

    [Test]
    public async Task CreateScansTest_GetsClientAndUserInfo_CallsGetManyOnTransactionStatusTypeRepo() {
        await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanData);

        Assert.DoesNotThrow(
            () => _transactionStatusTypeRepoMock!.Verify(
                r => r!.GetManyAsync(
                    It.IsAny<Expression<Func<TransactionStatusType, bool>>?>(),
                    It.IsAny<Func<IQueryable<TransactionStatusType>, IOrderedQueryable<TransactionStatusType>>?>(),
                    It.IsAny<int?>(),
                    It.IsAny<int?>(),
                    It.IsAny<Func<IQueryable<TransactionStatusType>, IIncludableQueryable<TransactionStatusType, object>>?>()
                ),
            Times.Once)
        );
    }

    [Test]
    public void CreateScansTest_PendingTransactionStatusTypeDoesNotExist_ThrowsMasterDataException() {
        _transactionStatusTypeRepoMock!.Setup(r => r!.GetManyAsync(
            It.IsAny<Expression<Func<TransactionStatusType, bool>>?>(),
            It.IsAny<Func<IQueryable<TransactionStatusType>, IOrderedQueryable<TransactionStatusType>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<TransactionStatusType>, IIncludableQueryable<TransactionStatusType, object>>?>()
        )).Returns(Task.FromResult(new List<TransactionStatusType>().AsEnumerable()));

        Assert.ThrowsAsync<MasterDataException>(
            async () => await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanData)
        );
    }

    [Test]
    public async Task CreateScansTest_GetsClientAndUserInfo_CallsGetManyOnReportTypeRepo() {
        await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanData);

        Assert.DoesNotThrow(
            () => _reportTypeRepoMock!.Verify(
                r => r!.GetManyAsync(
                    It.IsAny<Expression<Func<ReportType, bool>>?>(),
                    It.IsAny<Func<IQueryable<ReportType>, IOrderedQueryable<ReportType>>?>(),
                    It.IsAny<int?>(),
                    It.IsAny<int?>(),
                    It.IsAny<Func<IQueryable<ReportType>, IIncludableQueryable<ReportType, object>>?>()
                ),
            Times.Once)
        );
    }

    [Test]
    public void CreateScansTest_ScanReportTypeDoesNotExist_ThrowsMasterDataException() {
        _reportTypeRepoMock!.Setup(r => r!.GetManyAsync(
            It.IsAny<Expression<Func<ReportType, bool>>?>(),
            It.IsAny<Func<IQueryable<ReportType>, IOrderedQueryable<ReportType>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<ReportType>, IIncludableQueryable<ReportType, object>>?>()
        )).Returns(Task.FromResult(new List<ReportType>().AsEnumerable()));

        Assert.ThrowsAsync<MasterDataException>(
            async () => await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanData)
        );
    }

    [Test]
    public async Task CreateScansTest_GetsClientAndUserInfoAndHasReportData_CallsSaveImageOnBlobService() {
        await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanData);

        Assert.DoesNotThrow(
            () => _blobServiceMock!.Verify(s => s!.SaveImage(It.IsAny<string>()), Times.Once)
        );
    }

    [Test]
    public async Task CreateScansTest_BlobServiceThrows_DoesNotSaveSubsequentImages() {
        _blobServiceMock!.Setup(s => s!.SaveImage(It.Is<string>(str => str == "456")))
            .Throws<Exception>();

        await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanDataWithMultipleImages);

        Assert.DoesNotThrow(
            () => {
                _blobServiceMock!.Verify(s => s!.SaveImage(It.Is<string>(str => str == "123")), Times.Exactly(3));
                _blobServiceMock!.Verify(s => s!.SaveImage(It.Is<string>(str => str == "456")), Times.Exactly(1));
                _blobServiceMock!.Verify(s => s!.SaveImage(It.Is<string>(str => str == "789")), Times.Never);
            }
        );
    }

    [Test]
    public async Task CreateScansTest_BlobServiceThrows_ReturnsEntryAsFailed() {
        _blobServiceMock!.Setup(s => s!.SaveImage(It.Is<string>(str => str == "456")))
            .Throws<Exception>();

        var actual = await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanDataWithMultipleImages);

        Assert.That(actual.Failed?.Count() == 1);
    }

    [TestCaseSource(nameof(CreateScanCases))]
    public async Task CreateScansTest_BlobServiceDoesNotThrow_CallsCreateMultipleOnTransactionRepo(
        IEnumerable<ScanData> scans,
        IEnumerable<Transaction> expected,
        string caseName
    ) {
        await _service!.CreateScans(DummyUserData, DummyClientData, scans);

        Assert.DoesNotThrow(
            () => _transactionsRepoMock!.Verify(
                m => m.CreateMultipleAsync(It.IsAny<IEnumerable<Transaction>>()),
                Times.Once),
            caseName
        );
    }

    [TestCaseSource(nameof(CreateScanCases))]
    public async Task CreateScansTest_TransactionRepoThrows_ReturnsFailedEntries(
        IEnumerable<ScanData> scans,
        IEnumerable<Transaction> expected,
        string caseName
    ) {
        _transactionsRepoMock!.Setup(m => m.CreateMultipleAsync(It.IsAny<IEnumerable<Transaction>>()))
            .Throws<Exception>();

        var actual = await _service!.CreateScans(DummyUserData, DummyClientData, scans);

        Assert.That(actual.Submitted?.Count() == 0 && actual.Failed?.Count() == scans.Count());
    }

    [Test]
    public async Task CreateScansTest_RepoReturnsEntities_ReturnsSubmittedEntries() {
        var result = await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanData);

        Assert.That(result.Submitted?.Count() == DummyScanData.Count() && result.Failed?.Count() == 0);
    }
    #endregion

    #region Data mapping tests
    [Test]
    public async Task CreateScansTest_BlobServiceDoesNotThrow_MapsScanDataCorrectly() {
        IEnumerable<Transaction> received = new List<Transaction>();
        _transactionsRepoMock!.Setup(r => r.CreateMultipleAsync(It.IsAny<IEnumerable<Transaction>>()))
            .Callback<IEnumerable<Transaction>>(transactions => received = transactions);
        var scans = new List<ScanData> {
            new ScanData { ScanEntry = DummyScanEntry, ReportData = DummyReportEntry }
        }.AsEnumerable();

        await _service!.CreateScans(DummyUserData, DummyClientData, scans);

        Assert.Multiple(() => {
            Assert.That(received!.Count(), Is.EqualTo(1), "Invalid number of scan entries");

            var first = received.FirstOrDefault();
            if (first is not null) {
                Assert.That(
                    first.CreationTime, Is.Not.EqualTo(default(DateTime)),
                    $"{nameof(first.CreationTime)} mapping failed");
                Assert.That(
                    first.LastUpdateTime, Is.Not.EqualTo(default(DateTime)),
                    $"{nameof(first.LastUpdateTime)} mapping failed");
                Assert.That(
                    first.StatusId, Is.EqualTo(DummyTransactionStatusType.Id),
                    $"{nameof(first.StatusId)} mapping failed");
                Assert.That(
                    first.GeoLocation, Is.EqualTo(DummyTransactionWithResult.GeoLocation),
                    $"{nameof(first.GeoLocation)} mapping failed");

                var scan = first.ScanInputDetails;
                var expectedScan = DummyScanInputDetails;
                Assert.That(scan, Is.Not.Null, $"{nameof(first.ScanInputDetails)} mapping failed");
                if (scan is not null) {
                    Assert.That(
                        scan!.Fingerprint, Is.EqualTo(expectedScan.Fingerprint),
                        $"{nameof(scan.Fingerprint)} mapping failed");
                    Assert.That(
                        scan!.RawData, Is.EqualTo(expectedScan.RawData),
                        $"{nameof(scan.RawData)} mapping failed");
                    Assert.That(
                        scan!.ScanTime, Is.EqualTo(expectedScan.ScanTime),
                        $"{nameof(scan.ScanTime)} mapping failed");
                    Assert.That(
                        scan!.IsManualScan, Is.EqualTo(expectedScan.IsManualScan),
                        $"{nameof(scan.IsManualScan)} mapping failed");
                    Assert.That(
                        scan!.IsOffline, Is.EqualTo(expectedScan.IsOffline),
                        $"{nameof(scan.IsOffline)} mapping failed");
                    Assert.That(
                        scan!.CodeTypeId, Is.EqualTo(DummyCodeType_DataMatrix.Id),
                        $"{nameof(scan.CodeTypeId)} mapping failed");
                    Assert.That(
                        scan!.ClientAppTypeId, Is.EqualTo(DummyClientAppType.Id),
                        $"{nameof(scan.ClientAppTypeId)} mapping failed");
                }

                var report = first.TransactionReport;
                var expectedReport = DummyReport;
                Assert.That(report, Is.Not.Null, $"{nameof(first.TransactionReport)} mapping failed");
                if (report is not null) {
                    Assert.That(
                        report!.TypeId, Is.EqualTo(expectedReport.TypeId),
                        $"{nameof(report.TypeId)} mapping failed");
                    Assert.That(
                        report!.ScanningPointId, Is.EqualTo(expectedReport.ScanningPointId),
                        $"{nameof(report.ScanningPointId)} mapping failed");
                    Assert.That(
                        report!.ScanAddress, Is.EqualTo(expectedReport.ScanAddress),
                        $"{nameof(report.ScanAddress)} mapping failed");
                    Assert.That(
                        report!.ScanLocationName, Is.EqualTo(expectedReport.ScanLocationName),
                        $"{nameof(report.ScanLocationName)} mapping failed");
                    Assert.That(
                        report!.ReportReasonId, Is.EqualTo(expectedReport.ReportReasonId),
                        $"{nameof(report.ReportReasonId)} mapping failed");
                    Assert.That(
                        report!.CustomReportReason, Is.EqualTo(expectedReport.CustomReportReason),
                        $"{nameof(report.CustomReportReason)} mapping failed");
                    Assert.That(
                        report!.Comment, Is.EqualTo(expectedReport.Comment),
                        $"{nameof(report.Comment)} mapping failed");
                    Assert.That(report!.Images, Is.Not.Null, $"{nameof(report.Images)} mapping failed");
                    if (report!.Images is not null) {
                        Assert.That(
                            report!.Images!.Count(), Is.EqualTo(expectedReport.Images!.Count()),
                            $"{nameof(report.Images)} count mismatch");
                    }
                }
            }
        });
    }

    [Test]
    public async Task CreateScansTest_BlobServiceDoesNotThrow_ReturnsCorrectlyMappedData() {
        _transactionsRepoMock!.Setup(r => r.CreateMultipleAsync(It.IsAny<IEnumerable<Transaction>>()))
            .Returns(Task.FromResult(DummyCreatedTransactions));

        var actual = await _service!.CreateScans(DummyUserData, DummyClientData, DummyScanData);

        Assert.Multiple(() => {
            Assert.That(
                actual.Submitted?.Count(), Is.EqualTo(4),
                $"Invalid submitted elements count: <{actual.Submitted?.Count()}>");

            if (actual.Submitted?.Count() > 1) {
                var transaction = actual.Submitted!.First();
                var expectedTransaction = DummyTransactionWithReport;
                Assert.That(transaction.Id, Is.EqualTo(expectedTransaction.Id),
                    $"{nameof(transaction.Id)} mapping failed.");
                Assert.That(transaction.Status, Is.EqualTo(expectedTransaction.Status!.Type),
                    $"{nameof(transaction.Status)} mapping failed.");
                Assert.That(transaction.CreationTime, Is.Not.EqualTo(default(DateTime)),
                    $"{nameof(transaction.CreationTime)} mapping failed.");
                Assert.That(transaction.LastUpdateTime, Is.Not.EqualTo(default(DateTime)),
                    $"{nameof(transaction.LastUpdateTime)} mapping failed.");

                Assert.That(
                    transaction.ScanData, Is.Not.Null,
                    $"{nameof(transaction.ScanData)} mapping failed.");
                if (transaction.ScanData is not null) {
                    var scanData = transaction.ScanData!;
                    var expectedScanData = expectedTransaction.ScanInputDetails!;
                    Assert.That(
                        scanData.Any(i => i.Key == "ScanData_Fingerprint"),
                        "Couldn't find <Fingerprint> key in scan data array");
                    Assert.That(
                        scanData.FirstOrDefault(i => i.Key == "ScanData_Fingerprint")!.Value,
                        Is.EqualTo(expectedScanData.Fingerprint),
                        $"{nameof(expectedScanData.Fingerprint)} mapping failed.");
                    Assert.That(
                        scanData.Any(i => i.Key == "ScanData_RawData"),
                        "Couldn't find <Raw data> key in scan data array");
                    Assert.That(
                        scanData.FirstOrDefault(i => i.Key == "ScanData_RawData")!.Value,
                        Is.EqualTo(expectedScanData.RawData),
                        $"{nameof(expectedScanData.RawData)} mapping failed.");
                    Assert.That(
                        scanData.Any(i => i.Key == "ScanData_Time"),
                        "Couldn't find <Scan time> key in scan data array");
                    Assert.That(
                        scanData.FirstOrDefault(i => i.Key == "ScanData_Time")!.Value,
                        Is.EqualTo(expectedScanData.ScanTime.ToString("yyyy-MM-ddThh:mm:ssZ")),
                        $"{nameof(expectedScanData.ScanTime)} mapping failed.");
                    Assert.That(
                        scanData.Any(i => i.Key == "ScanData_Location"),
                        "Couldn't find <Location> key in scan data array");
                    Assert.That(
                        scanData.FirstOrDefault(i => i.Key == "ScanData_Location")!.Value,
                        Is.EqualTo(expectedTransaction.GeoLocation),
                        $"{nameof(expectedTransaction.GeoLocation)} mapping failed.");
                }

                Assert.That(
                    transaction.ReportData, Is.Not.Null,
                    $"{nameof(transaction.ReportData)} mapping failed.");
                if (transaction.ReportData is not null) {
                    var report = transaction.ReportData!;
                    var expectedReport = expectedTransaction.TransactionReport!;
                    Assert.That(
                        report.ScanningPoint, Is.EqualTo(expectedReport.ScanningPoint!.Point),
                        $"{nameof(report.ScanningPoint)} mapping failed.");
                    Assert.That(
                        report.ScanAddress, Is.EqualTo(expectedReport.ScanAddress),
                        $"{nameof(report.ScanAddress)} mapping failed.");
                    Assert.That(
                        report.ScanLocationName, Is.EqualTo(expectedReport.ScanLocationName),
                        $"{nameof(report.ScanLocationName)} mapping failed.");
                    Assert.That(
                        report.ReportReasonId, Is.EqualTo(expectedReport.ReportReasonId),
                        $"{nameof(report.ReportReasonId)} mapping failed.");
                    Assert.That(
                        report.CustomReportReason, Is.EqualTo(expectedReport.CustomReportReason),
                        $"{nameof(report.CustomReportReason)} mapping failed.");
                    Assert.That(
                        report.Comments, Is.EqualTo(expectedReport.Comment),
                        $"{nameof(report.Comments)} mapping failed.");

                    Assert.That(
                        report.Images, Is.Not.Null,
                        $"{nameof(report.Images)} collection mapping failed.");
                    if (report.Images is not null) {
                        Assert.That(
                            report.Images!.FirstOrDefault(),
                            Is.EqualTo(expectedReport.Images!.FirstOrDefault()!.ImageFile!.StorageUrl),
                            $"{nameof(report.Images)} mapping failed.");
                    }
                }
            }
        });
    }


    [Test]
    public async Task CreateScansTestWithQRCodes_BlobServiceDoesNotThrow_ReturnsCorrectlyMappedData() {
        _transactionsRepoMock!.Setup(r => r.CreateMultipleAsync(It.IsAny<IEnumerable<Transaction>>()))
            .Returns(Task.FromResult(DummyCreatedTransactions_WithQRCodes));

        _codeTypeCheckRuleRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<CodeTypeCheckRule, bool>>?>(),
            It.IsAny<Func<IQueryable<CodeTypeCheckRule>, IOrderedQueryable<CodeTypeCheckRule>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<CodeTypeCheckRule>, IIncludableQueryable<CodeTypeCheckRule, object>>?>()
        )).Returns(Task.FromResult(new List<CodeTypeCheckRule>() { DummyCodeTypeCheckRule_QR }.AsEnumerable()));

        _transactionsRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Transaction, bool>>?>(),
            It.IsAny<Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Transaction>, IIncludableQueryable<Transaction, object>>?>()
        )).Returns(Task.FromResult(DummyCreatedTransactions_WithQRCodes));

        _transactionResultRepoMock!.Setup(r => r.CreateAsync(It.IsAny<TransactionResult>()))
            .ThrowsAsync(new Exception("Trying to create transaction result with valid code type and domain name"));

        var actual = await _service!.CreateScans(DummyUserData_T2, DummyClientData, DummyScanData_WithQRCodes);

        Assert.Multiple(() => {
            Assert.That(
                actual.Submitted?.Count(), Is.EqualTo(4),
                $"Invalid submitted elements count: <{actual.Submitted?.Count()}>");

            if (actual.Submitted?.Count() > 1) {
                var transaction = actual.Submitted!.First();
                var expectedTransaction = DummyTransactionWithReport_WithQRCodes;
                Assert.That(transaction.Id, Is.EqualTo(expectedTransaction.Id),
                    $"{nameof(transaction.Id)} mapping failed.");
                Assert.That(transaction.Status, Is.EqualTo(expectedTransaction.Status!.Type),
                    $"{nameof(transaction.Status)} mapping failed.");
                Assert.That(transaction.CreationTime, Is.Not.EqualTo(default(DateTime)),
                    $"{nameof(transaction.CreationTime)} mapping failed.");
                Assert.That(transaction.LastUpdateTime, Is.Not.EqualTo(default(DateTime)),
                    $"{nameof(transaction.LastUpdateTime)} mapping failed.");

                Assert.That(
                    transaction.ScanData, Is.Not.Null,
                    $"{nameof(transaction.ScanData)} mapping failed.");
                if (transaction.ScanData is not null) {
                    var scanData = transaction.ScanData!;
                    var expectedScanData = expectedTransaction.ScanInputDetails!;
                    Assert.That(
                        scanData.Any(i => i.Key == "ScanData_Fingerprint"),
                        "Couldn't find <Fingerprint> key in scan data array");
                    Assert.That(
                        scanData.FirstOrDefault(i => i.Key == "ScanData_Fingerprint")!.Value,
                        Is.EqualTo(expectedScanData.Fingerprint),
                        $"{nameof(expectedScanData.Fingerprint)} mapping failed.");
                    Assert.That(
                        scanData.Any(i => i.Key == "ScanData_RawData"),
                        "Couldn't find <Raw data> key in scan data array");
                    Assert.That(
                        scanData.FirstOrDefault(i => i.Key == "ScanData_RawData")!.Value,
                        Is.EqualTo(expectedScanData.RawData),
                        $"{nameof(expectedScanData.RawData)} mapping failed.");
                    Assert.That(
                        scanData.Any(i => i.Key == "ScanData_Time"),
                        "Couldn't find <Scan time> key in scan data array");
                    Assert.That(
                        scanData.FirstOrDefault(i => i.Key == "ScanData_Time")!.Value,
                        Is.EqualTo(expectedScanData.ScanTime.ToString("yyyy-MM-ddThh:mm:ssZ")),
                        $"{nameof(expectedScanData.ScanTime)} mapping failed.");
                    Assert.That(
                        scanData.Any(i => i.Key == "ScanData_Location"),
                        "Couldn't find <Location> key in scan data array");
                    Assert.That(
                        scanData.FirstOrDefault(i => i.Key == "ScanData_Location")!.Value,
                        Is.EqualTo(expectedTransaction.GeoLocation),
                        $"{nameof(expectedTransaction.GeoLocation)} mapping failed.");
                }

                Assert.That(
                    transaction.ReportData, Is.Not.Null,
                    $"{nameof(transaction.ReportData)} mapping failed.");
                if (transaction.ReportData is not null) {
                    var report = transaction.ReportData!;
                    var expectedReport = expectedTransaction.TransactionReport!;
                    Assert.That(
                        report.ScanningPoint, Is.EqualTo(expectedReport.ScanningPoint!.Point),
                        $"{nameof(report.ScanningPoint)} mapping failed.");
                    Assert.That(
                        report.ScanAddress, Is.EqualTo(expectedReport.ScanAddress),
                        $"{nameof(report.ScanAddress)} mapping failed.");
                    Assert.That(
                        report.ScanLocationName, Is.EqualTo(expectedReport.ScanLocationName),
                        $"{nameof(report.ScanLocationName)} mapping failed.");
                    Assert.That(
                        report.ReportReasonId, Is.EqualTo(expectedReport.ReportReasonId),
                        $"{nameof(report.ReportReasonId)} mapping failed.");
                    Assert.That(
                        report.CustomReportReason, Is.EqualTo(expectedReport.CustomReportReason),
                        $"{nameof(report.CustomReportReason)} mapping failed.");
                    Assert.That(
                        report.Comments, Is.EqualTo(expectedReport.Comment),
                        $"{nameof(report.Comments)} mapping failed.");

                    Assert.That(
                        report.Images, Is.Not.Null,
                        $"{nameof(report.Images)} collection mapping failed.");
                    if (report.Images is not null) {
                        Assert.That(
                            report.Images!.FirstOrDefault(),
                            Is.EqualTo(expectedReport.Images!.FirstOrDefault()!.ImageFile!.StorageUrl),
                            $"{nameof(report.Images)} mapping failed.");
                    }
                }
            }
        });
    }
    #endregion
    #endregion

    #region SubmitFollowUpAction tests
    #region Algorithm tests
    [TestCaseSource(nameof(SubmitFollowUpActionInvalidIdCases))]
    public void SubmitFollowUpActionTest_ReceivesInvalidIds_ThrowsArgumentException(FollowUpActionData data) =>
        Assert.ThrowsAsync<ArgumentException>(
            async () => await _service!.SubmitFollowUpAction(data));

    [Test]
    public async Task SubmitFollowUpActionTest_ReceivesValidIds_CallsGetManyOnFollowUpActionRepo() {
        await _service!.SubmitFollowUpAction(DummyFollowUpActionData);

        Assert.DoesNotThrow(
            () => _followUpActionRepoMock!.Verify(r => r.GetManyAsync(
                It.IsAny<Expression<Func<FollowUpAction, bool>>?>(),
                It.IsAny<Func<IQueryable<FollowUpAction>, IOrderedQueryable<FollowUpAction>>?>(),
                It.IsAny<int?>(),
                It.IsAny<int?>(),
                It.IsAny<Func<IQueryable<FollowUpAction>, IIncludableQueryable<FollowUpAction, object>>?>()
            )));
    }

    [Test]
    public void SubmitFollowUpActionTest_FollowUpActionRepoReturnsNothing_ThrowsKeyNotFoundException() {
        _followUpActionRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<FollowUpAction, bool>>?>(),
            It.IsAny<Func<IQueryable<FollowUpAction>, IOrderedQueryable<FollowUpAction>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<FollowUpAction>, IIncludableQueryable<FollowUpAction, object>>?>()
        )).Returns(Task.FromResult(new List<FollowUpAction>().AsEnumerable()));

        Assert.ThrowsAsync<KeyNotFoundException>(
            async () => await _service!.SubmitFollowUpAction(DummyFollowUpActionData));
    }

    [Test]
    public async Task SubmitFollowUpActionTest_FollowUpActionRepoReturnsEntry_CallsUpdateOnFollowUpActionRepo() {
        await _service!.SubmitFollowUpAction(DummyFollowUpActionData);

        Assert.DoesNotThrow(
            () => _followUpActionRepoMock!.Verify(
                r => r.UpdateAsync(It.Is<FollowUpAction>(
                    a => a.IsCompleted == true
                      && a.PickedOptionId == DummyExpectedFollowUpAction.PickedOptionId
                      && a.Data == DummyExpectedFollowUpAction.Data))));
    }

    [Test]
    public void SubmitFollowUpActionTest_RepoThrowsOnUpdate_ThrowsException() {
        _followUpActionRepoMock!.Setup(r => r.UpdateAsync(It.IsAny<FollowUpAction>())).Throws<Exception>();

        Assert.ThrowsAsync<Exception>(
            async () => await _service!.SubmitFollowUpAction(DummyFollowUpActionData));
    }

    [Test]
    public async Task SubmitFollowUpActionTest_RepoDoesNotThrowOnUpdate_CallsGetManyOnFollowUpRulesRepo() {
        await _service!.SubmitFollowUpAction(DummyFollowUpActionData);

        Assert.DoesNotThrow(
            () => _followUpActionRepoMock!.Verify(
                r => r.UpdateAsync(It.Is<FollowUpAction>(
                    a => a.IsCompleted == true
                      && a.PickedOptionId == DummyFollowUpActionData.OptionId))));
    }

    [Test]
    public void SubmitFollowUpActionTest_RelatedTransactionIsNull_ThrowsDataConsistencyException() {
        _transactionsRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Transaction, bool>>?>(),
            It.IsAny<Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Transaction>, IIncludableQueryable<Transaction, object>>?>()
        )).Returns(Task.FromResult(new List<Transaction>().AsEnumerable()));

        Assert.ThrowsAsync<DataConsistencyException>(async () =>
            await _service!.SubmitFollowUpAction(DummyFollowUpActionData));
    }

    [Test]
    public void SubmitFollowUpActionTest_RulesRepoThrows_ThrowsException() {
        _followUpRulesRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<FollowUpActionResultRule, bool>>?>(),
            It.IsAny<Func<IQueryable<FollowUpActionResultRule>, IOrderedQueryable<FollowUpActionResultRule>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<
                IQueryable<FollowUpActionResultRule>,
                IIncludableQueryable<FollowUpActionResultRule, object>>?
            >()
        )).Throws<Exception>();

        Assert.ThrowsAsync<Exception>(
            async () => await _service!.SubmitFollowUpAction(DummyFollowUpActionData));
    }

    [Test]
    public async Task SubmitFollowUpActionTest_RulesRepoReturnsNoRulesAndNoFollowUpsLeft_CallsUpdateOnTransactionRepo() {
        _transactionStatusTypeRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<TransactionStatusType, bool>>?>(),
            It.IsAny<Func<IQueryable<TransactionStatusType>, IOrderedQueryable<TransactionStatusType>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<TransactionStatusType>, IIncludableQueryable<TransactionStatusType, object>>?>()
        )).Returns(Task.FromResult(new List<TransactionStatusType> {
            DummyCompletedTransactionStatusType,
        }.AsEnumerable()));
        _followUpRulesRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<FollowUpActionResultRule, bool>>?>(),
            It.IsAny<Func<IQueryable<FollowUpActionResultRule>, IOrderedQueryable<FollowUpActionResultRule>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<
                IQueryable<FollowUpActionResultRule>,
                IIncludableQueryable<FollowUpActionResultRule, object>>?
            >()
        )).Returns(Task.FromResult(new List<FollowUpActionResultRule>().AsEnumerable()));

        await _service!.SubmitFollowUpAction(DummyFollowUpActionData);

        Assert.DoesNotThrow(
            () => _transactionsRepoMock!.Verify(
                r => r.UpdateAsync(It.Is<Transaction>(
                    t => t.StatusId == DummyCompletedTransactionStatusType.Id))));
    }

    [Test]
    public async Task SubmitFollowUpActionTest_RulesRepoReturnsNoRulesAndSomeFollowUpsLeft_DoesNotCallUpdateOnTransactionRepo() {
        _transactionsRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Transaction, bool>>?>(),
            It.IsAny<Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Transaction>, IIncludableQueryable<Transaction, object>>?>()
        )).Returns(Task.FromResult(new List<Transaction> {
            DummyTransactionWithResultAndFollowUps
        }.AsEnumerable()));
        _followUpRulesRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<FollowUpActionResultRule, bool>>?>(),
            It.IsAny<Func<IQueryable<FollowUpActionResultRule>, IOrderedQueryable<FollowUpActionResultRule>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<
                IQueryable<FollowUpActionResultRule>,
                IIncludableQueryable<FollowUpActionResultRule, object>>?
            >()
        )).Returns(Task.FromResult(new List<FollowUpActionResultRule>().AsEnumerable()));

        await _service!.SubmitFollowUpAction(DummyFollowUpActionData);

        Assert.DoesNotThrow(() => _transactionsRepoMock!.Verify(
            r => r.UpdateAsync(It.IsAny<Transaction>()), Times.Never()));
    }

    [Test]
    public async Task SubmitFollowUpActionTest_RulesRepoReturnsRuleWithNoResultNorFollowUpAndNoFollowUpsLeft_CallsUpdateOnTransactionRepo() {
        _transactionStatusTypeRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<TransactionStatusType, bool>>?>(),
            It.IsAny<Func<IQueryable<TransactionStatusType>, IOrderedQueryable<TransactionStatusType>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<TransactionStatusType>, IIncludableQueryable<TransactionStatusType, object>>?>()
        )).Returns(Task.FromResult(new List<TransactionStatusType> {
            DummyCompletedTransactionStatusType,
        }.AsEnumerable()));

        await _service!.SubmitFollowUpAction(DummyFollowUpActionData);

        Assert.DoesNotThrow(
            () => _transactionsRepoMock!.Verify(
                r => r.UpdateAsync(It.Is<Transaction>(
                    t => t.StatusId == DummyCompletedTransactionStatusType.Id))));
    }

    [Test]
    public async Task SubmitFollowUpActionTest_RulesRepoReturnsRuleWithNoResultNorFollowUpAndSomeFollowUpsLeft_DoesNotCallUpdateOnTransactionRepo() {
        _transactionsRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Transaction, bool>>?>(),
            It.IsAny<Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Transaction>, IIncludableQueryable<Transaction, object>>?>()
        )).Returns(Task.FromResult(new List<Transaction> {
            DummyTransactionWithResultAndFollowUps
        }.AsEnumerable()));

        await _service!.SubmitFollowUpAction(DummyFollowUpActionData);

        Assert.DoesNotThrow(() => _transactionsRepoMock!.Verify(
            r => r.UpdateAsync(It.IsAny<Transaction>()), Times.Never()));
    }

    [Test]
    public async Task SubmitFollowUpActionTest_RulesRepoReturnsRuleWithResultAndNoFollowUpsLeft_CallsUpdateOnTransactionRepo() {
        var rule = DummyFollowUpRule(true, 0);
        _followUpRulesRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<FollowUpActionResultRule, bool>>?>(),
            It.IsAny<Func<IQueryable<FollowUpActionResultRule>, IOrderedQueryable<FollowUpActionResultRule>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<
                IQueryable<FollowUpActionResultRule>,
                IIncludableQueryable<FollowUpActionResultRule, object>>?
            >()
        )).Returns(Task.FromResult(new List<FollowUpActionResultRule> { rule }.AsEnumerable()));
        _transactionsRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Transaction, bool>>?>(),
            It.IsAny<Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Transaction>, IIncludableQueryable<Transaction, object>>?>()
        )).Returns(Task.FromResult(new List<Transaction> {
            DummyTransactionWithResult,
        }.AsEnumerable()));
        _transactionStatusTypeRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<TransactionStatusType, bool>>?>(),
            It.IsAny<Func<IQueryable<TransactionStatusType>, IOrderedQueryable<TransactionStatusType>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<TransactionStatusType>, IIncludableQueryable<TransactionStatusType, object>>?>()
        )).Returns(Task.FromResult(new List<TransactionStatusType> {
            DummyCompletedTransactionStatusType,
        }.AsEnumerable()));

        await _service!.SubmitFollowUpAction(DummyFollowUpActionData);

        Assert.DoesNotThrow(
            () => _transactionsRepoMock!.Verify(
                r => r.UpdateAsync(It.Is<Transaction>(
                    t => t.StatusId == DummyCompletedTransactionStatusType.Id
                      && t.Result!.TypeId == rule.DesiredTransactionResultTypeId))));
    }

    [Test]
    public async Task SubmitFollowUpActionTest_RulesRepoReturnsRuleWithResultAndMoreFollowUpsLeft_CallsUpdateOnTransactionRepo() {
        var rule = DummyFollowUpRule(true, 0);
        _followUpRulesRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<FollowUpActionResultRule, bool>>?>(),
            It.IsAny<Func<IQueryable<FollowUpActionResultRule>, IOrderedQueryable<FollowUpActionResultRule>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<
                IQueryable<FollowUpActionResultRule>,
                IIncludableQueryable<FollowUpActionResultRule, object>>?
            >()
        )).Returns(Task.FromResult(new List<FollowUpActionResultRule> { rule }.AsEnumerable()));
        _transactionsRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Transaction, bool>>?>(),
            It.IsAny<Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Transaction>, IIncludableQueryable<Transaction, object>>?>()
        )).Returns(Task.FromResult(new List<Transaction> {
            DummyTransactionWithResultAndFollowUps
        }.AsEnumerable()));

        await _service!.SubmitFollowUpAction(DummyFollowUpActionData);

        Assert.DoesNotThrow(
            () => _transactionsRepoMock!.Verify(
                r => r.UpdateAsync(It.Is<Transaction>(
                    t => t.Result!.TypeId == rule.DesiredTransactionResultTypeId))));
    }

    [Test]
    public async Task SubmitFollowUpActionTest_RulesRepoReturnsRuleWithFollowUp_CallsCreateMultipleOnFollowUpRepo() {
        var rule = DummyFollowUpRule(false, 1);
        _followUpRulesRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<FollowUpActionResultRule, bool>>?>(),
            It.IsAny<Func<IQueryable<FollowUpActionResultRule>, IOrderedQueryable<FollowUpActionResultRule>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<
                IQueryable<FollowUpActionResultRule>,
                IIncludableQueryable<FollowUpActionResultRule, object>>?
            >()
        )).Returns(Task.FromResult(new List<FollowUpActionResultRule> { rule }.AsEnumerable()));

        await _service!.SubmitFollowUpAction(DummyFollowUpActionData);

        Assert.DoesNotThrow(() => _followUpActionRepoMock!.Verify(
            r => r.CreateMultipleAsync(It.Is<IEnumerable<FollowUpAction>>(
                al => al.Count() == 1
                   && al.First().TypeId == rule.DesiredFollowUpActions!.First().FollowUpActionTypeId))));
    }

    [Test]
    public async Task SubmitFollowUpActionTest_RulesRepoReturnsRuleWithFollowUp_DoesNotCallUpdateOnTransactionRepo() {
        _followUpRulesRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<FollowUpActionResultRule, bool>>?>(),
            It.IsAny<Func<IQueryable<FollowUpActionResultRule>, IOrderedQueryable<FollowUpActionResultRule>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<
                IQueryable<FollowUpActionResultRule>,
                IIncludableQueryable<FollowUpActionResultRule, object>>?
            >()
        )).Returns(Task.FromResult(new List<FollowUpActionResultRule> {
            DummyFollowUpRule(false, 1)
        }.AsEnumerable()));

        await _service!.SubmitFollowUpAction(DummyFollowUpActionData);

        Assert.DoesNotThrow(() => _transactionsRepoMock!.Verify(
            r => r.UpdateAsync(It.IsAny<Transaction>()), Times.Never()));
    }

    [Test]
    public async Task SubmitFollowUpActionTest_RulesRepoReturnsRuleWithMultipleFollowUps_CallsCreateMultipleOnFollowUpRepo() {
        var rule = DummyFollowUpRule(false, 2);
        _followUpRulesRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<FollowUpActionResultRule, bool>>?>(),
            It.IsAny<Func<IQueryable<FollowUpActionResultRule>, IOrderedQueryable<FollowUpActionResultRule>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<
                IQueryable<FollowUpActionResultRule>,
                IIncludableQueryable<FollowUpActionResultRule, object>>?
            >()
        )).Returns(Task.FromResult(new List<FollowUpActionResultRule> { rule }.AsEnumerable()));

        await _service!.SubmitFollowUpAction(DummyFollowUpActionData);

        Assert.DoesNotThrow(() => _followUpActionRepoMock!.Verify(
            r => r.CreateMultipleAsync(It.Is<IEnumerable<FollowUpAction>>(
                al => al.Count() == rule.DesiredFollowUpActions!.Count()
                   && al.All(a => rule.DesiredFollowUpActions!.Any(da => da.FollowUpActionTypeId == a.TypeId))))));
    }

    [Test]
    public async Task SubmitFollowUpActionTest_RulesRepoReturnsRuleWithMultipleFollowUps_DoesNotCallUpdateOnTransactionRepo() {
        _followUpRulesRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<FollowUpActionResultRule, bool>>?>(),
            It.IsAny<Func<IQueryable<FollowUpActionResultRule>, IOrderedQueryable<FollowUpActionResultRule>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<
                IQueryable<FollowUpActionResultRule>,
                IIncludableQueryable<FollowUpActionResultRule, object>>?
            >()
        )).Returns(Task.FromResult(new List<FollowUpActionResultRule> {
            DummyFollowUpRule(false, 2)
        }.AsEnumerable()));

        await _service!.SubmitFollowUpAction(DummyFollowUpActionData);

        Assert.DoesNotThrow(() => _transactionsRepoMock!.Verify(
            r => r.UpdateAsync(It.IsAny<Transaction>()), Times.Never()));
    }

    [Test]
    public async Task SubmitFollowUpActionTest_RulesRepoReturnsRuleWithResultAndMultipleFollowUps_CallsUpdateOnTransactionRepo() {
        var rule = DummyFollowUpRule(true, 2);
        _followUpRulesRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<FollowUpActionResultRule, bool>>?>(),
            It.IsAny<Func<IQueryable<FollowUpActionResultRule>, IOrderedQueryable<FollowUpActionResultRule>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<
                IQueryable<FollowUpActionResultRule>,
                IIncludableQueryable<FollowUpActionResultRule, object>>?
            >()
        )).Returns(Task.FromResult(new List<FollowUpActionResultRule> { rule }.AsEnumerable()));
        _transactionsRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Transaction, bool>>?>(),
            It.IsAny<Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Transaction>, IIncludableQueryable<Transaction, object>>?>()
        )).Returns(Task.FromResult(new List<Transaction> {
            DummyTransactionWithResultAndFollowUps,
        }.AsEnumerable()));

        await _service!.SubmitFollowUpAction(DummyFollowUpActionData);

        Assert.DoesNotThrow(
            () => _transactionsRepoMock!.Verify(
                r => r.UpdateAsync(It.Is<Transaction>(
                    t => t.Result!.TypeId == rule.DesiredTransactionResultTypeId))));
    }

    [Test]
    public async Task SubmitFollowUpActionTest_RulesRepoReturnsRuleWithResultAndMultipleFollowUps_CallsCreateMultipleOnFollowUpRepo() {
        var rule = DummyFollowUpRule(true, 2);
        _followUpRulesRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<FollowUpActionResultRule, bool>>?>(),
            It.IsAny<Func<IQueryable<FollowUpActionResultRule>, IOrderedQueryable<FollowUpActionResultRule>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<
                IQueryable<FollowUpActionResultRule>,
                IIncludableQueryable<FollowUpActionResultRule, object>>?
            >()
        )).Returns(Task.FromResult(new List<FollowUpActionResultRule> { rule }.AsEnumerable()));
        _transactionsRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Transaction, bool>>?>(),
            It.IsAny<Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Transaction>, IIncludableQueryable<Transaction, object>>?>()
        )).Returns(Task.FromResult(new List<Transaction> {
            DummyTransactionWithResultAndFollowUps,
        }.AsEnumerable()));

        await _service!.SubmitFollowUpAction(DummyFollowUpActionData);

        Assert.DoesNotThrow(() => _followUpActionRepoMock!.Verify(
            r => r.CreateMultipleAsync(It.Is<IEnumerable<FollowUpAction>>(
                al => al.Count() == rule.DesiredFollowUpActions!.Count()
                   && al.All(a => rule.DesiredFollowUpActions!.Any(da => da.FollowUpActionTypeId == a.TypeId))))));
    }

    [Test]
    public void SubmitFollowUpActionTest_TransactionStatusTypeRepoDoesNotReturnCompleted_ThrowsMasterDataException() {
        _transactionStatusTypeRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<TransactionStatusType, bool>>?>(),
            It.IsAny<Func<IQueryable<TransactionStatusType>, IOrderedQueryable<TransactionStatusType>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<TransactionStatusType>, IIncludableQueryable<TransactionStatusType, object>>?>()
        )).Returns(Task.FromResult(new List<TransactionStatusType>().AsEnumerable()));

        Assert.ThrowsAsync<MasterDataException>(async () =>
            await _service!.SubmitFollowUpAction(DummyFollowUpActionData));
    }
    #endregion
    #endregion
    #endregion

    #region MarkScanReviewed tests
    #region Algorithm tests
    [Test]
    public void MarkScanReviewedTest_ReceivesInvalidIds_ThrowsArgumentException() {
        const int invalidId = 0;
        Assert.ThrowsAsync<ArgumentException>(
            async () => await _service!.MarkScanReviewed(invalidId)
            );
    }

    [Test]
    public void MarkScanReviewedTest_TransactionAlreadyReviewedDoesNotCallCreateMethod_DoesNotThrow() {
        const int transactionId = 1;

        _transactionsRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Transaction, bool>>?>(),
            It.IsAny<Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Transaction>, IIncludableQueryable<Transaction, object>>?>()
        )).Returns(Task.FromResult(DummyTransactionsWithCompletedStatus));

        _transactionReviewRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<TransactionReview, bool>>?>(),
            It.IsAny<Func<IQueryable<TransactionReview>, IOrderedQueryable<TransactionReview>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<TransactionReview>, IIncludableQueryable<TransactionReview, object>>?>()
        )).Returns(Task.FromResult(DummyTransactionsReview));

        Assert.DoesNotThrowAsync(
            async () => {
                await _service!.MarkScanReviewed(transactionId);
                _transactionReviewRepoMock.Verify(review => review.CreateAsync(DummyTransactionsReview.First()), Times.Never);
                }
            );

        
    }

    [Test]
    public void MarkScanReviewedTest_TransactionIsNotPresent_ThrowsKeyNotFoundException() {
        const int transactionId = 1;

        _transactionsRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Transaction, bool>>?>(),
            It.IsAny<Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Transaction>, IIncludableQueryable<Transaction, object>>?>()
        )).Returns(Task.FromResult(new List<Transaction>().AsEnumerable()));

        Assert.ThrowsAsync<KeyNotFoundException>(
            async () => await _service!.MarkScanReviewed(transactionId)
            );
    }

    [Test]
    public void MarkScanReviewedTest_TransactionInNotCompeleteStatus_ThrowsInvalidOperationException() {
        const int transactionId = 1;
        _transactionsRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Transaction, bool>>?>(),
            It.IsAny<Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Transaction>, IIncludableQueryable<Transaction, object>>?>()
        )).Returns(Task.FromResult(DummyTransactionsWithPendingStatus));

        _transactionReviewRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<TransactionReview, bool>>?>(),
            It.IsAny<Func<IQueryable<TransactionReview>, IOrderedQueryable<TransactionReview>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<TransactionReview>, IIncludableQueryable<TransactionReview, object>>?>()
        )).Returns(Task.FromResult(new List<TransactionReview>().AsEnumerable()));

        Assert.ThrowsAsync<InvalidOperationException>(
            async () => await _service!.MarkScanReviewed(transactionId)
            );
    }

    [Test]
    public void MarkScanReviewedTest_ReceivesValidTransactionNotReviewed_DoesNotThrow() {
        const int transactionId = 1;

        _transactionsRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Transaction, bool>>?>(),
            It.IsAny<Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Transaction>, IIncludableQueryable<Transaction, object>>?>()
        )).Returns(Task.FromResult(DummyTransactionsWithCompletedStatus));

        _transactionReviewRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<TransactionReview, bool>>?>(),
            It.IsAny<Func<IQueryable<TransactionReview>, IOrderedQueryable<TransactionReview>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<TransactionReview>, IIncludableQueryable<TransactionReview, object>>?>()
        )).Returns(Task.FromResult(new List<TransactionReview>().AsEnumerable()));

        _transactionReviewRepoMock!.Setup(trr => trr.CreateAsync(It.IsAny<TransactionReview>())).
                    Returns<TransactionReview>((model) => Task.FromResult(model.TransactionId == transactionId ? model : throw new Exception()));

        Assert.DoesNotThrowAsync(
            async () => await _service!.MarkScanReviewed(transactionId)
            );
    }
    #endregion
    #endregion

    #region Dummy data
    private static UserData DummyUserData => new UserData {
        Login = "dummy@example.org",
        Name = "Test Dummy",
        RoleCode = "TR",
        CountryCode = "TC"
    };

    private static UserData DummyUserData_T2 => new UserData {
        Login = "dummy2@example.org",
        Name = "Test Dummy 2",
        RoleCode = "TR",
        CountryCode = "TC2"
    };

    private static User DummyUser => new User {
        Id = 1,
        CountryId = 1,
        Country = DummyCountry,
        RoleId = 1,
        Role = DummyRole,
        Name = "Test Dummy",
        AuthId = "dummy@example.org",
    };

    private static User DummyUser_T2 => new User {
        Id = 2,
        CountryId = 2,
        Country = DummyCountry_T2,
        RoleId = 1,
        Role = DummyRole,
        Name = "Test Dummy 2",
        AuthId = "dummy2@example.org",
    };

    private static ClientData DummyClientData => new ClientData {
        LanguageCode = "TL",
        HostAppName = "Hostify",
        PluginVersion = "4.20",
        HostAppDetails = new HostAppData {
            Device = "myPhone S3 X",
            OS = "myOS",
            Version = "6.9"
        }
    };

    private static ClientAppType DummyClientAppType => new ClientAppType {
        Id = 1,
        ClientId = 1,
        Client = new Client { Id = 1, Name = "Hostify" },
        AppTypeId = 1,
        AppType = new AppType { Id = 1, Name = "myOS" },
        ClientAppName = "Hostify",
    };

    private static Country DummyCountry => new Country {
        Id = 1,
        Name = "Test",
        Code = "TC"
    };

    private static Country DummyCountry_T2 => new Country {
        Id = 2,
        Name = "Test 2",
        Code = "TC2"
    };

    private static Role DummyRole => new Role {
        Id = 1,
        Name = "Test",
        Code = "TR"
    };

    private static CodeTypeCheckRule DummyCodeTypeCheckRule_DataMatrix => new CodeTypeCheckRule {
        Id = 1,
        AllowedCodeTypeId = 1,
        CountryId = 1,
        Order = 1,
        FailScanResultId = 1,
        AllowedCodeType = DummyCodeType_DataMatrix
    };

    private static CodeTypeCheckRule DummyCodeTypeCheckRule_QR => new CodeTypeCheckRule {
        Id = 1,
        AllowedCodeTypeId = 2,
        CountryId = 2,
        Order = 2,
        FailScanResultId = 1,
        AllowedCodeType = DummyCodeType_QR
    };

    private static QRDomainCheckRule DummyQRDomainCheckRule => new QRDomainCheckRule {
        Id = 1,
        CountryId = 1,
        Order = 1,
        FailScanResultId = 1,
        AllowedDomain = "test.co.in"
    };

    private static ScanEntry DummyScanEntry => new ScanEntry {
        Fingerprint = "12345000",
        Location = new Geolocation(28.1f, 45.3f),
        Time = new DateTime(2022, 12, 31, 7, 0, 0),
        Code = "010123456789012321SN12345678",
        CodeType = "DATA_MATRIX",
        IsManualEntry = false,
        IsOffline = false
    };

    private static ScanEntry DummyScanEntry_WithQR => new ScanEntry {
        Fingerprint = "12345000",
        Location = new Geolocation(28.1f, 45.3f),
        Time = new DateTime(2022, 12, 31, 7, 0, 0),
        Code = "https://test.co.in/gpd/01/12345678/21/123456/10/ABDCD1234?11=201021&17=210221%0A",
        CodeType = "QR_CODE",
        IsManualEntry = false,
        IsOffline = false
    };

    private static ReportEntry DummyReportEntry => new ReportEntry() {
        ScanningPoint = "Retailer",
        ScanLocationName = "The Store, Inc.",
        ScanAddress = "Anywhere Town, 1",
        ReportReasonId = 1,
        CustomReportReason = "wtf",
        Comments = "no",
        Images = new List<string> { "123" }
    };

    private static ReportEntry DummyMultiImageReportEntry => new ReportEntry() {
        ScanningPoint = "Retailer",
        ScanLocationName = "The Store, Inc.",
        ScanAddress = "Anywhere Town, 1",
        ReportReasonId = 1,
        CustomReportReason = "wtf",
        Comments = "no",
        Images = new List<string> { "123", "456", "789" }
    };

    private static ScanInputDetails DummyScanInputDetails => new ScanInputDetails() {
        Id = 1,
        Fingerprint = "12345000",
        ScanTime = new DateTime(2022, 12, 31, 7, 0, 0),
        RawData = "010123456789012321SN12345678",
        IsManualScan = false,
        IsOffline = false,
        TransactionId = 1,
        CodeTypeId = 1,
        ClientAppTypeId = 1,
        ClientAppType = new ClientAppType { Id = 1, ClientId = 1 }
    };

    private static ScanInputDetails DummyScanInputDetails_WithQRCodes => new ScanInputDetails() {
        Id = 2,
        Fingerprint = "12345000",
        ScanTime = new DateTime(2022, 12, 31, 7, 0, 0),
        RawData = "https://test.co.in/gpd/01/12345678/21/123456/10/ABDCD1234?11=201021&17=210221%0A",
        IsManualScan = false,
        IsOffline = false,
        TransactionId = 1,
        CodeTypeId = 2,
        ClientAppTypeId = 1
    };

    private static Report DummyReport => new Report {
        Id = 1,
        TypeId = 1,
        TransactionId = 1,
        ScanningPointId = 1,
        ScanningPoint = DummyScanningPointSetting.ScanningPoint,
        ReportReasonId = 1,
        SendTime = new DateTime(2022, 12, 31, 7, 0, 0),
        ScanAddress = "Anywhere Town, 1",
        ScanLocationName = "The Store, Inc.",
        CustomReportReason = "wtf",
        Comment = "no",
        Images = new List<ReportImage> {
            new ReportImage {
                ImageFile = new BlobStorageFile {
                    Id = 1,
                    FileName = Guid.NewGuid().ToString(),
                    StorageUrl = "https://localhost/images/1"
                }
            }
        }
    };

    private static BarcodeType DummyCodeType_DataMatrix => new BarcodeType() {
        Id = 1,
        Name = "2D Code",
        Value = "DATA_MATRIX"
    };

    private static BarcodeType DummyCodeType_QR => new BarcodeType() {
        Id = 2,
        Name = "QRCode",
        Value = "QR_CODE"
    };

    private static ScanningPointSetting DummyScanningPointSetting => new ScanningPointSetting {
        Id = 4,
        ClientId = 1,
        CountryId = 1,
        RoleId = 1,
        ScanningPointId = 1,
        ScanningPoint = new ScanningPoint { Id = 1, Point = "Retailer" }
    };

    private static TransactionStatusType DummyTransactionStatusType => new TransactionStatusType {
        Id = 1,
        Type = "Pending"
    };

    private static TransactionStatusType DummyCompletedTransactionStatusType => new TransactionStatusType {
        Id = 2,
        Type = "Completed"
    };

    private static IEnumerable<ReportType> DummyReportTypes => new List<ReportType> {
        new ReportType { Id = 1, Type = "Scan report" },
        new ReportType { Id = 2, Type = "Direct report" },
    };

    private static Transaction DummyTransactionWithoutReport => new Transaction() {
        Id = 1,
        StatusId = 1,
        CreationTime = DateTime.Now,
        LastUpdateTime = DateTime.Now,
        Status = DummyTransactionStatusType,
        UserId = 1,
        UserRoleId = 1,
        UserCountryId = 1,
        ScanInputDetails = DummyScanInputDetails,
        GeoLocation = "28.1,45.3",
    };

    private static Transaction DummyTransactionWithoutReport_WithQRCodes => new Transaction() {
        Id = 2,
        StatusId = 1,
        CreationTime = DateTime.Now,
        LastUpdateTime = DateTime.Now,
        Status = DummyTransactionStatusType,
        UserId = 2,
        UserRoleId = 1,
        UserCountryId = 2,
        ScanInputDetails = DummyScanInputDetails_WithQRCodes,
        GeoLocation = "28.1,45.3",
    };

    private static Transaction DummyTransactionWithReport => new Transaction() {
        Id = 1,
        StatusId = 1,
        CreationTime = DateTime.Now,
        LastUpdateTime = DateTime.Now,
        Status = DummyTransactionStatusType,
        ScanInputDetails = DummyScanInputDetails,
        UserId = 1,
        UserRoleId = 1,
        UserCountryId = 1,
        TransactionReport = DummyReport,
        GeoLocation = "28.1,45.3",
    };

    private static Transaction DummyTransactionWithResult => new Transaction() {
        Id = 1,
        StatusId = 1,
        CreationTime = DateTime.Now,
        LastUpdateTime = DateTime.Now,
        Status = DummyTransactionStatusType,
        UserId = 1,
        UserRoleId = 1,
        UserCountryId = 1,
        ScanInputDetails = DummyScanInputDetails,
        GeoLocation = "28.1,45.3",
        Result = new TransactionResult { Id = 1, TypeId = 1 }
    };

    private static IEnumerable<Transaction> DummyTransactionsWithPendingStatus => new List<Transaction>() {
        new Transaction() {
        Id = 1,
        StatusId = 1,
        CreationTime = DateTime.Now,
        LastUpdateTime = DateTime.Now,
        Status = DummyTransactionStatusType,
        ScanInputDetails = DummyScanInputDetails,
        //Result = new TransactionResult { Id = 1, TypeId = 1 }
        }
    }.AsEnumerable();

    private static IEnumerable<Transaction> DummyTransactionsWithCompletedStatus => new List<Transaction>() {
        new Transaction() {
        Id = 1,
        StatusId = 2,
        CreationTime = DateTime.Now,
        LastUpdateTime = DateTime.Now,
        Status = DummyCompletedTransactionStatusType,
        ScanInputDetails = DummyScanInputDetails,
        }
    }.AsEnumerable();
    private static IEnumerable<TransactionReview> DummyTransactionsReview => new List<TransactionReview>() {
        new TransactionReview() {
        Id = 1,
        TransactionId = 1,
        ReviewTime = DateTime.UtcNow
        }
    }.AsEnumerable();

    private static Transaction DummyTransactionWithResultAndFollowUps => new Transaction() {
        Id = 1,
        StatusId = 1,
        CreationTime = DateTime.Now,
        LastUpdateTime = DateTime.Now,
        Status = DummyTransactionStatusType,
        ScanInputDetails = DummyScanInputDetails,
        UserId = 1,
        UserRoleId = 1,
        UserCountryId = 1,
        GeoLocation = "28.1,45.3",
        Result = new TransactionResult { Id = 1, TypeId = 1 },
        FollowUpActions = DummyRemainingFollowUpActions
    };

    private static Transaction DummyTransactionWithReport_WithQRCodes => new Transaction() {
        Id = 2,
        StatusId = 1,
        CreationTime = DateTime.Now,
        LastUpdateTime = DateTime.Now,
        Status = DummyTransactionStatusType,
        UserId = 2,
        UserRoleId = 1,
        UserCountryId = 2,
        ScanInputDetails = DummyScanInputDetails_WithQRCodes,
        TransactionReport = DummyReport,
        GeoLocation = "28.1,45.3",
    };

    private static IEnumerable<ScanData> DummyScanData => new List<ScanData> {
        new ScanData { ScanEntry = DummyScanEntry, ReportData = DummyReportEntry },
        new ScanData { ScanEntry = DummyScanEntry },
        new ScanData { ScanEntry = DummyScanEntry },
        new ScanData { ScanEntry = DummyScanEntry },
    }.AsEnumerable();

    private static IEnumerable<ScanData> DummyScanData_WithQRCodes => new List<ScanData> {
        new ScanData { ScanEntry = DummyScanEntry_WithQR, ReportData = DummyReportEntry },
        new ScanData { ScanEntry = DummyScanEntry_WithQR },
        new ScanData { ScanEntry = DummyScanEntry_WithQR },
        new ScanData { ScanEntry = DummyScanEntry_WithQR },
    }.AsEnumerable();

    private static IEnumerable<Transaction> DummyCreatedTransactions => new List<Transaction>{
        DummyTransactionWithReport,
        DummyTransactionWithoutReport,
        DummyTransactionWithoutReport,
        DummyTransactionWithoutReport,
    }.AsEnumerable();

    private static IEnumerable<Transaction> DummyCreatedTransactions_WithQRCodes => new List<Transaction>{
        DummyTransactionWithReport_WithQRCodes,
        DummyTransactionWithoutReport_WithQRCodes,
        DummyTransactionWithoutReport_WithQRCodes,
        DummyTransactionWithoutReport_WithQRCodes,
    }.AsEnumerable();

    private static IEnumerable<ScanData> DummyScanDataWithMultipleImages => new List<ScanData> {
        new ScanData { ScanEntry = DummyScanEntry, ReportData = DummyReportEntry },
        new ScanData { ScanEntry = DummyScanEntry },
        new ScanData { ScanEntry = DummyScanEntry, ReportData = DummyMultiImageReportEntry },
        new ScanData { ScanEntry = DummyScanEntry, ReportData = DummyReportEntry },
    }.AsEnumerable();

    private static FollowUpActionData DummyFollowUpActionData => new FollowUpActionData {
        Id = 1,
        OptionId = 1,
        Data = new List<KeyValuePair<string, string>>() {
            new KeyValuePair<string, string>("Test", "Yas"),
            new KeyValuePair<string, string>("More", "Nah"),
            new KeyValuePair<string, string>("Colon", "Aight: take it"),
            new KeyValuePair<string, string>("Escape", "You've asked for it: pay now!"),
        },
    };

    private static FollowUpAction DummyExpectedFollowUpAction => new FollowUpAction {
        Id = 1,
        PickedOptionId = 1,
        Data = "Test:Yas\nMore:Nah\nColon:'Aight: take it'\nEscape:'You\'ve asked for it: pay now!'"
    };

    private static FollowUpActionResultRule DummyFollowUpRule(bool hasResult, int followUpCount) =>
        new FollowUpActionResultRule {
            Id = 1,
            Order = 1,
            CountryId = 1,
            DesiredTransactionResultTypeId = hasResult ? 1 : null,
            DesiredFollowUpActions = followUpCount > 0
                ? Enumerable.Range(0, followUpCount)
                    .Select(i => new FollowUpResultFollowUpAction { Id = i, FollowUpActionResultRuleId = 1, FollowUpActionTypeId = i })
                : null
        };

    private static IEnumerable<FollowUpAction> DummyFollowUpActions => new List<FollowUpAction> {
        new FollowUpAction {
            Id = 1,
            TransactionId = 1,
            TypeId = 1,
            IsCompleted = false,
        }
    }.AsEnumerable();

    private static IEnumerable<FollowUpAction> DummyRemainingFollowUpActions => new List<FollowUpAction> {
        new FollowUpAction {
            Id = 1,
            TransactionId = 1,
            TypeId = 1,
            IsCompleted = false
        },
        new FollowUpAction {
            Id = 2,
            TransactionId = 1,
            TypeId = 1,
            IsCompleted = false
        },
    }.AsEnumerable();

    private static IEnumerable<TestCaseData> SubmitFollowUpActionInvalidIdCases() {
        yield return new TestCaseData(new FollowUpActionData { Id = -1, OptionId = 1 })
            .SetName($"Invalid {nameof(FollowUpActionData.Id)}");
        yield return new TestCaseData(new FollowUpActionData { Id = 1, OptionId = -1 })
            .SetName($"Invalid {nameof(FollowUpActionData.OptionId)}");
    }

    private static IEnumerable<TestCaseData> CreateScanCases() {
        yield return new TestCaseData(
            new List<ScanData> {
                new ScanData { ScanEntry = DummyScanEntry },
            }.AsEnumerable(),
            new List<Transaction> {
                DummyTransactionWithoutReport,
            }.AsEnumerable(),
            "1 scan without report data"
        );

        yield return new TestCaseData(
            new List<ScanData> {
                new ScanData { ScanEntry = DummyScanEntry },
                new ScanData { ScanEntry = DummyScanEntry },
                new ScanData { ScanEntry = DummyScanEntry },
            }.AsEnumerable(),
            new List<Transaction> {
                DummyTransactionWithoutReport,
                DummyTransactionWithoutReport,
                DummyTransactionWithoutReport,
            }.AsEnumerable(),
            "3 scans without report data"
        );

        yield return new TestCaseData(
            new List<ScanData> {
                new ScanData { ScanEntry = DummyScanEntry, ReportData = DummyReportEntry },
            }.AsEnumerable(),
            new List<Transaction> {
                DummyTransactionWithReport,
            }.AsEnumerable(),
            "1 scan with report data"
        );

        yield return new TestCaseData(
            new List<ScanData> {
                new ScanData { ScanEntry = DummyScanEntry, ReportData = DummyReportEntry },
                new ScanData { ScanEntry = DummyScanEntry, ReportData = DummyReportEntry },
                new ScanData { ScanEntry = DummyScanEntry },
                new ScanData { ScanEntry = DummyScanEntry, ReportData = DummyReportEntry },
            }.AsEnumerable(),
            new List<Transaction> {
                DummyTransactionWithReport,
                DummyTransactionWithReport,
                DummyTransactionWithoutReport,
                DummyTransactionWithReport,
            }.AsEnumerable(),
            "4 scans with 3 entries with report data"
        );
    }
    #endregion
}