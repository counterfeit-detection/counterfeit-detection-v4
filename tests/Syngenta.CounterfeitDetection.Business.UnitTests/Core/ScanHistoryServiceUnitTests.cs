﻿using AutoMapper;
using Microsoft.EntityFrameworkCore.Query;
using Moq;
using Syngenta.CounterfeitDetection.Business.Core.Implementations;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.Business.Core.Models;
using Syngenta.CounterfeitDetection.Business.UnitTests.DummyMapper;
using Syngenta.CounterfeitDetection.DataAccess.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Models;
using Syngenta.CounterfeitDetection.Infrastructure.Constants;
using Syngenta.CounterfeitDetection.Infrastructure.Exceptions;
using System.Linq.Expressions;

namespace Syngenta.CounterfeitDetection.Business.UnitTests.Core;

[TestFixture]
internal class ScanHistoryServiceUnitTests {
    private IScanHistoryService? _scanHistoryService;
    private readonly IMapper _mapper = new Mapper(
        new MapperConfiguration(cfg => cfg.AddProfile<DummyMappingProfile>()));

    private IGenericRepository<Transaction>? _transactionsRepo;
    private IGenericRepository<TransactionResult>? _transactionsResultRepo;
    private IGenericRepository<ScanResultRepresentationRule>? _scanResultRepresentationRuleRepo;
    private ITranslationService? _translationService;
    private IGenericRepository<Language>? _languageRepo;

    private Mock<IGenericRepository<Transaction>>? _transactionsRepoMock;
    private Mock<IGenericRepository<TransactionResult>>? _transactionsResultRepoMock;
    private Mock<IGenericRepository<ScanResultRepresentationRule>>? _scanResultRepresentationRuleRepoMock;
    private Mock<ITranslationService>? _translationServiceMock;
    private Mock<IGenericRepository<Language>>? _languageRepoMock;

    [SetUp]
    public void SetUp(){
        _transactionsRepo = Mock.Of<IGenericRepository<Transaction>>();
        _transactionsResultRepo = Mock.Of<IGenericRepository<TransactionResult>>();
        _scanResultRepresentationRuleRepo = Mock.Of<IGenericRepository<ScanResultRepresentationRule>>();
        _translationService = Mock.Of<ITranslationService>();
        _languageRepo = Mock.Of<IGenericRepository<Language>>();

        _scanHistoryService = new ScanHistoryService(
        _transactionsRepo,
        _transactionsResultRepo,
        _scanResultRepresentationRuleRepo,
        _translationService,
        _languageRepo,
        _mapper
        );

        _transactionsRepoMock = Mock.Get(_transactionsRepo);
        _transactionsResultRepoMock = Mock.Get(_transactionsResultRepo);
        _scanResultRepresentationRuleRepoMock = Mock.Get(_scanResultRepresentationRuleRepo);
        _translationServiceMock = Mock.Get(_translationService);
        _languageRepoMock = Mock.Get(_languageRepo);
        
        _transactionsRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Transaction, bool>>?>(),
            It.IsAny<Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Transaction>, IIncludableQueryable<Transaction, object>>?>()
        )).Returns<Expression<Func<Transaction, bool>>?,
        Func<IQueryable<Transaction>, IOrderedQueryable<Transaction>>?, int?, int?, 
        Func<IQueryable<Transaction>, IIncludableQueryable<Transaction, object>>?>((exp, order, skip, take, include) 
        => Task.FromResult(DummyCreatedTransactions.AsQueryable().Where(exp!).AsEnumerable()));

        _transactionsResultRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<TransactionResult, bool>>?>(),
            It.IsAny<Func<IQueryable<TransactionResult>, IOrderedQueryable<TransactionResult>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<TransactionResult>, IIncludableQueryable<TransactionResult, object>>?>()
        )).Returns<Expression<Func<TransactionResult, bool>>?,
        Func<IQueryable<TransactionResult>, IOrderedQueryable<TransactionResult>>?, int?, int?, 
        Func<IQueryable<TransactionResult>, IIncludableQueryable<TransactionResult, object>>?>((exp, order, skip, take, include) 
        => Task.FromResult(DummyCreatedTransactionResults.AsQueryable().Where(exp!).AsEnumerable()));

        _scanResultRepresentationRuleRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<ScanResultRepresentationRule, bool>>?>(),
            It.IsAny<Func<IQueryable<ScanResultRepresentationRule>, IOrderedQueryable<ScanResultRepresentationRule>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<ScanResultRepresentationRule>, IIncludableQueryable<ScanResultRepresentationRule, object>>?>()
        )).ReturnsAsync(DummyScanResultRepresentationRules);

        _translationServiceMock!.Setup(r => r.GetTranslation(
            It.IsAny<string>(),
            It.IsAny<int?>()
        )).Returns<string, int?>((key, translationId) 
            => Task.FromResult(DummyTranslationMasters.AsQueryable().First(x => x.Key!.Equals(key)).DefaultValue!));

        _languageRepoMock!.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Language, bool>>?>(),
            It.IsAny<Func<IQueryable<Language>, IOrderedQueryable<Language>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Language>, IIncludableQueryable<Language, object>>?>()
        )).Returns<Expression<Func<Language, bool>>?,
        Func<IQueryable<Language>, IOrderedQueryable<Language>>?, int?, int?, 
        Func<IQueryable<Language>, IIncludableQueryable<Language, object>>?>((exp, order, skip, take, include) 
        => Task.FromResult(DummyLanguages.AsQueryable().Where(exp!).AsEnumerable()));
    }

    [Test]
    public void GetScanResultTest_ReceivesNonExistingScanId_ThrowsKeyNotFoundException() {
        //Arrange
        const int invalidId = 99;

        //Act & Assert
        Assert.ThrowsAsync<KeyNotFoundException>(async () => await _scanHistoryService!.GetScanResult(invalidId));
    }
    
    [Test]
    public async Task GetScanResultTest_ReceivesPendingTransactionId_ReturnsDefaultScanResultForPendingTransaction() {
        //Arrange
        const int pendingTransactionId = 1;
        const string expectedResultTitleForPendingTransaction = "Your scan is being processed";

        //Act
        var scanResult = await _scanHistoryService!.GetScanResult(pendingTransactionId);

        //Assert
        Assert.That(scanResult.Title, Is.EqualTo(expectedResultTitleForPendingTransaction));
    }
    
    [Test]
    public async Task GetScanResultTest_ReceivesPendingTransactionId_ReturnsTranslatedScanResultForPendingTransaction() {
        //Arrange
        const int pendingTransactionId = 1;
        const int languageId = 1;
        const string expectedResultTitle = "Your scan is being processed";

        //Act
        var scanResult = await _scanHistoryService!.GetScanResult(pendingTransactionId, languageId);

        //Assert
        Assert.That(scanResult.Title, Is.EqualTo(expectedResultTitle));
    }

    [Test]
    public void GetScanResultTest_ReceivesCompletedTransactionIdWithoutResultSpecified_ThrowsDataConsistencyException() {
        //Arrange
        const int errorTransactionId = 3;

        //Act & Assert
        Assert.ThrowsAsync<DataConsistencyException>(async () => await _scanHistoryService!.GetScanResult(errorTransactionId));
    }
    
    [Test]
    public async Task GetScanResultTest_ReceivesCompletedTransactionIdWithResultSpecified_ReturnsScanResultForCompletedTransaction() {
        //Arrange
        const int completedTransactionId = 2;
        const string expectedResultTitle= "Title";
        const string expectedResultAction= "Action";
        const int expectedResultTypeId= 1;

        //Act
        var scanResult = await _scanHistoryService!.GetScanResult(completedTransactionId);

        //Assert
        Assert.Multiple(() =>{
            Assert.That(scanResult.Title, Is.EqualTo(expectedResultTitle));
            Assert.That(scanResult.Message, Is.EqualTo(expectedResultAction));
            Assert.That(scanResult.TypeId, Is.EqualTo(expectedResultTypeId));
            Assert.That(scanResult.Style!.TitleColor, Is.EqualTo(ExpectedScanResultStyle.TitleColor));
            Assert.That(scanResult.Style!.MessageColor, Is.EqualTo(ExpectedScanResultStyle.MessageColor));
        });
    }
    
    [Test]
    public void GetScanResultTest_ReceivesInvalidId_ThrowsArgumentOutOfRangeException() {
        //Arrange
        const int errorTransactionId = -1;
        
        //Act & Assert
        Assert.ThrowsAsync<ArgumentOutOfRangeException>(async () => await _scanHistoryService!.GetScanResult(errorTransactionId));
    }
    
    [Test]
    public void GetScansTest_ReceivesInvalidLanguageCode_ThrowsKeyNotFoundException() {
        //Arrange
        var filter = new ScanHistoryFilter(){
            LanguageCode = "BC"
        };
        
        //Act & Assert
        Assert.ThrowsAsync<KeyNotFoundException>(async () => await _scanHistoryService!.GetScans(filter));
    }
    
    [Test]
    public async Task GetScansTest_ReceivesScanHistoryFilter_ValidResponseWithPendingTransaction() {
        //Arrange
        const int expectedTotalScans = 1;
        const string expectedStatus = "Pending";
        const string expectedResultTitle = "Your scan is being processed";
        var filter = new ScanHistoryFilter(){
            LanguageCode = "TL",
            UserLogin = "test.user@gmail.com",
            Before = DateTime.Now
        };
        
        //Act
        var result = await _scanHistoryService!.GetScans(filter);

        //Assert
        Assert.Multiple(() =>{
            Assert.That(result.TotalScans, Is.EqualTo(expectedTotalScans));
            var scan = result.Scans.FirstOrDefault();

            Assert.That(scan, Is.Not.Null);
            Assert.That(scan!.Status, Is.EqualTo(expectedStatus));
            Assert.That(scan!.CreationTime.ToShortDateString(), Is.EqualTo(DummyPendingTransaction.CreationTime.ToShortDateString()));
            Assert.That(scan!.LastUpdateTime.ToShortDateString(), Is.EqualTo(DummyPendingTransaction.LastUpdateTime.ToShortDateString()));
            Assert.That(scan!.Result!.Title, Is.EqualTo(expectedResultTitle));
            Assert.That(scan!.ScanData!.First(x => x.Key.Equals("ScanData_Fingerprint")).Value, Is.EqualTo(DummyScanInputDetails.Fingerprint));
            Assert.That(scan!.ScanData!.First(x => x.Key.Equals("ScanData_Location")).Value, Is.EqualTo(DummyPendingTransaction.GeoLocation));
            Assert.That(scan!.ScanData!.First(x => x.Key.Equals("ScanData_Time")).Value, Is.EqualTo(DummyScanInputDetails.ScanTime.ToString("yyyy-MM-ddThh:mm:ssZ")));
            Assert.That(scan!.Actions, Is.Null);
            Assert.That(scan!.FollowUp, Is.Null);
        });
    }
    
    [Test]
    public async Task GetScansTest_ReceivesScanHistoryFilter_ValidResponseWithCompletedTransactions() {
        //Arrange
        const int expectedTotalScans = 1;
        const int expectedResultId = 1;
        const int expectedActionsCount = 2;
        const int expectedFollowUpOptionsCount = 2;
        const string expectedStatus = "Completed";
        const string expectedResultTitle = "Title";
        const string expectedResultAction = "Action";
        const string expectedFirstOption = "Yes";
        var filter = new ScanHistoryFilter(){
            LanguageCode = "TL",
            UserLogin = "test.user@gmail.com",
            After = DateTime.Now.AddDays(1),
            Take = 1,
            Skip = 0
        };
        
        //Act
        var result = await _scanHistoryService!.GetScans(filter);

        //Assert
        Assert.Multiple(() =>{
            Assert.That(result.TotalScans, Is.EqualTo(expectedTotalScans));

            var scan = result.Scans.FirstOrDefault();

            Assert.That(scan, Is.Not.Null);
            Assert.That(scan!.Status, Is.EqualTo(expectedStatus));
            Assert.That(scan!.CreationTime.ToShortDateString(), Is.EqualTo(DummyCompletedTransaction.CreationTime.ToShortDateString()));
            Assert.That(scan!.LastUpdateTime.ToShortDateString(), Is.EqualTo(DummyCompletedTransaction.LastUpdateTime.ToShortDateString()));
            Assert.That(scan!.Result!.TypeId, Is.EqualTo(expectedResultId));
            Assert.That(scan!.Result!.Title, Is.EqualTo(expectedResultTitle));
            Assert.That(scan!.Result!.Message, Is.EqualTo(expectedResultAction));
            Assert.That(scan!.ScanData!.First(x => x.Key.Equals("ScanData_Fingerprint")).Value, Is.EqualTo(DummyScanInputDetails.Fingerprint));
            Assert.That(scan!.ScanData!.First(x => x.Key.Equals("ScanData_Location")).Value, Is.EqualTo(DummyCompletedTransaction.GeoLocation));
            Assert.That(scan!.ScanData!.First(x => x.Key.Equals("ScanData_Time")).Value, Is.EqualTo(DummyScanInputDetails.ScanTime.ToString("yyyy-MM-ddThh:mm:ssZ")));
            Assert.That(scan!.Actions, Is.Not.Null);
            Assert.That(scan!.Actions!.Count(), Is.EqualTo(expectedActionsCount));

            var followUp = result.Scans.First().FollowUp;

            Assert.That(followUp, Is.Not.Null);
            Assert.That(followUp!.Id, Is.EqualTo(DummyFollowUpAction.Id));
            Assert.That(followUp!.Title, Is.EqualTo(DummyFollowUpAction.Type!.Type));
            Assert.That(followUp!.Options, Is.Not.Null);
            Assert.That(followUp!.Options!.Count(), Is.EqualTo(expectedFollowUpOptionsCount));
            Assert.That(followUp!.Options!.First().Label, Is.EqualTo(expectedFirstOption));
            Assert.That(followUp!.Inputs, Is.Null);
        });
    }

    [TestCaseSource(nameof(GetScansCountCases))]
    public async Task GetScansCountTest_ReceivesScanHistoryFilter_ReturnsCorrectAmountOfScans(ScanHistoryFilter filter, int expectedScansCount) {
        //Arrange &Act
        var result = await _scanHistoryService!.GetScans(filter);

        //Assert
        Assert.That(result.TotalScans, Is.EqualTo(expectedScansCount));
    }

    private static IEnumerable<TestCaseData> GetScansCountCases() {
        yield return new TestCaseData(
            new ScanHistoryFilter {
                LanguageCode = "TL",
                UserLogin = "test.user@gmail.com",
                Before = DateTime.Now
            },
            1
        );
            
        yield return new TestCaseData(
            new ScanHistoryFilter {
                LanguageCode = "TL",
                UserLogin = "test.user@gmail.com",
            },
            3
        );

        yield return new TestCaseData(
            new ScanHistoryFilter {
                LanguageCode = "TL",
                UserLogin = "some.user@gmail.com",
                Before = DateTime.Now
            },
            0
        );
    }

    private static ScanResultStyle ExpectedScanResultStyle => new ScanResultStyle(){
        TitleColor = "#000000",
        MessageColor = "#000001"
    };

    private static IEnumerable<Transaction> DummyCreatedTransactions => new List<Transaction>{ 
        DummyPendingTransaction,
        DummyCompletedTransaction,
        DummyErrorTransaction,
    }.AsEnumerable();
    
    private static IEnumerable<TransactionResult> DummyCreatedTransactionResults => new List<TransactionResult>{ 
        DummyPendingTransactionResult,
        DummyCompletedTransactionResult
    }.AsEnumerable();
    
    private static IEnumerable<ScanResultRepresentationRule> DummyScanResultRepresentationRules => new List<ScanResultRepresentationRule>{ 
        DummyScanResultRepresentationRule,
    }.AsEnumerable();
    
    private static IEnumerable<TranslationMaster> DummyTranslationMasters => new List<TranslationMaster>{ 
        new TranslationMaster{ 
           Id = 1,
           Key = "scanresult.transaction-still-pending.title",
           DefaultValue = "Your scan is being processed"
        },
        new TranslationMaster{ 
           Id = 2,
           Key = Constants.TranslationKeys.ScanData_Fingerprint,
           DefaultValue = "ScanData_Fingerprint"
        },
        new TranslationMaster{ 
           Id = 3,
           Key = Constants.TranslationKeys.ScanData_Location,
           DefaultValue = "ScanData_Location"
        },
        new TranslationMaster{ 
           Id = 4,
           Key = Constants.TranslationKeys.ScanData_Time,
           DefaultValue = "ScanData_Time"
        },
        new TranslationMaster{ 
           Id = 5,
           Key = Constants.TranslationKeys.ScanData_GTIN,
           DefaultValue = "ScanData_GTIN"
        },
        new TranslationMaster{ 
           Id = 6,
           Key = Constants.TranslationKeys.ScanData_SerialNumber,
           DefaultValue = "ScanData_SerialNumber"
        },
        new TranslationMaster{ 
           Id = 7,
           Key = Constants.TranslationKeys.ScanData_NumberOfScans,
           DefaultValue = "ScanData_NumberOfScans"
        },
        new TranslationMaster{ 
           Id = 8,
           Key = Constants.TranslationKeys.Action_Report,
           DefaultValue = "Action_Report"
        },
        new TranslationMaster{ 
           Id = 9,
           Key = Constants.TranslationKeys.Action_Review,
           DefaultValue = "Action_Review"
        },
    }.AsEnumerable();

    private static TransactionResult DummyPendingTransactionResult => new TransactionResult() {
        Id = 1,
        TransactionId = 2,
        TypeId = 1
    };
    
    private static TransactionResult DummyCompletedTransactionResult => new TransactionResult() {
        Id = 2,
        TransactionId = 2,
        TypeId = 2
    };

    private static Transaction DummyPendingTransaction => new Transaction() {
        Id = 1,
        StatusId = 1,
        CreationTime = DateTime.MinValue,
        LastUpdateTime = DateTime.MinValue,
        Status = DummyPendingTransactionStatusType,
        ScanInputDetails = DummyScanInputDetails,
        GeoLocation = "12.3,45.6",
        UserId = 1,
        UserRoleId = 1,
        UserCountryId = 1,
        User = DummyUser
    };
    
    private static Transaction DummyCompletedTransaction => new Transaction() {
        Id = 2,
        StatusId = 2,
        CreationTime = DateTime.Now.AddDays(1),
        LastUpdateTime = DateTime.Now.AddDays(1),
        Status = DummyCompletedTransactionStatusType,
        ScanInputDetails = DummyScanInputDetails,
        FollowUpActions = new List<FollowUpAction>(){
            DummyFollowUpAction
        },
        UserId = 1,
        GeoLocation = "12.3,45.6",
        UserRoleId = 1,
        UserCountryId = 1,
        User = DummyUser
    };
    
    private static FollowUpAction DummyFollowUpAction => new FollowUpAction() {
        Id =1,
        IsCompleted = false,
        Type = new FollowUpActionType(){
            IsAutomatic = false,
            Type = "Conditional question",
            Options = new List<FollowUpActionTypeOption>(){
                new FollowUpActionTypeOption(){
                    Id = 1,
                    Option = "No",
                    Order = 2
                },
                new FollowUpActionTypeOption(){
                    Id = 2,
                    Option = "Yes",
                    Order = 1
                },
            }
        },
        
    };

    private static Transaction DummyErrorTransaction => new Transaction() {
        Id = 3,
        StatusId = 2,
        CreationTime = DateTime.Now,
        LastUpdateTime = DateTime.Now,
        Status = DummyCompletedTransactionStatusType,
        ScanInputDetails = DummyScanInputDetails,
        UserId = 1,
        UserRoleId = 1,
        GeoLocation = "12.3,45.6",
        UserCountryId = 1,
        User = DummyUser
    };

    private static ScanResultRepresentationRule DummyScanResultRepresentationRule => new ScanResultRepresentationRule() {
        Id = 1,
        Order = 100,
        CountryId = 1,
        ClientId = 1,
        RoleId = 1,
        TransactionResultTypeId = 1,
        ResultTitleKey = "default-result-title-key",
        ResultActionKey = "default-result-action-key",
        ResultTitleColor = "#000000",
        ResultActionColor = "#000001",
        MasterResultTitle = new TranslationMaster(){
            Key = "default-result-title-key",
            DefaultValue = "Title"
        },
        MasterResultAction = new TranslationMaster(){
            Key = "default-result-action-key",
            DefaultValue = "Action"
        },
    };

    private static ScanInputDetails DummyScanInputDetails => new ScanInputDetails() {
        Id = 1,
        Fingerprint = "12345678",
        ScanTime = new DateTime(2023, 02, 01, 7, 0, 0),
        RawData = "010123456789012321SN12345678",
        IsManualScan = false,
        IsOffline = false,
        CodeTypeId = 1,
        ClientAppTypeId = 1,
        ClientAppType = DummyClientAppType
    };

    private static ClientAppType DummyClientAppType => new ClientAppType(){
        ClientId = 1
    };
    
    private static IEnumerable<Language> DummyLanguages => new List<Language>(){
        DummyLanguage
    };
    
    private static Language DummyLanguage => new Language(){
        Id = 1,
        Code = "TL",
        Name = "Test Language"
    };
    
    private static User DummyUser => new User(){
        Id = 1,
        Name = "Test user",
        AuthId = "test.user@gmail.com"
    };

    private static TransactionStatusType DummyPendingTransactionStatusType => new TransactionStatusType {
        Id = 1,
        Type = "Pending"
    };
    
    private static TransactionStatusType DummyCompletedTransactionStatusType => new TransactionStatusType {
        Id = 2,
        Type = "Completed"
    };
}
