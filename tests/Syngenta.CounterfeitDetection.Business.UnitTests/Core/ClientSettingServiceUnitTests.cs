﻿using AutoMapper;
using Microsoft.EntityFrameworkCore.Query;
using Moq;
using Syngenta.CounterfeitDetection.Business.Core.Implementations;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.Business.Core.Models;
using Syngenta.CounterfeitDetection.Business.Mappings;
using Syngenta.CounterfeitDetection.Business.UnitTests.DummyMapper;
using Syngenta.CounterfeitDetection.DataAccess.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Migrations;
using Syngenta.CounterfeitDetection.DataAccess.Models;

using System.Linq.Expressions;

using Infra = Syngenta.CounterfeitDetection.Infrastructure.Models;

namespace Syngenta.CounterfeitDetection.Business.UnitTests.Core;
internal class ClientSettingServiceUnitTests {
    #region SetUp
    private IGenericRepository<Client>? _clientRepo;
    private IGenericRepository<DataAccess.Models.Language>? _languageRepo;
    private IGenericRepository<Country>? _countryRepo;
    private IGenericRepository<Role>? _roleRepo;
    private IGenericRepository<ClientAppType>? _clientAppTypeRepo;
    private readonly IMapper _mapper = new Mapper(
        new MapperConfiguration(cfg => cfg.AddProfile<MappingProfile>()));

    private IClientSettingService? _service;

    private Mock<IGenericRepository<Client>>? _clientRepoMock;
    private Mock<IGenericRepository<DataAccess.Models.Language>>? _languageRepoMock;
    private Mock<IGenericRepository<Country>>? _countryRepoMock;
    private Mock<IGenericRepository<Role>>? _roleRepoMock;
    private Mock<IGenericRepository<ClientAppType>>? _clientAppTypeRepoMock;

    [SetUp]
    public void Setup() {
        _clientRepo = Mock.Of<IGenericRepository<Client>>();
        _languageRepo = Mock.Of<IGenericRepository<DataAccess.Models.Language>>();
        _countryRepo = Mock.Of<IGenericRepository<Country>>();
        _roleRepo = Mock.Of<IGenericRepository<Role>>();
        _clientAppTypeRepo = Mock.Of<IGenericRepository<ClientAppType>>();

        _service = new ClientSettingService(
            _clientRepo,
            _languageRepo,
            _countryRepo,
            _roleRepo,
            _clientAppTypeRepo,
            _mapper);

        _clientRepoMock = Mock.Get(_clientRepo);
        _languageRepoMock = Mock.Get(_languageRepo);
        _countryRepoMock = Mock.Get(_countryRepo);
        _roleRepoMock = Mock.Get(_roleRepo);
        _clientAppTypeRepoMock = Mock.Get(_clientAppTypeRepo);

        _clientRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Client, bool>>?>(),
            It.IsAny<Func<IQueryable<Client>, IOrderedQueryable<Client>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Client>, IIncludableQueryable<Client, object>>?>()
        )).Returns<Expression<Func<Client, bool>>?,
        Func<IQueryable<Client>, IOrderedQueryable<Client>>?, int?, int?,
        Func<IQueryable<Client>, IIncludableQueryable<Client, object>>?>((exp, order, skip, take, include)
        => Task.FromResult(DummyClients.AsQueryable().Where(exp!).AsEnumerable()));

        _languageRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<DataAccess.Models.Language, bool>>?>(),
            It.IsAny<Func<IQueryable<DataAccess.Models.Language>, IOrderedQueryable<DataAccess.Models.Language>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<DataAccess.Models.Language>, IIncludableQueryable<DataAccess.Models.Language, object>>?>()
        )).Returns(Task.FromResult(new List<DataAccess.Models.Language> { DummyLanguages }.AsEnumerable()));

        _countryRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Country, bool>>?>(),
            It.IsAny<Func<IQueryable<Country>, IOrderedQueryable<Country>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Country>, IIncludableQueryable<Country, object>>?>()
        )).Returns<Expression<Func<Country, bool>>?,
        Func<IQueryable<Country>, IOrderedQueryable<Country>>?, int?, int?,
        Func<IQueryable<Country>, IIncludableQueryable<Country, object>>?>((exp, order, skip, take, include)
        => Task.FromResult(DummyCountries.AsQueryable().Where(exp!).AsEnumerable()));

        _roleRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<Role, bool>>?>(),
            It.IsAny<Func<IQueryable<Role>, IOrderedQueryable<Role>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<Role>, IIncludableQueryable<Role, object>>?>()
        )).Returns<Expression<Func<Role, bool>>?,
        Func<IQueryable<Role>, IOrderedQueryable<Role>>?, int?, int?,
        Func<IQueryable<Role>, IIncludableQueryable<Role, object>>?>((exp, order, skip, take, include)
        => Task.FromResult(DummyRoles.AsQueryable().Where(exp!).AsEnumerable()));

        _clientAppTypeRepoMock.Setup(r => r.GetManyAsync(
            It.IsAny<Expression<Func<ClientAppType, bool>>?>(),
            It.IsAny<Func<IQueryable<ClientAppType>, IOrderedQueryable<ClientAppType>>?>(),
            It.IsAny<int?>(),
            It.IsAny<int?>(),
            It.IsAny<Func<IQueryable<ClientAppType>, IIncludableQueryable<ClientAppType, object>>?>()
        )).Returns<Expression<Func<ClientAppType, bool>>?,
        Func<IQueryable<ClientAppType>, IOrderedQueryable<ClientAppType>>?, int?, int?,
        Func<IQueryable<ClientAppType>, IIncludableQueryable<ClientAppType, object>>?>((exp, order, skip, take, include)
        => Task.FromResult(DummyClientAppTypes.AsQueryable().Where(exp!).AsEnumerable()));
    }
    #endregion

    #region Tests
    [TestCaseSource(nameof(InvalidParametersCases))]
    public void GetClientSettings_PassedInvalidParameterValues_ThrowsKeyNotFoundException(string appName, string countryCode, string roleCode)
        => Assert.ThrowsAsync<KeyNotFoundException>(async () => await _service!.GetClientSettings(appName, countryCode, roleCode));

    private static IEnumerable<TestCaseData> InvalidParametersCases() {
        yield return new TestCaseData("TestApp", "IN", "TR");
        yield return new TestCaseData("StandaloneApp", "TC", "TR");
        yield return new TestCaseData("StandaloneApp", "IN", "TT");
        yield return new TestCaseData("StandaloneApp", "IN", "TT");
        yield return new TestCaseData("StandaloneApp", "TC", "TR");
        yield return new TestCaseData("TestApp", "TC", "TR");
        yield return new TestCaseData("TestApp", "TC", "TT");
    }

    [Test]
    public async Task GetClientSettings_ValidCase_ReturnsClientSettings() {
        //Arrange
        const string appName = "StandaloneApp";
        const string countryCode = "IN";
        const string roleCode = "TR";

        //Act
        var actual = await _service!.GetClientSettings(appName, countryCode, roleCode);

        //Assert
        Assert.Multiple(() => {
            Assert.That(actual, Is.Not.Null);
            Assert.That(actual.Themes!.Primary!.Main, Is.EqualTo(_expectedDummyClientSettings.Themes!.Primary!.Main));
            Assert.That(actual.Themes!.Secondary!.Light, Is.EqualTo(_expectedDummyClientSettings.Themes!.Secondary!.Light));
            Assert.That(actual.Themes!.Tertiary!.Contrast, Is.EqualTo(_expectedDummyClientSettings.Themes!.Tertiary!.Contrast));
            Assert.That(actual.Languages.Count(), Is.EqualTo(_expectedDummyClientSettings.Languages.Count()));
            Assert.That(actual.Languages.First().Name, Is.EqualTo(_expectedDummyClientSettings.Languages.First().Name));
            Assert.That(actual.ReportReasons.Count(), Is.EqualTo(_expectedDummyClientSettings.ReportReasons.Count()));
            Assert.That(actual.ReportReasons.First().Name, Is.EqualTo(_expectedDummyClientSettings.ReportReasons.First().Name));
            Assert.That(actual.ScanOrigins.Count(), Is.EqualTo(_expectedDummyClientSettings.ScanOrigins.Count()));
            Assert.That(actual.ScanOrigins.First().Order, Is.EqualTo(_expectedDummyClientSettings.ScanOrigins.First().Order));
            Assert.That(actual.Features.Count(), Is.EqualTo(_expectedDummyClientSettings.Features.Count()));
            Assert.That(actual.Features.First().Name, Is.EqualTo(_expectedDummyClientSettings.Features.First().Name));
        });

    }

    #endregion

    #region Dummy data

    private static Country DummyCountry => new Country {
        Id = 3,
        Name = "India",
        Code = "IN"
    };

    private static IEnumerable<Country> DummyCountries => new List<Country>{
        DummyCountry
    }.AsEnumerable();

    private static Role DummyRole => new Role {
        Id = 1,
        Name = "Test",
        Code = "TR"
    };

    private static IEnumerable<Role> DummyRoles => new List<Role>{
        DummyRole
    }.AsEnumerable();

    private static ClientAppType DummyClientAppType => new ClientAppType {
        Id = 1,
        ClientId = 4,
        AppTypeId = 1,
        ClientAppName = "StandaloneApp",
    };

    private static IEnumerable<ClientAppType> DummyClientAppTypes => new List<ClientAppType>{
        DummyClientAppType
    }.AsEnumerable();

    private static DataAccess.Models.Language DummyLanguages => new DataAccess.Models.Language {
        Id = 1,
        Code = "EN",
        Name = "English",
    };

    private static Client DummyClient => new Client {
        Id = 4,
        Name = "StandaloneApp",
        Features = new List<ClientFeatureSetting> {
            new ClientFeatureSetting { 
                Id = 1, 
                CountryId = 3, 
                RoleId = 1, 
                ClientId =4, 
                FeatureId= 1,
                ClientFeature = new ClientFeature { Id = 1, Name = "Reported Scans"} 
            }
        },
        Themes = new List<ThemeColorsSetting> {
            new ThemeColorsSetting {
                Id=1,
                CountryId=3,
                RoleId=1,
                ClientId=4,
                ThemeId=1,
                ContrastText = "#CC00CC",
                Dark = "#003300",
                Main = "#009900",
                Light = "#00CC00",
                Theme = new Theme  { Id = 1, Name= "First" }
            },
            new ThemeColorsSetting { 
                Id=2, 
                CountryId=3, 
                RoleId=1, 
                ClientId=4, 
                ThemeId=2, 
                ContrastText = "#00CCCC", 
                Dark = "#330000", 
                Main = "#990000", 
                Light = "#CC0000",
                Theme = new Theme  { Id = 1, Name= "Second" } 
            },
            new ThemeColorsSetting { 
                Id=3, CountryId=3,
                RoleId=1, ClientId=4, 
                ThemeId=3, 
                ContrastText = "#CCCC00", 
                Dark = "#000033", 
                Main = "#000099", 
                Light = "#0000CC",
                Theme = new Theme  { Id = 1, Name= "Third" }
            }
        },
        ReportReasons = new List<ReportReasonSetting> {
            new ReportReasonSetting {
                Id = 1, CountryId = 3,
                RoleId = 1,
                ClientId = 4,
                GeneralReasonId = 1,
                GeneralReportReason = new GeneralReportReason { Id=1, Reason = "Just testing" } }
        },
        ScanOrigins = new List<ScanningPointSetting> {
            new ScanningPointSetting {
                Id = 1,
                CountryId = 3,
                RoleId = 1,
                ClientId =4,
                ScanningPointId = 1,
                Order = 100,
                ScanningPoint = new ScanningPoint { Id=1, Point="Farm" } }
        },
        ResultTypes = new List<ScanResultRepresentationRule> {
            new ScanResultRepresentationRule {
                Id = 1,
                CountryId = 3,
                RoleId = 1,
                ClientId =4,
                ResultTitleKey = "Valid: low risk",
                ResultActionKey = "Congrats! Your scan is valid",
                ResultTitleColor = "#00CC00",
                ResultActionColor = "#000000"
            }
        }
    };

    private static IEnumerable<Client> DummyClients => new List<Client>{
        DummyClient
    }.AsEnumerable();

    private readonly ClientSettings _expectedDummyClientSettings = new ClientSettings() {
        Themes = new ThemeSettings {
            Primary = new Infra.Theme {
                Main = "#009900",
                Dark = "#003300",
                Light = "#00CC00",
                Contrast = "#CC00CC",
            },
            Secondary = new Infra.Theme {
                Main = "#990000",
                Dark = "#330000",
                Light = "#CC0000",
                Contrast = "#00CCCC",
            },
            Tertiary = new Infra.Theme {
                Main = "#000099",
                Dark = "#000033",
                Light = "#0000CC",
                Contrast = "#CCCC00",
            }
        },
        Languages = new List<Business.Core.Models.Languages> {
            new Business.Core.Models.Languages {
                Id = 1,
                Name = "English",
                Code = "EN"
            }
        },
        ResultTypes = new List<ScanResult> {
            new ScanResult {
                TypeId = 1,
                Title = "Valid: low risk",
                Message = "Congrats! Your scan is valid",
                Style = new ScanResultStyle {
                    TitleColor = "#00CC00",
                    MessageColor = "#000000"
                }
            }
        },
        ReportReasons = new List<ReportReason> {
            new ReportReason {
                Id = 1,
                Name = "Just testing",
                RequireCustomReason = false,
                Order = 100
            }
        },
        ScanOrigins = new List<ScanOrigin> {
            new ScanOrigin {
                Id = 1,
                Name = "Farm",
                Order = 100,
            }
        },
        Features = new List<ClientFeatures> {
            new ClientFeatures {
                Id = 1,
                Name = "Reported Scans"
            }
        }
    };
    #endregion
}
