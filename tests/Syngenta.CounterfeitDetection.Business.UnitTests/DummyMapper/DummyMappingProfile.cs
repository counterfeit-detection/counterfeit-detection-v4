﻿using Syngenta.CounterfeitDetection.Business.Mappings;
using Syngenta.CounterfeitDetection.DataAccess.Models;

namespace Syngenta.CounterfeitDetection.Business.UnitTests.DummyMapper;
internal class DummyMappingProfile : MappingProfile {
    public DummyMappingProfile() : base() {
        CreateMap<Tuple<Transaction, string>, IEnumerable<KeyValuePair<string, string>>>()
            .ConvertUsing<DummyScanInputDetailsToKeyValueCollectionConverter>();
    }
}
