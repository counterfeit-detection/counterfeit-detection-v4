﻿using Moq;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.Business.Mappings;
using Syngenta.CounterfeitDetection.DataAccess.Models;
using Syngenta.CounterfeitDetection.Infrastructure.Constants;

namespace Syngenta.CounterfeitDetection.Business.UnitTests.DummyMapper;
internal class DummyScanInputDetailsToKeyValueCollectionConverter : ScanInputDetailsToKeyValueCollectionConverter {
    private readonly static Mock<ITranslationService> s_translationServiceMock = new Mock<ITranslationService>();

    public DummyScanInputDetailsToKeyValueCollectionConverter() : base(s_translationServiceMock.Object){
        s_translationServiceMock.Setup(r => r.GetTranslation(
            It.IsAny<string>(),
            It.IsAny<string>()
        )).Returns<string, string?>((key, translationCode) 
            => Task.FromResult(DummyTranslationMasters.AsQueryable().First(x => x.Key!.Equals(key)).DefaultValue!));
    }

    private static IEnumerable<TranslationMaster> DummyTranslationMasters => new List<TranslationMaster>{ 
        new TranslationMaster{ 
            Id = 1,
            Key = Constants.TranslationKeys.ScanData_Fingerprint,
            DefaultValue = "ScanData_Fingerprint"
        },
        new TranslationMaster{ 
            Id = 2,
            Key = Constants.TranslationKeys.ScanData_RawData,
            DefaultValue = "ScanData_RawData"
        },
        new TranslationMaster{ 
            Id = 3,
            Key = Constants.TranslationKeys.ScanData_BarcodeType,
            DefaultValue = "ScanData_BarcodeType"
        },
        new TranslationMaster{ 
            Id = 4,
            Key = Constants.TranslationKeys.ScanData_Location,
            DefaultValue = "ScanData_Location"
        },
        new TranslationMaster{ 
            Id = 5,
            Key = Constants.TranslationKeys.ScanData_Time,
            DefaultValue = "ScanData_Time"
        },
    };
}
