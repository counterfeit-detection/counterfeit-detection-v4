﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using Syngenta.CounterfeitDetection.DataAccess.Implementations;
using Syngenta.CounterfeitDetection.DataAccess.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Models;

namespace Syngenta.CounterfeitDetection.DataAccess.UnitTests.Implementations;
internal class GenericRepositoryUnitTest {
    private SyngentaContext? _context;
    private IGenericRepository<User>? _repository;
    private Mock<ILogger<SyngentaContext>>? _loggerMock;

    [SetUp]
    public void SetUp() {
        _loggerMock = new Mock<ILogger<SyngentaContext>>();
        var options = new DbContextOptionsBuilder<SyngentaContext>()
            .UseInMemoryDatabase(databaseName: "SyngentaDatabase")
            .Options;

        using (var context = new SyngentaContext(options, _loggerMock.Object))
        {
            context.Users.Add(new User { Id = 1, Name = "noname" });
            context.Users.Add(new User { Id = 2, Name = "noname" });
            context.Users.Add(new User { Id = 3, Name = "noname" });
            context.SaveChanges();
        }

        _context = new SyngentaContext(options, _loggerMock.Object);
        
        _repository = new GenericRepository<User>(_context
            ?? throw new ArgumentNullException(nameof(_context)));
    }

    [TearDown]
    public void TearDown() => _context?.Database.EnsureDeleted();

    [Test]
    public async Task CreateMultipleAsyncTest_CalledWithValidEntities_CreatesEntitiesInDB() {
        var entities = new List<User> {
            new User() { Name = "1" },
            new User() { Name = "2" },
            new User() { Name = "3" }
        }.AsEnumerable();

        var result = await _repository!.CreateMultipleAsync(entities);

        Assert.That(
            _context!.Users.FirstOrDefault(u => u.Name == "1") is not null &&
            _context!.Users.FirstOrDefault(u => u.Name == "2") is not null &&
            _context!.Users.FirstOrDefault(u => u.Name == "3") is not null
        );
    }
   
    [Test]
    public async Task CreateMultipleAsyncTest_CalledWithValidEntities_SetupsCorrectEntityIds() {
        var lastId = _context?.Users?.OrderBy(x => x.Id).Last().Id;
        var entities = new List<User> {
            new User() { Name = "1" },
            new User() { Name = "2" },
            new User() { Name = "3" }
        }.AsEnumerable();

        var result = await _repository!.CreateMultipleAsync(entities);

        Assert.That(
            result.FirstOrDefault(e => e.Name == "1")?.Id == lastId + 1 &&
            result.FirstOrDefault(e => e.Name == "2")?.Id == lastId + 2 &&
            result.FirstOrDefault(e => e.Name == "3")?.Id == lastId + 3
        );
    }

    [Test]
    public async Task CreateAsyncTest_CalledWithValidEntity_CreatesEntityInDB() {
        //Arrange
        var entity = new User() { Name = "4" };
        var expectedNextId = _context?.Users?.OrderBy(x => x.Id).Last().Id + 1;

        //Act
        await _repository!.CreateAsync(entity);
        var insertedEntity = _context?.Users?.Find(expectedNextId);

        //Assert
        Assert.IsNotNull(insertedEntity);
    }

    [Test]
    public async Task CreateAsyncTest_CalledWithValidEntity_SetupsCorrectEntityId() {
        //Arrange
        var expectedNextId = _context?.Users?.OrderBy(x => x.Id).Last().Id + 1;
        var entity = new User() { Name = "test" };

        //Act
        var result = await _repository!.CreateAsync(entity);

        //Assert
        Assert.That(result.Id, Is.EqualTo(expectedNextId));
    }

    [Test]
    public void CreateAsyncTest_CalledWithExistingEntity_ThrowsException() {
        //Arrange
        var entity = new User() { Id = 3 };

        //Act & Assert
        Assert.ThrowsAsync<ArgumentException>(async () => await _repository!.CreateAsync(entity));
    }

    [Test]
    public async Task UpdateAsyncTest_CalledWithValidEntity_UpdatesEntityInDB() {
        //Arrange
        const int testId = 1;
        const string testName = "test";
        var entityBeforeUpdate = _context?.Users?.Find(testId)
            ?? throw new ArgumentNullException("Entity not found");

        //Act
        entityBeforeUpdate.Name = testName;
        var result = await _repository!.UpdateAsync(entityBeforeUpdate);
        var entityAfterUpdate = _context?.Users?.Find(testId);

        //Assert
        Assert.Multiple(() => {
            Assert.That(entityAfterUpdate?.Id, Is.EqualTo(testId));
            Assert.That(entityAfterUpdate?.Name, Is.EqualTo(testName));
        });
    }
    
    [Test]
    public void UpdateAsyncTest_CalledWithNotExistingEntity_ThrowsException() {
        //Arrange
        var entity = new User() { Id = 4 };

        //Act & Assert
        Assert.ThrowsAsync<DbUpdateConcurrencyException>(async () => await _repository!.UpdateAsync(entity));
    }

    [Test]
    public async Task DeleteAsyncTest_CalledWithValidEntity_DeletesEntityFromDB() {
        //Arrange
        const int testId = 1;
        var entity = _context?.Users?.Find(testId)
            ?? throw new ArgumentNullException("Entity not found");

        //Act
        await _repository!.DeleteAsync(entity);
        var deletedEntity = _context?.Users?.Find(testId);

        //Assert
        Assert.IsNull(deletedEntity);
    }
    
    [Test]
    public void DeleteAsyncTest_CalledWithNotExistingEntity_ThrowsException() {
        //Arrange
        var entity = new User() { Id = 4 };

        //Act & Assert
        Assert.ThrowsAsync<DbUpdateConcurrencyException>(async () => await _repository!.DeleteAsync(entity));
    }

    [Test]
    public async Task GetAllAsyncTest_CalledWithValidRequest_ReturnsListWithAllDBEntities() {
        //Arrange
        const int expectedAmountofEntities = 3;

        //Act
        var result = await _repository!.GetAllAsync();

        //Assert
        Assert.That(result.Count(), Is.EqualTo(expectedAmountofEntities));
    }

    [Test]
    public async Task GetManyAsyncTest_CalledWithFilterParameter_ReturnsFilteredListWithEntities() {
        //Arrange
        const int expectedAmountofEntities = 2;

        //Act
        var result = await _repository!.GetManyAsync(x => x.Id >= 2);

        //Assert
        Assert.Multiple( () => {
            Assert.That(result.Count(), Is.EqualTo(expectedAmountofEntities));
            Assert.True(result.Any(x => x.Id == 2));
            Assert.True(result.Any(x => x.Id == 3));
        });
    }
    
    [Test]
    public async Task GetManyAsyncTest_CalledWithPaginationParameters_ReturnsPaginatedListWithEntities() {
        //Arrange
        const int expectedAmountofEntities = 2;

        //Act
        var result = await _repository!.GetManyAsync(skip: 1, take: 2);

        //Assert
        Assert.Multiple( () => {
            Assert.That(result.Count(), Is.EqualTo(expectedAmountofEntities));
            Assert.True(result.Any(x => x.Id == 2));
            Assert.True(result.Any(x => x.Id == 3));
        });
    }
    
    [Test]
    public async Task GetManyAsyncTest_CalledWithOrderingParameters_ReturnsOrderedListWithEntities() {
        //Arrange
        const int expectedAmountofEntities = 3;

        //Act
        var result = await _repository!.GetManyAsync(orderBy: x => x.OrderByDescending(y => y.Id));

        //Assert
        Assert.Multiple( () => { 
            Assert.That(result.Count(), Is.EqualTo(expectedAmountofEntities));
            Assert.True(result.FirstOrDefault()?.Id == 3);
        });
    }
    
    [Test]
    public async Task GetByIdAsyncTest_CalledWithExistingId_ReturnsCorrectEntity() {
        //Arrange
        const int id = 3;

        //Act
        var result = await _repository!.GetByIdAsync(id);

        //Assert
        Assert.That(result?.Id, Is.EqualTo(id));
    }
    
    [Test]
    public async Task GetByIdAsyncTest_CalledWithNonExistingId_ReturnsNull() {
        //Arrange
        const int id = 4;

        //Act
        var result = await _repository!.GetByIdAsync(id);

        //Assert
        Assert.IsNull(result);
    }
}
