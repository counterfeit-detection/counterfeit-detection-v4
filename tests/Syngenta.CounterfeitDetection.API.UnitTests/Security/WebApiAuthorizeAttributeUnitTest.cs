﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Routing;
using Moq;
using Syngenta.CounterfeitDetection.API.Security;

namespace Syngenta.CounterfeitDetection.API.UnitTests.Security;

[TestFixture]
internal sealed class WebApiAuthorizeAttributeUnitTest {

    private WebApiAuthorizeAttribute? _authorizeAttribute;

    [SetUp]
    public void SetUp() => _authorizeAttribute = new WebApiAuthorizeAttribute();

    [Test]
    public async Task When_TryAuthorize_Get_Unauthorized() {
        //Arrange
        var httpContextMock = new Mock<HttpContext>();
        httpContextMock.SetupGet(x => x.Request.Headers)
            .Returns(new HeaderDictionary(new Dictionary<string, Microsoft.Extensions.Primitives.StringValues>()));
        var actContext = new ActionContext(
                httpContextMock.Object,
                Mock.Of<RouteData>(),
                Mock.Of<ActionDescriptor>(),
                Mock.Of<ModelStateDictionary>()
            );
        var actExecutingContext = new AuthorizationFilterContext(
                actContext,
                new List<IFilterMetadata>()
            );

        //Act
        await _authorizeAttribute!.OnAuthorizationAsync(actExecutingContext);
        var result = actExecutingContext.Result;

        //Assert
        Assert.IsNotNull(result);
        Assert.That(result is UnauthorizedResult, Is.True);
    }

    [Test]
    public async Task When_TryAuthorize_Get_AuthorizationPassed() {
        //Arrange
        var httpContextMock = new Mock<HttpContext>();
        httpContextMock.SetupGet(x => x.Request.Headers)
            .Returns(new HeaderDictionary(new Dictionary<string, Microsoft.Extensions.Primitives.StringValues>() {
                {"AppName", "StandaloneApp" },
                {"Token", "123" },
            }));
        var actContext = new ActionContext(
                httpContextMock.Object,
                Mock.Of<RouteData>(),
                Mock.Of<ActionDescriptor>(),
                Mock.Of<ModelStateDictionary>()
            );
        var actExecutingContext = new AuthorizationFilterContext(
                actContext,
                new List<IFilterMetadata>()
            );

        //Act
        await _authorizeAttribute!.OnAuthorizationAsync(actExecutingContext);
        var result = actExecutingContext.Result;

        //Assert
        Assert.IsNull(result);
    }
}
