using AutoMapper; 
using Newtonsoft.Json;
using Moq;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using Syngenta.CounterfeitDetection.API.Controllers;
using Syngenta.CounterfeitDetection.API.Extensions;
using Syngenta.CounterfeitDetection.API.Models;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.Business.Core.Models;

using static Syngenta.CounterfeitDetection.API.UnitTests.Controllers.DummyEntries;

namespace Syngenta.CounterfeitDetection.API.UnitTests.Controllers;

[TestFixture]
internal sealed class ScansControllerUnitTests {
    #region Setup
    private IMapper _mapper = new Mapper(
        new MapperConfiguration(cfg => cfg.RegisterAPI2BusinessMappings()));
    private IScanProcessingService? _scanProcessingService;
    private IScanHistoryService? _scanHistoryService;
    private IReportService? _reportService;
    private ScansController? _controller; 

    [SetUp]
    public void Setup() {
        _scanProcessingService = Mock.Of<IScanProcessingService>(s =>
            s.CreateScans(
                It.IsAny<UserData>(),
                It.IsAny<ClientData>(),
                It.IsAny<IEnumerable<ScanData>>()
            ).Result == DummyCreateScanResult);
        _scanHistoryService = Mock.Of<IScanHistoryService>(s =>
            s.GetScanById(It.IsAny<int>()).Result == DummyScanTransactions[0] &&
            s.GetScans(It.IsAny<ScanHistoryFilter>()).Result == DummyScanHistoryPage);
        _reportService = Mock.Of<IReportService>();

        _controller = new ScansController(
            _scanProcessingService,
            _scanHistoryService,
            _reportService,
            _mapper,
            Mock.Of<ILogger<ScansController>>());
    }
    #endregion

    #region Tests
    #region SubmitScanBatch
    [Test]
    public async Task SubmitScansBatchTest_ReceivedValidRequest_CallsCreateScansOnService() {
        var serviceMock = Mock.Get(_scanProcessingService); 

        await _controller!.SubmitScansBatch(DummySubmitScanBatchRequest);

        serviceMock.Verify(s =>
            s!.CreateScans(
                It.IsAny<UserData>(),
                It.IsAny<ClientData>(),
                It.IsAny<IEnumerable<ScanData>>()));
    }

    [Test]
    public async Task SubmitScansBatchTest_ServiceReturnedData_Returns200OkWithData() {
        var expected = JsonConvert.SerializeObject(DummySubmitScanBatchResponse);
        
        var response = await _controller!.SubmitScansBatch(DummySubmitScanBatchRequest);

        var actual = JsonConvert.SerializeObject((response as OkObjectResult)?.Value);
        Assert.That(actual, Is.EqualTo(expected));
    }

    [Test]
    public void SubmitScansBatchTest_ServiceThrownException_Throws() {
        var serviceMock = Mock.Get(_scanProcessingService);
        serviceMock.Setup(s => 
            s!.CreateScans(
                It.IsAny<UserData>(),
                It.IsAny<ClientData>(),
                It.IsAny<IEnumerable<ScanData>>()
            )).Throws<Exception>();

        Assert.ThrowsAsync<Exception>(
            async () => await _controller!.SubmitScansBatch(DummySubmitScanBatchRequest)
        );
    }
    #endregion

    #region GetScanHistory
    [Test]
    public async Task GetScanHistoryTest_ReceivedValidFilter_CallsGetScansOnService() {
        var serviceMock = Mock.Get(_scanHistoryService); 

        await _controller!.GetScanHistory(DummyScanHistoryRequest);

        serviceMock.Verify(s => s!.GetScans(It.IsAny<ScanHistoryFilter>()));
    }

    [Test]
    public async Task GetScanHistoryTest_ServiceReturnedData_Returns200OkWithData() {
        var expected = JsonConvert.SerializeObject(DummyScanHistoryResponse);
        
        var res = await _controller!.GetScanHistory(DummyScanHistoryRequest); 
        
        var actual = JsonConvert.SerializeObject((res as OkObjectResult)?.Value);
        Assert.That(actual, Is.EqualTo(expected));
    }
    #endregion

    #region GetScanDetails
    [Test]
    public async Task GetScanDetailsTest_ReceivedValidId_CallsGetScanByIdOnService() {
        var serviceMock = Mock.Get(_scanHistoryService); 

        await _controller!.GetScanDetails(1);

        serviceMock.Verify(s => s!.GetScanById(1));
    }

    [Test]
    public async Task GetScanDetailsTest_ServiceReturnedData_Returns200OkWithData() {
        var expected = JsonConvert.SerializeObject(DummyScanStatusModels[0]);
        
        var res = await _controller!.GetScanDetails(1); 
        
        var actual = JsonConvert.SerializeObject((res as OkObjectResult)?.Value);
        Assert.That(actual, Is.EqualTo(expected));
    }

    [Test]
    public async Task GetScanDetailsTest_ServiceThrownKeyNotFoundException_Returns404NotFound() {
        var serviceMock = Mock.Get(_scanHistoryService); 
        serviceMock.Setup(s => s!.GetScanById(It.IsAny<int>()))
            .Throws<KeyNotFoundException>();

        var res = await _controller!.GetScanDetails(1);

        Assert.That(res is NotFoundResult);
    }

    [Test]
    public void GetScanDetailsTest_ServiceThrownOtherException_ThrowsException() {
        var serviceMock = Mock.Get(_scanHistoryService); 
        serviceMock.Setup(s => s!.GetScanById(It.IsAny<int>()))
            .Throws<Exception>();

        Assert.ThrowsAsync<Exception>(
            async () => await _controller!.GetScanDetails(1)
        );   
    }
    #endregion

    #region SubmitFollowUpAction
    [Test]
    public async Task SubmitFollowUpActionTest_ReceivesValidData_CallsSubmitFollowUpActionOnService() {
        var serviceMock = Mock.Get(_scanProcessingService); 

        await _controller!.SumbitFollowUpAction(1, 1, DummyFollowUpActionRequest);

        serviceMock.Verify(s => s!.SubmitFollowUpAction(It.IsAny<FollowUpActionData>()));
    }

    [Test]
    public async Task SubmitFollowUpActionTest_ServiceThrownKeyNotFoundException_Returns404NotFound() {
        var serviceMock = Mock.Get(_scanProcessingService); 
        serviceMock.Setup(s => 
            s!.SubmitFollowUpAction(It.IsAny<FollowUpActionData>()))
                .Throws<KeyNotFoundException>();

        var res = await _controller!.SumbitFollowUpAction(1, 1, DummyFollowUpActionRequest);

        Assert.That(res is NotFoundResult);
    }

    [Test]
    public async Task SubmitFollowUpActionTest_ServiceThrowsArgumentException_Returns400BadRequest() {
        var serviceMock = Mock.Get(_scanProcessingService); 
        serviceMock.Setup(s => 
            s!.SubmitFollowUpAction(It.IsAny<FollowUpActionData>()))
                .Throws<ArgumentException>();

        var res = await _controller!.SumbitFollowUpAction(1, 1, DummyFollowUpActionRequest);

        Assert.That(res is BadRequestResult);
    }

    [Test]
    public async Task SubmitFollowUpActionTest_ServiceDoesNotThrow_Returns200Ok() {
        var res = await _controller!.SumbitFollowUpAction(1, 1, DummyFollowUpActionRequest);

        Assert.That(res is OkResult);
    }
    #endregion
    
    #region SubmitScanReport
    [Test]
    public async Task SubmitScanReportTest_ReceivesValidData_CallsCreateScanReportOnService() {
        var serviceMock = Mock.Get(_reportService); 

        await _controller!.SubmitScanReport(1, DummyReportEntryModels[0]);

        serviceMock.Verify(s => s!.CreateScanReport(1, It.IsAny<ReportEntry>()));
    }

    [Test]
    public async Task SubmitScanReportTest_ServiceThrowsKeyNotFoundException_Returns404NotFound() {
        var serviceMock = Mock.Get(_reportService); 
        serviceMock.Setup(s => s!.CreateScanReport(It.IsAny<int>(), It.IsAny<ReportEntry>()))
            .Throws<KeyNotFoundException>();

        var res = await _controller!.SubmitScanReport(1, DummyReportEntryModels[0]);

        Assert.That(res is NotFoundResult);
    }

    [Test]
    public async Task SubmitScanReportTest_ServiceThrowsArgumentException_Returns400BadRequest() {
        var serviceMock = Mock.Get(_reportService); 
        serviceMock.Setup(s => s!.CreateScanReport(It.IsAny<int>(), It.IsAny<ReportEntry>()))
            .Throws<ArgumentException>();

        var res = await _controller!.SubmitScanReport(1, DummyReportEntryModels[0]);

        Assert.That(res is BadRequestResult);
    }

    [Test]
    public async Task SubmitScanReportTest_ServiceDoesNotThrow_Returns200Ok() {
        var res = await _controller!.SubmitScanReport(1, DummyReportEntryModels[0]);

        Assert.That(res is OkResult);
    }
    #endregion

    #region MarkScanReviewed
    [Test]
    public async Task MarkScanReviewedTest_ReceivesValidData_CallsCreateScanReportOnService() {
        var serviceMock = Mock.Get(_scanProcessingService);

        await _controller!.MarkScanReviewed(1);

        serviceMock.Verify(s => s!.MarkScanReviewed(1));
    }

    [Test]
    public async Task MarkScanReviewedTest_ServiceThrowsKeyNotFoundException_Returns404NotFound() {
        var serviceMock = Mock.Get(_scanProcessingService);
        serviceMock.Setup(s => s!.MarkScanReviewed(It.IsAny<int>()))
            .Throws<KeyNotFoundException>();

        var res = await _controller!.MarkScanReviewed(1);

        Assert.That(res is NotFoundObjectResult);
    }

    [Test]
    public async Task MarkScanReviewedTest_ServiceThrowsArgumentException_Returns400BadRequest() {
        var serviceMock = Mock.Get(_scanProcessingService);
        serviceMock.Setup(s => s!.MarkScanReviewed(It.IsAny<int>()))
            .Throws<ArgumentException>();

        var res = await _controller!.MarkScanReviewed(1);

        Assert.That(res is BadRequestObjectResult);
    }

    [Test]
    public async Task MarkScanReviewedTest_ServiceThrowsInvalidOperationException_Returns400BadRequest() {
        var serviceMock = Mock.Get(_scanProcessingService);
        serviceMock.Setup(s => s!.MarkScanReviewed(It.IsAny<int>()))
            .Throws<InvalidOperationException>();

        var res = await _controller!.MarkScanReviewed(1);

        Assert.That(res is BadRequestObjectResult);
    }

    [Test]
    public async Task MarkScanReviewedTest_ServiceDoesNotThrow_Returns200Ok() {
        var res = await _controller!.MarkScanReviewed(1);

        Assert.That(res is OkResult);
    }
    #endregion
    #endregion



    #region Dummy data
    private static SubmitScanBatchRequest DummySubmitScanBatchRequest => new SubmitScanBatchRequest {
        UserData = DummyUserViewModel,
        ClientData = DummyClientViewModel,
        Scans = new List<SubmittedScanViewModel> {
            new SubmittedScanViewModel {
                ScanData = DummyScanEntryModels[0],
                ReportEntry = DummyReportEntryModels[0],
            },
            new SubmittedScanViewModel {
                ScanData = DummyScanEntryModels[1],
                ReportEntry = DummyReportEntryModels[1],
            }
        }
    };

    private CreateScanResult DummyCreateScanResult => new CreateScanResult {
        Submitted = new List<ScanTransaction> {
            DummyScanTransactions[0]
        },
        Failed = new List<ScanData> {
            new ScanData() {
                ScanEntry = DummyScanEntries[1],
                ReportData = DummyReportEntries[1]
            }
        }
    };

    private SubmitScanBatchResponse DummySubmitScanBatchResponse => new SubmitScanBatchResponse {
        Submitted = new List<ScanStatusViewModel> {
            DummyScanStatusModels[0]
        },
        Failed = new List<SubmittedScanViewModel> {
            new SubmittedScanViewModel() {
                ScanData = DummyScanEntryModels[1],
                ReportEntry = DummyReportEntryModels[1]
            }
        }
    };

    private GetScanHistoryRequest DummyScanHistoryRequest => new GetScanHistoryRequest {
        UserLogin = "dummy@example.org",
        UserRoleCode = "TR",
        LanguageCode = "TL",
        Skip = 0,
        Take = 5
    };

    private ScanHistoryPage DummyScanHistoryPage => new ScanHistoryPage {
        TotalScans = 2,
        Scans = DummyScanTransactions
    };

    private GetScanHistoryResponse DummyScanHistoryResponse => new GetScanHistoryResponse {
        TotalScans = 2,
        PageItems = DummyScanStatusModels
    };

    private SubmitFollowUpActionRequest DummyFollowUpActionRequest => 
        new SubmitFollowUpActionRequest { OptionId = 1 };
    #endregion
}