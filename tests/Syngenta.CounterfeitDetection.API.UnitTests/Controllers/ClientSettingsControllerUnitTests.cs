﻿using AutoMapper;
using Newtonsoft.Json;
using Moq;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using Syngenta.CounterfeitDetection.API.Controllers;
using Syngenta.CounterfeitDetection.API.Extensions;
using Syngenta.CounterfeitDetection.API.Models;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.Business.Core.Models;

using Infra = Syngenta.CounterfeitDetection.Infrastructure.Models;

namespace Syngenta.CounterfeitDetection.API.UnitTests.Controllers;

[TestFixture]
internal sealed class ClientSettingsControllerUnitTests {
    #region SetUp
    private IMapper _mapper = new Mapper(
        new MapperConfiguration(cfg => cfg.RegisterAPI2BusinessMappings()));
    private IClientSettingService? _clientSettingService;
    private ClientSettingsController? _controller;

    [SetUp]
    public void Setup() {
        _clientSettingService = Mock.Of<IClientSettingService>(s =>
            s.GetClientSettings(AppName, CountryCode, RoleCode).Result == _dummyClientSettings);

        _controller = new ClientSettingsController(
            Mock.Of<ILogger<ClientSettingsController>>(),
            _clientSettingService,
            _mapper);
    }
    #endregion

    #region Tests
    [Test]
    public async Task GetClientSettingsTest_ServiceReturnedData_Returns200OkWithData() {
        //Arrange
        var expected = JsonConvert.SerializeObject(_expectedDummyClientSettings);

        //Act
        var res = await _controller!.GetClientSettings(AppName, CountryCode, RoleCode);

        var actual = JsonConvert.SerializeObject((res as OkObjectResult)?.Value);

        //Assert
        Assert.That(actual, Is.EqualTo(expected));

    }
    #endregion

    #region DummyData
    private const string AppName = "StandaloneApp";

    private const string CountryCode = "IN";

    private const string RoleCode = "TR";

    private readonly ClientSettings _dummyClientSettings = new ClientSettings() {
        Themes = new ThemeSettings {
            Primary = new Infra.Theme {
                Main = "#009900",
                Dark = "#003300",
                Light = "#00CC00",
                Contrast = "#CC00CC",
            },
            Secondary = new Infra.Theme {
                Main = "#990000",
                Dark = "#330000",
                Light = "#CC0000",
                Contrast = "#00CCCC",
            },
            Tertiary = new Infra.Theme {
                Main = "#000099",
                Dark = "#000033",
                Light = "#0000CC",
                Contrast = "#CCCC00",
            }
        },
        Languages = new List<Business.Core.Models.Languages> {
            new Business.Core.Models.Languages {
                Id = 1,
                Name = "English",
                Code = "EN"
            }
        },
        ResultTypes = new List<ScanResult> {
            new ScanResult {
                TypeId = 1,
                Title = "Valid: low risk",
                Message = "Congrats! Your scan is valid",
                Style = new ScanResultStyle {
                    TitleColor = "#00CC00",
                    MessageColor = "#000000"
                }
            }
        },
        ReportReasons = new List<ReportReason> {
            new ReportReason {
                Id = 1,
                Name = "Just testing",
                RequireCustomReason = false,
                Order = 100
            }
        },
        ScanOrigins = new List<ScanOrigin> {
            new ScanOrigin {
                Id = 1,
                Name = "Farm",
                Order = 100,
            }
        },
        Features = new List<ClientFeatures> {
            new ClientFeatures {
                Id = 1,
                Name = "Reported Scans"
            }
        }
    };

    private readonly ClientSettingsViewModel _expectedDummyClientSettings = new ClientSettingsViewModel() {
        Themes = new ThemeViewModel {
            Primary = new Infra.Theme {
                Main = "#009900",
                Dark = "#003300",
                Light = "#00CC00",
                Contrast = "#CC00CC",
            },
            Secondary = new Infra.Theme {
                Main = "#990000",
                Dark = "#330000",
                Light = "#CC0000",
                Contrast = "#00CCCC",
            },
            Tertiary = new Infra.Theme {
                Main = "#000099",
                Dark = "#000033",
                Light = "#0000CC",
                Contrast = "#CCCC00",
            }
        },
        Languages = new List<LanguageViewModel> {
            new LanguageViewModel {
                Id = 1,
                Name = "English",
                Code = "EN"
            }
        },
        ResultTypes = new List<ScanResultViewModel> {
            new ScanResultViewModel {
                TypeId = 1,
                Title = "Valid: low risk",
                Message = "Congrats! Your scan is valid",
                Style = new ScanResultStyleViewModel {
                    TitleColor = "#00CC00",
                    MessageColor = "#000000"
                }
            }
        },
        ReportReasons = new List<ReportReasonViewModel> {
            new ReportReasonViewModel {
                Id = 1,
                Name = "Just testing",
                RequireCustomReason = false,
                Order = 100
            }
        },
        ScanOrigins = new List<ScanOriginViewModel> {
            new ScanOriginViewModel {
                Id = 1,
                Name = "Farm",
                Order = 100,
            }
        },
        Features = new List<ClientFeatureViewModel> {
            new ClientFeatureViewModel {
                Id = 1,
                Name = "Reported Scans"
            }
        }
    };
    #endregion
}
