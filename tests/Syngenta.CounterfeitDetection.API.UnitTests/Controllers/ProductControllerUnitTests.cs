﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using Syngenta.CounterfeitDetection.API.Controllers;
using Syngenta.CounterfeitDetection.API.Extensions;
using Syngenta.CounterfeitDetection.API.Models;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.Business.Core.Models;

namespace Syngenta.CounterfeitDetection.API.UnitTests.Controllers;

[TestFixture]
internal class ProductControllerUnitTests {
    private readonly IMapper _mapper = new Mapper(
        new MapperConfiguration(cfg => cfg.RegisterAPI2BusinessMappings()));
    private IProductService? _productService;
    private ProductsController? _controller;


    private Mock<IProductService>? _productServiceMock;

    [SetUp]
    public void SetUp() {
        _productService = Mock.Of<IProductService>();

        _controller = new ProductsController(Mock.Of<ILogger<ProductsController>>(),
            _productService, _mapper);

        _productServiceMock = Mock.Get(_productService);

        _productServiceMock.Setup(x => x.GetProductCatalogue(It.IsAny<string?>()))
            .ReturnsAsync(DummyProductData);

        _productServiceMock.Setup(x => x.GetProductJourney(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(DummyProductJourney);
        _productServiceMock.Setup(x => x.GetProductDetails(It.IsAny<string?>()))
            .Returns<string>(gtin => Task.FromResult(DummyProductData.AsQueryable().First(x => x.GTIN.Equals(gtin))));
    }

    [Test]
    public async Task GetProductCatalogueTest_WithoutCountryCode_Returns200WithCorrectValues() {
        var expectedModel = JsonConvert.SerializeObject(DummyExpectedProductViewModel);

        var result = await _controller!.GetProductCatalogue() as ObjectResult;

        Assert.Multiple(() => {
            Assert.That(result, Is.Not.Null);
            Assert.That(result!.StatusCode, Is.EqualTo(200));

            var model = JsonConvert.SerializeObject(result!.Value);
            Assert.That(model, Is.EqualTo(expectedModel));
        });
    }

    [Test]
    public async Task GetProductCatalogueTest_WithCountryCode_Returns200WithCorrectValues() {
        var expectedModel = JsonConvert.SerializeObject(DummyExpectedProductViewModel);
        const string countryCode = "TR";

        var result = await _controller!.GetProductCatalogue(countryCode) as ObjectResult;

        Assert.Multiple(() => {
            Assert.That(result, Is.Not.Null);
            Assert.That(result!.StatusCode, Is.EqualTo(200));

            var model = JsonConvert.SerializeObject(result!.Value);
            Assert.That(model, Is.EqualTo(expectedModel));
        });
    }
    
    [Test]
    public async Task GetProductJourneyTest_WithGTINAndSerialNumber_Returns200WithCorrectValues() {
        var expectedModel = JsonConvert.SerializeObject(DummyExpectedProductJourney);
        const string gtin = "12345678";
        const string serialNumber = "SN1234";

        var result = await _controller!.GetProductJourney(gtin, serialNumber) as ObjectResult;

        Assert.Multiple(() => {
            Assert.That(result, Is.Not.Null);
            Assert.That(result!.StatusCode, Is.EqualTo(200));

            var model = JsonConvert.SerializeObject(result!.Value);
            Assert.That(model, Is.EqualTo(expectedModel));
        });
    }

    private static IEnumerable<ProductJourneyStep> DummyProductJourney => new List<ProductJourneyStep>{
        new ProductJourneyStep(){
            ShippingId = "1",
            GTIN = "12345678",
            SN = "SN1234",
            UserName = "test user",
            SalesRep = "test user",
            Location = new Infrastructure.Models.Geolocation(12.34f, 56.78f),
            AuthenticationEventTime = DateTime.MinValue,
            ShippingEventTime = DateTime.MinValue
        },
        new ProductJourneyStep(){
            ShippingId = "2",
            GTIN = "12345678",
            SN = "SN1234",
            UserName = "test user",
            SalesRep = "test user",
            Location = new Infrastructure.Models.Geolocation(34.12f, 78.56f),
            AuthenticationEventTime = DateTime.MinValue,
            ShippingEventTime = DateTime.MinValue
        },
    };
    
    private static IEnumerable<ProductJourneyStepViewModel> DummyExpectedProductJourney => new List<ProductJourneyStepViewModel>{
        new ProductJourneyStepViewModel(){
            ShippingId = "1",
            GTIN = "12345678",
            SN = "SN1234",
            UserName = "test user",
            SalesRep = "test user",
            Location = new Infrastructure.Models.Geolocation(12.34f, 56.78f),
            AuthenticationEventTime = DateTime.MinValue,
            ShippingEventTime = DateTime.MinValue
        },
        new ProductJourneyStepViewModel(){
            ShippingId = "2",
            GTIN = "12345678",
            SN = "SN1234",
            UserName = "test user",
            SalesRep = "test user",
            Location = new Infrastructure.Models.Geolocation(34.12f, 78.56f),
            AuthenticationEventTime = DateTime.MinValue,
            ShippingEventTime = DateTime.MinValue
        },
    };

    
    [Test]
    public async Task GetProductDetailsTest_ValidGTIN_Returns200WithCorrectValues() {
        const string gtin = "12345678901234";
        var expectedModel = JsonConvert.SerializeObject(DummyExpectedProductViewModel
            .FirstOrDefault(x => x.GTIN.Equals(gtin)));

        var result = await _controller!.GetProductDetails(gtin) as ObjectResult;

        Assert.Multiple(() => {
            Assert.That(result, Is.Not.Null);
            Assert.That(result!.StatusCode, Is.EqualTo(200));

            var model = JsonConvert.SerializeObject(result!.Value);
            Assert.That(model, Is.EqualTo(expectedModel));
        });
    }

    private static IEnumerable<ProductData> DummyProductData => new List<ProductData>{
        new ProductData(){
            Id = 1,
            Name = "test name",
            GTIN = "12345678901234",
            IsUnderGPD = true,
            Capacity = "100 ml"
        },
        new ProductData(){
            Id = 2,
            Name = "another test name",
            GTIN = "12345678905678",
            IsUnderGPD = false,
            Capacity = "10 ml"
        },
    }.AsEnumerable();


    private static IEnumerable<ProductViewModel> DummyExpectedProductViewModel => new List<ProductViewModel>{
        new ProductViewModel(){
            Id = 1,
            Name = "test name",
            GTIN = "12345678901234",
            IsUnderGPD = true,
            Capacity = "100 ml"
        },
        new ProductViewModel(){
            Id = 2,
            Name = "another test name",
            GTIN = "12345678905678",
            IsUnderGPD = false,
            Capacity = "10 ml"
        },
    }.AsEnumerable();
}
