using AutoMapper; 
using Newtonsoft.Json;
using Moq;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using Syngenta.CounterfeitDetection.API.Controllers;
using Syngenta.CounterfeitDetection.API.Extensions;
using Syngenta.CounterfeitDetection.API.Models;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.Business.Core.Models;

using static Syngenta.CounterfeitDetection.API.UnitTests.Controllers.DummyEntries;

namespace Syngenta.CounterfeitDetection.API.UnitTests.Controllers;

[TestFixture]
internal sealed class ReportsControllerUnitTests {
    #region Setup
    private IMapper _mapper = new Mapper(
        new MapperConfiguration(cfg => cfg.RegisterAPI2BusinessMappings()));
    private IReportService? _reportService;
    private ReportsController? _controller; 

     [SetUp]
    public void Setup() {
        _reportService = Mock.Of<IReportService>(s => 
            s.CreateDirectReports(
                It.IsAny<UserData>(),
                It.IsAny<ClientData>(),
                It.IsAny<IEnumerable<ReportData>>()
            ).Result == DummyDirectReportResult);

        _controller = new ReportsController(
            _reportService,
            _mapper,
            Mock.Of<ILogger<ReportsController>>());
    }
    #endregion

    #region Tests
    [Test]
    public async Task SubmitReportsBatchTest_ReceivedRequest_CallsCreateDirectReportsOnService() {
        var serviceMock = Mock.Get(_reportService);

        await _controller!.SubmitReportsBatch(DummyDirectReportRequest);

        serviceMock.Verify(s => 
            s!.CreateDirectReports(
                It.IsAny<UserData>(),
                It.IsAny<ClientData>(),
                It.IsAny<IEnumerable<ReportData>>()));
    }

    [Test]
    public async Task SubmitReportsBatchTest_ServiceReturnedData_Returns200OkWithData() {
        var expected = JsonConvert.SerializeObject(DummyDirectReportResponse);
       
        var res = await _controller!.SubmitReportsBatch(DummyDirectReportRequest);
        
        var actual = JsonConvert.SerializeObject((res as OkObjectResult)?.Value);
        Assert.That(actual, Is.EqualTo(expected));
    }

    [Test]
    public void SubmitReportsBatchTest_ServiceThrownException_Throws() {
        var serviceMock = Mock.Get(_reportService);
        serviceMock.Setup(s => s!.CreateDirectReports(
            It.IsAny<UserData>(),
            It.IsAny<ClientData>(),
            It.IsAny<IEnumerable<ReportData>>()
        )).Throws<Exception>();

        Assert.ThrowsAsync<Exception>(
            async () => await _controller!.SubmitReportsBatch(DummyDirectReportRequest)
        );
    }
    #endregion

    #region Dummy data
    private static SubmitReportBatchRequest DummyDirectReportRequest => new SubmitReportBatchRequest {
        UserData = DummyUserViewModel,
        ClientData = DummyClientViewModel,
        Reports = new List<SubmittedScanViewModel> {
            new SubmittedScanViewModel {
                ScanData = DummyScanEntryModels[2],
                ReportEntry = DummyReportEntryModels[0]
            },
            new SubmittedScanViewModel {
                ScanData = DummyScanEntryModels[2],
                ReportEntry = DummyReportEntryModels[1]
            }
        }
    };

    private static CreateDirectReportResult DummyDirectReportResult => new CreateDirectReportResult {
        Submitted = new List<CreatedReportData> {
            new CreatedReportData {
                Id = 1,
                ScanEntry = DummyScanEntries[2],
                ReportEntry = DummyReportEntries[0]
            }
        },
        Failed = new List<ReportData> {
            new ReportData {
                ScanEntry = DummyScanEntries[2],
                ReportEntry = DummyReportEntries[1]
            }
        }
    };

    private static SubmitReportBatchResponse DummyDirectReportResponse => new SubmitReportBatchResponse {
        Submitted = new List<SubmittedReportViewModel> {
            new SubmittedReportViewModel {
                Id = 1,
                ScanData = DummyScanEntryModels[2],
                ReportEntry = DummyReportEntryModels[0]
            }
        },
        Failed = new List<SubmittedScanViewModel> {
            new SubmittedScanViewModel {
                ScanData = DummyScanEntryModels[2],
                ReportEntry = DummyReportEntryModels[1]
            }
        }
    };
    #endregion
}