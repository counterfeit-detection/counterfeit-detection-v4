using Syngenta.CounterfeitDetection.API.Models;
using Syngenta.CounterfeitDetection.Business.Core.Models;
using Syngenta.CounterfeitDetection.Infrastructure.Models;

namespace Syngenta.CounterfeitDetection.API.UnitTests.Controllers;

internal static class DummyEntries {
    public static UserViewModel DummyUserViewModel => new UserViewModel {
        Login = "dummy@example.org",
        Name = "Test Dummy",
        RoleCode = "TR",
        CountryCode = "TC"
    };

    public static ClientViewModel DummyClientViewModel => new ClientViewModel {
        LanguageCode = "TL",
        HostAppName = "Hostify",
        PluginVersion = "4.20",
        HostAppDetails = new HostAppDetailsViewModel {
            Device = "myPhone S3 X",
            OS = "myOS",
            Version = "6.9"
        }
    };

    public static ScanEntry[] DummyScanEntries => new List<ScanEntry> {
        new ScanEntry() {
            Fingerprint = "12345000",
            Location = new Geolocation(28.1f, 45.3f),
            Time = new DateTime(2022, 12, 31, 7, 0, 0),
            Code = "010123456789012321SN12345678",
            CodeType = "DATA_MATRIX",
            IsManualEntry = false,
            IsOffline = false
        },
        new ScanEntry() {
            Fingerprint = "12345001",
            Location = new Geolocation(28.5f, 45.7f),
            Time = new DateTime(2022, 12, 31, 7, 0, 1),
            Code = "010123456789012321SN12345678",
            CodeType = "DATA_MATRIX",
            IsManualEntry = false,
            IsOffline = false
        },
        new ScanEntry() {
            Fingerprint = "12345002",
            Location = new Geolocation(28.7f, 45.9f),
            Time = new DateTime(2022, 12, 31, 7, 5, 2),
            Code = "010123456789012321SN12345678",
            IsManualEntry = true,
            IsOffline = false
        },
    }.ToArray();

    public static ScanEntryViewModel[] DummyScanEntryModels => new List<ScanEntryViewModel> {
        new ScanEntryViewModel() {
            Fingerprint = "12345000",
            Location = new Geolocation(28.1f, 45.3f),
            Time = new DateTime(2022, 12, 31, 7, 0, 0),
            Code = "010123456789012321SN12345678",
            CodeType = "DATA_MATRIX",
            IsManualEntry = false,
            IsOffline = false
        },
        new ScanEntryViewModel() {
            Fingerprint = "12345001",
            Location = new Geolocation(28.5f, 45.7f),
            Time = new DateTime(2022, 12, 31, 7, 0, 1),
            Code = "010123456789012321SN12345678",
            CodeType = "DATA_MATRIX",
            IsManualEntry = false,
            IsOffline = false
        },
        new ScanEntryViewModel() {
            Fingerprint = "12345002",
            Location = new Geolocation(28.7f, 45.9f),
            Time = new DateTime(2022, 12, 31, 7, 5, 2),
            Code = "010123456789012321SN12345678",
            IsManualEntry = true,
            IsOffline = false
        },
    }.ToArray();

    public static ReportEntry[] DummyReportEntries => new List<ReportEntry> {
        new ReportEntry() {
            ScanningPoint = "Retailer",
            ScanLocationName = "The Store, Inc.",
            ScanAddress = "Anywhere Town, 1",
            ReportReasonId = 1,
            CustomReportReason = "wtf",
            Comments = "no",
            Images = new List<string> { "123" }
        },
        new ReportEntry() {
            ScanningPoint = "Distributor",
            ScanLocationName = "The Warehouse, Inc.",
            ScanAddress = "Anywhere Town, 3",
            ReportReasonId = 2,
            CustomReportReason = "ftw",
            Comments = "yas",
            Images = new List<string> { "123" }
        },
    }.ToArray();

    public static ReportEntryViewModel[] DummyReportEntryModels => new List<ReportEntryViewModel> {
        new ReportEntryViewModel() {
            ScanningPoint = "Retailer",
            ScanLocationName = "The Store, Inc.",
            ScanAddress = "Anywhere Town, 1",
            ReportReasonId = 1,
            CustomReportReason = "wtf",
            Comments = "no",
            Images = new List<string> { "123" }
        },
        new ReportEntryViewModel() {
            ScanningPoint = "Distributor",
            ScanLocationName = "The Warehouse, Inc.",
            ScanAddress = "Anywhere Town, 3",
            ReportReasonId = 2,
            CustomReportReason = "ftw",
            Comments = "yas",
            Images = new List<string> { "123" }
        },
    }.ToArray();

    public static ScanTransaction[] DummyScanTransactions => new List<ScanTransaction> {
        new ScanTransaction() {
            Id = 1,
            Status = "Pending",
            CreationTime = new DateTime(2022, 12, 31, 12, 30, 0),
            LastUpdateTime = new DateTime(2022, 12, 31, 12, 30, 0),
            ScanData = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("Time", "2022-12-31T07:00:00.000Z"),
                new KeyValuePair<string, string>("GTIN", "01234567890123"),
                new KeyValuePair<string, string>("SN", "SN12345678"),
            },
            ReportData = DummyReportEntries[0],
        },
        new ScanTransaction() {
            Id = 2,
            Status = "Pending",
            CreationTime = new DateTime(2022, 12, 31, 12, 30, 1),
            LastUpdateTime = new DateTime(2022, 12, 31, 12, 30, 1),
            ScanData = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("Time", "2022-12-31T07:00:01.000Z"),
                new KeyValuePair<string, string>("GTIN", "01234567890123"),
                new KeyValuePair<string, string>("SN", "SN12345678"),
            },
            ReportData = DummyReportEntries[1],
        },
        new ScanTransaction() {
            Id = 3,
            Status = "Completed",
            CreationTime = new DateTime(2022, 12, 31, 12, 35, 2),
            LastUpdateTime = new DateTime(2022, 12, 31, 12, 35, 2),
            ScanData = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("Time", "2022-12-31T07:05:02.000Z"),
                new KeyValuePair<string, string>("GTIN", "01234567890123"),
                new KeyValuePair<string, string>("SN", "SN12345678"),
            },
            ReportData = DummyReportEntries[0]
        }
    }.ToArray();
    
    public static ScanStatusViewModel[] DummyScanStatusModels => new List<ScanStatusViewModel> {
        new ScanStatusViewModel() {
            Id = 1,
            Status = "Pending",
            Data = new List<CustomFieldViewModel> {
                new CustomFieldViewModel { Title = "Time", Value = "2022-12-31T07:00:00.000Z" },
                new CustomFieldViewModel { Title = "GTIN", Value = "01234567890123" },
                new CustomFieldViewModel { Title = "SN", Value = "SN12345678" },
            },
            ReportEntry = DummyReportEntryModels[0]
        },
        new ScanStatusViewModel() {
            Id = 2,
            Status = "Pending",
            Data = new List<CustomFieldViewModel> {
                new CustomFieldViewModel { Title = "Time", Value = "2022-12-31T07:00:01.000Z" },
                new CustomFieldViewModel { Title = "GTIN", Value = "01234567890123" },
                new CustomFieldViewModel { Title = "SN", Value = "SN12345678" },
            },
            ReportEntry = DummyReportEntryModels[1]
        },
        new ScanStatusViewModel() {
            Id = 3,
            Status = "Completed",
            Data = new List<CustomFieldViewModel> {
                new CustomFieldViewModel { Title = "Time", Value = "2022-12-31T07:05:02.000Z" },
                new CustomFieldViewModel { Title = "GTIN", Value = "01234567890123" },
                new CustomFieldViewModel { Title = "SN", Value = "SN12345678" },
            },
            ReportEntry = DummyReportEntryModels[0]
        }
    }.ToArray();
}