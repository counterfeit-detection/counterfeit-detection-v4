namespace Syngenta.CounterfeitDetection.API.Models;

public class SubmitReportBatchRequest {
    public UserViewModel UserData { get; set; } = new UserViewModel();
    public ClientViewModel ClientData { get; set; } = new ClientViewModel();
    public IEnumerable<SubmittedScanViewModel> Reports { get; set; } = new List<SubmittedScanViewModel>();
}