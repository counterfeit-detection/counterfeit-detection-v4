using System.ComponentModel.DataAnnotations;

namespace Syngenta.CounterfeitDetection.API.Models;

public class GetScanHistoryRequest {
    [MinLength(3)]
    public string UserLogin { get; set; } = string.Empty;

    [MinLength(2)]
    public string UserRoleCode { get; set; } = string.Empty;

    [MinLength(2)]
    [MaxLength(2)]
    public string LanguageCode { get; set; } = string.Empty;

    public DateTime? After { get; set; }

    public DateTime? Before { get; set; }
    
    [Range(1, int.MaxValue)]
    public IEnumerable<int>? ProductId { get; set; }

    public string? ResultType { get; set; }
    
    public bool? Reported { get; set; }
    
    public bool? Reviewed { get; set; }
    
    public bool? Offline { get; set; }
    
    [Range(0, int.MaxValue)]
    public int Skip { get; set; } = 0;
    
    [Range(1, int.MaxValue)]
    public int Take { get; set; } = 10;
}