namespace Syngenta.CounterfeitDetection.API.Models;

public class SubmitScanBatchRequest {
    public UserViewModel UserData { get; set; } = new UserViewModel();
    public ClientViewModel ClientData { get; set; } = new ClientViewModel();
    public IEnumerable<SubmittedScanViewModel> Scans { get; set; } = new List<SubmittedScanViewModel>();
}