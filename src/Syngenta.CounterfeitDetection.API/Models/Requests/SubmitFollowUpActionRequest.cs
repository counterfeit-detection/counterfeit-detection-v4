using System.ComponentModel.DataAnnotations;

namespace Syngenta.CounterfeitDetection.API.Models;

public class SubmitFollowUpActionRequest {
    [Range(1, int.MaxValue)]
    public int OptionId { get; set; }
    
    public IEnumerable<CustomFieldViewModel>? Data { get; set; }
}