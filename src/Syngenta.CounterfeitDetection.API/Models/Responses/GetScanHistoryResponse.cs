namespace Syngenta.CounterfeitDetection.API.Models;

public class GetScanHistoryResponse {
    public int TotalScans { get; set; }
    public IEnumerable<ScanStatusViewModel>? PageItems { get; set; }
}