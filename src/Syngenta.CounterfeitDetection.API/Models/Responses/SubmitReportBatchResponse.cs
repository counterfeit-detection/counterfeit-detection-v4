namespace Syngenta.CounterfeitDetection.API.Models;

public class SubmitReportBatchResponse {
    public IEnumerable<SubmittedReportViewModel>? Submitted { get; set; }
    public IEnumerable<SubmittedScanViewModel>? Failed { get; set; }
}