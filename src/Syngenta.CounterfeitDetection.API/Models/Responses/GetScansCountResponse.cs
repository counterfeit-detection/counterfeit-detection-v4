﻿namespace Syngenta.CounterfeitDetection.API.Models;

public class GetScansCountResponse {
    public int TotalCount { get; set; }
}
