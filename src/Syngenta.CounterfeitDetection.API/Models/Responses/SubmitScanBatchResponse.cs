namespace Syngenta.CounterfeitDetection.API.Models;

public class SubmitScanBatchResponse {
    public IEnumerable<ScanStatusViewModel>? Submitted { get; set; }
    public IEnumerable<SubmittedScanViewModel>? Failed { get; set; }
}