namespace Syngenta.CounterfeitDetection.API.Models;

public class FollowUpEntryViewModel {
    public int Id { get; set; }
    public string Title { get; set; } = string.Empty;
    public IEnumerable<FollowUpOptionViewModel>? Options { get; set; }
    public IEnumerable<FollowUpInputViewModel>? Inputs { get; set; }
}