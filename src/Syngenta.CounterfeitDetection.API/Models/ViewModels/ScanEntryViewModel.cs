using System.ComponentModel.DataAnnotations;
using Syngenta.CounterfeitDetection.Infrastructure.Models;

namespace Syngenta.CounterfeitDetection.API.Models;

public class ScanEntryViewModel {
    [RegularExpression("^[A-Za-z0-9][A-Za-z0-9_]{0,8}[A-Za-z0-9]_[0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{12}$")]
    public string Fingerprint { get; set; } = string.Empty;
    
    public Geolocation Location { get; set; }
    
    public DateTime Time { get; set; }
    
    public string Code { get; set; } = string.Empty;

    public string CodeType { get; set; } = string.Empty;
    
    public bool IsManualEntry { get; set; }
    
    public bool IsOffline { get; set; }
}