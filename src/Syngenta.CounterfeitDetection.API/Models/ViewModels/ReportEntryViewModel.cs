using System.ComponentModel.DataAnnotations;

namespace Syngenta.CounterfeitDetection.API.Models;

public class ReportEntryViewModel {

    [MinLength(3)]
    public string ScanningPoint { get; set; } = string.Empty;
    
    [MinLength(3)]
    public string ScanLocationName { get; set; } = string.Empty;
    
    [MinLength(3)]
    public string ScanAddress { get; set; } = string.Empty;
    
    [Range(1, int.MaxValue)]
    public int ReportReasonId { get; set; }
    
    public string? CustomReportReason { get; set; }
    
    [MinLength(3)]
    public string Comments { get; set; } = string.Empty;
    
    [MinLength(1)]
    [MaxLength(3)]
    public IEnumerable<string> Images { get; set; } = new List<string>();
}