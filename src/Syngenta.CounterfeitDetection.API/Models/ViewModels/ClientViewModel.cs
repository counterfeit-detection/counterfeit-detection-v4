using System.ComponentModel.DataAnnotations;

namespace Syngenta.CounterfeitDetection.API.Models;

public class ClientViewModel {
    [StringLength(2, MinimumLength = 2)]
    public string LanguageCode { get; set; } = string.Empty;

    [MinLength(2)]
    public string HostAppName { get; set; } = string.Empty;

    [MinLength(1)]
    public string PluginVersion { get; set; } = string.Empty;

    public HostAppDetailsViewModel? HostAppDetails { get; set; }
}