namespace Syngenta.CounterfeitDetection.API.Models;

public class ScanResultViewModel {
    public int? TypeId { get; set; }
    public string Title { get; set; } = string.Empty;
    public string? Message { get; set; }
    public ScanResultStyleViewModel? Style { get; set; }
}