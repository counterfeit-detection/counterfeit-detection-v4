namespace Syngenta.CounterfeitDetection.API.Models;

public class ScanOriginViewModel {
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty; 
    public int Order { get; set; }
}