namespace Syngenta.CounterfeitDetection.API.Models;

public class ClientFeatureViewModel {
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
}