namespace Syngenta.CounterfeitDetection.API.Models;

public class SubmittedReportViewModel {
    public int Id { get; set; }
    public ScanEntryViewModel ScanData { get; set; } = new ScanEntryViewModel();
    public ReportEntryViewModel ReportEntry { get; set; } = new ReportEntryViewModel();
}