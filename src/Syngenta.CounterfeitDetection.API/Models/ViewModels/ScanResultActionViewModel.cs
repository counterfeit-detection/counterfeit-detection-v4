namespace Syngenta.CounterfeitDetection.API.Models;

public class ScanResultActionViewModel {
    public string Action { get; set; } = string.Empty;
    public string Label { get; set; } = string.Empty;
}