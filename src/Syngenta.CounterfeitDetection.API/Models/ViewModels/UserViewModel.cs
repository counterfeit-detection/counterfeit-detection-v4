using System.ComponentModel.DataAnnotations;

namespace Syngenta.CounterfeitDetection.API.Models;

public class UserViewModel {
    [MinLength(3)]
    public string Login { get; set; } = string.Empty;

    [MinLength(3)]
    public string Name { get; set; } = string.Empty;

    [MinLength(2)]
    public string CountryCode { get; set; } = string.Empty;

    [MinLength(2)]
    public string RoleCode { get; set; } = string.Empty;
}