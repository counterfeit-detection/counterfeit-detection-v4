namespace Syngenta.CounterfeitDetection.API.Models;

public class SubmittedScanViewModel {
    public ScanEntryViewModel ScanData { get; set; } = new ScanEntryViewModel();
    public ReportEntryViewModel? ReportEntry { get; set; }
}