using Syngenta.CounterfeitDetection.Infrastructure.Models;

namespace Syngenta.CounterfeitDetection.API.Models;

public class ThemeViewModel {
    public Theme? Primary { get; set; }
    public Theme? Secondary { get; set; }
    public Theme? Tertiary { get; set; }
}
