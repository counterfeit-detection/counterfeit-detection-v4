namespace Syngenta.CounterfeitDetection.API.Models;

public class ClientSettingsViewModel {
    public ThemeViewModel? Themes { get; set; }
    public IEnumerable<LanguageViewModel> Languages { get; set; } = new List<LanguageViewModel>();
    public IEnumerable<ScanResultViewModel> ResultTypes { get; set; } = new List<ScanResultViewModel>();
    public IEnumerable<ReportReasonViewModel> ReportReasons { get; set; } = new List<ReportReasonViewModel>();
    public IEnumerable<ScanOriginViewModel> ScanOrigins { get; set; } = new List<ScanOriginViewModel>();
    public IEnumerable<ClientFeatureViewModel> Features { get; set; } = new List<ClientFeatureViewModel>();
}