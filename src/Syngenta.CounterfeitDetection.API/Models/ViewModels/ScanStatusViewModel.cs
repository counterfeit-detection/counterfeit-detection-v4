namespace Syngenta.CounterfeitDetection.API.Models;

public class ScanStatusViewModel {
    public int Id { get; set; }
    public string Status { get; set; } = string.Empty;
    public ScanResultViewModel? Result { get; set; }
    public IEnumerable<CustomFieldViewModel>? Data { get; set; }
    public ReportEntryViewModel? ReportEntry { get; set; }
    public FollowUpEntryViewModel? FollowUp { get; set; }
    public IEnumerable<ScanResultActionViewModel>? Actions { get; set; }
}