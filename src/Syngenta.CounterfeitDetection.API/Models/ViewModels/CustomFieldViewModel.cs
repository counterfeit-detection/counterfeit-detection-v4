using System.ComponentModel.DataAnnotations;

namespace Syngenta.CounterfeitDetection.API.Models;

public class CustomFieldViewModel {
    [MinLength(1)]
    public string Title { get; set; } = string.Empty;

    [MinLength(1)]
    public string Value { get; set; } = string.Empty;
}