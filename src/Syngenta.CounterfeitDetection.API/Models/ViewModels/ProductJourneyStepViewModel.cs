using Syngenta.CounterfeitDetection.Infrastructure.Models;

namespace Syngenta.CounterfeitDetection.API.Models;

public class ProductJourneyStepViewModel {
    public string GTIN { get; set; } = string.Empty;
    public string SN { get; set; } = string.Empty;
    public Geolocation? Location { get; set; }
    public string ShippingId { get; set; } = string.Empty;
    public DateTime? ShippingEventTime { get; set; }
    public DateTime? AuthenticationEventTime { get; set; }
    public string UserName { get; set; } = string.Empty;
    public string SalesRep { get; set; } = string.Empty;

}