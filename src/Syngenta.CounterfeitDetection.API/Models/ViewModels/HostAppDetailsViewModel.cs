using System.ComponentModel.DataAnnotations;

namespace Syngenta.CounterfeitDetection.API.Models;

public class HostAppDetailsViewModel {
    [MinLength(2)]
    public string Device { get; set; } = string.Empty;

    [MinLength(2)]
    public string OS { get; set; } = string.Empty;

    [MinLength(1)]
    public string Version { get; set; } = string.Empty;
}