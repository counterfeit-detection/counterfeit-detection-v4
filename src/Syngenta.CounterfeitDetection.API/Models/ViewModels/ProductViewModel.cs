namespace Syngenta.CounterfeitDetection.API.Models;

public class ProductViewModel {
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string GTIN { get; set; } = string.Empty;
    public string Capacity { get; set; } = string.Empty;
    public bool IsUnderGPD { get; set; }
}