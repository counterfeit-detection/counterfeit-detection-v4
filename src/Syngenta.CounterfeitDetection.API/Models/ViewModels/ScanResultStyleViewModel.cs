namespace Syngenta.CounterfeitDetection.API.Models;

public class ScanResultStyleViewModel {
    public string? TitleColor { get; set; }
    public string? MessageColor { get; set; }
}