namespace Syngenta.CounterfeitDetection.API.Models;

public class FollowUpOptionViewModel {
    public int Id { get; set; }
    public string Type { get; set; } = string.Empty;
    public string Label { get; set; } = string.Empty;
}