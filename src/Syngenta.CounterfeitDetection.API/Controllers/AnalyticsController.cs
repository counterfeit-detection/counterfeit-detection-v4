using Microsoft.AspNetCore.Mvc;
using Syngenta.CounterfeitDetection.API.Security;

namespace Syngenta.CounterfeitDetection.API.Controllers;

[WebApiAuthorize]
[Route("api/{version}/analytics")]
[Consumes("application/json")]
public class AnalyticsController : ControllerBase 
{
    private readonly ILogger<AnalyticsController> _logger;

    public AnalyticsController(ILogger<AnalyticsController> logger) : base() {
        _logger = logger;
    } 

    [HttpGet()]
    [ProducesResponseType(StatusCodes.Status200OK)]
    // mockup
    public async Task<IActionResult> GetGPDProducts() =>
        await Task.FromResult(new OkObjectResult(new { Note = "This is a mockup; response payload is yet to be defined" }));
}