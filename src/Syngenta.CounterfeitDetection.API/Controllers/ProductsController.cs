using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Syngenta.CounterfeitDetection.API.Models;
using Syngenta.CounterfeitDetection.API.Security;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace Syngenta.CounterfeitDetection.API.Controllers;

[WebApiAuthorize]
[Route("api/{version}/products")]
[Consumes("application/json")]
public class ProductsController : ControllerBase {
    private readonly ILogger<ProductsController> _logger;
    private readonly IProductService _productService;
    private readonly IMapper _mapper;

    public ProductsController(
        ILogger<ProductsController> logger,
        IProductService productService,
        IMapper mapper
    ) : base() {
        _logger = logger;
        _productService = productService;
        _mapper = mapper;
    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<ProductViewModel>))]
    public async Task<IActionResult> GetProductCatalogue(string? countryCode = null) {
        var catalogue = await _productService.GetProductCatalogue(countryCode);

        return Ok(_mapper.Map<IEnumerable<ProductViewModel>>(catalogue));
    }

    [HttpGet("{gtin}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ProductViewModel))]
    public async Task<IActionResult> GetProductDetails([RegularExpression("^[0-9]{14}$")] string gtin) {
        var catalogue = await _productService.GetProductDetails(gtin);

        return Ok(_mapper.Map<ProductViewModel>(catalogue));
    }

    [HttpGet("{gtin}/journey/{serialNumber}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<ProductJourneyStepViewModel>))]
    public async Task<IActionResult> GetProductJourney(
        [RegularExpression("^[0-9]{14}$")] string gtin,
        [RegularExpression("^[A-Za-z0-9]{5,25}$")] string serialNumber
    ) => Ok(_mapper.Map<IEnumerable<ProductJourneyStepViewModel>>(
        await _productService.GetProductJourney(gtin, serialNumber)));
}