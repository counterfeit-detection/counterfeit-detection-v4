using AutoMapper;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Syngenta.CounterfeitDetection.API.Models;
using Syngenta.CounterfeitDetection.API.Security;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.Business.Core.Models;
using Azure.Core;

namespace Syngenta.CounterfeitDetection.API.Controllers;

[ApiController]
[WebApiAuthorize]
[Route("api/{version}/scans")]
[Consumes("application/json")]
public class ScansController : ControllerBase {
    private readonly IScanProcessingService _scanProcessingSerivce;
    private readonly IScanHistoryService _scanHistoryService;
    private readonly IReportService _reportService;
    private readonly IMapper _mapper;
    private readonly ILogger<ScansController> _logger;

    public ScansController(
        IScanProcessingService scanProcessingService,
        IScanHistoryService scanHistoryService,
        IReportService reportService,
        IMapper mapper,
        ILogger<ScansController> logger
    ) : base() {
        _scanProcessingSerivce = scanProcessingService;
        _scanHistoryService = scanHistoryService;
        _reportService = reportService;
        _mapper = mapper;
        _logger = logger;
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SubmitScanBatchResponse))]
    public async Task<IActionResult> SubmitScansBatch(SubmitScanBatchRequest request) {
        var res = await _scanProcessingSerivce.CreateScans(
            _mapper.Map<UserData>(request.UserData),
            _mapper.Map<ClientData>(request.ClientData),
            _mapper.Map<IEnumerable<ScanData>>(request.Scans));

        return Ok(_mapper.Map<SubmitScanBatchResponse>(res));
    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetScanHistoryResponse))]
    public async Task<IActionResult> GetScanHistory([FromQuery] GetScanHistoryRequest request) {
        var res = await _scanHistoryService.GetScans(_mapper.Map<ScanHistoryFilter>(request));
        return Ok(_mapper.Map<GetScanHistoryResponse>(res));
    }

    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ScanStatusViewModel))]
    public async Task<IActionResult> GetScanDetails([Range(1, int.MaxValue)] int id) {
        try {
            var scan = await _scanHistoryService.GetScanById(id);
            return Ok(_mapper.Map<ScanStatusViewModel>(scan));
        }
        catch (KeyNotFoundException) {
            _logger.LogInformation($"Couldn't find a scan with id <{id}>");
            return NotFound();
        }
        catch (Exception ex) {
            throw new Exception(
                $"<{_scanHistoryService.GetType().Name}> unexpectedly thrown in <{nameof(GetScanDetails)}>",
                ex);
        }
    }

    [HttpPost("{id}/report-details")]
    public async Task<IActionResult> SubmitReportDetails([Range(1, int.MaxValue)] int id) {
        await Task.CompletedTask;
        throw new NotImplementedException();
    }

    [HttpGet("count")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<IActionResult> GetScanCount([FromQuery] GetScanHistoryRequest request) => 
        Ok(_mapper.Map<GetScansCountResponse>(await _scanHistoryService
                .GetScansCount(_mapper.Map<ScanHistoryFilter>(request))));

    [HttpPost("{id}/follow-up/{followUpActionId}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<IActionResult> SumbitFollowUpAction(
        [Range(1, int.MaxValue)] int id,
        [Range(1, int.MaxValue)] int followUpActionId,
        SubmitFollowUpActionRequest request
    ) {
        try {
            var followUpData = _mapper.Map<FollowUpActionData>(request);
            followUpData.Id = followUpActionId;

            await _scanProcessingSerivce.SubmitFollowUpAction(followUpData);
            return Ok();
        }
        catch (KeyNotFoundException) {
            return NotFound();
        }
        catch (ArgumentException) {
            return BadRequest();
        }
    }

    [HttpPost("{id}/report")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<IActionResult> SubmitScanReport(
        [Range(1, int.MaxValue)] int id,
        ReportEntryViewModel request
    ) {
        try {
            await _reportService.CreateScanReport(id, _mapper.Map<ReportEntry>(request));
            return Ok();
        }
        catch (KeyNotFoundException) {
            return NotFound();
        }
        catch (ArgumentException) {
            return BadRequest();
        }
    }

    [HttpPost("{id}/review")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<IActionResult> MarkScanReviewed([Range(1, int.MaxValue)] int id) {
        try {
            await _scanProcessingSerivce.MarkScanReviewed(id);
            return Ok();
        }
        catch (KeyNotFoundException ex) {
            return NotFound(ex.Message);
        }
        catch (ArgumentException ex) {
            return BadRequest(ex.Message);
        }
        catch (InvalidOperationException ex) { 
            return BadRequest(ex.Message); 
        }
        catch (Exception ex) {
            throw new Exception(
                $"<{_scanProcessingSerivce.GetType().Name}> unexpectedly thrown in <{nameof(MarkScanReviewed)}>",
                ex);
        }
    }
}