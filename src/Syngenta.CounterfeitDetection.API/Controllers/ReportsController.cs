using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Syngenta.CounterfeitDetection.API.Models;
using Syngenta.CounterfeitDetection.API.Security;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.Business.Core.Models;

namespace Syngenta.CounterfeitDetection.API.Controllers;

[WebApiAuthorize]
[Route("api/{version}/reports")]
[Consumes("application/json")]
public class ReportsController : ControllerBase 
{
    private readonly IReportService _reportService;
    private readonly IMapper _mapper;
    private readonly ILogger<ReportsController> _logger;

    public ReportsController(
        IReportService reportService,
        IMapper mapper,
        ILogger<ReportsController> logger
    ) : base() {
        _reportService = reportService;
        _mapper = mapper;
        _logger = logger;
    } 

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SubmitReportBatchResponse))]
    public async Task<IActionResult> SubmitReportsBatch(SubmitReportBatchRequest request) {
        var res = await _reportService.CreateDirectReports(
            _mapper.Map<UserData>(request.UserData),
            _mapper.Map<ClientData>(request.ClientData),
            _mapper.Map<IEnumerable<ReportData>>(request.Reports)
        );
        return Ok(_mapper.Map<SubmitReportBatchResponse>(res));
    }
}