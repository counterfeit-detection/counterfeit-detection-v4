using Microsoft.AspNetCore.Mvc;
using Syngenta.CounterfeitDetection.API.Security;
using Syngenta.CounterfeitDetection.API.Models;
using Syngenta.CounterfeitDetection.Infrastructure.Models;
using AutoMapper;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Azure.Core;
using Syngenta.CounterfeitDetection.Business.Core.Models;

namespace Syngenta.CounterfeitDetection.API.Controllers;

[WebApiAuthorize]
[Route("api/{version}/client-settings")]
[Consumes("application/json")]
public class ClientSettingsController : ControllerBase 
{
    private readonly IClientSettingService _clientSettingService;
    private readonly IMapper _mapper;
    private readonly ILogger<ClientSettingsController> _logger;

    public ClientSettingsController(
        ILogger<ClientSettingsController> logger,
        IClientSettingService clientSettingService,
        IMapper mapper
    ) : base() {
        _logger = logger;
        _clientSettingService = clientSettingService;
        _mapper = mapper;
    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ClientSettingsViewModel))]
    public async Task<IActionResult> GetClientSettings([FromHeader]string appName, string country, string role) {
        var res = await _clientSettingService.GetClientSettings(appName, country, role);
        return Ok(_mapper.Map<ClientSettingsViewModel>(res));
    }


    #region Data mocks
    private readonly ClientSettingsViewModel _clientSettingsResponseMock = new ClientSettingsViewModel() {
        Themes = new ThemeViewModel {
            Primary = new Theme {
                Main = "#009900",
                Dark = "#003300",
                Light = "#00CC00",
                Contrast = "#CC00CC",
            },
            Secondary = new Theme {
                Main = "#990000",
                Dark = "#330000",
                Light = "#CC0000",
                Contrast = "#00CCCC",
            },
            Tertiary = new Theme {
                Main = "#000099",
                Dark = "#000033",
                Light = "#0000CC",
                Contrast = "#CCCC00",
            }
        },
        Languages = new List<LanguageViewModel> {
            new LanguageViewModel {
                Id = 1,
                Name = "English",
                Code = "EN"
            },
            new LanguageViewModel {
                Id = 2,
                Name = "Esperanto",
                Code = "EO"
            }
        },
        ResultTypes = new List<ScanResultViewModel> {
            new ScanResultViewModel {
                TypeId = 1,
                Title = "Valid: low risk",
                Message = "Congrats! Your scan is valid",
                Style = new ScanResultStyleViewModel {
                    TitleColor = "#00CC00",
                    MessageColor = "#000000"
                }
            },
            new ScanResultViewModel {
                TypeId = 2,
                Title = "Invalid: tracking ID is invalid",
                Message = "Oops! Your scan is invalid - please check if you're scanning a correct thing.",
                Style = new ScanResultStyleViewModel {
                    TitleColor = "#996600",
                    MessageColor = "#000000"
                }
            }
        },
        ReportReasons = new List<ReportReasonViewModel> {
            new ReportReasonViewModel {
                Id = 1,
                Name = "Just testing",
                RequireCustomReason = false,
                Order = 100
            },
            new ReportReasonViewModel {
                Id = 1,
                Name = "Other",
                RequireCustomReason = true,
                Order = 200
            }
        },
        ScanOrigins = new List<ScanOriginViewModel> {
            new ScanOriginViewModel {
                Id = 1,
                Name = "Farm",
                Order = 300,
            },
            new ScanOriginViewModel {
                Id = 2,
                Name = "Shop",
                Order = 300,
            },
            new ScanOriginViewModel {
                Id = 3,
                Name = "Warehouse",
                Order = 200,
            }
        },
        Features = new List<ClientFeatureViewModel> {
            new ClientFeatureViewModel {
                Id = 1,
                Name = "Reported Scans"
            },
            new ClientFeatureViewModel {
                Id = 2,
                Name = "My Scan History"
            },
            new ClientFeatureViewModel {
                Id = 3,
                Name = "Scan Product"
            },
            new ClientFeatureViewModel {
                Id = 4,
                Name = "Offline Report Non-GPD"
            },
            new ClientFeatureViewModel {
                Id = 5,
                Name = "Offline Scan"
            },
            new ClientFeatureViewModel {
                Id = 6,
                Name = "Product Journey"
            },
            new ClientFeatureViewModel {
                Id = 7,
                Name = "Report GPD Product"
            },
            new ClientFeatureViewModel {
                Id = 8,
                Name = "Report Non-GPD Product"
            },
        }
    };
    #endregion
}