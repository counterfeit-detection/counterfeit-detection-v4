using AutoMapper;
using Syngenta.CounterfeitDetection.API.Mappings;

namespace Syngenta.CounterfeitDetection.API.Extensions;

public static class AutomapperExtensions {
    public static IMapperConfigurationExpression RegisterAPI2BusinessMappings(
        this IMapperConfigurationExpression cfg
    ) {
        cfg.AddProfile<MappingProfile>();
        return cfg;
    } 

    public static IServiceCollection RegisterAPI2BusinessTypeConverters(
        this IServiceCollection services
    ) {
        services.AddTransient<KeyValuePairToCustomFieldConverter>();
        services.AddTransient<CustomFieldToKeyValuePairConverter>();
        return services;
    }
}