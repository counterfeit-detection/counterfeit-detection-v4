﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Syngenta.CounterfeitDetection.Infrastructure.Constants;

namespace Syngenta.CounterfeitDetection.API.Security;

internal sealed class WebApiAuthorizeAttribute : Attribute, IAsyncAuthorizationFilter {
    private const string StandaloneApp = "StandaloneApp";

    public Task OnAuthorizationAsync(AuthorizationFilterContext context) {
        var appName = GetHeaderValue(context, Constants.AppName);

        if (String.IsNullOrWhiteSpace(appName)) {
            context.Result = new UnauthorizedResult();
            return Task.CompletedTask;
        }

        switch (true) {
            case var _ when appName.Equals(StandaloneApp, StringComparison.OrdinalIgnoreCase):
                HandleStandaloneApp(context);
                return Task.CompletedTask;
            default : 
                HandleDefault(context);
                return Task.CompletedTask;
        }
    }

    private static void HandleStandaloneApp(AuthorizationFilterContext context) {
        if (String.IsNullOrWhiteSpace(GetHeaderValue(context, Constants.Token))) 
            context.Result = new UnauthorizedResult();
    }

    private static void HandleDefault(AuthorizationFilterContext context) => 
        context.Result = new UnauthorizedResult();

    private static string? GetHeaderValue(AuthorizationFilterContext context, string key) 
        => context.HttpContext.Request.Headers.ContainsKey(key)
            ? (string?)context.HttpContext.Request.Headers[key]
            : null;
}
