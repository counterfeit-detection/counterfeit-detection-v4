using Microsoft.AspNetCore.Mvc;
using Syngenta.CounterfeitDetection.API.Extensions;
using Syngenta.CounterfeitDetection.Business;
using Syngenta.CounterfeitDetection.Business.Extensions;
using Syngenta.CounterfeitDetection.Infrastructure.Helpers;

[assembly: ApiController]

var builder = WebApplication.CreateBuilder(args);
const string corsPolicyName = "DefaultCORSPolicy";

// Add services to the container.
builder.Services.AddCors(options => options.AddPolicy(corsPolicyName,
        policy => policy.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod()));

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddSingleton<IConfiguration>(builder.Configuration);
builder.Services.AddSingleton<HttpClient>(new HttpClient());
builder.Services.RegisterBusiness(builder.Configuration.GetSection("ConnectionStrings"));
builder.Services.RegisterAPI2BusinessTypeConverters();
builder.Services.RegisterBusiness2DataAccessTypeConverters();
builder.Services.AddAutoMapper(cfg => cfg
    .RegisterAPI2BusinessMappings()
    .RegisterBusiness2DataAccessMappings());

ConfigurationHelper.Initialize(builder.Configuration);

WebApplication app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    await scope.UpdateDatabase(Boolean.TryParse(ConfigurationHelper.Get("AllowSeedMasterData"), out var result) && result);
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseRouting();

app.UseCors(corsPolicyName);

app.UseAuthorization();

app.MapControllers();

app.Run();
