using AutoMapper;
using PL = Syngenta.CounterfeitDetection.API.Models;
using BL = Syngenta.CounterfeitDetection.Business.Core.Models;
using Infra = Syngenta.CounterfeitDetection.Infrastructure.Models;

namespace Syngenta.CounterfeitDetection.API.Mappings;

public class MappingProfile : Profile {
    public MappingProfile() {
        // submit scan models
        CreateMap<PL.UserViewModel, BL.UserData>();
        CreateMap<PL.ClientViewModel, BL.ClientData>();
        CreateMap<PL.HostAppDetailsViewModel, BL.HostAppData>();
        CreateMap<PL.SubmittedScanViewModel, BL.ScanData>()
            .ForMember(dest => dest.ScanEntry, opt => opt.MapFrom(src => src.ScanData))
            .ForMember(dest => dest.ReportData, opt => opt.MapFrom(src => src.ReportEntry));
        CreateMap<PL.ScanEntryViewModel, BL.ScanEntry>();
        CreateMap<PL.ReportEntryViewModel, BL.ReportEntry>();

        CreateMap<BL.CreateScanResult, PL.SubmitScanBatchResponse>();
        CreateMap<BL.ScanData, PL.SubmittedScanViewModel>()
            .ForMember(dest => dest.ScanData, opt => opt.MapFrom(src => src.ScanEntry))
            .ForMember(dest => dest.ReportEntry, opt => opt.MapFrom(src => src.ReportData));
        CreateMap<BL.ScanEntry, PL.ScanEntryViewModel>();
        CreateMap<BL.ReportEntry, PL.ReportEntryViewModel>();
        CreateMap<BL.ScanTransaction, PL.ScanStatusViewModel>()
            .ForMember(dest => dest.Data, opt => opt.MapFrom(src => src.ScanData))
            .ForMember(dest => dest.ReportEntry, opt => opt.MapFrom(src => src.ReportData))
            .ForMember(dest => dest.Actions, opt => opt.AllowNull());
        CreateMap<BL.ScanResult, PL.ScanResultViewModel>();
        CreateMap<BL.ScanResultStyle, PL.ScanResultStyleViewModel>();
        CreateMap<KeyValuePair<string, string>, PL.CustomFieldViewModel>()
            .ConvertUsing<KeyValuePairToCustomFieldConverter>();
        CreateMap<BL.ScanFollowUpEntry, PL.FollowUpEntryViewModel>();
        CreateMap<BL.ScanFollowUpOption, PL.FollowUpOptionViewModel>();
        CreateMap<BL.ScanFollowUpInput, PL.FollowUpInputViewModel>();
        CreateMap<BL.ScanResultAction, PL.ScanResultActionViewModel>();

        // get scan history models
        CreateMap<PL.GetScanHistoryRequest, BL.ScanHistoryFilter>();
        CreateMap<BL.ScanHistoryPage, PL.GetScanHistoryResponse>()
            .ForMember(dest => dest.PageItems, opt => opt.MapFrom(src => src.Scans));

        // submit follow-up action models
        CreateMap<PL.SubmitFollowUpActionRequest, BL.FollowUpActionData>();
        CreateMap<PL.CustomFieldViewModel, KeyValuePair<string, string>>()
            .ConvertUsing<CustomFieldToKeyValuePairConverter>();

        // submit direct report models
        CreateMap<PL.SubmittedScanViewModel, BL.ReportData>();
        CreateMap<BL.CreateDirectReportResult, PL.SubmitReportBatchResponse>();
        CreateMap<BL.CreatedReportData, PL.SubmittedReportViewModel>()
            .ForMember(dest => dest.ScanData, opt => opt.MapFrom(src => src.ScanEntry));
        CreateMap<BL.ReportData, PL.SubmittedScanViewModel>()
            .ForMember(dest => dest.ScanData, opt => opt.MapFrom(src => src.ScanEntry));
        CreateMap<int, PL.GetScansCountResponse>()
            .ForMember(dest => dest.TotalCount, opt => opt.MapFrom(src => src));
        CreateMap<BL.Languages, PL.LanguageViewModel>();
        CreateMap<BL.ThemeSettings, PL.ThemeViewModel>();
        CreateMap<BL.ReportReason, PL.ReportReasonViewModel>();
        CreateMap<BL.ScanOrigin, PL.ScanOriginViewModel>();
        CreateMap<BL.ClientFeatures, PL.ClientFeatureViewModel>();
        CreateMap<BL.ScanResultStyle, PL.ScanResultStyleViewModel>();
        CreateMap<BL.ScanResult, PL.ScanResultViewModel>();
        CreateMap<BL.ClientSettings, PL.ClientSettingsViewModel>();
        
        CreateMap<BL.ProductData, PL.ProductViewModel>();
        CreateMap<BL.ProductJourneyStep, PL.ProductJourneyStepViewModel>();
    }
}