using AutoMapper;
using Syngenta.CounterfeitDetection.API.Models;

namespace Syngenta.CounterfeitDetection.API.Mappings;

public class CustomFieldToKeyValuePairConverter 
    : ITypeConverter<CustomFieldViewModel, KeyValuePair<string, string>>
{
    public KeyValuePair<string, string> Convert(
        CustomFieldViewModel src,
        KeyValuePair<string, string> dest,
        ResolutionContext context
    ) => 
        new KeyValuePair<string, string>(src.Title, src.Value);
}