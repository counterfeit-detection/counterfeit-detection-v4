using AutoMapper;
using Syngenta.CounterfeitDetection.API.Models;

namespace Syngenta.CounterfeitDetection.API.Mappings;

public class KeyValuePairToCustomFieldConverter 
    : ITypeConverter<KeyValuePair<string, string>, CustomFieldViewModel> 
{
    public CustomFieldViewModel Convert(
        KeyValuePair<string, string> src,
        CustomFieldViewModel dest,
        ResolutionContext context
    ) => 
        new CustomFieldViewModel { 
            Title = src.Key,
            Value = src.Value
        };
}