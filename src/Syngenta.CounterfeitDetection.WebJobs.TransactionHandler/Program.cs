﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Syngenta.CounterfeitDetection.DataAccess;
using Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Constants;
using Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Interfaces;
using Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Services;

IConfiguration configuration = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
    .AddEnvironmentVariables()
    .Build();

var builder = new HostBuilder();
builder.UseEnvironment(EnvironmentName.Development);

builder.ConfigureWebJobs(b => {
    b.AddAzureStorageCoreServices();
    b.AddTimers();
});

builder.ConfigureLogging((context, b) => b.AddConsole());

builder.ConfigureServices(services => {
    services.AddSingleton<IConfiguration>(configuration);
    services.AddSingleton<HttpClient>(new HttpClient());
    services.RegisterDataAccess(configuration.GetSection(Constants.ConnectionStrings));
    services.AddScoped<ITransactionHandlerService, TransactionHandlerService>();
    services.AddScoped<ISAPIntegrationService, SAPIntegrationService>();
    services.AddLogging();
});

var host = builder.Build();

using (host) {
    await host.RunAsync();
}