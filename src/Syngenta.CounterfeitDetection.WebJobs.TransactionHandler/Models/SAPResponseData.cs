﻿using Newtonsoft.Json;

namespace Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Models;

internal class SAPResponseData {
    public string? EventTime { get; set; }
    [JsonProperty("ProductDesc_EN")]
    public string? ProductName { get; set; }
    [JsonProperty("ProductDesc_Local")]
    public string? ProductDescription { get; set; }
    [JsonProperty("ProfRelCountry")]
    public string? ProductCountryCode { get; set; }
    [JsonProperty("ProductBU")]
    public string? ProductBusinessUnit { get; set; }
    public string? UnitOfMeasure { get; set; }
    public string? SGTIN { get; set; }
    public string? GTIN { get; set; }
    public string? SerialNumber { get; set; }
    public string? BatchNumber { get; set; }
    public string? ExpiryDate { get; set; }
    [JsonProperty("ManufacturingDate")]
    public string? ManufacturedDate { get; set; }
    public int NoOfScans { get; set; }
    public string? ResponseMessage1 { get; set; }
    public string? ResponseMessage2 { get; set; }
    public int ResponseMessageType { get; set; }
    public string? ScanId { get; set; }
    public bool IsGenuine { get; set; }
    public IEnumerable<SapEvent>? EventList { get; set; }

    public class SapEvent {
        public string? BusinessStep { get; set; }
        public SapEventReadPoint? ReadPoint { get; set; }
        public string? EventTime { get; set; }
    }

    public class SapEventReadPoint {
        [JsonProperty("@id")]
        public string? Id { get; set; }
        public IEnumerable<SapEventReadPointAttribute>? Attribute { get; set; }
    }

    public class SapEventReadPointAttribute {
        [JsonProperty("@id")]
        public string? Id { get; set; }
        [JsonProperty("$")]
        public string? Value { get; set; }
    }
}
