﻿using Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Enums;

namespace Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Models;

internal class BarcodeDetails {
    public BarCodeTypes BarCodeType { get; set; }

    public string? RawString { get; set; }

    public string TrackingId =>
            GTIN?.Length == 16 && SN?.Length > 2 && SN?.Length <= 22
            ? $"{GTIN}{SN}"
            : RawString!;

    public string? GTIN { get; set; }

    public string? SN { get; set; }

    public string? BatchNumber { get; set; }

    public DateTime? ExpireDate { get; set; }

    public DateTime? ManufacturedDate { get; set; }
}
