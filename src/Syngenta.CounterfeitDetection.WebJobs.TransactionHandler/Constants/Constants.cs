﻿using System.Drawing;

namespace Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Constants;

internal class Constants {
    public const string ConnectionStrings = "ConnectionStrings";

    internal class ConfigurationKeys {
        public const string SapIntegration_CodeTypeParameterName = "SapIntegration:CodeTypeParameterName";
        public const string SapIntegration_GrowerUserType = "SapIntegration:GrowerUserType";
        public const string GTINScanAPIKey = "GTINScanAPIKey";
    }
}
