﻿namespace Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Interfaces;

public interface ITransactionHandlerService {
    public Task ExecuteAsync();
}
