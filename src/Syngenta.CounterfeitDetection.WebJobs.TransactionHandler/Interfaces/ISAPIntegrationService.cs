﻿using Syngenta.CounterfeitDetection.DataAccess.Models;
using Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Models;

namespace Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Interfaces;

internal interface ISAPIntegrationService {
    Task<SapResult> SubmitSAPRequest(ScanInputDetails scanInputDetails, BarcodeDetails barcodeDetails, Transaction transaction);
}
