﻿using Syngenta.CounterfeitDetection.DataAccess.Models;
using Syngenta.CounterfeitDetection.Infrastructure.Helpers;
using Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Enums;
using Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Models;
using System.Text.RegularExpressions;

namespace Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Utilities;

internal static class BarcodeParser {
    internal static BarcodeDetails ParseBarCode(ScanInputDetails scanDetails) {
        try {
            var codeType = EnumExtensions.GetValueFromDescription<BarCodeTypes>(scanDetails.CodeType!.Name!);
            var barCodeDetails = codeType == BarCodeTypes.QRCode && !scanDetails.IsManualScan
                ? GetTrackingIdFromURL(scanDetails.RawData!)
                : codeType == BarCodeTypes.DataMatrixCode && !scanDetails.IsManualScan
                    ? GetTrackingIdFromRawData(scanDetails.RawData!)
                    : new BarcodeDetails() {
                        RawString = scanDetails.RawData!,
                        BarCodeType = BarCodeTypes.Unknown,
                    };

            if (!String.IsNullOrEmpty(barCodeDetails.RawString) && barCodeDetails.RawString.Contains("&")) {
                barCodeDetails.RawString = barCodeDetails.RawString.Replace("&", "%26");
            }

            return barCodeDetails!;
        }
        catch (Exception ex) {
            Console.WriteLine($"Tracking ID parsing error: {ex.Message}", ex);
            return new BarcodeDetails() { RawString = scanDetails.RawData, BarCodeType = BarCodeTypes.Unknown };
        }
    }

    private static BarcodeDetails GetTrackingIdFromRawData(string rawString) {
        var groupSeparators = new string[] { "%1D", "%5CF", "\u001d" };
        var parsedParams = new Dictionary<string, string>();
        var barCodeDetails = new BarcodeDetails() {
            RawString = rawString,
            BarCodeType = BarCodeTypes.DataMatrixCode,
        };

        foreach (var gs in groupSeparators) {
            if (!rawString.Contains(gs))
                continue;

            // replace custom FNC1 representation with standard control symbol
            if (rawString[..1].Contains('f', StringComparison.OrdinalIgnoreCase)) {
                rawString = gs + rawString.Remove(0, 1);
            }

            var groupSeparatorsCount = CountStringOccurrences(rawString, gs);

            var elementStrings = Regex.Split(rawString, gs).Where(elem => !String.IsNullOrEmpty(elem));
            foreach (var es in elementStrings) {
                parsedParams.Remove(String.Empty);
                parsedParams = SGTINParsing(es, parsedParams, groupSeparatorsCount);
            }
        }

        if (parsedParams.ContainsKey(String.Empty) || parsedParams.Count == 0) {
            return barCodeDetails;
        }

        barCodeDetails.BatchNumber = parsedParams.Where(x => x.Key.Contains("10"))
            .Select(x => x.Value)
            .FirstOrDefault();
        barCodeDetails.ManufacturedDate = DateTime.TryParse(parsedParams.Where(x => x.Key.Contains("11"))
                .Select(x => x.Value)
                .FirstOrDefault(), out var manufacturedDate)
            ? manufacturedDate
            : null;
        barCodeDetails.ExpireDate = DateTime.TryParse(parsedParams.Where(x => x.Key.Contains("17"))
                .Select(x => x.Value)
                .FirstOrDefault(), out var expireDate)
            ? expireDate
            : null;
        barCodeDetails.GTIN = parsedParams.Where(x => x.Key.Contains("01"))?
            .Select(x => x.Value)
            .FirstOrDefault();
        barCodeDetails.SN = parsedParams.Where(x => x.Key.Contains("21"))?
            .Select(x => x.Value)
            .FirstOrDefault();

        return barCodeDetails;
    }

    private static BarcodeDetails GetTrackingIdFromURL(string urlString) {
        var uri = new Uri(urlString);
        var urlPath = uri.LocalPath;
        var barCodeDetails = new BarcodeDetails() {
            RawString = urlString,
            BarCodeType = BarCodeTypes.QRCode
        };

        if (String.IsNullOrEmpty(urlPath) || urlPath.Length <= 5) {
            return barCodeDetails;
        }

        var parameters = parseUriParams();

        if (parameters.Count == 0) {
            return barCodeDetails;
        }

        var gtin = parameters.Where(x => x.Key.Contains("01")).Select(x => x.Value).FirstOrDefault();
        var sn = parameters.Where(x => x.Key.Contains("21")).Select(x => x.Value).FirstOrDefault();

        barCodeDetails.BatchNumber = parameters.Where(x => x.Key.Contains("10")).Select(x => x.Value).FirstOrDefault();
        barCodeDetails.ManufacturedDate = DateTime.TryParse(parameters.Where(x => x.Key.Contains("11")).Select(x => x.Value).FirstOrDefault(), out var manufacturedDate)
            ? manufacturedDate
            : null;
        barCodeDetails.ExpireDate = DateTime.TryParse(parameters.Where(x => x.Key.Contains("17")).Select(x => x.Value).FirstOrDefault(), out var expireDate)
            ? expireDate
            : null;
        barCodeDetails.GTIN = gtin;
        barCodeDetails.SN = sn;
        return barCodeDetails;

        Dictionary<string, string> parseUriParams() {
            var applicationIdentifiers = new string[] { "01", "21", "10", "17", "11" };

            var parsedParams = uri.ParseQueryString();

            var pathParams = urlPath.Replace("gpd", String.Empty).Split('/');
            if (pathParams != null && pathParams.Length > 0) {
                for (var i = 0; i < pathParams.Length - 1; i++) {
                    // looking for GS1 application keys
                    var identifier = pathParams[i];
                    if (!String.IsNullOrEmpty(identifier) && applicationIdentifiers.Any(ai => ai == identifier)) {
                        // if parameter is GS1 application identifier, next parameter should contain value for it
                        var value = pathParams[i + 1];
                        parsedParams.Add(identifier, $"{identifier}{value}");
                        i++;
                    }
                }
            }

            return parsedParams;
        }
    }

    private static int CountStringOccurrences(string text, string pattern) {
        var count = 0;
        var i = 0;

        while ((i = text.IndexOf(pattern, i)) != -1) {
            i += pattern.Length;
            count++;
        }

        return count;
    }

    private static Dictionary<string, string> SGTINParsing(string elementString, Dictionary<string, string> initialParsedValues, int groupSeparatorsCount = 0) {
        var parsedValues = new Dictionary<string, string>(initialParsedValues);
        var remainingString = new string(elementString);

        var i = 0;
        while (!String.IsNullOrEmpty(remainingString) && i <= 5 && remainingString.Length > 2) {
            var key = remainingString[..2];

            switch (key) {
                case "01": cutGTINIntoDictionary(key); break;
                case "21": cutValueFromStringToDictionary(key, remainingString); break;
                case "10": cutValueFromStringToDictionary(key, remainingString); break;
                case "11": cutProductionOrExpirationDateIntoDictionary(key); break;
                case "17": cutProductionOrExpirationDateIntoDictionary(key); break;
            }

            i++;
        }

        if (!String.IsNullOrEmpty(remainingString))
            parsedValues.Add(String.Empty, remainingString);

        return parsedValues;

        void cutGTINIntoDictionary(string key) {
            var gtin = remainingString.Substring(remainingString.IndexOf(key), 16);
            cutValueFromStringToDictionary(key, gtin);
        }

        void cutProductionOrExpirationDateIntoDictionary(string key) {
            var parsedDate = remainingString.Substring(remainingString.IndexOf(key), 8);
            cutValueFromStringToDictionary(key, parsedDate);
        }

        void cutValueFromStringToDictionary(string key, string value) {
            parsedValues.Add(key, value);
            remainingString = remainingString.Replace(value, String.Empty);
        }
    }
}
