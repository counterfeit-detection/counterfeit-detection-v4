﻿using System.Text.RegularExpressions;

namespace Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Utilities;

internal static class UriExtensions {
    private static readonly Regex s_regex = new Regex(@"[?|&]([\w\.]+)=([^?|^&]+)");

    public static Dictionary<string, string> ParseQueryString(this Uri uri) {
        var match = s_regex.Match(uri.PathAndQuery);
        var paramaters = new Dictionary<string, string>();
        while (match.Success) {
            paramaters.Add(match.Groups[1].Value, match.Groups[2].Value);
            match = match.NextMatch();
        }

        return paramaters;
    }
}