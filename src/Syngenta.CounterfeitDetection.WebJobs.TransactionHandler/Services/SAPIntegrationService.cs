﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Syngenta.CounterfeitDetection.DataAccess.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Models;
using Syngenta.CounterfeitDetection.Infrastructure.Helpers;
using Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Enums;
using Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Interfaces;
using Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Models;
using System.Text;

namespace Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Services;

internal class SAPIntegrationService : ISAPIntegrationService {
    private readonly IConfiguration _configuration;
    private readonly HttpClient _client;
    private readonly IGenericRepository<SapResultType> _sapResultTypeRepo;
    private readonly IGenericRepository<SapResult> _sapResultRepo;
    private readonly IGenericRepository<ExternalEndpoint> _externalEndpointRepo;
    private const string INTERNAL = "INTERNAL";
    private const string SAP = "SAP";
    private const string Grower = "Grower";

    public SAPIntegrationService(
        IConfiguration configuration,
        HttpClient client,
        IGenericRepository<SapResultType> sapResultTypeRepo,
        IGenericRepository<SapResult> sapResultRepo,
        IGenericRepository<ExternalEndpoint> externalEndpointRepo) {
        _configuration = configuration;
        _client = client;
        _sapResultTypeRepo = sapResultTypeRepo;
        _sapResultRepo = sapResultRepo;
        _externalEndpointRepo = externalEndpointRepo;
    }

    public async Task<SapResult> SubmitSAPRequest(ScanInputDetails scanInputDetails, BarcodeDetails barcodeDetails, Transaction transaction) {
        var sapScanEndpointUrl = (await _externalEndpointRepo
            .GetManyAsync(endpoint => endpoint.CountryId == transaction.UserCountryId
                && endpoint.ExternalEndpointType!.Type!.Equals(SAP)))
            .FirstOrDefault()?
            .EndpointUrl
            ?? throw new Exception($"SAP integration endpoint for country with ID: {transaction.UserCountryId} not founded.");

        var APIKey = _configuration.GetValue<string>(Constants.Constants.ConfigurationKeys.GTINScanAPIKey);

        var request = new HttpRequestMessage(HttpMethod.Get, BuildRequestURL(sapScanEndpointUrl, scanInputDetails, barcodeDetails, transaction));
        request.Headers.Add(nameof(APIKey), APIKey);

        var response = await _client.SendAsync(request);
        var responseContent = await response.Content.ReadAsStringAsync();

        var SAPResponseData = !response.IsSuccessStatusCode
            ? throw new Exception($"SAP request has failed with status code {response.StatusCode}. Response content: {responseContent}")
            : JsonConvert.DeserializeObject<SAPResponseData>(responseContent);

        var scanResultType = EnumExtensions.GetEnumDescription(DefineScanResultType(SAPResponseData!));
        var type = (await _sapResultTypeRepo.GetManyAsync(resultType => resultType.Type!.Equals(scanResultType))).First();

        var sapResult = (await _sapResultRepo
                .GetManyAsync(result => result.TransactionId == scanInputDetails.TransactionId))
                .FirstOrDefault();

        var SAPResult = new SapResult() {
            Id = sapResult?.Id ?? default,
            TransactionId = scanInputDetails.TransactionId,
            TypeId = type.Id,
            ResponseTypeCode = SAPResponseData!.ResponseMessageType.ToString(),
            ResponseErrorCode = SAPResponseData!.ResponseMessage2,
            ResponseText = SAPResponseData!.ResponseMessage1,
            ProductBusinessUnit = SAPResponseData!.ProductBusinessUnit,
            ProductDestinationCountry = SAPResponseData!.ProductCountryCode,
            NumberOfScans = SAPResponseData!.NoOfScans,
        };

        SAPResult = await _sapResultRepo.UpdateAsync(SAPResult);
        SAPResult.Type = type;

        return SAPResult;
    }

    private string BuildRequestURL(string endpointUrl, ScanInputDetails scanInputDetails, BarcodeDetails barcodeDetails, Transaction transaction) {
        var sb = new StringBuilder(endpointUrl);
        sb.Append($"?ScanLocation={transaction.GeoLocation}");
        sb.Append($"&SubscriberSystem={scanInputDetails.ClientAppType!.ClientAppName}");
        sb.Append($"&ScanTime={scanInputDetails.ScanTime:yyyy.MM.ddTHH:mm:ssZ}");
        sb.Append($"&UserInfo={transaction.User!.AuthId}");
        sb.Append($"&UserType={GetSAPUserType(scanInputDetails.ClientAppType!.Client!.Name!)}");
        sb.Append($"&TrackingID={barcodeDetails.TrackingId}");
        sb.Append($"&ScanID={scanInputDetails.Fingerprint}");

        var sapCodeTypeParameterName = _configuration
            .GetValue<string>(Constants.Constants.ConfigurationKeys.SapIntegration_CodeTypeParameterName);

        if (!String.IsNullOrWhiteSpace(sapCodeTypeParameterName))
            sb.Append($"&{sapCodeTypeParameterName}={scanInputDetails.CodeType!.Name}");

        return sb.ToString();
    }

    private string GetSAPUserType(string appName) => appName.Equals(Grower)
        ? _configuration.GetValue<string>(Constants.Constants.ConfigurationKeys.SapIntegration_GrowerUserType) ?? INTERNAL
        : INTERNAL;

    private SAPResultTypes DefineScanResultType(SAPResponseData SAPResponseData) {
        return SAPResponseData.ResponseMessageType switch {
            1 => SAPResultTypes.LowRisk,
            2 => SAPResultTypes.MedRisk,
            3 => SAPResultTypes.HighRisk,
            _ => SAPResponseData.ResponseMessage2 switch {
                "001" => SAPResultTypes.HighRisk,
                "002" => SAPResultTypes.TrackingIdNotAvailable,
                "003" => SAPResultTypes.TrackingIdNotActive,
                "005" => SAPResultTypes.TrackingIdMissing,
                "006" => SAPResultTypes.TrackingIdInvalid,
                "007" => SAPResultTypes.Blacklisted,
                "008" => SAPResultTypes.AuthenticationFailed,
                "009" => SAPResultTypes.Relabelled,
                "010" => SAPResultTypes.NonExistingGTIN,
                "011" => SAPResultTypes.NonExistingSN,
                "012" => SAPResultTypes.Stolen,
                _ => throw new Exception($"Unable to define scan result type based on SAP response data."),
            }
        };
    }
}
