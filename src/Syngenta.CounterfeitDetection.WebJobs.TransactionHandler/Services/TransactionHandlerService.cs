﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Syngenta.CounterfeitDetection.DataAccess.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Models;
using Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Interfaces;
using Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Models;
using Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Utilities;

using SharedConstants = Syngenta.CounterfeitDetection.Infrastructure.Constants;

namespace Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Services;

internal class TransactionHandlerService : ITransactionHandlerService {
    private readonly IGenericRepository<Transaction> _transactionRepo;
    private readonly IGenericRepository<ScanInputDetails> _scanInputDetailsRepo;
    private readonly IGenericRepository<FollowUpAction> _followUpActionRepo;
    private readonly IGenericRepository<TransactionStatusType> _transactionStatusTypeRepo;
    private readonly IGenericRepository<TransactionResult> _transactionResultRepo;
    private readonly IGenericRepository<TransactionResultDefinitionRule> _transactionResultDefinitionRuleRepo;
    private readonly IGenericRepository<ItemDetails> _itemDetailsRepo;
    private readonly ILogger<TransactionHandlerService> _logger;
    private readonly ISAPIntegrationService _integrationService;

    public TransactionHandlerService(
        IGenericRepository<Transaction> transactionRepo,
        IGenericRepository<ScanInputDetails> scanInputDetailsRepo,
        IGenericRepository<FollowUpAction> followUpActionRepo,
        IGenericRepository<TransactionStatusType> transactionStatusTypeRepo,
        IGenericRepository<TransactionResult> transactionResultRepo,
        IGenericRepository<TransactionResultDefinitionRule> transactionResultDefinitionRuleRepo,
        IGenericRepository<ItemDetails> itemDetailsRepo,
        ILogger<TransactionHandlerService> logger,
        ISAPIntegrationService integrationService
    ) {
        _transactionRepo = transactionRepo;
        _scanInputDetailsRepo = scanInputDetailsRepo;
        _followUpActionRepo = followUpActionRepo;
        _transactionStatusTypeRepo = transactionStatusTypeRepo;
        _transactionResultRepo = transactionResultRepo;
        _logger = logger;
        _integrationService = integrationService;
        _itemDetailsRepo = itemDetailsRepo;
        _transactionResultDefinitionRuleRepo = transactionResultDefinitionRuleRepo;
    }

    public async Task ExecuteAsync() {
        var unhandledTransactions = await _transactionRepo
            .GetManyAsync(transaction => transaction.Status!.Type!.Equals(SharedConstants.Constants.TransactionStatusType.Pending),
                include: transactions => transactions
                    .Include(x => x.User!)
                    .Include(x => x.UserCountry!)
                    .Include(x => x.UserRole!));

        foreach (var transaction in unhandledTransactions) {
            try {
                await HandleTransaction(transaction);
            }
            catch (Exception exception) {
                _logger.LogError($"Failed to handle transaction with ID: <{transaction.Id}>.", exception);
            }
        }
    }

    private async Task HandleTransaction(Transaction transaction) {
        var scanInputDetails = (await _scanInputDetailsRepo.GetManyAsync(scanInputDetails => scanInputDetails.TransactionId == transaction.Id,
            include: source => source
                .Include(x => x.ClientAppType)
                    .ThenInclude(x => x!.Client!)
                .Include(x => x.CodeType!)
            )).FirstOrDefault();

        var barcodeDetails = await ParseScanInputDetails(scanInputDetails!);
        var response = await _integrationService.SubmitSAPRequest(scanInputDetails!, barcodeDetails, transaction);
        var transactionStatus = await DefineNewTransactionStatus(scanInputDetails!, response, transaction.UserCountryId);

        transaction.StatusId = transactionStatus;
        transaction.LastUpdateTime = DateTime.Now;

        await _transactionRepo.UpdateAsync(transaction);
    }

    private async Task<BarcodeDetails> ParseScanInputDetails(ScanInputDetails scanInputDetails) {
        var barcodeDetails = BarcodeParser.ParseBarCode(scanInputDetails!);

        var existingDetails = (await _itemDetailsRepo
                .GetManyAsync(details => details.TransactionId == scanInputDetails.TransactionId))
                .FirstOrDefault();
            
        var inputDetails = new ItemDetails() {
            Id = existingDetails?.Id ?? default,
            TransactionId = scanInputDetails.TransactionId,
            GTIN = barcodeDetails.GTIN,
            SerialNumber = barcodeDetails.SN,
            BatchNumber = barcodeDetails.BatchNumber,
            ExpiryDate = barcodeDetails.ExpireDate,
            ManufacturedDate = barcodeDetails.ManufacturedDate
        };

        await _itemDetailsRepo.UpdateAsync(inputDetails);

        return barcodeDetails;
    }

    private async Task<int> DefineNewTransactionStatus(ScanInputDetails scanInputDetails, SapResult sapResult, int? userCountryId) {
        var transactionResultDefinitionType = (await _transactionResultDefinitionRuleRepo
            .GetManyAsync(result => (result.SapResultTypeId == sapResult.TypeId || result.SapResultTypeId == null)
                && (result.ClientId == scanInputDetails!.ClientAppType!.ClientId || result.ClientId == null)
                && (result.CountryId == userCountryId || result.CountryId == null)
                && (result.SAPScanCount >= sapResult.NumberOfScans || result.SAPScanCount == null)
                && (result.SAPBU == sapResult.ProductBusinessUnit || result.SAPBU == null)
                && (result.SAPCountry == null || result.SAPCountry.Contains(sapResult.ProductDestinationCountry!)),
                include: source => source
                    .Include(x => x.FollowUpActions!),
                orderBy: source => source.OrderBy(x => x.Order)))
            .First();

        if (transactionResultDefinitionType.FollowUpActions?.Any() ?? false) {
            foreach (var followUpAction in transactionResultDefinitionType.FollowUpActions)
                await CreateFollowUpAction(scanInputDetails.TransactionId, followUpAction.FollowUpActionTypeId);

            return (await _transactionStatusTypeRepo
                .GetManyAsync(statusType => statusType.Type!.Equals(SharedConstants.Constants.TransactionStatusType.PendingFollowUpAction)))
                .First()
                .Id;
        }

        await CreateTransactionResult(scanInputDetails.TransactionId, transactionResultDefinitionType.DesiredTransactionResultTypeId!.Value);

        return (await _transactionStatusTypeRepo
            .GetManyAsync(statusType => statusType.Type!.Equals(SharedConstants.Constants.TransactionStatusType.Completed)))
            .First()
            .Id;
    }

    private async Task CreateFollowUpAction(int transactionId, int followUpActionTypeId) {
        var followUpAction = new FollowUpAction() {
            TransactionId = transactionId,
            IsCompleted = false,
            PickedOptionId = null,
            TypeId = followUpActionTypeId
        };

        await _followUpActionRepo.CreateAsync(followUpAction);
    }

    private async Task CreateTransactionResult(int transactionId, int typeId) {
         var existingResult = (await _transactionResultRepo
            .GetManyAsync(details => details.TransactionId == transactionId))
            .FirstOrDefault();

        var transactionResult = new TransactionResult() {
            Id = existingResult?.Id ?? default,
            TransactionId = transactionId,
            TypeId = typeId
        };

        await _transactionResultRepo.UpdateAsync(transactionResult);
    }
}
