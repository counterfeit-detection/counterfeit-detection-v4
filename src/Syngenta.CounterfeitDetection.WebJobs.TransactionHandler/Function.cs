﻿using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Interfaces;

namespace Syngenta.CounterfeitDetection.WebJob.TransactionHandler {
    public class Function {
        private readonly ITransactionHandlerService _service;
        private readonly ILogger<Function> _logger;

        public Function(ITransactionHandlerService service, ILogger<Function> logger) {
            _service = service;
            _logger = logger;
        }

        [FunctionName(nameof(TransactionHandler))]
        public async Task TransactionHandler([TimerTrigger("%WebJobs:TransactionHandler:Timer%", RunOnStartup = true)] TimerInfo timer) {
            _logger.LogInformation($"{nameof(TransactionHandler)} WebJob started.");

            await _service.ExecuteAsync();

            _logger.LogInformation($"{nameof(TransactionHandler)} WebJob finished.");
        }
    }
}