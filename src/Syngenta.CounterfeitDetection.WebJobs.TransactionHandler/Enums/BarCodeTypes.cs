﻿using System.ComponentModel;

namespace Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Enums;

internal enum BarCodeTypes {
    [Description("QR Code")]
    QRCode = 1,
    [Description("Data Matrix Code")]
    DataMatrixCode = 2,
    [Description("Unknown")]
    Unknown = 3,
}
