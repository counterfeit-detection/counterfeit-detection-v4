﻿using System.ComponentModel;

namespace Syngenta.CounterfeitDetection.WebJob.TransactionHandler.Enums;

internal enum SAPResultTypes {
    [Description("Valid: low risk")]
    LowRisk = 1,

    [Description("Valid: med risk")]
    MedRisk = 2,

    [Description("Valid: high risk")]
    HighRisk = 3,

    [Description("Invalid: tracking ID is not available")]
    TrackingIdNotAvailable = 102,

    [Description("Invalid: tracking ID is not active")]
    TrackingIdNotActive = 103,

    [Description("Invalid: tracking ID is missing")]
    TrackingIdMissing = 105,

    [Description("Invalid: tracking ID is invalid")]
    TrackingIdInvalid = 106,

    [Description("Blacklisted")]
    Blacklisted = 107,

    [Description("Invalid: authentication failed")]
    AuthenticationFailed = 108,

    [Description("Relabelled code")]
    Relabelled = 109,

    [Description("Invalid: GTIN doesn't exist")]
    NonExistingGTIN = 110,

    [Description("Invalid: SN doesn't exist")]
    NonExistingSN = 111,

    [Description("Stolen item")]
    Stolen = 112,
}
