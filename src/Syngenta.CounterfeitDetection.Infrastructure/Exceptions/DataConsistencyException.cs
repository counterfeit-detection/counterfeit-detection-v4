namespace Syngenta.CounterfeitDetection.Infrastructure.Exceptions;

/// <summary>
/// An exception type to throw in case of data inconsistency in database
/// (e.g. missing mandatory foreign key relations, etc.)
/// </summary>
public class DataConsistencyException : Exception { 
    public DataConsistencyException() : base() { }
    public DataConsistencyException(string? message) : base(message) { }
    public DataConsistencyException(string? message, Exception innerException)
        : base (message, innerException) { }
}