namespace Syngenta.CounterfeitDetection.Infrastructure.Exceptions;

/// <summary>
/// An exception type to throw in case of master data discrepancies in database
/// </summary>
public class MasterDataException : Exception { 
    public MasterDataException() : base() { }
    public MasterDataException(string? message) : base(message) { }
    public MasterDataException(string? message, Exception innerException)
        : base (message, innerException) { }
}