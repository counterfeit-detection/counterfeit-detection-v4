using System.Diagnostics.CodeAnalysis;

namespace Syngenta.CounterfeitDetection.Infrastructure.Models;

public struct Geolocation {
    private float _latitude;
    private float _longitude;

    public float Latitude { 
        get => _latitude;
        set => _latitude = value <= 90 && value >= -90 
                   ? value 
                   : throw new ArgumentOutOfRangeException("Latitude value should be between -90 and 90"); 
    }
    public float Longitude { 
        get => _longitude;
        set => _longitude = 
                   value <= 180 && value >= -180 
                   ? value
                   : throw new ArgumentOutOfRangeException("Longitude value should be between -180 and 180");
    }

    public Geolocation() => _longitude = _latitude = 0;
    public Geolocation(float latitude, float longitude) => (_latitude, _longitude) = (latitude, longitude);
    public Geolocation(string location) {
        var values = location.Split(',');
        if (float.TryParse(values[0], out _latitude) || float.TryParse(values[1], out _longitude)) 
            throw new ArgumentException($"Invalid location value: <{location}>");
    }

    public static bool operator == (Geolocation a, Geolocation b) => a.Equals(b);  
    public static bool operator != (Geolocation a, Geolocation b) => !a.Equals(b);  

    public override bool Equals([NotNullWhen(true)] object? obj) =>
        obj is Geolocation o &&
        o.Latitude == this._latitude &&
        o.Longitude == this._longitude;

    public override int GetHashCode() => HashCode.Combine(_latitude, _longitude);

    public override string ToString() => $"{Latitude},{Longitude}";
}