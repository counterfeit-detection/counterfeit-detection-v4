namespace Syngenta.CounterfeitDetection.Infrastructure.Models;

public class Theme {
    public string Main { get; set; } = "default";
    public string Dark { get; set; } = "default";
    public string Light { get; set; } = "default";
    public string Contrast { get; set; } = "default";
}