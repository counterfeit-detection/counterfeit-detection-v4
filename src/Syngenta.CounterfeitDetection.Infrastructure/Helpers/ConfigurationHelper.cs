﻿using Microsoft.Extensions.Configuration;

namespace Syngenta.CounterfeitDetection.Infrastructure.Helpers;

public static class ConfigurationHelper {
    private static IConfiguration? Config {get; set;} = null;

    public static void Initialize(IConfiguration? configuration) 
        => Config = Config is null 
        ? configuration ?? throw new ArgumentNullException(nameof(configuration)) 
        : Config;

    public static string Get(string key) => Config?.GetSection("AppSettings")[key] 
        ?? throw new KeyNotFoundException($"{nameof(key)} not presented in Application Settings");
}
