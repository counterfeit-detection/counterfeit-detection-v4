﻿using System.ComponentModel;

namespace Syngenta.CounterfeitDetection.Infrastructure.Helpers;

public static class EnumExtensions {
    public static string GetEnumDescription(Enum value)
    {
        var fi = value.GetType().GetField(value.ToString());
        var attributes = (DescriptionAttribute[])fi!.GetCustomAttributes(typeof(DescriptionAttribute), false);

        return attributes?.Any() ?? false 
            ? attributes.First().Description 
            : value.ToString();
    }

    public static T GetValueFromDescription<T>(string description) where T : Enum
    {
        foreach (var field in typeof(T).GetFields())
        {
            if (Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) is DescriptionAttribute attribute)
            {
                if (attribute.Description.Equals(description))
                {
                    return (T)field.GetValue(null)!;
                }
            }
            else
            {
                if (field.Name.Equals(description))
                {
                    return (T)field.GetValue(null)!;
                }
            }
        }

        throw new ArgumentException("Not found.", nameof(description));
    }
}
