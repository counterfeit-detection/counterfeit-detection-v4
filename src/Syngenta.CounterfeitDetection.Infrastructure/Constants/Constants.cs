﻿using System.Drawing;

namespace Syngenta.CounterfeitDetection.Infrastructure.Constants;

public class Constants {
    public const string AppName = "AppName";
    public const string Token = "Token";

    public class TransactionStatusType {
        public const string Pending = "Pending";
        public const string PendingFollowUpAction = "Pending follow-up action";
        public const string Completed = "Completed";
    }

    public class BarcodeType {
        public const string QRCode = "QR_CODE";
        public const string DataMatrixCCode = "DATA_MATRIX";
        public const string Unknown = "UNKNOWN";
    }

    public class TranslationKeys {
        public const string ScanResult_TransactionTtillPending = "scanresult.transaction-still-pending.title";
        public const string ScanData_Fingerprint = "scandata.fingerprint";
        public const string ScanData_Location = "scandata.location";
        public const string ScanData_Time = "scandata.time";
        public const string ScanData_GTIN = "scandata.gtin";
        public const string ScanData_RawData = "scandata.raw-data";
        public const string ScanData_BarcodeType = "scandata.barcode-type";
        public const string ScanData_SerialNumber = "scandata.serial-number";
        public const string ScanData_NumberOfScans = "scandata.number-of-scans";
        public const string Action_Report = "action.report";
        public const string Action_Review = "action.review";
    }

    public class ActionTypes {
        public const string Report = "report";
        public const string Review = "review";
    }

    public class FollowUpActionTypes {
        public const string ScanningPointDetailsRequest = "Scanning point details request";
    }

    public class Themes {
        public const string First = "First";
        public const string Second = "Second";
        public const string Third = "Third";
    }
}
