﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Syngenta.CounterfeitDetection.Business.Core.Implementations;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess;
using Syngenta.CounterfeitDetection.Business.Integrations.Implementations;
using Syngenta.CounterfeitDetection.Business.Integrations.Interfaces;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Syngenta.CounterfeitDetection.Business.UnitTests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]

namespace Syngenta.CounterfeitDetection.Business; 

public static class Bootstrapper {
    public static IServiceCollection RegisterBusiness(
        this IServiceCollection services,
        IConfigurationSection connectionStrings
    ) {
        // DAL
        services.RegisterDataAccess(connectionStrings);
        
        // core services
        services.AddScoped<IScanProcessingService, ScanProcessingService>();
        services.AddScoped<IScanHistoryService, ScanHistoryService>();
        services.AddScoped<IReportService, ReportService>();
        services.AddScoped<ITranslationService, TranslationService>();
        services.AddScoped<IClientSettingService, ClientSettingService>();
        services.AddScoped<IProductService, ProductService>();
        services.AddScoped<ISAPIntegrationService, SAPIntegrationService>();

        services.AddScoped<IBlobStorageIntegrationService, BlobStorageIntegrationService>();
        
        return services;
    } 

    public static async Task UpdateDatabase(this IServiceScope services, bool seedMasterData = false) {
        await services.ApplyMigrations();
        if (seedMasterData)
            await services.SeedMasterData();
    }
}