using AutoMapper;
using Syngenta.CounterfeitDetection.Business.Integrations.Models;
using Syngenta.CounterfeitDetection.Infrastructure.Models;
using BLCore = Syngenta.CounterfeitDetection.Business.Core.Models;
using DL = Syngenta.CounterfeitDetection.DataAccess.Models;

namespace Syngenta.CounterfeitDetection.Business.Mappings;

public class MappingProfile : Profile {
    private const string Dash = "-";
    public MappingProfile() {
        CreateMap<Tuple<BLCore.UserData, DL.Country, DL.Role>, DL.User>()
            .ForMember(dest => dest.AuthId, opt => opt.MapFrom(src => src.Item1.Login))
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Item1.Name))
            .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src => src.Item2.Id))
            .ForMember(dest => dest.RoleId, opt => opt.MapFrom(src => src.Item3.Id));

        CreateMap<BLCore.ScanEntry, DL.ScanInputDetails>()
            .ForMember(dest => dest.ScanTime, opt => opt.MapFrom(src => src.Time))
            .ForMember(dest => dest.RawData, opt => opt.MapFrom(src => src.Code))
            .ForMember(dest => dest.CodeType, opt => opt.Ignore()); // custom mapping requiring DB interaction
        CreateMap<BLCore.ReportEntry, DL.Report>()
            .ForMember(dest => dest.ScanningPoint, opt => opt.Ignore()) // custom mapping requiring DB interaction
            .ForMember(dest => dest.Comment, opt => opt.MapFrom(src => src.Comments))
            .ForMember(dest => dest.Images, opt => opt.MapFrom(_ => new List<DL.ReportImage>())); // custom mapping requiring external system interaction

        CreateMap<Tuple<DL.Transaction, string>, BLCore.ScanTransaction>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Item1.Id))
            .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Item1.Status!.Type))
            .ForMember(dest => dest.CreationTime, opt => opt.MapFrom(src => src.Item1.CreationTime))
            .ForMember(dest => dest.LastUpdateTime, opt => opt.MapFrom(src => src.Item1.LastUpdateTime))
            .ForMember(dest => dest.Result, opt => opt.Ignore())
            .ForMember(dest => dest.ScanData, opt => opt.MapFrom(src => new Tuple<DL.Transaction, string>(src.Item1, src.Item2)))
            .ForMember(dest => dest.ReportData, opt => opt.MapFrom(src => src.Item1.TransactionReport ?? null))
            .ForMember(dest => dest.FollowUp, opt => opt.Ignore())
            .ForMember(dest => dest.Actions, opt => opt.Ignore());
        CreateMap<Tuple<DL.Transaction, string>, IEnumerable<KeyValuePair<string, string>>>()
            .ConvertUsing<ScanInputDetailsToKeyValueCollectionConverter>();
        CreateMap<IEnumerable<DL.ThemeColorsSetting>, BLCore.ThemeSettings>()
            .ConvertUsing<ThemeClientSettingsDataConverter>();
        CreateMap<DL.Report, BLCore.ReportEntry>()
            .ForMember(dest => dest.ScanningPoint, opt => opt.MapFrom(src => src.ScanningPoint != null ? src.ScanningPoint.Point : null))
            .ForMember(dest => dest.Comments, opt => opt.MapFrom(src => src.Comment))
            .ForMember(dest => dest.Images, opt => opt.MapFrom(
                src => src.Images != null && src.Images.Any(i => i.ImageFile != null) 
                    ? src.Images.Where(i => i.ImageFile != null).Select(i => i.ImageFile!.StorageUrl) 
                    : null));
        CreateMap<DL.ScanResultRepresentationRule, BLCore.ScanResultStyle>()
            .ForMember(dest => dest.MessageColor, opt => opt.MapFrom(src => src.ResultActionColor))
            .ForMember(dest => dest.TitleColor, opt => opt.MapFrom(src => src.ResultTitleColor));

        CreateMap<DL.Report, BLCore.ReportEntry>()
            .ForMember(dest => dest.Comments, opt => opt.MapFrom(src => src.Comment))
            .ForMember(dest => dest.ScanningPoint, opt => opt.MapFrom(src => src.ScanningPoint!.Point))
            .ForMember(dest => dest.Images, opt => opt.MapFrom(src => src.Images != null ? src.Images.Select(x => x.ImageFile!.StorageUrl) : null));
        CreateMap<DL.ReportReasonSetting, BLCore.ReportReason>()
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.GeneralReportReason!.Reason));
        CreateMap<DL.ScanningPointSetting, BLCore.ScanOrigin>()
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.ScanningPoint!.Point));
        CreateMap<DL.ClientFeatureSetting, BLCore.ClientFeatures>()
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.ClientFeature!.Name));
        CreateMap<DL.ScanResultRepresentationRule, BLCore.ScanResultStyle>()
            .ForMember(dest => dest!.TitleColor, opt => opt.MapFrom(src => src.ResultTitleColor))
            .ForMember(dest => dest!.MessageColor, opt => opt.MapFrom(src => src.ResultActionColor));
        CreateMap<DL.ScanResultRepresentationRule, BLCore.ScanResult>()
            .ForMember(dest => dest.TypeId, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.ResultTitleKey))
            .ForMember(dest => dest.Message, opt => opt.MapFrom(src => src.ResultActionKey));
        CreateMap<DL.Language, BLCore.Languages>();
        CreateMap<DL.Client, BLCore.ClientSettings>();

        CreateMap<IEnumerable<DL.ProductBrand>, IEnumerable<BLCore.ProductData>>()
            .ConvertUsing<ProductCatalogueConverter>();
            
        CreateMap<ProductJourneyEntry, BLCore.ProductJourneyStep>()
            .ForMember(dest => dest.Location, opt => opt.MapFrom(src => ParseGeoLocation(src.Latitude, src.Longitude)))
            .ForMember(dest => dest.ShippingId, opt => opt.MapFrom(src => src.Evtid))
            .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Userid ?? Dash))
            .ForMember(dest => dest.SalesRep, opt => opt.MapFrom(src => src.Userid ?? Dash))
            .ForMember(dest => dest.ShippingEventTime, opt => opt.MapFrom(src => src.Eventdatetime))
            .ForMember(dest => dest.AuthenticationEventTime, opt => opt.MapFrom(src => src.Eventdatetime));

        CreateMap<DL.ProductPackSizeGTIN, BLCore.ProductData>()
            .ConvertUsing<ProductPackSizeGTINIntoProductDataConverter>();
    }

    private Geolocation? ParseGeoLocation(string? latitude, string? longitude)
        => latitude is null || longitude is null 
                ? null 
                : Single.TryParse(latitude.Replace(Dash, String.Empty), out var parsedLatitude) && Single.TryParse(longitude.Replace(Dash, String.Empty),out var parsedLongitude) 
                    ? new Geolocation(parsedLatitude, parsedLongitude)
                    : null;
} 