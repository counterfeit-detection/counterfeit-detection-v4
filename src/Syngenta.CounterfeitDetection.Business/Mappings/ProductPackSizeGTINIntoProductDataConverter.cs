﻿using AutoMapper;
using Syngenta.CounterfeitDetection.Business.Core.Models;
using Syngenta.CounterfeitDetection.DataAccess.Models;

namespace Syngenta.CounterfeitDetection.Business.Mappings;

internal class ProductPackSizeGTINIntoProductDataConverter
    : ITypeConverter<ProductPackSizeGTIN, ProductData> {
    public ProductData Convert(ProductPackSizeGTIN source, ProductData destination, ResolutionContext context) {
        var result = new ProductData(){
            Id = source.PackSize!.ProductCatalogue!.Id,
            Name = source.PackSize!.ProductCatalogue!.Name!,
            IsUnderGPD = source.IsGPD,
            GTIN = source.GTIN!,
            Capacity = $"{source.PackSize!.Size} {source.PackSize!.MeasureUnit?.Unit}"
        };

        return result;
    }
}
