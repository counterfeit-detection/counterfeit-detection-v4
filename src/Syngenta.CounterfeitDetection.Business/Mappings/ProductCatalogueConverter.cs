﻿using AutoMapper;
using Syngenta.CounterfeitDetection.Business.Core.Models;
using Syngenta.CounterfeitDetection.DataAccess.Models;

namespace Syngenta.CounterfeitDetection.Business.Mappings;

internal class ProductCatalogueConverter
    : ITypeConverter<IEnumerable<ProductBrand>, IEnumerable<ProductData>> {
    public IEnumerable<ProductData> Convert(IEnumerable<ProductBrand> source, IEnumerable<ProductData> destination, ResolutionContext context) {
        if (source is null) {
            return new List<ProductData>();
        }

        var result = source.SelectMany(
            product => product.ProductPackSizes!.SelectMany(
                packSize => packSize.ProductPackSizeGTINs!.Select(
                    packSizeGTIN => new ProductData {
                        Id = product.Id,
                        Name = product.Name!,
                        IsUnderGPD = packSizeGTIN.IsGPD,
                        GTIN = packSizeGTIN.GTIN!,
                        Capacity = $"{packSize.Size} {packSize.MeasureUnit?.Unit}"
                    })));

        return result;
    }
}
