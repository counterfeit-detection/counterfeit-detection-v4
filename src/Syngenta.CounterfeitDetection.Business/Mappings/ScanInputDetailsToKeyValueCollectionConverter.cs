using AutoMapper;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Models;
using Syngenta.CounterfeitDetection.Infrastructure.Constants;

namespace Syngenta.CounterfeitDetection.Business.Mappings;

public class ScanInputDetailsToKeyValueCollectionConverter 
    : ITypeConverter<Tuple<Transaction, string>, IEnumerable<KeyValuePair<string, string>>>
{
    private readonly ITranslationService _translationService;

    public ScanInputDetailsToKeyValueCollectionConverter(ITranslationService translationService) 
        => _translationService = translationService;

    public IEnumerable<KeyValuePair<string, string>> Convert(
        Tuple<Transaction, string> src,
        IEnumerable<KeyValuePair<string, string>> dest,
        ResolutionContext context
    ) {
        var result = new List<KeyValuePair<string, string>>();
        var details = src.Item1.ScanInputDetails;
        var languageCode = src.Item2;

        // todo: enable feature configuration check
        if (details?.Fingerprint is not null) {
            result.Add(new KeyValuePair<string, string>(_translationService
                .GetTranslation(Constants.TranslationKeys.ScanData_Fingerprint, languageCode).GetAwaiter().GetResult(), details.Fingerprint));
        }

        if (details?.RawData is not null) {
            result.Add(new KeyValuePair<string, string>(_translationService
                .GetTranslation(Constants.TranslationKeys.ScanData_RawData, languageCode).GetAwaiter().GetResult(), details.RawData));
        }

        if (details?.ScanTime is not null) {
            result.Add(new KeyValuePair<string, string>(_translationService
                .GetTranslation(Constants.TranslationKeys.ScanData_Time, languageCode).GetAwaiter().GetResult(), details.ScanTime.ToString("yyyy-MM-ddThh:mm:ssZ"))); // todo: enable configurable date formats
        }

        if (src.Item1?.GeoLocation is not null) {
            result.Add(new KeyValuePair<string, string>(_translationService
                .GetTranslation(Constants.TranslationKeys.ScanData_Location, languageCode).GetAwaiter().GetResult(), src.Item1!.GeoLocation!));
        }

        if (details?.CodeType?.Name is not null) {
            result.Add(new KeyValuePair<string, string>(_translationService
                .GetTranslation(Constants.TranslationKeys.ScanData_BarcodeType, languageCode).GetAwaiter().GetResult(), details.CodeType.Name));
        }

        dest = result;
        return dest;
    }
}