﻿using AutoMapper;
using Syngenta.CounterfeitDetection.Business.Core.Models;
using Syngenta.CounterfeitDetection.DataAccess.Models;
using Infra = Syngenta.CounterfeitDetection.Infrastructure.Models;
using SharedConstants = Syngenta.CounterfeitDetection.Infrastructure.Constants;

namespace Syngenta.CounterfeitDetection.Business.Mappings;

public class ThemeClientSettingsDataConverter
    : ITypeConverter<IEnumerable<ThemeColorsSetting>, ThemeSettings> {
    public ThemeSettings Convert(
        IEnumerable<ThemeColorsSetting> src,
        ThemeSettings dest,
        ResolutionContext context
    ) {
        var res = new ThemeSettings();

        var first = src?.FirstOrDefault(x => x.Theme!.Name!.Equals(SharedConstants.Constants.Themes.First));
        var second = src?.FirstOrDefault(x => x.Theme!.Name!.Equals(SharedConstants.Constants.Themes.Second));
        var third = src?.FirstOrDefault(x => x.Theme!.Name!.Equals(SharedConstants.Constants.Themes.Third));

        if (first is not null) {
            res.Primary = new Infra.Theme {
                Dark = first.Dark!,
                Main = first.Main!,
                Light = first.Light!,
                Contrast = first.ContrastText!,
            };
        }
        if (second is not null) {
            res.Secondary = new Infra.Theme {
                Dark = second.Dark!,
                Main = second.Main!,
                Light = second.Light!,
                Contrast = second.ContrastText!,
            };
        }
        if (third is not null) {
            res.Tertiary = new Infra.Theme {
                Dark = third.Dark!,
                Main = third.Main!,
                Light = third.Light!,
                Contrast = third.ContrastText!,
            };
        }

        return res;
    }
}
