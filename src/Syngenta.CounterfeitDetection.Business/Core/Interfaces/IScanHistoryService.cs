using Syngenta.CounterfeitDetection.Business.Core.Models;

namespace Syngenta.CounterfeitDetection.Business.Core.Interfaces;

public interface IScanHistoryService {
    Task<ScanHistoryPage> GetScans(ScanHistoryFilter filter);
    Task<int> GetScansCount(ScanHistoryFilter filter);
    Task<ScanTransaction> GetScanById(int id);
    Task<ScanResult> GetScanResult(int transactionId, int? languageId = null);
}