﻿namespace Syngenta.CounterfeitDetection.Business.Core.Interfaces;

public interface ITranslationService {
    public Task<string> GetTranslation(string key, int? languageId);
    public Task<string> GetTranslation(string key, string languageCode);
}
