﻿using Syngenta.CounterfeitDetection.Business.Core.Models;

namespace Syngenta.CounterfeitDetection.Business.Core.Interfaces;

public interface IProductService {

    Task<IEnumerable<ProductData>> GetProductCatalogue(string? countryCode);
    Task<IEnumerable<ProductJourneyStep>> GetProductJourney(string gtin, string serialNumber);
    Task<ProductData> GetProductDetails(string? gtin);
}
