using Syngenta.CounterfeitDetection.Business.Core.Models;

namespace Syngenta.CounterfeitDetection.Business.Core.Interfaces;

public interface IReportService {
    Task CreateScanReport(int scanId, ReportEntry report);
    Task<CreateDirectReportResult> CreateDirectReports(
        UserData user,
        ClientData client,
        IEnumerable<ReportData> reports
    );
} 