﻿using Syngenta.CounterfeitDetection.Business.Core.Models;

namespace Syngenta.CounterfeitDetection.Business.Core.Interfaces;

public interface IClientSettingService {
    Task<ClientSettings> GetClientSettings(
        string appName, 
        string countryCode, 
        string roleCode);
}
