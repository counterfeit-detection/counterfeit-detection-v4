using Syngenta.CounterfeitDetection.Business.Core.Models;

namespace Syngenta.CounterfeitDetection.Business.Core.Interfaces;

public interface IScanProcessingService {
    Task<CreateScanResult> CreateScans(
        UserData user,
        ClientData client,
        IEnumerable<ScanData> scans
    );

    Task SubmitFollowUpAction(FollowUpActionData data);

    Task MarkScanReviewed(int transactionId);
}