namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ReportEntry {
    public string ScanningPoint { get; set; } = string.Empty;
    public string ScanLocationName { get; set; } = string.Empty;
    public string ScanAddress { get; set; } = string.Empty;
    public int ReportReasonId { get; set; }
    public string CustomReportReason { get; set; } = string.Empty;
    public string Comments { get; set; } = string.Empty;
    public IEnumerable<string> Images { get; set; } = new List<string>();
}