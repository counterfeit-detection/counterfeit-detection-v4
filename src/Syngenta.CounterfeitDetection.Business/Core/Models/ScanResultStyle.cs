namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ScanResultStyle {
    public string? TitleColor { get; set; }
    public string? MessageColor { get; set; }
}