namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class CreatedReportData : ReportData {
    public int Id { get; set; }
}