namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ReportData {
    public ScanEntry ScanEntry { get; set; } = new ScanEntry();
    public ReportEntry ReportEntry { get; set; } = new ReportEntry();
}