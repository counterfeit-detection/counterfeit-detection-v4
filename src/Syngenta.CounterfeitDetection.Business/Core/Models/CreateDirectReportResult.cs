namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class CreateDirectReportResult {
    public IEnumerable<CreatedReportData>? Submitted { get; set; }
    public IEnumerable<ReportData>? Failed { get; set; }
}