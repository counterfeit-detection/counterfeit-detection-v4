﻿namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ScanOrigin {
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public int Order { get; set; }
}
