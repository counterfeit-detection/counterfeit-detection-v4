namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ScanHistoryPage {
    public int TotalScans { get; set; }
    public IEnumerable<ScanTransaction> Scans { get; set; } = new List<ScanTransaction>();
}