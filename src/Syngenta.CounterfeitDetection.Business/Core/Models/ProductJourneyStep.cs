﻿using Syngenta.CounterfeitDetection.Infrastructure.Models;

namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ProductJourneyStep {
    public string GTIN { get; set; } = String.Empty;
    public string SN { get; set; } = String.Empty;
    public Geolocation? Location { get; set; }
    public string ShippingId { get; set; } = String.Empty;
    public DateTime? ShippingEventTime { get; set; }
    public DateTime? AuthenticationEventTime { get; set; }
    public string UserName { get; set; } = String.Empty;
    public string SalesRep { get; set; } = String.Empty;
}