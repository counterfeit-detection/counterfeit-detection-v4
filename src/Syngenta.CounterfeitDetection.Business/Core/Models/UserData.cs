namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class UserData {
    public string Login { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
    public string CountryCode { get; set; } = string.Empty;
    public string RoleCode { get; set; } = string.Empty;
}