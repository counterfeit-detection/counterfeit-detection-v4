﻿using Syngenta.CounterfeitDetection.Infrastructure.Models;

namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ThemeSettings {
    public Theme? Primary { get; set; }
    public Theme? Secondary { get; set; }
    public Theme? Tertiary { get; set; }
}
