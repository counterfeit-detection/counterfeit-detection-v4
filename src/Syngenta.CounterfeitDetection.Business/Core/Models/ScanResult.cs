namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ScanResult {
    public int? TypeId { get; set; }
    public string Title { get; set; } = string.Empty;
    public string? Message { get; set; }
    public ScanResultStyle? Style { get; set; }
}