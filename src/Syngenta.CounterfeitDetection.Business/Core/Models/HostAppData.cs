namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class HostAppData {
    public string Device { get; set; } = string.Empty;
    public string OS { get; set; } = string.Empty;
    public string Version { get; set; } = string.Empty;
}