namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ScanResultAction {
    public string Action { get; set; } = string.Empty;
    public string Label { get; set; } = string.Empty;
}