﻿namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ClientFeatures {
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
}
