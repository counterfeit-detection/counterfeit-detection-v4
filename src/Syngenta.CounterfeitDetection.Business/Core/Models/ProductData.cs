﻿namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ProductData {
    public int Id { get; set; }
    public string Name { get; set; } = String.Empty;
    public string GTIN { get; set; } = String.Empty;
    public string Capacity { get; set; } = String.Empty;
    public bool IsUnderGPD { get; set; }
}
