using Syngenta.CounterfeitDetection.Infrastructure.Models;

namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ScanEntry {
    public string Fingerprint { get; set; } = String.Empty;
    public Geolocation Location { get; set; }
    public DateTime Time { get; set; }
    public string Code { get; set; } = String.Empty;
    public string CodeType { get; set; } = String.Empty;
    public bool IsManualEntry { get; set; }
    public bool IsOffline { get; set; }
}