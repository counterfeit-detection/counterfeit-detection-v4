namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ScanData {
    public ScanEntry ScanEntry { get; set; } = new ScanEntry();
    public ReportEntry? ReportData { get; set; }
}