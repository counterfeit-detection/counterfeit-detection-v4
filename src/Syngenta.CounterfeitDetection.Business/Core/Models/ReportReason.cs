﻿namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ReportReason {
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public bool RequireCustomReason { get; set; }
    public int Order { get; set; }
}
