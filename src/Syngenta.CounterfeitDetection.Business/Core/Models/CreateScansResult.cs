namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class CreateScanResult {
    public IEnumerable<ScanTransaction>? Submitted { get; set; }
    public IEnumerable<ScanData>? Failed { get; set; }
}