namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ScanFollowUpInput {
    public string Key { get; set; } = string.Empty;
    public string Type { get; set; } = string.Empty;
    public string Label { get; set; } = string.Empty;
    public string HelpText { get; set; } = string.Empty;
    public bool IsMandatory { get; set; }
}