namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ScanHistoryFilter {
    public string UserLogin { get; set; } = string.Empty;
    public string UserRoleCode { get; set; } = string.Empty;
    public string LanguageCode { get; set; } = string.Empty;
    public DateTime? After { get; set; }
    public DateTime? Before { get; set; }
    public IEnumerable<int>? ProductId { get; set; }
    public string? ResultType { get; set; }
    public bool? Reported { get; set; }
    public bool? Reviewed { get; set; }
    public bool? Offline { get; set; }
    public int Skip { get; set; }
    public int Take { get; set; }
}