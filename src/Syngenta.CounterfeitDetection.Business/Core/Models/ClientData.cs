namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ClientData {
    public string LanguageCode { get; set; } = string.Empty;
    public string HostAppName { get; set; } = string.Empty;
    public string PluginVersion { get; set; } = string.Empty;
    public HostAppData? HostAppDetails { get; set; }
}