namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ScanFollowUpOption {
    public int Id { get; set; }
    public string Label { get; set; } = string.Empty;
}