namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ScanTransaction {
    public int Id { get; set; }
    public string Status { get; set; } = String.Empty;
    public DateTime CreationTime { get; set; }
    public DateTime LastUpdateTime { get; set; }
    public ScanResult? Result { get; set; }
    public IEnumerable<KeyValuePair<string, string>>? ScanData { get; set; }
    public ReportEntry? ReportData { get; set; }
    public ScanFollowUpEntry? FollowUp { get; set; }
    public IEnumerable<ScanResultAction>? Actions { get; set; }
}