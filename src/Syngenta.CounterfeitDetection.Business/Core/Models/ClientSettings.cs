﻿namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ClientSettings {
    public ThemeSettings? Themes { get; set; }
    public IEnumerable<Languages> Languages { get; set; } = new List<Languages>();
    public IEnumerable<ScanResult> ResultTypes { get; set; } = new List<ScanResult>();
    public IEnumerable<ReportReason> ReportReasons { get; set; } = new List<ReportReason>();
    public IEnumerable<ScanOrigin> ScanOrigins { get; set; } = new List<ScanOrigin>();
    public IEnumerable<ClientFeatures> Features { get; set; } = new List<ClientFeatures>();
}
