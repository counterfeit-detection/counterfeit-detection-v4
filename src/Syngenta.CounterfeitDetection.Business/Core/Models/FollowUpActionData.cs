namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class FollowUpActionData {
    public int Id { get; set; }
    public int OptionId { get; set; }
    public IEnumerable<KeyValuePair<string, string>>? Data { get; set; }
}