namespace Syngenta.CounterfeitDetection.Business.Core.Models;

public class ScanFollowUpEntry {
    public int Id { get; set; }
    public string Title { get; set; } = string.Empty;
    public IEnumerable<ScanFollowUpOption>? Options { get; set; }
    public IEnumerable<ScanFollowUpInput>? Inputs { get; set; }
}