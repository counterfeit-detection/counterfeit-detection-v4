using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.Business.Core.Models;
using Syngenta.CounterfeitDetection.DataAccess.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Models;
using Syngenta.CounterfeitDetection.Infrastructure.Constants;
using Syngenta.CounterfeitDetection.Infrastructure.Exceptions;
using System.Linq.Expressions;

namespace Syngenta.CounterfeitDetection.Business.Core.Implementations;

internal class ScanHistoryService : IScanHistoryService {
    private readonly IGenericRepository<Transaction> _transactionRepository;
    private readonly IGenericRepository<TransactionResult> _transactionResultRepository;
    private readonly IGenericRepository<ScanResultRepresentationRule> _scanResultRepresentationRuleRepository;
    private readonly ITranslationService _translationService;
    private readonly IGenericRepository<Language> _languageRepository;
    private readonly IMapper _mapper;

    public ScanHistoryService(
        IGenericRepository<Transaction> transactionRepository,
        IGenericRepository<TransactionResult> transactionResultRepository,
        IGenericRepository<ScanResultRepresentationRule> scanResultRepresentationRuleRepository,
        ITranslationService translationService,
        IGenericRepository<Language> languageRepository,
        IMapper mapper
    ) {
        _transactionRepository = transactionRepository;
        _transactionResultRepository = transactionResultRepository;
        _scanResultRepresentationRuleRepository = scanResultRepresentationRuleRepository;
        _translationService = translationService;
        _languageRepository = languageRepository;
        _mapper = mapper;
    }

    public async Task<ScanHistoryPage> GetScans(ScanHistoryFilter filter) {
        var language = (await _languageRepository.GetManyAsync(lang => lang.Code!.Equals(filter.LanguageCode)))
            .FirstOrDefault() ?? throw new KeyNotFoundException($"Language with code <{filter.LanguageCode}> not presented in database");

        Expression<Func<Transaction, bool>> filterExpression =
            transaction => transaction.User!.AuthId!.Equals(filter.UserLogin)
                && (filter.ResultType == null || transaction.Result!.Type!.Type!.Equals(filter.ResultType))
                && (filter.After == null || transaction.CreationTime >= filter.After)
                && (filter.Before == null || transaction.CreationTime <= filter.Before)
                && (filter.Offline == null || transaction.ScanInputDetails!.IsOffline == filter.Offline)
                && (filter.Reviewed == null || (filter.Reviewed.Value ? transaction.TransactionReview != null : transaction.TransactionReview == null))
                && (filter.Reported == null || (filter.Reported.Value ? transaction.TransactionReport != null : transaction.TransactionReport == null));

        var totalScans = (await _transactionRepository
            .GetManyAsync(filterExpression))
            .Count();

        var scanTransactions = await _transactionRepository
            .GetManyAsync(filterExpression,
                orderBy: transactions => transactions
                    .OrderByDescending(transaction => transaction.CreationTime),
                skip: filter.Skip,
                take: filter.Take,
                include: transactions => transactions
                    .Include(x => x.ScanInputDetails!)
                        .ThenInclude(x => x.CodeType)
                    .Include(x => x.Status!)
                    .Include(x => x.FollowUpActions!)
                        .ThenInclude(x => x.Type!)
                        .ThenInclude(x => x.Options!)
                    .Include(x => x.SapDetails!)
                    .Include(x => x.ItemDetails!)
                    .Include(x => x.TransactionReview!)
                    .Include(x => x.User!)
                    .Include(x => x.TransactionReport!)
                        .ThenInclude(x => x.Images!)
                            .ThenInclude(x => x.ImageFile!));

        var scans = scanTransactions.Select(async transaction => {
            var scanTransaction = _mapper.Map<ScanTransaction>(new Tuple<Transaction, string>(transaction, filter.LanguageCode));

            scanTransaction.Result = await GetScanResult(transaction.Id, language.Id);

            //TODO: replace with automapper mapping
            var scanData = new List<KeyValuePair<string, string>> {
                    new KeyValuePair<string, string>(
                        await _translationService.GetTranslation(Constants.TranslationKeys.ScanData_GTIN, language.Id), transaction.ItemDetails?.GTIN ?? String.Empty),
                    new KeyValuePair<string, string>(
                        await _translationService.GetTranslation(Constants.TranslationKeys.ScanData_SerialNumber, language.Id), transaction.ItemDetails?.SerialNumber ?? String.Empty),
                    new KeyValuePair<string, string>(
                        await _translationService.GetTranslation(Constants.TranslationKeys.ScanData_NumberOfScans, language.Id), transaction.SapDetails?.NumberOfScans.ToString() ?? String.Empty)
            };
            scanData.AddRange(scanTransaction.ScanData!);

            scanTransaction.ScanData = scanData;

            var folowUp = transaction.FollowUpActions == null ? default : transaction.FollowUpActions
            .FirstOrDefault(folowUp => !folowUp.IsCompleted && !folowUp.Type!.IsAutomatic);

            scanTransaction.FollowUp = folowUp is null ? default : new ScanFollowUpEntry {
                Id = folowUp.Id,
                Title = folowUp.Type!.Type!,
                Options = folowUp?.Type?.Options == null ? default : folowUp.Type.Options.OrderBy(x => x.Order).Select(option => new ScanFollowUpOption() {
                    Id = option.Id,
                    Label = option.Option!
                }),
                Inputs = defineFollowUpActionInputs(folowUp?.Type?.Type)
            };

            //TODO: replace with FollowUpProcessorFactory in GPDCE-3854
            scanTransaction.Actions = await defineActions(transaction);

            return scanTransaction;
        }).Select(t => t.Result);

        return new ScanHistoryPage {
            TotalScans = totalScans,
            Scans = scans
        };

        //TODO: replace with FollowUpProcessorFactory in GPDCE-3854
        IEnumerable<ScanFollowUpInput>? defineFollowUpActionInputs(string? type) => type switch {
            Constants.FollowUpActionTypes.ScanningPointDetailsRequest => new List<ScanFollowUpInput> {
                    new ScanFollowUpInput {
                        Key = "input.shop-name",
                        Type = "input",
                        Label = "Shop name",
                        HelpText = "Name of the place you've bought the product at",
                        IsMandatory = true
                    },
                    new ScanFollowUpInput {
                        Key = "input.shop-address",
                        Type = "input",
                        Label = "Shop address",
                        HelpText = "Address of the place you've bought the product at",
                        IsMandatory = true
                    },
                    new ScanFollowUpInput {
                        Key = "datetime.purchase-time",
                        Type = "datetime",
                        Label = "Purchase time",
                        HelpText = "A day when have you bought the product",
                        IsMandatory = true
                    },
                }.AsEnumerable(),
            _ => default
        };

        async Task<IEnumerable<ScanResultAction>?> defineActions(Transaction transaction) {
            var actions = new List<ScanResultAction>();

            if (transaction.Status!.Type!.Equals(Constants.TransactionStatusType.Pending)) {
                return default;
            }

            if (transaction.TransactionReport is not null || transaction.TransactionReview is not null) {
                return default;
            }

            actions.Add(new ScanResultAction {
                Action = Constants.ActionTypes.Report,
                Label = await _translationService.GetTranslation(Constants.TranslationKeys.Action_Report, language.Id)
            });

            actions.Add(new ScanResultAction {
                Action = Constants.ActionTypes.Review,
                Label = await _translationService.GetTranslation(Constants.TranslationKeys.Action_Review, language.Id)
            });

            return actions;
        }
    }

    // mockup
    public async Task<ScanTransaction> GetScanById(int id) {
        await Task.CompletedTask;
        return new ScanTransaction {
            Id = 1,
            Status = "Pending",
            CreationTime = new DateTime(2022, 12, 31, 12, 30, 0),
            LastUpdateTime = new DateTime(2022, 12, 31, 12, 30, 0),
            ScanData = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("Time", "2022-12-31T12:30:00.000Z"),
                new KeyValuePair<string, string>("GTIN", "01234567890123"),
                new KeyValuePair<string, string>("SN", "SN12345678"),
            },
            ReportData = new ReportEntry() {
                ScanningPoint = "Retailer",
                ScanLocationName = "The Store, Inc.",
                ScanAddress = "Anywhere Town, 1",
                ReportReasonId = 1,
                CustomReportReason = "wtf",
                Comments = "no",
                Images = new List<string> { "123" }
            },
        };
    }

    public async Task<ScanResult> GetScanResult(int transactionId, int? languageId = null) {
        if (transactionId <= 0) {
            throw new ArgumentOutOfRangeException(nameof(transactionId), "Transaction ID must be greter greater 0");
        }

        var transaction = (await _transactionRepository
            .GetManyAsync(transaction => transaction.Id == transactionId,
                include: transactions => transactions
                    .Include(x => x.Status!)
                    .Include(x => x.ScanInputDetails!)
                        .ThenInclude(x => x.ClientAppType!)))
            .FirstOrDefault() ?? throw new KeyNotFoundException($"Transaction with ID {transactionId} not found");

        if (transaction.Status!.Type!.Equals(Constants.TransactionStatusType.Pending)) {
            var translatedTitle =
                await _translationService.GetTranslation(Constants.TranslationKeys.ScanResult_TransactionTtillPending, languageId);

            return new ScanResult() {
                Title = await _translationService.GetTranslation(Constants.TranslationKeys.ScanResult_TransactionTtillPending, languageId)
            };
        }

        var transactionResult = (await _transactionResultRepository
                .GetManyAsync(transactionResult => transactionResult.TransactionId == transactionId))
            .FirstOrDefault() ?? throw new DataConsistencyException($"Result for transaction with ID {transactionId} not found");

        var rule = (await _scanResultRepresentationRuleRepository
                .GetManyAsync(rule => (rule.CountryId == null || rule.CountryId == transaction.UserCountryId)
                    && (rule.ClientId == null || rule.ClientId == transaction.ScanInputDetails!.ClientAppType!.ClientId)
                    && (rule.RoleId == null || rule.RoleId == transaction.UserRoleId)
                    && (rule.TransactionResultTypeId == transactionResult.TypeId),
                    include: rules => rules
                        .Include(x => x.MasterResultTitle!)
                            .ThenInclude(x => x.LanguageTranslations!)
                        .Include(x => x.MasterResultAction!)
                            .ThenInclude(x => x.LanguageTranslations!),
                    orderBy: rules => rules.OrderBy(rule => rule.Order)))
            .FirstOrDefault();

        return new ScanResult() {
            TypeId = transactionResult.TypeId,
            Title = rule!.MasterResultTitle?.LanguageTranslations?
                .FirstOrDefault(translation => translation.LanguageId == languageId)?.Value
                ?? rule!.MasterResultTitle?.DefaultValue ?? rule.ResultTitleKey!,
            Message = rule!.MasterResultAction?.LanguageTranslations?
                .FirstOrDefault(translation => translation.LanguageId == languageId)?.Value
                ?? rule.MasterResultAction?.DefaultValue ?? rule.ResultActionKey!,
            Style = _mapper.Map<ScanResultStyle>(rule)
        };
    }

    public async Task<int> GetScansCount(ScanHistoryFilter filter) {
        Expression<Func<Transaction, bool>> filterExpression =
            transaction => transaction.User!.AuthId!.Equals(filter.UserLogin)
                && (filter.ResultType == null || transaction.Result!.Type!.Type!.Equals(filter.ResultType))
                && (filter.After == null || transaction.CreationTime >= filter.After)
                && (filter.Before == null || transaction.CreationTime <= filter.Before)
                && (filter.Offline == null || transaction.ScanInputDetails!.IsOffline == filter.Offline)
                && (filter.Reviewed == null || (filter.Reviewed.Value ? transaction.TransactionReview != null : transaction.TransactionReview == null))
                && (filter.Reported == null || (filter.Reported.Value ? transaction.TransactionReport != null : transaction.TransactionReport == null));

        var totalScans = (await _transactionRepository
            .GetManyAsync(filterExpression))
            .Count();

        return totalScans;
    }
}