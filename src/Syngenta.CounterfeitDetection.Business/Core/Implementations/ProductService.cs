﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.Business.Core.Models;
using Syngenta.CounterfeitDetection.Business.Integrations.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Models;

namespace Syngenta.CounterfeitDetection.Business.Core.Implementations;
internal class ProductService : IProductService {
    private const string AuthenticationEventType = "authentication";

    private readonly IGenericRepository<ProductBrand> _productRepo;
    private readonly IGenericRepository<Country> _countryRepo;
    private readonly ISAPIntegrationService _sapIntegrationService;
    private readonly IGenericRepository<ProductPackSizeGTIN> _productPackSizeGTINRepo;
    private readonly IMapper _mapper;

    public ProductService(
        IGenericRepository<ProductBrand> productRepo,
        IGenericRepository<Country> countryRepo,
        ISAPIntegrationService sapIntegrationService,
        IGenericRepository<ProductPackSizeGTIN> productPackSizeGTINRepo,
        IMapper mapper
    ) {
        _productRepo = productRepo;
        _mapper = mapper;
        _countryRepo = countryRepo;
        _productPackSizeGTINRepo = productPackSizeGTINRepo;
        _sapIntegrationService = sapIntegrationService;
    }

    public async Task<IEnumerable<ProductData>> GetProductCatalogue(string? countryCode) {
        var countryId = (await _countryRepo.GetManyAsync(country => country.Code!.Equals(countryCode)))
            .FirstOrDefault()?.Id
            ?? (countryCode is null
                ? null
                : throw new KeyNotFoundException($"Unable to found <Country> with code: <{countryCode}>"));

        var products = await _productRepo.GetManyAsync(product => countryCode == null
                || product.ProductCountry!.CountryId == countryId,
            include: products => products
                .Include(x => x.ProductPackSizes!)
                    .ThenInclude(x => x.ProductPackSizeGTINs!)
                .Include(x => x.ProductPackSizes!)
                    .ThenInclude(x => x.MeasureUnit!)
                .Include(x => x.ProductCountry!));

        return _mapper.Map<IEnumerable<ProductData>>(products);
    }

    public async Task<ProductData> GetProductDetails(string? gtin) {
        var product = (await _productPackSizeGTINRepo.GetManyAsync(product => product.GTIN!.Equals(gtin),
            include: products => products
                .Include(x => x.PackSize!)
                    .ThenInclude(x => x.ProductCatalogue!)
                        .ThenInclude(x => x.ProductType!)
                .Include(x => x.PackSize!)
                    .ThenInclude(x => x.ProductCatalogue!)
                        .ThenInclude(x => x.ProductCountry!)
                .Include(x => x.PackSize!)
                    .ThenInclude(x => x.MeasureUnit!)))
            .FirstOrDefault() ?? throw new KeyNotFoundException($"Product details for GTIN: <{gtin}> not found");

        return _mapper.Map<ProductData>(product);
    }

    public async Task<IEnumerable<ProductJourneyStep>> GetProductJourney(string gtin, string serialNumber) {
        if(String.IsNullOrWhiteSpace(gtin)){
            throw new ArgumentNullException(nameof(gtin), "<GTIN> should be specified");
        }

        if(String.IsNullOrWhiteSpace(serialNumber)){
            throw new ArgumentNullException(nameof(serialNumber), "<Serial Number> should be specified");
        }

        var trackingId = $"01{gtin}21{serialNumber}";

        var productJourney = await _sapIntegrationService.GetProductJourney(trackingId);

        return productJourney.Where(productJourney => !String.IsNullOrEmpty(productJourney.Evttype)
                && productJourney.Evttype.Equals(AuthenticationEventType, StringComparison.InvariantCultureIgnoreCase))
            .Select(productJourney => {
                var journeyStep = _mapper.Map<ProductJourneyStep>(productJourney);
                journeyStep.GTIN = gtin;
                journeyStep.SN = serialNumber;
                return journeyStep;
            });
    }
}
