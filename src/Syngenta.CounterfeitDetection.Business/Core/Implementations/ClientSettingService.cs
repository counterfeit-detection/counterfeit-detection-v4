﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.Business.Core.Models;
using Syngenta.CounterfeitDetection.DataAccess.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Models;

using SharedConstants = Syngenta.CounterfeitDetection.Infrastructure.Constants;

namespace Syngenta.CounterfeitDetection.Business.Core.Implementations;

public class ClientSettingService : IClientSettingService {
    private readonly IGenericRepository<Client> _clientRepo;
    private readonly IGenericRepository<DataAccess.Models.Language> _languageRepo;
    private readonly IGenericRepository<Country> _countryRepo;
    private readonly IGenericRepository<Role> _roleRepo;
    private readonly IGenericRepository<ClientAppType> _clientAppTypeRepo;
    private readonly IMapper _mapper;

    public ClientSettingService(
        IGenericRepository<Client> clientRepo,
        IGenericRepository<DataAccess.Models.Language> languageRepo,
        IGenericRepository<Country> countryRepo,
        IGenericRepository<Role> roleRepo,
        IGenericRepository<ClientAppType> clientAppTypeRepo,
        IMapper mapper
    ) {
        _clientRepo = clientRepo;
        _languageRepo = languageRepo;
        _countryRepo = countryRepo;
        _roleRepo = roleRepo;
        _clientAppTypeRepo = clientAppTypeRepo;
        _mapper = mapper;
    }

    public async Task<ClientSettings> GetClientSettings(
        string appName,
        string countryCode,
        string roleCode
    ) {
        var clientId = (await _clientAppTypeRepo.GetManyAsync(c => c.ClientAppName!.Equals(appName))).FirstOrDefault()?.ClientId
            ?? throw new KeyNotFoundException($"No App found with App Name <{appName}>");
        var countryId = (await _countryRepo.GetManyAsync(c => c.Code!.Equals(countryCode))).FirstOrDefault()?.Id
            ?? throw new KeyNotFoundException($"No country found with code <{countryCode}>");
        var roleId = (await _roleRepo.GetManyAsync(r => r.Code!.Equals(roleCode))).FirstOrDefault()?.Id
            ?? throw new KeyNotFoundException($"No role found with code <{roleCode}>");

        var result = (await _clientRepo.GetManyAsync(c => c.Id == clientId,
                include: q => q
                    .Include(t => t.ReportReasons!)
                        .ThenInclude(g => g.GeneralReportReason)
                    .Include(t => t.ScanOrigins!)
                        .ThenInclude(s => s.ScanningPoint)
                    .Include(t => t.Features!)
                        .ThenInclude(c => c.ClientFeature)
                    .Include(t => t.Themes!)
                        .ThenInclude(r => r.Theme!)
            )).FirstOrDefault()!;

        result.Themes = result?.Themes?.Where(t => t.CountryId == countryId && t.RoleId == roleId);
        result!.Features = result?.Features?.Where(t => t.CountryId == countryId && t.RoleId == roleId);
        result!.ReportReasons = result?.ReportReasons?.Where(t => t.CountryId == countryId && t.RoleId == roleId);
        result!.ScanOrigins = result?.ScanOrigins?.Where(t => t.CountryId == countryId && t.RoleId == roleId);
        result!.ResultTypes = result?.ResultTypes?.Where(t => t.CountryId == countryId && t.RoleId == roleId);

        var languages = await _languageRepo.GetManyAsync();
        var clientSettingsData = _mapper.Map<ClientSettings>(result);
        clientSettingsData.Languages = _mapper.Map<IEnumerable<Models.Languages>>(languages);
        return clientSettingsData;
    }
}