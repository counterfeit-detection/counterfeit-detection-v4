﻿using Microsoft.EntityFrameworkCore;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Models;

namespace Syngenta.CounterfeitDetection.Business.Core.Implementations;

internal class TranslationService : ITranslationService {
    private readonly IGenericRepository<TranslationMaster> _translationMasterRepository;
    private readonly IGenericRepository<Language> _languageRepository;

    public TranslationService(
    IGenericRepository<TranslationMaster> translationMasterRepository,
    IGenericRepository<Language> languageRepository
    ) {
        _translationMasterRepository = translationMasterRepository;
        _languageRepository = languageRepository;
    }

    public async Task<string> GetTranslation(string key, int? languageId) {
        if(String.IsNullOrWhiteSpace(key)) {
            throw new ArgumentNullException(nameof(key), $"Translation {nameof(key)} can't be null or empty.");
        }

        var translatedTitle = (await _translationMasterRepository
            .GetManyAsync(translation => translation.Key!.Equals(key),
                include: translations => translations
                    .Include(x => x.LanguageTranslations!)))
             .FirstOrDefault();

        return translatedTitle?.LanguageTranslations?
            .FirstOrDefault(translation => translation.LanguageId == languageId)?.Value 
            ?? translatedTitle?.DefaultValue 
            ?? key;
    }

    public async Task<string> GetTranslation(string key, string languageCode) {
        if(String.IsNullOrWhiteSpace(languageCode)) {
            throw new ArgumentNullException(nameof(key), $"Translation {nameof(languageCode)} can't be null or empty.");
        }

        var language = (await _languageRepository.GetManyAsync(lang => lang.Code!.Equals(languageCode)))
            .FirstOrDefault() ?? throw new KeyNotFoundException($"Language with code <{languageCode}> not presented in database");

        return await GetTranslation(key, language.Id);
    }
}
