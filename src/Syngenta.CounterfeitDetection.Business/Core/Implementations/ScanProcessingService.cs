using AutoMapper;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.Business.Core.Models;
using Syngenta.CounterfeitDetection.Business.Integrations.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Models;
using Syngenta.CounterfeitDetection.Infrastructure.Exceptions;

using SharedConstants = Syngenta.CounterfeitDetection.Infrastructure.Constants;
using System.Reflection.Metadata.Ecma335;
using System;

namespace Syngenta.CounterfeitDetection.Business.Core.Implementations;

internal class ScanProcessingService : IScanProcessingService {
    private readonly IGenericRepository<Transaction> _transactionsRepo;
    private readonly IGenericRepository<TransactionStatusType> _transactionStatusTypeRepo;
    private readonly IGenericRepository<User> _usersRepo;
    private readonly IGenericRepository<Country> _countryRepo;
    private readonly IGenericRepository<Role> _roleRepo;
    private readonly IGenericRepository<ClientAppType> _clientsRepo;
    private readonly IGenericRepository<BarcodeType> _codeTypeRepo;
    private readonly IGenericRepository<ScanningPointSetting> _scanningPointSettingRepo;
    private readonly IGenericRepository<ReportType> _reportTypeRepo;
    private readonly IGenericRepository<FollowUpAction> _followUpActionsRepo;
    private readonly IGenericRepository<FollowUpActionResultRule> _followUpRulesRepo;
    private readonly IGenericRepository<CodeTypeCheckRule> _codeTypeCheckRuleRepo;
    private readonly IGenericRepository<QRDomainCheckRule> _qrDomainCheckRuleRepo;
    private readonly IGenericRepository<TransactionResult> _transactionResultRepo;
    private readonly IGenericRepository<TransactionReview> _transactionReviewRepo;
    private readonly IBlobStorageIntegrationService _blobService;
    private readonly IMapper _mapper;
    private readonly ILogger<ScanProcessingService> _logger;

    private const string ScanReport = "Scan report";

    public ScanProcessingService(
        IGenericRepository<Transaction> transactionsRepo,
        IGenericRepository<TransactionStatusType> transactionStatusTypeRepo,
        IGenericRepository<User> usersRepo,
        IGenericRepository<Country> countryRepo,
        IGenericRepository<Role> roleRepo,
        IGenericRepository<ClientAppType> clientsRepo,
        IGenericRepository<BarcodeType> codeTypeRepo,
        IGenericRepository<ScanningPointSetting> scanningPointSettingRepo,
        IGenericRepository<ReportType> reportTypeRepo,
        IGenericRepository<CodeTypeCheckRule> codeTypeCheckRuleRepo,
        IGenericRepository<QRDomainCheckRule> qrDomainCheckRuleRepo,
        IGenericRepository<TransactionResult> transactionResultRepo,
        IGenericRepository<TransactionReview> transactionReviewRepo,
        IGenericRepository<FollowUpAction> followUpActionRepo,
        IGenericRepository<FollowUpActionResultRule> followUpRulesRepo,
        IBlobStorageIntegrationService blobService,
        IMapper mapper,
        ILogger<ScanProcessingService> logger
    ) {
        _transactionsRepo = transactionsRepo;
        _transactionStatusTypeRepo = transactionStatusTypeRepo;
        _usersRepo = usersRepo;
        _countryRepo = countryRepo;
        _roleRepo = roleRepo;
        _clientsRepo = clientsRepo;
        _codeTypeRepo = codeTypeRepo;
        _scanningPointSettingRepo = scanningPointSettingRepo;
        _reportTypeRepo = reportTypeRepo;
        _codeTypeCheckRuleRepo = codeTypeCheckRuleRepo;
        _qrDomainCheckRuleRepo = qrDomainCheckRuleRepo;
        _transactionResultRepo = transactionResultRepo;
        _transactionReviewRepo = transactionReviewRepo;
        _followUpActionsRepo = followUpActionRepo;
        _followUpRulesRepo = followUpRulesRepo;
        _blobService = blobService;
        _mapper = mapper;
        _logger = logger;
    }

    public async Task<CreateScanResult> CreateScans(
        UserData user,
        ClientData client,
        IEnumerable<ScanData> scans
    ) {
        if (!scans.Any()) {
            return new CreateScanResult() {
                Submitted = new List<ScanTransaction>(),
                Failed = new List<ScanData>()
            };
        }

        var clients = await _clientsRepo.GetManyAsync(c => c.ClientAppName == client.HostAppName);
        var clientEntry = clients.FirstOrDefault();
        if (clientEntry is null)
            throw new ArgumentException($"Invalid client: <{client.HostAppName}>");

        // todo: update user country and role if needed
        var userEntry =
            (await _usersRepo.GetManyAsync(
                u => u.AuthId == user.Login,
                include: q => q.Include(u => u.Country).Include(u => u.Role!)
            )).FirstOrDefault();

        if (userEntry is null) {
            var country = (await _countryRepo.GetManyAsync(c => c.Code == user.CountryCode)).FirstOrDefault()
                ?? throw new ArgumentException($"No country found with code <{user.CountryCode}>");
            var role = (await _roleRepo.GetManyAsync(r => r.Code == user.RoleCode)).FirstOrDefault()
                ?? throw new ArgumentException($"No role found with code <{user.RoleCode}>");
            userEntry = await _usersRepo.CreateAsync(
                _mapper.Map<User>(new Tuple<UserData, Country?, Role?>(user, country, role)));
        }

        var now = DateTime.Now;
        var statusId =
            (await _transactionStatusTypeRepo.GetManyAsync(
                s => s.Type == SharedConstants.Constants.TransactionStatusType.Pending)
            ).FirstOrDefault()?.Id
                ?? throw new MasterDataException(
                    $"Initial transaction status type <Pending> is not defined in DB.");
        var reportTypeId =
            (await _reportTypeRepo.GetManyAsync(s => s.Type == ScanReport))
                .FirstOrDefault()?.Id
                ?? throw new MasterDataException(
                    $"Report type <Scan report> is not defined in DB.");
        try {
            var transactions = new List<Transaction>();
            var failed = new List<ScanData>();
            foreach (var s in scans) {
                var transaction = new Transaction {
                    StatusId = statusId!,
                    CreationTime = now,
                    LastUpdateTime = now,
                    ScanInputDetails = _mapper.Map<ScanInputDetails>(s.ScanEntry),
                    GeoLocation = s.ScanEntry.Location.ToString()
                };
                transaction.UserId = userEntry.Id;
                transaction.UserCountryId = userEntry.CountryId;
                transaction.UserRoleId = userEntry.RoleId;
                transaction.ScanInputDetails.ClientAppTypeId = clientEntry.Id;

                transaction.ScanInputDetails.CodeTypeId =
                    (await _codeTypeRepo.GetManyAsync(r => r.Value == s.ScanEntry.CodeType))
                        .FirstOrDefault()?.Id
                        ?? (await _codeTypeRepo.GetManyAsync(r => r.Value == null))
                            .FirstOrDefault()?.Id;

                if (s.ReportData is not null) {
                    transaction.TransactionReport = _mapper.Map<Report>(s.ReportData);

                    transaction.TransactionReport.TypeId = reportTypeId;
                    transaction.TransactionReport.ScanningPointId =
                        (await _scanningPointSettingRepo.GetManyAsync(
                            r => r.ScanningPoint!.Point == s.ReportData!.ScanningPoint,
                            include: q => q.Include(s => s.ScanningPoint!)
                        )).FirstOrDefault()?.ScanningPoint?.Id;

                    try {
                        if (s.ReportData.Images.Any()) {
                            var images = new List<ReportImage>();

                            foreach (var i in s.ReportData.Images) {
                                var imageUrl = await _blobService.SaveImage(i);
                                images.Add(new ReportImage {
                                    ImageFile = new BlobStorageFile {
                                        FileName = Guid.NewGuid().ToString(),
                                        StorageUrl = imageUrl.ToString()
                                    }
                                });
                            }

                            transaction.TransactionReport.Images = images;
                        }
                    }
                    catch (Exception ex) {
                        _logger.LogWarning($"Image saving failure for transaction <{s.ScanEntry.Fingerprint}>", ex);
                        failed.Add(s);
                        continue;
                    }
                }

                transactions.Add(transaction);
            }

            // todo: elimitane duplicates by fingerprint
            var addedTransactions = (await _transactionsRepo.CreateMultipleAsync(transactions));
            var addedIds = transactions.Select(t => t.Id).ToList();

            foreach (var transaction in addedTransactions) {
                await ExecuteCodeTypeCheck(transaction);
            }

            var result = await _transactionsRepo.GetManyAsync(
                t => addedIds.Any(i => i == t.Id),
                include: q => q
                    .Include(t => t.Status!)
                    .Include(t => t.ScanInputDetails!)
                    .Include(t => t.TransactionReport!)
                        .ThenInclude(r => r.ScanningPoint!)
                    .Include(t => t.TransactionReport!)
                        .ThenInclude(r => r.Images!)
                        .ThenInclude(ri => ri.ImageFile!)
            );

            return new CreateScanResult() {
                Submitted = _mapper.Map<IEnumerable<ScanTransaction>>(result!.Select(x => new Tuple<Transaction,string>(x, client.LanguageCode))),
                Failed = failed
            };
        }
        catch (Exception ex) {
            _logger.LogError(ex.Message);
            return new CreateScanResult { 
                Submitted = new List<ScanTransaction>(),
                Failed = scans
            };
        }
    }

    public async Task SubmitFollowUpAction(FollowUpActionData data) {
        if (data.Id <= 0 || data.OptionId <= 0)
            throw new ArgumentException("Identifiers should be positive integers.", nameof(data));

        var action = (await _followUpActionsRepo.GetManyAsync(
            a => a.Id == data.Id
        )).FirstOrDefault()
            ?? throw new KeyNotFoundException($"Couldn't find a follow-up action with id <{data.Id}>.");

        action.IsCompleted = true;
        action.PickedOptionId = data.OptionId;
        action.Data = data.Data is not null 
            ? String.Join('\n', data.Data!.Select(d => $"{EscapeString(d.Key)}:{EscapeString(d.Value)}"))
            : null;
        try {
            await _followUpActionsRepo.UpdateAsync(action);
        }
        catch (Exception ex) {
            throw new Exception($"Follow-up action update failed for action id <{data.Id}>.", ex);
        }

        await ApplyFollowUpRules(action);
    }

    public async Task MarkScanReviewed(int transactionId) {
        const string completed = SharedConstants.Constants.TransactionStatusType.Completed;

        if (transactionId <= 0)
            throw new ArgumentException("Identifiers should be positive integers.");

        var transcation = (await _transactionsRepo.GetManyAsync(
            transaction => transaction.Id == transactionId,
            include: transactions => transactions
                .Include(transcation => transcation.Status!)
        )).FirstOrDefault()
        ?? throw new KeyNotFoundException($"Couldn't find a transaction with id <{transactionId}>.");

        TransactionReview? review = (await _transactionReviewRepo.GetManyAsync(
            review => review.TransactionId == transactionId)).FirstOrDefault();
        if (review is not null)
            return;

        var isCompleted = transcation!.Status!.Type!.Equals(completed, StringComparison.OrdinalIgnoreCase);
        if (isCompleted) {
            try {
                await CreateTransactionReviewResult(transactionId);
            }
            catch (Exception ex) {
                throw new Exception($"Transaction update failed for Transaction id <{transactionId}>.", ex);
            }
        }
        else {
            throw new InvalidOperationException($"Transaction with id <{transactionId}> is not in {completed} status.");
        }
    }

    private async Task CreateTransactionReviewResult(int transactionId) {
        var transactionReview = new TransactionReview {
            ReviewTime = DateTime.UtcNow,
            TransactionId = transactionId,
        };

        await _transactionReviewRepo.CreateAsync(transactionReview);
    }

    private async Task ApplyFollowUpRules(FollowUpAction action) {
        var transaction = (await _transactionsRepo.GetManyAsync(
            t => t.Id == action.TransactionId,
            include: q => q
                .AsTracking()
                .Include(t => t.FollowUpActions!)
                .Include(t => t.Result!)
                .Include(t => t.ScanInputDetails!)
                    .ThenInclude(t => t.ClientAppType!)
        )).FirstOrDefault()
            ?? throw new DataConsistencyException(
                $"Follow-up action <{action.Id}> doesn't have associated transaction.");
        var countryId = transaction.UserCountryId;
        var clientId = transaction.ScanInputDetails?.ClientAppType?.ClientId;

        var rule = (await _followUpRulesRepo.GetManyAsync(
            r => r.FollowUpActionTypeId == action.TypeId
              && r.FollowUpPickedOptionId == action.PickedOptionId
              && (countryId == null || r.CountryId == null || r.CountryId == countryId)
              && (clientId == null || r.ClientId == null || r.ClientId == clientId),
            r => r.OrderBy(a => a.Order),
            include: q => q.Include(r => r.DesiredFollowUpActions!)
        )).FirstOrDefault();

        if (rule is null) {
            await makeTransactionCompleted(transaction);
            return;
        }

        if (rule.DesiredFollowUpActions?.Any() ?? false) {
            var actions = rule.DesiredFollowUpActions.Select(
                i => new FollowUpAction {
                    TransactionId = transaction.Id,
                    TypeId = i.FollowUpActionTypeId
                });
            await _followUpActionsRepo.CreateMultipleAsync(actions);
        }

        if (rule.DesiredTransactionResultTypeId is not null) {
            transaction.Result!.TypeId = rule.DesiredTransactionResultTypeId.Value;
            await _transactionsRepo.UpdateAsync(transaction);
        }

        if (!(rule.DesiredFollowUpActions?.Any() ?? false)) {
            await makeTransactionCompleted(transaction);
        }

        #region local helpers
        async Task makeTransactionCompleted(Transaction tran) {
            if (tran.FollowUpActions?.Any(a => a.IsCompleted == false) ?? false) return;

            const string completed = SharedConstants.Constants.TransactionStatusType.Completed;
            var status = (await _transactionStatusTypeRepo.GetManyAsync(
                s => s.Type == completed
            )).FirstOrDefault()?.Id
                ?? throw new MasterDataException($"Transaction status type <{completed}> isn't defined in DB.");

            tran.StatusId = status;
            await _transactionsRepo.UpdateAsync(tran);
        }
        #endregion
    }

    private async Task ExecuteCodeTypeCheck(Transaction transaction) {
        if (transaction.ScanInputDetails!.IsManualScan)
            return;

        var rule = (await _codeTypeCheckRuleRepo
            .GetManyAsync(rule => rule.CountryId == transaction!.UserCountryId,
                orderBy: rules => rules.OrderBy(rule => rule.Order),
                include: rules => rules
                    .Include(x => x.AllowedCodeType!)))
            .FirstOrDefault() 
            ?? throw new MasterDataException($"Code type check rule not defined for country with id: <{transaction!.UserCountryId}>");

        if (rule.AllowedCodeTypeId == transaction.ScanInputDetails!.CodeTypeId) {
            if (rule.AllowedCodeType!.Value!.Equals(SharedConstants.Constants.BarcodeType.QRCode))
                await ExecuteDomainNameCheck(transaction);
            return;
        }

        await CreateTransactionResult(transaction.Id, rule.FailScanResultId);
    }

    private async Task ExecuteDomainNameCheck(Transaction transaction) {
        var rule = (await _qrDomainCheckRuleRepo
            .GetManyAsync(rule => rule.CountryId == transaction!.UserCountryId,
            orderBy: rules => rules.OrderBy(rule => rule.Order)))
            .FirstOrDefault() 
            ?? throw new MasterDataException($"QR domain check rule not defined for country with id: <{transaction!.UserCountryId}>");

        if (String.IsNullOrWhiteSpace(rule.AllowedDomain))
            return;

        Uri.TryCreate(transaction.ScanInputDetails!.RawData, UriKind.Absolute, out Uri? uri);

        if (uri is null)
            throw new ArgumentException($"Scanned URI <{transaction.ScanInputDetails!.RawData}> is not valid");

        if (uri.Host.Equals(rule.AllowedDomain))
            return;

        await CreateTransactionResult(transaction.Id, rule.FailScanResultId);
    }

    private async Task CreateTransactionResult(int transactionId, int transactionResultTypeId) {
        var transactionResult = new TransactionResult() {
            TransactionId = transactionId,
            TypeId = transactionResultTypeId
        };

        await _transactionResultRepo.CreateAsync(transactionResult);
    }

    // todo: move to shared infrastructure as custom converter
    // todo: add parsing string into key-value pair
    private string EscapeString(string input) =>
        input.Contains(':')
            ? $"'{input.Replace("'", "\'")}'"
            : input;
}