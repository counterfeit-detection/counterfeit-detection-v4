using Syngenta.CounterfeitDetection.Business.Core.Interfaces;
using Syngenta.CounterfeitDetection.Business.Core.Models;
using Syngenta.CounterfeitDetection.Infrastructure.Models;

namespace Syngenta.CounterfeitDetection.Business.Core.Implementations;

internal class ReportService : IReportService {
    // mockup
    public async Task CreateScanReport(int scanId, ReportEntry report) => await Task.CompletedTask;

    // mockup
    public async Task<CreateDirectReportResult> CreateDirectReports(
        UserData user,
        ClientData client,
        IEnumerable<ReportData> reports
    ) =>
        await Task.FromResult(
            new CreateDirectReportResult {
                Submitted = new List<CreatedReportData> {
                new CreatedReportData {
                    Id = 1,
                    ScanEntry = new ScanEntry() {
                        Fingerprint = "12345002",
                        Location = new Geolocation(28.7f, 45.9f),
                        Time = new DateTime(2022, 12, 31, 7, 5, 2),
                        Code = "010123456789012321SN12345678",
                        IsManualEntry = true,
                        IsOffline = false
                    },
                    ReportEntry = new ReportEntry() {
                        ScanningPoint = "Retailer",
                        ScanLocationName = "The Store, Inc.",
                        ScanAddress = "Anywhere Town, 1",
                        ReportReasonId = 1,
                        CustomReportReason = "wtf",
                        Comments = "no",
                        Images = new List<string> { "123" }
                    }
                }
            },
                Failed = new List<ReportData> {
                new ReportData {
                    ScanEntry = new ScanEntry() {
                        Fingerprint = "12345002",
                        Location = new Geolocation(28.7f, 45.9f),
                        Time = new DateTime(2022, 12, 31, 7, 5, 2),
                        Code = "010123456789012321SN12345678",
                        IsManualEntry = true,
                        IsOffline = false
                    },
                    ReportEntry = new ReportEntry() {
                        ScanningPoint = "Distributor",
                        ScanLocationName = "The Warehouse, Inc.",
                        ScanAddress = "Anywhere Town, 3",
                        ReportReasonId = 2,
                        CustomReportReason = "ftw",
                        Comments = "yas",
                        Images = new List<string> { "123" }
                    }
                }
            }
            });
}