using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Syngenta.CounterfeitDetection.Business.Mappings;

namespace Syngenta.CounterfeitDetection.Business.Extensions;

public static class AutomapperExtensions {
    public static IMapperConfigurationExpression RegisterBusiness2DataAccessMappings(
        this IMapperConfigurationExpression cfg
    ) {
        cfg.AddProfile<MappingProfile>();
        return cfg;
    } 

    public static IServiceCollection RegisterBusiness2DataAccessTypeConverters(
        this IServiceCollection services
    ) {
        services.AddTransient<ScanInputDetailsToKeyValueCollectionConverter>();
        services.AddTransient<ThemeClientSettingsDataConverter>();
        services.AddTransient<ProductCatalogueConverter>();
        services.AddTransient<ProductPackSizeGTINIntoProductDataConverter>();
        return services;
    }
}