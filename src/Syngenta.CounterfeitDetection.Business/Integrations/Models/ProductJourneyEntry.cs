﻿namespace Syngenta.CounterfeitDetection.Business.Integrations.Models;

internal class ProductJourneyEntry {
    public string? Evtid { get; set; }
    public string? Evttype { get; set; }
    public string? Disposition { get; set; }
    public DateTime? Eventdatetime { get; set; }
    public string? Location { get; set; }
    public string? Readpoint { get; set; }
    public string? Market { get; set; }
    public string? Usertype { get; set; }
    public string? Userid { get; set; }
    public string? Latitude { get; set; }
    public string? Longitude { get; set; }
    public string? Purchaseorderid { get; set; }
    public string? Loyaltytrackingid { get; set; }
    public string? Shiptoparty { get; set; }
    public string? Soldtoparty { get; set; }
    public string? Scanid { get; set; }
    public string? Deliveryno { get; set; }
    public string? Deliverytype { get; set; }
    public string? ErrorMessage { get; set; }
    public Epc? Epc { get; set; }
}

internal class Epc {
    public IEnumerable<string>? Item { get; set; }
}
