﻿using Syngenta.CounterfeitDetection.Business.Integrations.Models;

namespace Syngenta.CounterfeitDetection.Business.Integrations.Interfaces;

internal interface ISAPIntegrationService {
    Task<IEnumerable<ProductJourneyEntry>> GetProductJourney(string trackingId);
}
