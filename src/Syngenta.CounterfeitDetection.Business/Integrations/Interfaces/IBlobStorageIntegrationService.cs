namespace Syngenta.CounterfeitDetection.Business.Integrations.Interfaces;

public interface IBlobStorageIntegrationService {
    Task<Uri> SaveImage(string base64Image);
    Task<string> GetImage(Uri imageUri);
}