﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Syngenta.CounterfeitDetection.Business.Integrations.Interfaces;
using Syngenta.CounterfeitDetection.Business.Integrations.Models;
using System.Text.RegularExpressions;

namespace Syngenta.CounterfeitDetection.Business.Integrations.Implementations;

internal class SAPIntegrationService : ISAPIntegrationService {
    private readonly IConfiguration _configuration;
    private readonly HttpClient _httpClient;
    private const string FlexAPI = "FlexAPI";
    private const string FlexAPIClientId = "FlexAPIClientId";
    private const string FlexApiClientSecret = "FlexApiClientSecret";

    public SAPIntegrationService(
        IConfiguration configuration,
        HttpClient httpClient
    ) {
        _configuration = configuration;
        _httpClient = httpClient;
    }

    public async Task<IEnumerable<ProductJourneyEntry>> GetProductJourney(string trackingId) {
        var flexApi = _configuration.GetValue<string>(FlexAPI);
        var clientId = _configuration.GetValue<string>(FlexAPIClientId);
        var clientSecret = _configuration.GetValue<string>(FlexApiClientSecret);

        var request = new HttpRequestMessage(HttpMethod.Get, $"{flexApi}?trackingId={trackingId}");
        request.Headers.Add("Client_Id", clientId);
        request.Headers.Add("Client_Secret", clientSecret);

        var response = await _httpClient.SendAsync(request);
        var content = await response.Content.ReadAsStringAsync();

        var jsonWithoutEpc = Regex.Replace(content, ",\n\\s*\"Epc\":\\s{(\n\\s*.\"item\".*)*\n?\\s*}", String.Empty, RegexOptions.Compiled);
        return JsonConvert.DeserializeObject<IEnumerable<ProductJourneyEntry>>(jsonWithoutEpc)!;
    }
}
