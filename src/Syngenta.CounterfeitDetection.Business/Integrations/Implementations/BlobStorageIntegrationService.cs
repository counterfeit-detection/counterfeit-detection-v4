using Syngenta.CounterfeitDetection.Business.Integrations.Interfaces;

namespace Syngenta.CounterfeitDetection.Business.Integrations.Implementations;

internal class BlobStorageIntegrationService : IBlobStorageIntegrationService {
    public async Task<Uri> SaveImage(string base64Image) {
        await Task.CompletedTask;
        return new Uri("about:blank");
    }

    public async Task<string> GetImage(Uri imageUri) {
        await Task.CompletedTask;
        return String.Empty;
    }
}