﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Syngenta.CounterfeitDetection.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class FixedMigrations : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Transactions_TypeId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Reports_TypeId",
                table: "Reports");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_TransactionId",
                table: "Reports",
                column: "TransactionId",
                unique: true,
                filter: "[TransactionId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_TypeId",
                table: "Reports",
                column: "TypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Transactions_TransactionId",
                table: "Reports",
                column: "TransactionId",
                principalTable: "Transactions",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Transactions_TransactionId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Reports_TransactionId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Reports_TypeId",
                table: "Reports");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_TypeId",
                table: "Reports",
                column: "TypeId",
                unique: true,
                filter: "[TypeId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Transactions_TypeId",
                table: "Reports",
                column: "TypeId",
                principalTable: "Transactions",
                principalColumn: "Id");
        }
    }
}
