﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Syngenta.CounterfeitDetection.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AppTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AuthTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BarcodeTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Value = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BarcodeTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BlobStorageFiles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FileName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StorageUrl = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlobStorageFiles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ClientFeatures",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientFeatures", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ExternalEndpointTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExternalEndpointTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FollowUpActionTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    IsAutomatic = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FollowUpActionTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GeneralReportReasons",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Reason = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneralReportReasons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Languages",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Languages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MailMessageTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MailMessageTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MeasureUnits",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Unit = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MeasureUnits", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OutgoingMails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    To = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Cc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Bcc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    From = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FromMask = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Subject = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Body = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SendTime = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OutgoingMails", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Regions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Regions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ReportTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RepresentationFeatures",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RepresentationFeatures", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SapResultTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SapResultTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ScanningPoints",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Point = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScanningPoints", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ScanResultIcons",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Image = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScanResultIcons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Themes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Themes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TransactionResultTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionResultTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TransactionStatusTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionStatusTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TranslationMasters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Key = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    DefaultValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TranslationMasters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ClientAppTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ClientId = table.Column<int>(type: "int", nullable: false),
                    AppTypeId = table.Column<int>(type: "int", nullable: false),
                    ClientAppName = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientAppTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientAppTypes_AppTypes_AppTypeId",
                        column: x => x.AppTypeId,
                        principalTable: "AppTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientAppTypes_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClientAuthTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ClientId = table.Column<int>(type: "int", nullable: false),
                    AuthTypeId = table.Column<int>(type: "int", nullable: false),
                    AuthUrl = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientAuthTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientAuthTypes_AuthTypes_AuthTypeId",
                        column: x => x.AuthTypeId,
                        principalTable: "AuthTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientAuthTypes_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FollowUpActionTypeOptions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TypeId = table.Column<int>(type: "int", nullable: false),
                    Order = table.Column<int>(type: "int", nullable: false),
                    Option = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FollowUpActionTypeOptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FollowUpActionTypeOptions_FollowUpActionTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "FollowUpActionTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MailMessageTemplates",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TypeId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Subject = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Body = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MailMessageTemplates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MailMessageTemplates_MailMessageTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "MailMessageTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OutgoingMailAttachments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MailId = table.Column<int>(type: "int", nullable: false),
                    AttachmentId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OutgoingMailAttachments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OutgoingMailAttachments_BlobStorageFiles_AttachmentId",
                        column: x => x.AttachmentId,
                        principalTable: "BlobStorageFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OutgoingMailAttachments_OutgoingMails_MailId",
                        column: x => x.MailId,
                        principalTable: "OutgoingMails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductCatalogues",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductTypeId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsGPD = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCatalogues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductCatalogues_ProductTypes_ProductTypeId",
                        column: x => x.ProductTypeId,
                        principalTable: "ProductTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RegionId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Countries_Regions_RegionId",
                        column: x => x.RegionId,
                        principalTable: "Regions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Transactions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StatusId = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastUpdateTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Transactions_TransactionStatusTypes_StatusId",
                        column: x => x.StatusId,
                        principalTable: "TransactionStatusTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TranslationLanguages",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MasterId = table.Column<int>(type: "int", nullable: false),
                    LanguageId = table.Column<int>(type: "int", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TranslationLanguages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TranslationLanguages_Languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TranslationLanguages_TranslationMasters_MasterId",
                        column: x => x.MasterId,
                        principalTable: "TranslationMasters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductPackSizes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    UnitId = table.Column<int>(type: "int", nullable: false),
                    Size = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductPackSizes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductPackSizes_MeasureUnits_UnitId",
                        column: x => x.UnitId,
                        principalTable: "MeasureUnits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductPackSizes_ProductCatalogues_ProductId",
                        column: x => x.ProductId,
                        principalTable: "ProductCatalogues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClientFeatureSettings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CountryId = table.Column<int>(type: "int", nullable: true),
                    RoleId = table.Column<int>(type: "int", nullable: true),
                    ClientId = table.Column<int>(type: "int", nullable: true),
                    FeatureId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientFeatureSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientFeatureSettings_ClientFeatures_FeatureId",
                        column: x => x.FeatureId,
                        principalTable: "ClientFeatures",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ClientFeatureSettings_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ClientFeatureSettings_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ClientFeatureSettings_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CodeTypeCheckRules",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Order = table.Column<int>(type: "int", nullable: false),
                    CountryId = table.Column<int>(type: "int", nullable: false),
                    AllowedCodeTypeId = table.Column<int>(type: "int", nullable: false),
                    FailScanResultId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CodeTypeCheckRules", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CodeTypeCheckRules_BarcodeTypes_AllowedCodeTypeId",
                        column: x => x.AllowedCodeTypeId,
                        principalTable: "BarcodeTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CodeTypeCheckRules_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CodeTypeCheckRules_TransactionResultTypes_FailScanResultId",
                        column: x => x.FailScanResultId,
                        principalTable: "TransactionResultTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CountryMailSettings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CountryId = table.Column<int>(type: "int", nullable: false),
                    MessageTypeId = table.Column<int>(type: "int", nullable: false),
                    To = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Cc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Bcc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    From = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FromMask = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CountryMailSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CountryMailSettings_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CountryMailSettings_MailMessageTypes_MessageTypeId",
                        column: x => x.MessageTypeId,
                        principalTable: "MailMessageTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ExternalEndpoints",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CountryId = table.Column<int>(type: "int", nullable: false),
                    EndpointTypeId = table.Column<int>(type: "int", nullable: false),
                    EndpointUrl = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExternalEndpoints", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExternalEndpoints_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExternalEndpoints_ExternalEndpointTypes_EndpointTypeId",
                        column: x => x.EndpointTypeId,
                        principalTable: "ExternalEndpointTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FollowUpActionResultRules",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Order = table.Column<int>(type: "int", nullable: false),
                    CountryId = table.Column<int>(type: "int", nullable: true),
                    ClientId = table.Column<int>(type: "int", nullable: true),
                    FollowUpActionTypeId = table.Column<int>(type: "int", nullable: false),
                    FollowUpPickedOptionId = table.Column<int>(type: "int", nullable: true),
                    DesiredTransactionResultTypeId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FollowUpActionResultRules", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FollowUpActionResultRules_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_FollowUpActionResultRules_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_FollowUpActionResultRules_FollowUpActionTypeOptions_FollowUpPickedOptionId",
                        column: x => x.FollowUpPickedOptionId,
                        principalTable: "FollowUpActionTypeOptions",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_FollowUpActionResultRules_FollowUpActionTypes_FollowUpActionTypeId",
                        column: x => x.FollowUpActionTypeId,
                        principalTable: "FollowUpActionTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FollowUpActionResultRules_TransactionResultTypes_DesiredTransactionResultTypeId",
                        column: x => x.DesiredTransactionResultTypeId,
                        principalTable: "TransactionResultTypes",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProductCountries",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CountryId = table.Column<int>(type: "int", nullable: false),
                    ProductId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCountries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductCountries_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductCountries_ProductCatalogues_ProductId",
                        column: x => x.ProductId,
                        principalTable: "ProductCatalogues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "QrDomainCheckRules",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Order = table.Column<int>(type: "int", nullable: false),
                    CountryId = table.Column<int>(type: "int", nullable: false),
                    FailScanResultId = table.Column<int>(type: "int", nullable: false),
                    AllowedDomain = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QrDomainCheckRules", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QrDomainCheckRules_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_QrDomainCheckRules_TransactionResultTypes_FailScanResultId",
                        column: x => x.FailScanResultId,
                        principalTable: "TransactionResultTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ReportReasonSettings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Order = table.Column<int>(type: "int", nullable: false),
                    CountryId = table.Column<int>(type: "int", nullable: true),
                    RoleId = table.Column<int>(type: "int", nullable: true),
                    ClientId = table.Column<int>(type: "int", nullable: true),
                    GeneralReasonId = table.Column<int>(type: "int", nullable: true),
                    ReasonKey = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RequireCustomReason = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportReasonSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReportReasonSettings_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ReportReasonSettings_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ReportReasonSettings_GeneralReportReasons_GeneralReasonId",
                        column: x => x.GeneralReasonId,
                        principalTable: "GeneralReportReasons",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ReportReasonSettings_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ScanningPointSettings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Order = table.Column<int>(type: "int", nullable: false),
                    CountryId = table.Column<int>(type: "int", nullable: false),
                    RoleId = table.Column<int>(type: "int", nullable: false),
                    ClientId = table.Column<int>(type: "int", nullable: false),
                    ScanningPointId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScanningPointSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ScanningPointSettings_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ScanningPointSettings_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ScanningPointSettings_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ScanningPointSettings_ScanningPoints_ScanningPointId",
                        column: x => x.ScanningPointId,
                        principalTable: "ScanningPoints",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ScanResultRepresentationRules",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Order = table.Column<int>(type: "int", nullable: false),
                    CountryId = table.Column<int>(type: "int", nullable: false),
                    ClientId = table.Column<int>(type: "int", nullable: false),
                    RoleId = table.Column<int>(type: "int", nullable: false),
                    TransactionResultTypeId = table.Column<int>(type: "int", nullable: false),
                    ResultTitleKey = table.Column<int>(type: "int", nullable: false),
                    ResultActionKey = table.Column<int>(type: "int", nullable: false),
                    ResultIconId = table.Column<int>(type: "int", nullable: false),
                    ResultTitleColor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ResultActionColor = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScanResultRepresentationRules", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ScanResultRepresentationRules_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ScanResultRepresentationRules_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ScanResultRepresentationRules_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ScanResultRepresentationRules_ScanResultIcons_ResultIconId",
                        column: x => x.ResultIconId,
                        principalTable: "ScanResultIcons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ScanResultRepresentationRules_TransactionStatusTypes_TransactionResultTypeId",
                        column: x => x.TransactionResultTypeId,
                        principalTable: "TransactionStatusTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ThemeColorsSettings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CountryId = table.Column<int>(type: "int", nullable: true),
                    RoleId = table.Column<int>(type: "int", nullable: true),
                    ClientId = table.Column<int>(type: "int", nullable: false),
                    ThemeId = table.Column<int>(type: "int", nullable: false),
                    Main = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Dark = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Light = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ContrastText = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ThemeColorsSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ThemeColorsSettings_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ThemeColorsSettings_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ThemeColorsSettings_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ThemeColorsSettings_Themes_ThemeId",
                        column: x => x.ThemeId,
                        principalTable: "Themes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TransactionResultDefinitionRules",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Order = table.Column<int>(type: "int", nullable: false),
                    CountryId = table.Column<int>(type: "int", nullable: true),
                    ClientId = table.Column<int>(type: "int", nullable: true),
                    SapResultTypeId = table.Column<int>(type: "int", nullable: true),
                    DesiredTransactionResultTypeId = table.Column<int>(type: "int", nullable: true),
                    SAPScanCount = table.Column<int>(type: "int", nullable: true),
                    SAPBU = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SAPCountry = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionResultDefinitionRules", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransactionResultDefinitionRules_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TransactionResultDefinitionRules_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TransactionResultDefinitionRules_SapResultTypes_SapResultTypeId",
                        column: x => x.SapResultTypeId,
                        principalTable: "SapResultTypes",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TransactionResultDefinitionRules_TransactionResultTypes_DesiredTransactionResultTypeId",
                        column: x => x.DesiredTransactionResultTypeId,
                        principalTable: "TransactionResultTypes",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CountryId = table.Column<int>(type: "int", nullable: false),
                    RoleId = table.Column<int>(type: "int", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AuthId = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "FollowUpActions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransactionId = table.Column<int>(type: "int", nullable: false),
                    TypeId = table.Column<int>(type: "int", nullable: false),
                    IsCompleted = table.Column<bool>(type: "bit", nullable: false),
                    PickedOptionId = table.Column<int>(type: "int", nullable: true),
                    Data = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FollowUpActions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FollowUpActions_FollowUpActionTypeOptions_PickedOptionId",
                        column: x => x.PickedOptionId,
                        principalTable: "FollowUpActionTypeOptions",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_FollowUpActions_FollowUpActionTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "FollowUpActionTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FollowUpActions_Transactions_TransactionId",
                        column: x => x.TransactionId,
                        principalTable: "Transactions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ItemDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransactionId = table.Column<int>(type: "int", nullable: false),
                    GTIN = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SerialNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BatchNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ExpiryDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ManufacturedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ManufacturedAt = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PackSize = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ItemDetails_Transactions_TransactionId",
                        column: x => x.TransactionId,
                        principalTable: "Transactions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SapResults",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransactionId = table.Column<int>(type: "int", nullable: false),
                    TypeId = table.Column<int>(type: "int", nullable: false),
                    ResponseTypeCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ResponseErrorCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ResponseText = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ProductBusinessUnit = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ProductDestinationCountry = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NumberOfScans = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SapResults", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SapResults_SapResultTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "SapResultTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SapResults_Transactions_TransactionId",
                        column: x => x.TransactionId,
                        principalTable: "Transactions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TransactionResults",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransactionId = table.Column<int>(type: "int", nullable: false),
                    TypeId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionResults", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransactionResults_TransactionResultTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "TransactionResultTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransactionResults_Transactions_TransactionId",
                        column: x => x.TransactionId,
                        principalTable: "Transactions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TransactionReviews",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransactionId = table.Column<int>(type: "int", nullable: false),
                    ReviewTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionReviews", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransactionReviews_Transactions_TransactionId",
                        column: x => x.TransactionId,
                        principalTable: "Transactions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductPackSizeGTIN",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PackSizeId = table.Column<int>(type: "int", nullable: false),
                    GTIN = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    IsGPD = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductPackSizeGTIN", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductPackSizeGTIN_ProductPackSizes_PackSizeId",
                        column: x => x.PackSizeId,
                        principalTable: "ProductPackSizes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FollowUpResultFollowUpActions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FollowUpActionResultRuleId = table.Column<int>(type: "int", nullable: false),
                    FollowUpActionTypeId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FollowUpResultFollowUpActions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FollowUpResultFollowUpActions_FollowUpActionResultRules_FollowUpActionResultRuleId",
                        column: x => x.FollowUpActionResultRuleId,
                        principalTable: "FollowUpActionResultRules",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FollowUpResultFollowUpActions_FollowUpActionTypes_FollowUpActionTypeId",
                        column: x => x.FollowUpActionTypeId,
                        principalTable: "FollowUpActionTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Reports",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TypeId = table.Column<int>(type: "int", nullable: true),
                    ScanningPointId = table.Column<int>(type: "int", nullable: true),
                    ReportReasonId = table.Column<int>(type: "int", nullable: true),
                    SendTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ScanAddress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ScanLocationName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CustomReportReason = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Comment = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reports", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reports_ReportReasonSettings_ReportReasonId",
                        column: x => x.ReportReasonId,
                        principalTable: "ReportReasonSettings",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Reports_ReportTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "ReportTypes",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Reports_ScanningPoints_ScanningPointId",
                        column: x => x.ScanningPointId,
                        principalTable: "ScanningPoints",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ScanResultRepresentationFeatures",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RuleId = table.Column<int>(type: "int", nullable: false),
                    FeatureId = table.Column<int>(type: "int", nullable: false),
                    IsEnabled = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScanResultRepresentationFeatures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ScanResultRepresentationFeatures_RepresentationFeatures_FeatureId",
                        column: x => x.FeatureId,
                        principalTable: "RepresentationFeatures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ScanResultRepresentationFeatures_ScanResultRepresentationRules_RuleId",
                        column: x => x.RuleId,
                        principalTable: "ScanResultRepresentationRules",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TransactionResultFollowUpActions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DefinitionRuleId = table.Column<int>(type: "int", nullable: false),
                    FollowUpActionTypeId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionResultFollowUpActions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransactionResultFollowUpActions_FollowUpActionTypes_FollowUpActionTypeId",
                        column: x => x.FollowUpActionTypeId,
                        principalTable: "FollowUpActionTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransactionResultFollowUpActions_TransactionResultDefinitionRules_DefinitionRuleId",
                        column: x => x.DefinitionRuleId,
                        principalTable: "TransactionResultDefinitionRules",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AuditEntries",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ObjectType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Time = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Action = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Comment = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuditEntries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AuditEntries_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ScanInputDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Fingerprint = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    TransactionId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: true),
                    UserRoleId = table.Column<int>(type: "int", nullable: true),
                    UserCountryId = table.Column<int>(type: "int", nullable: true),
                    CodeTypeId = table.Column<int>(type: "int", nullable: true),
                    ClientAppTypeId = table.Column<int>(type: "int", nullable: true),
                    IsManualScan = table.Column<bool>(type: "bit", nullable: false),
                    IsOffline = table.Column<bool>(type: "bit", nullable: false),
                    ScanLocation = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RawData = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ScanTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScanInputDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ScanInputDetails_BarcodeTypes_CodeTypeId",
                        column: x => x.CodeTypeId,
                        principalTable: "BarcodeTypes",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ScanInputDetails_ClientAppTypes_ClientAppTypeId",
                        column: x => x.ClientAppTypeId,
                        principalTable: "ClientAppTypes",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ScanInputDetails_Countries_UserCountryId",
                        column: x => x.UserCountryId,
                        principalTable: "Countries",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ScanInputDetails_Roles_UserRoleId",
                        column: x => x.UserRoleId,
                        principalTable: "Roles",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ScanInputDetails_Transactions_TransactionId",
                        column: x => x.TransactionId,
                        principalTable: "Transactions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ScanInputDetails_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "DirectReportInfos",
                columns: table => new
                {
                    ReportId = table.Column<int>(type: "int", nullable: false),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    GTIN = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SerialNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BatchNumber = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DirectReportInfos", x => x.ReportId);
                    table.ForeignKey(
                        name: "FK_DirectReportInfos_ProductCatalogues_ProductId",
                        column: x => x.ProductId,
                        principalTable: "ProductCatalogues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DirectReportInfos_Reports_ReportId",
                        column: x => x.ReportId,
                        principalTable: "Reports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ReportImages",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReportId = table.Column<int>(type: "int", nullable: false),
                    ImageFileId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportImages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReportImages_BlobStorageFiles_ImageFileId",
                        column: x => x.ImageFileId,
                        principalTable: "BlobStorageFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ReportImages_Reports_ReportId",
                        column: x => x.ReportId,
                        principalTable: "Reports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ScanReportInfos",
                columns: table => new
                {
                    ReportId = table.Column<int>(type: "int", nullable: false),
                    TransactionId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScanReportInfos", x => x.ReportId);
                    table.ForeignKey(
                        name: "FK_ScanReportInfos_Reports_ReportId",
                        column: x => x.ReportId,
                        principalTable: "Reports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ScanReportInfos_Transactions_TransactionId",
                        column: x => x.TransactionId,
                        principalTable: "Transactions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AuditEntries_UserId",
                table: "AuditEntries",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_BarcodeTypes_Value",
                table: "BarcodeTypes",
                column: "Value",
                unique: true,
                filter: "[Value] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAppTypes_AppTypeId",
                table: "ClientAppTypes",
                column: "AppTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAppTypes_ClientId",
                table: "ClientAppTypes",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAuthTypes_AuthTypeId",
                table: "ClientAuthTypes",
                column: "AuthTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAuthTypes_ClientId",
                table: "ClientAuthTypes",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientFeatureSettings_ClientId",
                table: "ClientFeatureSettings",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientFeatureSettings_CountryId",
                table: "ClientFeatureSettings",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientFeatureSettings_FeatureId",
                table: "ClientFeatureSettings",
                column: "FeatureId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientFeatureSettings_RoleId",
                table: "ClientFeatureSettings",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_CodeTypeCheckRules_AllowedCodeTypeId",
                table: "CodeTypeCheckRules",
                column: "AllowedCodeTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_CodeTypeCheckRules_CountryId",
                table: "CodeTypeCheckRules",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_CodeTypeCheckRules_FailScanResultId",
                table: "CodeTypeCheckRules",
                column: "FailScanResultId");

            migrationBuilder.CreateIndex(
                name: "IX_CodeTypeCheckRules_Order",
                table: "CodeTypeCheckRules",
                column: "Order",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Countries_RegionId",
                table: "Countries",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_CountryMailSettings_CountryId",
                table: "CountryMailSettings",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_CountryMailSettings_MessageTypeId",
                table: "CountryMailSettings",
                column: "MessageTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_DirectReportInfos_ProductId",
                table: "DirectReportInfos",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ExternalEndpoints_CountryId",
                table: "ExternalEndpoints",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_ExternalEndpoints_EndpointTypeId",
                table: "ExternalEndpoints",
                column: "EndpointTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_FollowUpActionResultRules_ClientId",
                table: "FollowUpActionResultRules",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_FollowUpActionResultRules_CountryId",
                table: "FollowUpActionResultRules",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_FollowUpActionResultRules_DesiredTransactionResultTypeId",
                table: "FollowUpActionResultRules",
                column: "DesiredTransactionResultTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_FollowUpActionResultRules_FollowUpActionTypeId",
                table: "FollowUpActionResultRules",
                column: "FollowUpActionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_FollowUpActionResultRules_FollowUpPickedOptionId",
                table: "FollowUpActionResultRules",
                column: "FollowUpPickedOptionId");

            migrationBuilder.CreateIndex(
                name: "IX_FollowUpActionResultRules_Order",
                table: "FollowUpActionResultRules",
                column: "Order",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FollowUpActions_PickedOptionId",
                table: "FollowUpActions",
                column: "PickedOptionId");

            migrationBuilder.CreateIndex(
                name: "IX_FollowUpActions_TransactionId",
                table: "FollowUpActions",
                column: "TransactionId");

            migrationBuilder.CreateIndex(
                name: "IX_FollowUpActions_TypeId",
                table: "FollowUpActions",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_FollowUpActionTypeOptions_TypeId_Order",
                table: "FollowUpActionTypeOptions",
                columns: new[] { "TypeId", "Order" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FollowUpActionTypes_Type",
                table: "FollowUpActionTypes",
                column: "Type",
                unique: true,
                filter: "[Type] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_FollowUpResultFollowUpActions_FollowUpActionResultRuleId",
                table: "FollowUpResultFollowUpActions",
                column: "FollowUpActionResultRuleId");

            migrationBuilder.CreateIndex(
                name: "IX_FollowUpResultFollowUpActions_FollowUpActionTypeId",
                table: "FollowUpResultFollowUpActions",
                column: "FollowUpActionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_GeneralReportReasons_Reason",
                table: "GeneralReportReasons",
                column: "Reason",
                unique: true,
                filter: "[Reason] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ItemDetails_TransactionId",
                table: "ItemDetails",
                column: "TransactionId");

            migrationBuilder.CreateIndex(
                name: "IX_MailMessageTemplates_TypeId",
                table: "MailMessageTemplates",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_MailMessageTypes_Type",
                table: "MailMessageTypes",
                column: "Type",
                unique: true,
                filter: "[Type] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_MeasureUnits_Unit",
                table: "MeasureUnits",
                column: "Unit",
                unique: true,
                filter: "[Unit] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_OutgoingMailAttachments_AttachmentId",
                table: "OutgoingMailAttachments",
                column: "AttachmentId");

            migrationBuilder.CreateIndex(
                name: "IX_OutgoingMailAttachments_MailId",
                table: "OutgoingMailAttachments",
                column: "MailId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductCatalogues_ProductTypeId",
                table: "ProductCatalogues",
                column: "ProductTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductCountries_CountryId",
                table: "ProductCountries",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductCountries_ProductId",
                table: "ProductCountries",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPackSizeGTIN_GTIN",
                table: "ProductPackSizeGTIN",
                column: "GTIN",
                unique: true,
                filter: "[GTIN] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPackSizeGTIN_PackSizeId",
                table: "ProductPackSizeGTIN",
                column: "PackSizeId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPackSizes_ProductId",
                table: "ProductPackSizes",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPackSizes_UnitId",
                table: "ProductPackSizes",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_QrDomainCheckRules_CountryId",
                table: "QrDomainCheckRules",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_QrDomainCheckRules_FailScanResultId",
                table: "QrDomainCheckRules",
                column: "FailScanResultId");

            migrationBuilder.CreateIndex(
                name: "IX_QrDomainCheckRules_Order",
                table: "QrDomainCheckRules",
                column: "Order",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ReportImages_ImageFileId",
                table: "ReportImages",
                column: "ImageFileId");

            migrationBuilder.CreateIndex(
                name: "IX_ReportImages_ReportId",
                table: "ReportImages",
                column: "ReportId");

            migrationBuilder.CreateIndex(
                name: "IX_ReportReasonSettings_ClientId",
                table: "ReportReasonSettings",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ReportReasonSettings_CountryId",
                table: "ReportReasonSettings",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_ReportReasonSettings_GeneralReasonId",
                table: "ReportReasonSettings",
                column: "GeneralReasonId");

            migrationBuilder.CreateIndex(
                name: "IX_ReportReasonSettings_RoleId",
                table: "ReportReasonSettings",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_ReportReasonId",
                table: "Reports",
                column: "ReportReasonId");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_ScanningPointId",
                table: "Reports",
                column: "ScanningPointId");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_TypeId",
                table: "Reports",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_SapResults_TransactionId",
                table: "SapResults",
                column: "TransactionId");

            migrationBuilder.CreateIndex(
                name: "IX_SapResults_TypeId",
                table: "SapResults",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_SapResultTypes_Type",
                table: "SapResultTypes",
                column: "Type",
                unique: true,
                filter: "[Type] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ScanInputDetails_ClientAppTypeId",
                table: "ScanInputDetails",
                column: "ClientAppTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ScanInputDetails_CodeTypeId",
                table: "ScanInputDetails",
                column: "CodeTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ScanInputDetails_Fingerprint",
                table: "ScanInputDetails",
                column: "Fingerprint",
                unique: true,
                filter: "[Fingerprint] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ScanInputDetails_TransactionId",
                table: "ScanInputDetails",
                column: "TransactionId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ScanInputDetails_UserCountryId",
                table: "ScanInputDetails",
                column: "UserCountryId");

            migrationBuilder.CreateIndex(
                name: "IX_ScanInputDetails_UserId",
                table: "ScanInputDetails",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ScanInputDetails_UserRoleId",
                table: "ScanInputDetails",
                column: "UserRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_ScanningPoints_Point",
                table: "ScanningPoints",
                column: "Point",
                unique: true,
                filter: "[Point] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ScanningPointSettings_ClientId",
                table: "ScanningPointSettings",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ScanningPointSettings_CountryId",
                table: "ScanningPointSettings",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_ScanningPointSettings_RoleId",
                table: "ScanningPointSettings",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_ScanningPointSettings_ScanningPointId",
                table: "ScanningPointSettings",
                column: "ScanningPointId");

            migrationBuilder.CreateIndex(
                name: "IX_ScanReportInfos_TransactionId",
                table: "ScanReportInfos",
                column: "TransactionId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ScanResultRepresentationFeatures_FeatureId",
                table: "ScanResultRepresentationFeatures",
                column: "FeatureId");

            migrationBuilder.CreateIndex(
                name: "IX_ScanResultRepresentationFeatures_RuleId",
                table: "ScanResultRepresentationFeatures",
                column: "RuleId");

            migrationBuilder.CreateIndex(
                name: "IX_ScanResultRepresentationRules_ClientId",
                table: "ScanResultRepresentationRules",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ScanResultRepresentationRules_CountryId",
                table: "ScanResultRepresentationRules",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_ScanResultRepresentationRules_Order",
                table: "ScanResultRepresentationRules",
                column: "Order",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ScanResultRepresentationRules_ResultIconId",
                table: "ScanResultRepresentationRules",
                column: "ResultIconId");

            migrationBuilder.CreateIndex(
                name: "IX_ScanResultRepresentationRules_RoleId",
                table: "ScanResultRepresentationRules",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_ScanResultRepresentationRules_TransactionResultTypeId",
                table: "ScanResultRepresentationRules",
                column: "TransactionResultTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeColorsSettings_ClientId",
                table: "ThemeColorsSettings",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeColorsSettings_CountryId",
                table: "ThemeColorsSettings",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeColorsSettings_RoleId",
                table: "ThemeColorsSettings",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeColorsSettings_ThemeId",
                table: "ThemeColorsSettings",
                column: "ThemeId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionResultDefinitionRules_ClientId",
                table: "TransactionResultDefinitionRules",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionResultDefinitionRules_CountryId",
                table: "TransactionResultDefinitionRules",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionResultDefinitionRules_DesiredTransactionResultTypeId",
                table: "TransactionResultDefinitionRules",
                column: "DesiredTransactionResultTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionResultDefinitionRules_Order",
                table: "TransactionResultDefinitionRules",
                column: "Order",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransactionResultDefinitionRules_SapResultTypeId",
                table: "TransactionResultDefinitionRules",
                column: "SapResultTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionResultFollowUpActions_DefinitionRuleId",
                table: "TransactionResultFollowUpActions",
                column: "DefinitionRuleId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionResultFollowUpActions_FollowUpActionTypeId",
                table: "TransactionResultFollowUpActions",
                column: "FollowUpActionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionResults_TransactionId",
                table: "TransactionResults",
                column: "TransactionId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransactionResults_TypeId",
                table: "TransactionResults",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionResultTypes_Type",
                table: "TransactionResultTypes",
                column: "Type",
                unique: true,
                filter: "[Type] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionReviews_TransactionId",
                table: "TransactionReviews",
                column: "TransactionId");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_StatusId",
                table: "Transactions",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionStatusTypes_Type",
                table: "TransactionStatusTypes",
                column: "Type",
                unique: true,
                filter: "[Type] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_TranslationLanguages_LanguageId",
                table: "TranslationLanguages",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_TranslationLanguages_MasterId",
                table: "TranslationLanguages",
                column: "MasterId");

            migrationBuilder.CreateIndex(
                name: "IX_TranslationMasters_Key",
                table: "TranslationMasters",
                column: "Key",
                unique: true,
                filter: "[Key] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Users_CountryId",
                table: "Users",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_RoleId",
                table: "Users",
                column: "RoleId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AuditEntries");

            migrationBuilder.DropTable(
                name: "ClientAuthTypes");

            migrationBuilder.DropTable(
                name: "ClientFeatureSettings");

            migrationBuilder.DropTable(
                name: "CodeTypeCheckRules");

            migrationBuilder.DropTable(
                name: "CountryMailSettings");

            migrationBuilder.DropTable(
                name: "DirectReportInfos");

            migrationBuilder.DropTable(
                name: "ExternalEndpoints");

            migrationBuilder.DropTable(
                name: "FollowUpActions");

            migrationBuilder.DropTable(
                name: "FollowUpResultFollowUpActions");

            migrationBuilder.DropTable(
                name: "ItemDetails");

            migrationBuilder.DropTable(
                name: "MailMessageTemplates");

            migrationBuilder.DropTable(
                name: "OutgoingMailAttachments");

            migrationBuilder.DropTable(
                name: "ProductCountries");

            migrationBuilder.DropTable(
                name: "ProductPackSizeGTIN");

            migrationBuilder.DropTable(
                name: "QrDomainCheckRules");

            migrationBuilder.DropTable(
                name: "ReportImages");

            migrationBuilder.DropTable(
                name: "SapResults");

            migrationBuilder.DropTable(
                name: "ScanInputDetails");

            migrationBuilder.DropTable(
                name: "ScanningPointSettings");

            migrationBuilder.DropTable(
                name: "ScanReportInfos");

            migrationBuilder.DropTable(
                name: "ScanResultRepresentationFeatures");

            migrationBuilder.DropTable(
                name: "ThemeColorsSettings");

            migrationBuilder.DropTable(
                name: "TransactionResultFollowUpActions");

            migrationBuilder.DropTable(
                name: "TransactionResults");

            migrationBuilder.DropTable(
                name: "TransactionReviews");

            migrationBuilder.DropTable(
                name: "TranslationLanguages");

            migrationBuilder.DropTable(
                name: "AuthTypes");

            migrationBuilder.DropTable(
                name: "ClientFeatures");

            migrationBuilder.DropTable(
                name: "ExternalEndpointTypes");

            migrationBuilder.DropTable(
                name: "FollowUpActionResultRules");

            migrationBuilder.DropTable(
                name: "MailMessageTypes");

            migrationBuilder.DropTable(
                name: "OutgoingMails");

            migrationBuilder.DropTable(
                name: "ProductPackSizes");

            migrationBuilder.DropTable(
                name: "BlobStorageFiles");

            migrationBuilder.DropTable(
                name: "BarcodeTypes");

            migrationBuilder.DropTable(
                name: "ClientAppTypes");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Reports");

            migrationBuilder.DropTable(
                name: "RepresentationFeatures");

            migrationBuilder.DropTable(
                name: "ScanResultRepresentationRules");

            migrationBuilder.DropTable(
                name: "Themes");

            migrationBuilder.DropTable(
                name: "TransactionResultDefinitionRules");

            migrationBuilder.DropTable(
                name: "Transactions");

            migrationBuilder.DropTable(
                name: "Languages");

            migrationBuilder.DropTable(
                name: "TranslationMasters");

            migrationBuilder.DropTable(
                name: "FollowUpActionTypeOptions");

            migrationBuilder.DropTable(
                name: "MeasureUnits");

            migrationBuilder.DropTable(
                name: "ProductCatalogues");

            migrationBuilder.DropTable(
                name: "AppTypes");

            migrationBuilder.DropTable(
                name: "ReportReasonSettings");

            migrationBuilder.DropTable(
                name: "ReportTypes");

            migrationBuilder.DropTable(
                name: "ScanningPoints");

            migrationBuilder.DropTable(
                name: "ScanResultIcons");

            migrationBuilder.DropTable(
                name: "SapResultTypes");

            migrationBuilder.DropTable(
                name: "TransactionResultTypes");

            migrationBuilder.DropTable(
                name: "TransactionStatusTypes");

            migrationBuilder.DropTable(
                name: "FollowUpActionTypes");

            migrationBuilder.DropTable(
                name: "ProductTypes");

            migrationBuilder.DropTable(
                name: "Clients");

            migrationBuilder.DropTable(
                name: "Countries");

            migrationBuilder.DropTable(
                name: "GeneralReportReasons");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Regions");
        }
    }
}
