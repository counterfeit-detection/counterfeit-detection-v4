﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Syngenta.CounterfeitDetection.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class RemovedRelations_ItemDetails_ProductPackSizeGTIN : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ItemDetails_ProductPackSizeGTIN_GTIN",
                table: "ItemDetails");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_ProductPackSizeGTIN_GTIN",
                table: "ProductPackSizeGTIN");

            migrationBuilder.DropIndex(
                name: "IX_ProductPackSizeGTIN_GTIN",
                table: "ProductPackSizeGTIN");

            migrationBuilder.DropIndex(
                name: "IX_ProductCountries_ProductId",
                table: "ProductCountries");

            migrationBuilder.DropIndex(
                name: "IX_ItemDetails_GTIN",
                table: "ItemDetails");

            migrationBuilder.AlterColumn<string>(
                name: "GTIN",
                table: "ProductPackSizeGTIN",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AlterColumn<string>(
                name: "GTIN",
                table: "ItemDetails",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProductPackSizeGTIN_GTIN",
                table: "ProductPackSizeGTIN",
                column: "GTIN",
                unique: true,
                filter: "[GTIN] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ProductCountries_ProductId",
                table: "ProductCountries",
                column: "ProductId",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ProductPackSizeGTIN_GTIN",
                table: "ProductPackSizeGTIN");

            migrationBuilder.DropIndex(
                name: "IX_ProductCountries_ProductId",
                table: "ProductCountries");

            migrationBuilder.AlterColumn<string>(
                name: "GTIN",
                table: "ProductPackSizeGTIN",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "GTIN",
                table: "ItemDetails",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_ProductPackSizeGTIN_GTIN",
                table: "ProductPackSizeGTIN",
                column: "GTIN");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPackSizeGTIN_GTIN",
                table: "ProductPackSizeGTIN",
                column: "GTIN",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProductCountries_ProductId",
                table: "ProductCountries",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemDetails_GTIN",
                table: "ItemDetails",
                column: "GTIN");

            migrationBuilder.AddForeignKey(
                name: "FK_ItemDetails_ProductPackSizeGTIN_GTIN",
                table: "ItemDetails",
                column: "GTIN",
                principalTable: "ProductPackSizeGTIN",
                principalColumn: "GTIN");
        }
    }
}
