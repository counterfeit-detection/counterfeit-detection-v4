﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Syngenta.CounterfeitDetection.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class RefactoredDatabaseStructure : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScanInputDetails_Countries_UserCountryId",
                table: "ScanInputDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_ScanInputDetails_Roles_UserRoleId",
                table: "ScanInputDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_ScanInputDetails_Users_UserId",
                table: "ScanInputDetails");

            migrationBuilder.DropTable(
                name: "DirectReportInfos");

            migrationBuilder.DropTable(
                name: "ScanReportInfos");

            migrationBuilder.DropIndex(
                name: "IX_TransactionReviews_TransactionId",
                table: "TransactionReviews");

            migrationBuilder.DropIndex(
                name: "IX_ScanInputDetails_UserCountryId",
                table: "ScanInputDetails");

            migrationBuilder.DropIndex(
                name: "IX_ScanInputDetails_UserId",
                table: "ScanInputDetails");

            migrationBuilder.DropIndex(
                name: "IX_ScanInputDetails_UserRoleId",
                table: "ScanInputDetails");

            migrationBuilder.DropIndex(
                name: "IX_SapResults_TransactionId",
                table: "SapResults");

            migrationBuilder.DropIndex(
                name: "IX_Reports_TypeId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_ProductPackSizeGTIN_GTIN",
                table: "ProductPackSizeGTIN");

            migrationBuilder.DropIndex(
                name: "IX_ItemDetails_TransactionId",
                table: "ItemDetails");

            migrationBuilder.DropColumn(
                name: "UserCountryId",
                table: "ScanInputDetails");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "ScanInputDetails");

            migrationBuilder.DropColumn(
                name: "UserRoleId",
                table: "ScanInputDetails");

            migrationBuilder.AddColumn<int>(
                name: "UserCountryId",
                table: "Transactions",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Transactions",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserRoleId",
                table: "Transactions",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GeoLocation",
                table: "Reports",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TransactionId",
                table: "Reports",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "GTIN",
                table: "ProductPackSizeGTIN",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "GTIN",
                table: "ItemDetails",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_ProductPackSizeGTIN_GTIN",
                table: "ProductPackSizeGTIN",
                column: "GTIN");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_UserCountryId",
                table: "Transactions",
                column: "UserCountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_UserId",
                table: "Transactions",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_UserRoleId",
                table: "Transactions",
                column: "UserRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionReviews_TransactionId",
                table: "TransactionReviews",
                column: "TransactionId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SapResults_TransactionId",
                table: "SapResults",
                column: "TransactionId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Reports_TypeId",
                table: "Reports",
                column: "TypeId",
                unique: true,
                filter: "[TypeId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPackSizeGTIN_GTIN",
                table: "ProductPackSizeGTIN",
                column: "GTIN",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ItemDetails_GTIN",
                table: "ItemDetails",
                column: "GTIN");

            migrationBuilder.CreateIndex(
                name: "IX_ItemDetails_TransactionId",
                table: "ItemDetails",
                column: "TransactionId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ItemDetails_ProductPackSizeGTIN_GTIN",
                table: "ItemDetails",
                column: "GTIN",
                principalTable: "ProductPackSizeGTIN",
                principalColumn: "GTIN");

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Transactions_TypeId",
                table: "Reports",
                column: "TypeId",
                principalTable: "Transactions",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Countries_UserCountryId",
                table: "Transactions",
                column: "UserCountryId",
                principalTable: "Countries",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Roles_UserRoleId",
                table: "Transactions",
                column: "UserRoleId",
                principalTable: "Roles",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Users_UserId",
                table: "Transactions",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ItemDetails_ProductPackSizeGTIN_GTIN",
                table: "ItemDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Transactions_TypeId",
                table: "Reports");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Countries_UserCountryId",
                table: "Transactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Roles_UserRoleId",
                table: "Transactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Users_UserId",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_UserCountryId",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_UserId",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_UserRoleId",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_TransactionReviews_TransactionId",
                table: "TransactionReviews");

            migrationBuilder.DropIndex(
                name: "IX_SapResults_TransactionId",
                table: "SapResults");

            migrationBuilder.DropIndex(
                name: "IX_Reports_TypeId",
                table: "Reports");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_ProductPackSizeGTIN_GTIN",
                table: "ProductPackSizeGTIN");

            migrationBuilder.DropIndex(
                name: "IX_ProductPackSizeGTIN_GTIN",
                table: "ProductPackSizeGTIN");

            migrationBuilder.DropIndex(
                name: "IX_ItemDetails_GTIN",
                table: "ItemDetails");

            migrationBuilder.DropIndex(
                name: "IX_ItemDetails_TransactionId",
                table: "ItemDetails");

            migrationBuilder.DropColumn(
                name: "UserCountryId",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "UserRoleId",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "GeoLocation",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "TransactionId",
                table: "Reports");

            migrationBuilder.AddColumn<int>(
                name: "UserCountryId",
                table: "ScanInputDetails",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "ScanInputDetails",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserRoleId",
                table: "ScanInputDetails",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "GTIN",
                table: "ProductPackSizeGTIN",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AlterColumn<string>(
                name: "GTIN",
                table: "ItemDetails",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "DirectReportInfos",
                columns: table => new
                {
                    ReportId = table.Column<int>(type: "int", nullable: false),
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    BatchNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GTIN = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SerialNumber = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DirectReportInfos", x => x.ReportId);
                    table.ForeignKey(
                        name: "FK_DirectReportInfos_ProductCatalogues_ProductId",
                        column: x => x.ProductId,
                        principalTable: "ProductCatalogues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DirectReportInfos_Reports_ReportId",
                        column: x => x.ReportId,
                        principalTable: "Reports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ScanReportInfos",
                columns: table => new
                {
                    ReportId = table.Column<int>(type: "int", nullable: false),
                    TransactionId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScanReportInfos", x => x.ReportId);
                    table.ForeignKey(
                        name: "FK_ScanReportInfos_Reports_ReportId",
                        column: x => x.ReportId,
                        principalTable: "Reports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ScanReportInfos_Transactions_TransactionId",
                        column: x => x.TransactionId,
                        principalTable: "Transactions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TransactionReviews_TransactionId",
                table: "TransactionReviews",
                column: "TransactionId");

            migrationBuilder.CreateIndex(
                name: "IX_ScanInputDetails_UserCountryId",
                table: "ScanInputDetails",
                column: "UserCountryId");

            migrationBuilder.CreateIndex(
                name: "IX_ScanInputDetails_UserId",
                table: "ScanInputDetails",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ScanInputDetails_UserRoleId",
                table: "ScanInputDetails",
                column: "UserRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_SapResults_TransactionId",
                table: "SapResults",
                column: "TransactionId");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_TypeId",
                table: "Reports",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductPackSizeGTIN_GTIN",
                table: "ProductPackSizeGTIN",
                column: "GTIN",
                unique: true,
                filter: "[GTIN] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ItemDetails_TransactionId",
                table: "ItemDetails",
                column: "TransactionId");

            migrationBuilder.CreateIndex(
                name: "IX_DirectReportInfos_ProductId",
                table: "DirectReportInfos",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ScanReportInfos_TransactionId",
                table: "ScanReportInfos",
                column: "TransactionId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ScanInputDetails_Countries_UserCountryId",
                table: "ScanInputDetails",
                column: "UserCountryId",
                principalTable: "Countries",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ScanInputDetails_Roles_UserRoleId",
                table: "ScanInputDetails",
                column: "UserRoleId",
                principalTable: "Roles",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ScanInputDetails_Users_UserId",
                table: "ScanInputDetails",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id");
        }
    }
}
