﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Syngenta.CounterfeitDetection.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class UpdatedScanResultRepresentationRule : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScanResultRepresentationRules_Clients_ClientId",
                table: "ScanResultRepresentationRules");

            migrationBuilder.DropForeignKey(
                name: "FK_ScanResultRepresentationRules_Countries_CountryId",
                table: "ScanResultRepresentationRules");

            migrationBuilder.DropForeignKey(
                name: "FK_ScanResultRepresentationRules_Roles_RoleId",
                table: "ScanResultRepresentationRules");

            migrationBuilder.DropForeignKey(
                name: "FK_ScanResultRepresentationRules_ScanResultIcons_ResultIconId",
                table: "ScanResultRepresentationRules");

            migrationBuilder.DropForeignKey(
                name: "FK_ScanResultRepresentationRules_TransactionStatusTypes_TransactionResultTypeId",
                table: "ScanResultRepresentationRules");

            migrationBuilder.DropIndex(
                name: "IX_TranslationMasters_Key",
                table: "TranslationMasters");

            migrationBuilder.AlterColumn<string>(
                name: "Key",
                table: "TranslationMasters",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RoleId",
                table: "ScanResultRepresentationRules",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "ResultTitleKey",
                table: "ScanResultRepresentationRules",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "ResultIconId",
                table: "ScanResultRepresentationRules",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "ResultActionKey",
                table: "ScanResultRepresentationRules",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "CountryId",
                table: "ScanResultRepresentationRules",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "ClientId",
                table: "ScanResultRepresentationRules",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_TranslationMasters_Key",
                table: "TranslationMasters",
                column: "Key");

            migrationBuilder.CreateIndex(
                name: "IX_TranslationMasters_Key",
                table: "TranslationMasters",
                column: "Key",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ScanResultRepresentationRules_ResultActionKey",
                table: "ScanResultRepresentationRules",
                column: "ResultActionKey");

            migrationBuilder.CreateIndex(
                name: "IX_ScanResultRepresentationRules_ResultTitleKey",
                table: "ScanResultRepresentationRules",
                column: "ResultTitleKey");

            migrationBuilder.AddForeignKey(
                name: "FK_ScanResultRepresentationRules_Clients_ClientId",
                table: "ScanResultRepresentationRules",
                column: "ClientId",
                principalTable: "Clients",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ScanResultRepresentationRules_Countries_CountryId",
                table: "ScanResultRepresentationRules",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ScanResultRepresentationRules_Roles_RoleId",
                table: "ScanResultRepresentationRules",
                column: "RoleId",
                principalTable: "Roles",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ScanResultRepresentationRules_ScanResultIcons_ResultIconId",
                table: "ScanResultRepresentationRules",
                column: "ResultIconId",
                principalTable: "ScanResultIcons",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ScanResultRepresentationRules_TransactionResultTypes_TransactionResultTypeId",
                table: "ScanResultRepresentationRules",
                column: "TransactionResultTypeId",
                principalTable: "TransactionResultTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ScanResultRepresentationRules_TranslationMasters_ResultActionKey",
                table: "ScanResultRepresentationRules",
                column: "ResultActionKey",
                principalTable: "TranslationMasters",
                principalColumn: "Key");

            migrationBuilder.AddForeignKey(
                name: "FK_ScanResultRepresentationRules_TranslationMasters_ResultTitleKey",
                table: "ScanResultRepresentationRules",
                column: "ResultTitleKey",
                principalTable: "TranslationMasters",
                principalColumn: "Key",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScanResultRepresentationRules_Clients_ClientId",
                table: "ScanResultRepresentationRules");

            migrationBuilder.DropForeignKey(
                name: "FK_ScanResultRepresentationRules_Countries_CountryId",
                table: "ScanResultRepresentationRules");

            migrationBuilder.DropForeignKey(
                name: "FK_ScanResultRepresentationRules_Roles_RoleId",
                table: "ScanResultRepresentationRules");

            migrationBuilder.DropForeignKey(
                name: "FK_ScanResultRepresentationRules_ScanResultIcons_ResultIconId",
                table: "ScanResultRepresentationRules");

            migrationBuilder.DropForeignKey(
                name: "FK_ScanResultRepresentationRules_TransactionResultTypes_TransactionResultTypeId",
                table: "ScanResultRepresentationRules");

            migrationBuilder.DropForeignKey(
                name: "FK_ScanResultRepresentationRules_TranslationMasters_ResultActionKey",
                table: "ScanResultRepresentationRules");

            migrationBuilder.DropForeignKey(
                name: "FK_ScanResultRepresentationRules_TranslationMasters_ResultTitleKey",
                table: "ScanResultRepresentationRules");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_TranslationMasters_Key",
                table: "TranslationMasters");

            migrationBuilder.DropIndex(
                name: "IX_TranslationMasters_Key",
                table: "TranslationMasters");

            migrationBuilder.DropIndex(
                name: "IX_ScanResultRepresentationRules_ResultActionKey",
                table: "ScanResultRepresentationRules");

            migrationBuilder.DropIndex(
                name: "IX_ScanResultRepresentationRules_ResultTitleKey",
                table: "ScanResultRepresentationRules");

            migrationBuilder.AlterColumn<string>(
                name: "Key",
                table: "TranslationMasters",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AlterColumn<int>(
                name: "RoleId",
                table: "ScanResultRepresentationRules",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ResultTitleKey",
                table: "ScanResultRepresentationRules",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AlterColumn<int>(
                name: "ResultIconId",
                table: "ScanResultRepresentationRules",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ResultActionKey",
                table: "ScanResultRepresentationRules",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CountryId",
                table: "ScanResultRepresentationRules",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ClientId",
                table: "ScanResultRepresentationRules",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TranslationMasters_Key",
                table: "TranslationMasters",
                column: "Key",
                unique: true,
                filter: "[Key] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_ScanResultRepresentationRules_Clients_ClientId",
                table: "ScanResultRepresentationRules",
                column: "ClientId",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ScanResultRepresentationRules_Countries_CountryId",
                table: "ScanResultRepresentationRules",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ScanResultRepresentationRules_Roles_RoleId",
                table: "ScanResultRepresentationRules",
                column: "RoleId",
                principalTable: "Roles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ScanResultRepresentationRules_ScanResultIcons_ResultIconId",
                table: "ScanResultRepresentationRules",
                column: "ResultIconId",
                principalTable: "ScanResultIcons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ScanResultRepresentationRules_TransactionStatusTypes_TransactionResultTypeId",
                table: "ScanResultRepresentationRules",
                column: "TransactionResultTypeId",
                principalTable: "TransactionStatusTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
