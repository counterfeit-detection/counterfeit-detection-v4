﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Syngenta.CounterfeitDetection.Infrastructure.Helpers;

namespace Syngenta.CounterfeitDetection.DataAccess.Extensions;

public static class SeedHelper
{
    private const string SeedDataFolderName = "SeedDataFolderName";

    public static async Task PopulateSeedData<TEntity>(this DbSet<TEntity> dbSet) 
        where TEntity : class {
        IEnumerable<TEntity> seedData = GetSeedData<TEntity>();
        foreach (dynamic entity in seedData) {
            if(dbSet.Find(entity.Id) is null) 
                await dbSet.AddAsync(entity);
        }
    }

    private static IEnumerable<TEntity> GetSeedData<TEntity>()
    {
        try {
            var currentDirectory = AppDomain.CurrentDomain.BaseDirectory;
            var seedDataFolder = ConfigurationHelper.Get(SeedDataFolderName);

            var fullPath = Path.Combine(currentDirectory, seedDataFolder, $"{typeof(TEntity).Name}SeedData.json");

            var result = new List<TEntity>();
            using (var reader = new StreamReader(fullPath))
            {
                var json = reader.ReadToEnd();
                result = JsonConvert.DeserializeObject<List<TEntity>>(json);
            }

            return result ?? Enumerable.Empty<TEntity>();
        }
        catch {
            return Enumerable.Empty<TEntity>();
        }
    }
}
