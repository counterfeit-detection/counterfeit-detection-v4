﻿using Microsoft.EntityFrameworkCore.Query;
using System.Linq.Expressions;

namespace Syngenta.CounterfeitDetection.DataAccess.Interfaces;

public interface IGenericRepository<TEntity> where TEntity : class {
    Task<IEnumerable<TEntity>> CreateMultipleAsync(IEnumerable<TEntity> entities);
    
    Task<TEntity> CreateAsync(TEntity entity);

    Task<TEntity> UpdateAsync(TEntity entity); 

    Task<IEnumerable<TEntity>> UpdateRangeAsync(IEnumerable<TEntity> entities); 

    Task DeleteAsync(TEntity entity);

    Task<IEnumerable<TEntity>> GetAllAsync();    

    Task<IEnumerable<TEntity>> GetManyAsync(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        int? take  = null,
        int? skip = null,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null);

    Task<TEntity?> GetByIdAsync(int id);
}
