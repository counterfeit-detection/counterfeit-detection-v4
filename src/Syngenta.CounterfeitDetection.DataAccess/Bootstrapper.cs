﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Syngenta.CounterfeitDetection.DataAccess.Implementations;
using Microsoft.EntityFrameworkCore;
using Syngenta.CounterfeitDetection.DataAccess.Interfaces;

namespace Syngenta.CounterfeitDetection.DataAccess; 

public static class Bootstrapper {
    private const string DBConnection = "DBConnection";

    public static IServiceCollection RegisterDataAccess(this IServiceCollection services, IConfigurationSection connectionStrings) 
        => services.RegisterDBContext(connectionStrings)
        .RegisterServices();

    private static IServiceCollection RegisterDBContext(this IServiceCollection services, IConfigurationSection connectionStrings) {
        var connection = connectionStrings[DBConnection] 
                            ?? throw new KeyNotFoundException($"Connection string: {nameof(DBConnection)} doesn't exist in configuration.");
        services.AddDbContext<SyngentaContext>(options => options.UseSqlServer(connection));
        return services;
    }

    private static IServiceCollection RegisterServices(this IServiceCollection services) {
        services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
        return services;
    }

    public static async Task ApplyMigrations(this IServiceScope services) {
        var context = services.ServiceProvider.GetRequiredService<SyngentaContext>();
        await context.Database.MigrateAsync();
    }

    public static async Task SeedMasterData(this IServiceScope services) {
        var context = services.ServiceProvider.GetRequiredService<SyngentaContext>();
        await context.SeedMasterData();
    } 
}