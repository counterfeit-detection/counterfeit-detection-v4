﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Syngenta.CounterfeitDetection.DataAccess.Interfaces;
using System.Linq.Expressions;

namespace Syngenta.CounterfeitDetection.DataAccess.Implementations;

internal class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class {
    private readonly SyngentaContext _context;
    private readonly DbSet<TEntity> _dbSet;

    public GenericRepository(SyngentaContext context)
    {
        _context = context;
        _dbSet = context.Set<TEntity>();
    }

    public async Task<IEnumerable<TEntity>> CreateMultipleAsync(IEnumerable<TEntity> entities) {
        _dbSet.AddRange(entities);
        await _context.SaveChangesAsync();
        return entities;
    }

    public async Task<TEntity> CreateAsync(TEntity entity) {
        _dbSet.Add(entity);
        await _context.SaveChangesAsync();
        return entity;
    }

    public async Task DeleteAsync(TEntity entity) {
        _dbSet.Remove(entity);
        await _context.SaveChangesAsync();
    }

    public async Task<IEnumerable<TEntity>> GetAllAsync() => await _dbSet.AsNoTracking().ToListAsync();

    public async Task<IEnumerable<TEntity>> GetManyAsync(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        int? take  = null,
        int? skip = null,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null)
     {
        IQueryable<TEntity> query = _dbSet.AsNoTracking();
 
        if (filter != null)
            query = query.Where(filter);
 
        if (include != null)
            query = include(query);

        if (orderBy != null)
            query = orderBy(query);
 
        if (skip.HasValue)
            query = query.Skip(skip.Value);
 
        if (take.HasValue)
            query = query.Take(take.Value);
 
        return await query.ToListAsync();
    }

    public async Task<TEntity?> GetByIdAsync(int id) => await _dbSet.FindAsync(id);

    public async Task<TEntity> UpdateAsync(TEntity entity) {
        _dbSet.Update(entity);
        await _context.SaveChangesAsync();
        return entity;
    }

    public async Task<IEnumerable<TEntity>> UpdateRangeAsync(IEnumerable<TEntity> entities) {
        _dbSet.UpdateRange(entities);
        await _context.SaveChangesAsync();

        return entities;
    }
}
