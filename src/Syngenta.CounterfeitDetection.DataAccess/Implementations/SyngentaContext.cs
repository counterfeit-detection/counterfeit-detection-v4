﻿using Syngenta.CounterfeitDetection.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using Syngenta.CounterfeitDetection.DataAccess.Extensions;
using Microsoft.Extensions.Logging;

namespace Syngenta.CounterfeitDetection.DataAccess.Implementations;

public class SyngentaContext : DbContext {
    private readonly ILogger<SyngentaContext> _logger;

    public SyngentaContext(DbContextOptions<SyngentaContext> options, ILogger<SyngentaContext> logger)
        : base(options) => _logger = logger;

    public async Task SeedMasterData() {

        await proceed(AppTypes, nameof(AppTypes));
        await proceed(AuthTypes, nameof(AuthTypes));
        await proceed(BarcodeTypes, nameof(BarcodeTypes));
        await proceed(ClientFeatures, nameof(ClientFeatures));
        await proceed(FollowUpActionTypes, nameof(FollowUpActionTypes));
        await proceed(MailMessageTypes, nameof(MailMessageTypes));
        await proceed(MeasureUnits, nameof(MeasureUnits));
        await proceed(ProductTypes, nameof(ProductTypes));
        await proceed(ReportTypes, nameof(ReportTypes));
        await proceed(RepresentationFeatures, nameof(RepresentationFeatures));
        await proceed(SapResultTypes, nameof(SapResultTypes));
        await proceed(Themes, nameof(Themes));
        await proceed(TransactionStatusTypes, nameof(TransactionStatusTypes));
        await proceed(ExternalEndpointTypes, nameof(ExternalEndpointTypes));
        await proceed(FollowUpActionTypeOptions, nameof(FollowUpActionTypeOptions));
        await proceed(Regions, nameof(Regions));
        await proceed(Countries, nameof(Countries));
        await proceed(Clients, nameof(Clients));
        await proceed(TransactionResultTypes, nameof(TransactionResultTypes));
        await proceed(CodeTypeCheckRules, nameof(CodeTypeCheckRules));
        await proceed(QrDomainCheckRules, nameof(QrDomainCheckRules));
        await proceed(TransactionResultDefinitionRules, nameof(TransactionResultDefinitionRules));
        await proceed(TransactionResultFollowUpActions, nameof(TransactionResultFollowUpActions));
        await proceed(FollowUpActionResultRules, nameof(FollowUpActionResultRules));
        await proceed(FollowUpResultFollowUpActions, nameof(FollowUpResultFollowUpActions));
        await proceed(TranslationMasters, nameof(TranslationMasters));
        await proceed(ScanResultRepresentationRules, nameof(ScanResultRepresentationRules));
        await proceed(ScanningPoints, nameof(ScanningPoints));
        await proceed(Languages, nameof(Languages));

        async Task proceed<TEntity>(DbSet<TEntity> dbSet, string tableName) where TEntity : class {
            _logger.Log(LogLevel.Information, $"Populating seed data for <{tableName}>...");

            using var transaction = Database.BeginTransaction();

            await Database.ExecuteSqlRawAsync($"SET IDENTITY_INSERT [dbo].[{tableName}] ON");
            await SaveChangesAsync();
            
            await dbSet.PopulateSeedData();
            await SaveChangesAsync();

            await Database.ExecuteSqlRawAsync($"SET IDENTITY_INSERT [dbo].[{tableName}] OFF");
            transaction.Commit();
            
            _logger.Log(LogLevel.Information, $"Populating seed data for <{tableName}> completed");
        }
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<ScanResultRepresentationRule>()
            .HasOne(s => s.MasterResultTitle)
            .WithMany(c => c.ScanResultRepresentationRuleTitle)
            .HasForeignKey(s => s.ResultTitleKey)
            .HasPrincipalKey(c => c.Key);

        modelBuilder.Entity<ScanResultRepresentationRule>()
            .HasOne(s => s.MasterResultAction)
            .WithMany(c => c.ScanResultRepresentationRuleAction)
            .HasForeignKey(s => s.ResultActionKey)
            .HasPrincipalKey(c => c.Key);
    }

    public DbSet<AppType> AppTypes { get; set; } = null!;

    public DbSet<AuditEntry> AuditEntries { get; set; } = null!;

    public DbSet<AuthType> AuthTypes { get; set; } = null!;

    public DbSet<BarcodeType> BarcodeTypes { get; set; } = null!;

    public DbSet<BlobStorageFile> BlobStorageFiles { get; set; } = null!;

    public DbSet<Client> Clients { get; set; } = null!;

    public DbSet<ClientAppType> ClientAppTypes { get; set; } = null!;

    public DbSet<ClientAuthType> ClientAuthTypes { get; set; } = null!;

    public DbSet<ClientFeature> ClientFeatures { get; set; } = null!;

    public DbSet<ClientFeatureSetting> ClientFeatureSettings { get; set; } = null!;

    public DbSet<CodeTypeCheckRule> CodeTypeCheckRules { get; set; } = null!;

    public DbSet<Country> Countries { get; set; } = null!;
    
    public DbSet<CountryMailSetting> CountryMailSettings { get; set; } = null!;

    public DbSet<ExternalEndpoint> ExternalEndpoints { get; set; } = null!;

    public DbSet<ExternalEndpointType> ExternalEndpointTypes { get; set; } = null!;

    public DbSet<FollowUpAction> FollowUpActions { get; set; } = null!;

    public DbSet<FollowUpActionResultRule> FollowUpActionResultRules { get; set; } = null!;

    public DbSet<FollowUpActionType> FollowUpActionTypes { get; set; } = null!;

    public DbSet<FollowUpActionTypeOption> FollowUpActionTypeOptions { get; set; } = null!;

    public DbSet<FollowUpResultFollowUpAction> FollowUpResultFollowUpActions { get; set; } = null!;

    public DbSet<GeneralReportReason> GeneralReportReasons { get; set; } = null!;

    public DbSet<ItemDetails> ItemDetails { get; set; } = null!;

    public DbSet<Language> Languages { get; set; } = null!;

    public DbSet<MailMessageTemplate> MailMessageTemplates { get; set; } = null!;

    public DbSet<MailMessageType> MailMessageTypes { get; set; } = null!;

    public DbSet<MeasureUnit> MeasureUnits { get; set; } = null!;

    public DbSet<OutgoingMail> OutgoingMails { get; set; } = null!;

    public DbSet<OutgoingMailAttachment> OutgoingMailAttachments { get; set; } = null!;

    public DbSet<ProductBrand> ProductCatalogues { get; set; } = null!;

    public DbSet<ProductCountry> ProductCountries { get; set; } = null!;

    public DbSet<ProductPackSize> ProductPackSizes { get; set; } = null!;
    
    public DbSet<ProductPackSizeGTIN> ProductPackSizeGTIN { get; set; } = null!;

    public DbSet<ProductType> ProductTypes { get; set; } = null!;

    public DbSet<QRDomainCheckRule> QrDomainCheckRules { get; set; } = null!;

    public DbSet<Region> Regions { get; set; } = null!;

    public DbSet<Report> Reports { get; set; } = null!;

    public DbSet<ReportImage> ReportImages { get; set; } = null!;

    public DbSet<ReportReasonSetting> ReportReasonSettings { get; set; } = null!;

    public DbSet<ReportType> ReportTypes { get; set; } = null!;

    public DbSet<RepresentationFeature> RepresentationFeatures { get; set; } = null!;

    public DbSet<Role> Roles { get; set; } = null!;

    public DbSet<SapResult> SapResults { get; set; } = null!;

    public DbSet<SapResultType> SapResultTypes { get; set; } = null!;

    public DbSet<ScanInputDetails> ScanInputDetails { get; set; } = null!;

    public DbSet<ScanningPoint> ScanningPoints { get; set; } = null!;

    public DbSet<ScanningPointSetting> ScanningPointSettings { get; set; } = null!;

    public DbSet<ScanResultIcon> ScanResultIcons { get; set; } = null!;

    public DbSet<ScanResultRepresentationFeature> ScanResultRepresentationFeatures { get; set; } = null!;

    public DbSet<ScanResultRepresentationRule> ScanResultRepresentationRules { get; set; } = null!;

    public DbSet<Theme> Themes { get; set; } = null!;

    public DbSet<ThemeColorsSetting> ThemeColorsSettings { get; set; } = null!;

    public DbSet<Transaction> Transactions { get; set; } = null!;

    public DbSet<TransactionResult> TransactionResults { get; set; } = null!;

    public DbSet<TransactionResultDefinitionRule> TransactionResultDefinitionRules { get; set; } = null!;

    public DbSet<TransactionResultFollowUpActions> TransactionResultFollowUpActions { get; set; } = null!;

    public DbSet<TransactionResultType> TransactionResultTypes { get; set; } = null!;

    public DbSet<TransactionReview> TransactionReviews { get; set; } = null!;

    public DbSet<TransactionStatusType> TransactionStatusTypes { get; set; } = null!;

    public DbSet<TranslationLanguage> TranslationLanguages { get; set; } = null!;

    public DbSet<TranslationMaster> TranslationMasters { get; set; } = null!;

    public DbSet<User> Users { get; set; } = null!;
}
