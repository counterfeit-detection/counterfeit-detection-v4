﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class ScanResultRepresentationFeature {
    [Key]
    public int Id { get; set; }
    
    public int RuleId { get; set; }

    public int FeatureId { get; set; }

    public bool IsEnabled { get; set; }
    
    [ForeignKey(nameof(RuleId))]
    public ScanResultRepresentationRule? ScanResultRepresentationRule { get; set; }

    [ForeignKey(nameof(FeatureId))]
    public RepresentationFeature? RepresentationFeature { get; set; }
}