﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class ReportImage {
    [Key]
    public int Id { get; set; }

    public int ReportId { get; set; }

    public int ImageFileId { get; set; }

    [ForeignKey(nameof(ReportId))]
    public virtual Report? Report { get; set; }

    [ForeignKey(nameof(ImageFileId))]
    public virtual BlobStorageFile? ImageFile { get; set; }
}