﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class ScanningPointSetting {
    [Key]
    public int Id { get; set; }

    public int Order { get; set; }

    public int CountryId { get; set; }

    public int RoleId { get; set; }

    public int ClientId { get; set; }

    public int ScanningPointId { get; set; }

    [ForeignKey(nameof(CountryId))]
    public Country? Country { get; set; }

    [ForeignKey(nameof(RoleId))]
    public Role? Role { get; set; }

    [ForeignKey(nameof(ClientId))]
    public Client? Client { get; set; }

    [ForeignKey(nameof(ScanningPointId))]
    public ScanningPoint? ScanningPoint { get; set; }
}