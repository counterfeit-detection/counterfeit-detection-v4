﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class ClientAuthType {
    [Key]
    public int Id { get; set; }

    public int ClientId { get; set; }

    public int AuthTypeId { get; set; }

    public string? AuthUrl { get; set; }

    [ForeignKey(nameof(ClientId))]
    public Client? Client { get; set; }

    [ForeignKey(nameof(AuthTypeId))]
    public AuthType? AuthType { get; set; }
}