﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

[Index(nameof(GTIN), IsUnique = true)]
public class ProductPackSizeGTIN {
    [Key]
    public int Id { get; set; }

    public int PackSizeId { get; set; }

    public string? GTIN { get; set; }

    public bool IsGPD { get; set; }

    [ForeignKey(nameof(PackSizeId))]
    public ProductPackSize? PackSize { get; set; }
}