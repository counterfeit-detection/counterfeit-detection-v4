﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class TransactionResult {
    [Key]
    public int Id { get; set; }

    public int TransactionId { get; set; }

    public int TypeId { get; set; }

    [ForeignKey(nameof(TransactionId))]
    public virtual Transaction? Transaction { get; set; }

    [ForeignKey(nameof(TypeId))]
    public virtual TransactionResultType? Type { get; set; }
}