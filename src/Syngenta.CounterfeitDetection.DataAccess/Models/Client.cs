﻿using System.ComponentModel.DataAnnotations;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class Client {
    [Key]
    public int Id { get; set; }

    public string? Name { get; set; }

    public IEnumerable<ClientFeatureSetting>? Features { get; set; }

    public IEnumerable<ThemeColorsSetting>? Themes { get; set; }

    public IEnumerable<ScanResultRepresentationRule>? ResultTypes { get; set; }

    public IEnumerable<ReportReasonSetting>? ReportReasons { get; set; }

    public IEnumerable<ScanningPointSetting>? ScanOrigins { get; set; }
}