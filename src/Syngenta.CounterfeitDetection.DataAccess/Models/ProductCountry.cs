﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class ProductCountry {
    [Key]
    public int Id { get; set; }

    public int CountryId { get; set; }

    public int ProductId { get; set; }

    [ForeignKey(nameof(CountryId))]
    public Country? Country { get; set; }

    [ForeignKey(nameof(ProductId))]
    public ProductBrand? ProductCatalogue { get; set; }
}