﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace Syngenta.CounterfeitDetection.DataAccess.Models;

[Index(nameof(Type), IsUnique=true)]
public class MailMessageType {
    [Key]
    public int Id { get; set; }

    public string? Type { get; set; }
}