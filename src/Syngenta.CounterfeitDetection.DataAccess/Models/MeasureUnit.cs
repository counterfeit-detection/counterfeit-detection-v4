﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

[Index(nameof(Unit), IsUnique = true)]
public class MeasureUnit {
    [Key]
    public int Id { get; set; }

    public string? Unit { get; set; }
}