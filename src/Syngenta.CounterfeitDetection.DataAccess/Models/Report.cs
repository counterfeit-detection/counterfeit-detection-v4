﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class Report {
    [Key]
    public int Id { get; set; }

    public int? TypeId { get; set; }
    
    public int? TransactionId { get; set; }

    public int? ScanningPointId { get; set; }

    public int? ReportReasonId { get; set; }

    public DateTime SendTime { get; set; }

    public string? ScanAddress { get; set; }

    public string? ScanLocationName { get; set; }

    public string? CustomReportReason { get; set; }

    public string? Comment { get; set; }

    [ForeignKey(nameof(TypeId))]
    public virtual ReportType? Type { get; set; }
    
    [ForeignKey(nameof(TransactionId))]
    public virtual Transaction? Transaction { get; set; }

    [ForeignKey(nameof(ScanningPointId))]
    public virtual ScanningPoint? ScanningPoint { get; set; }
    
    [ForeignKey(nameof(ReportReasonId))]
    public ReportReasonSetting? ReportReason { get; set; }

    // inverse nav props
    public virtual IEnumerable<ReportImage>? Images { get; set; }
}