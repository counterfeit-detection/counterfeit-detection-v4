﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class TransactionResultFollowUpActions {
    [Key]
    public int Id { get; set; }

    public int DefinitionRuleId { get; set; }

    public int FollowUpActionTypeId { get; set; }

    [ForeignKey(nameof(DefinitionRuleId))]
    public TransactionResultDefinitionRule? DefinitionRule { get; set; }

    [ForeignKey(nameof(FollowUpActionTypeId))]
    public FollowUpActionType? FollowUpActionType { get; set; }
}