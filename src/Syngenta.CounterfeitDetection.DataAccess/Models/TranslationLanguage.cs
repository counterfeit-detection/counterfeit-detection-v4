﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class TranslationLanguage {
    [Key]
    public int Id { get; set; }

    public int MasterId { get; set; }

    public int LanguageId { get; set; }

    public string? Value { get; set; }

    [ForeignKey(nameof(MasterId))]
    public TranslationMaster? TranslationMaster { get; set; }

    [ForeignKey(nameof(LanguageId))]
    public Language? Language { get; set; }
}