﻿using System.ComponentModel.DataAnnotations;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class ReportType {
    [Key]
    public int Id { get; set; }

    public string? Type { get; set; }
}