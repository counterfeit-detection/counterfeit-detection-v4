﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class SapResult {
    [Key]
    public int Id { get; set; }

    public int TransactionId { get; set; }

    public int TypeId { get; set; }

    public string? ResponseTypeCode { get; set; }

    public string? ResponseErrorCode { get; set; }
    
    public string? ResponseText { get; set; }
    
    public string? ProductBusinessUnit { get; set; }
    
    public string? ProductDestinationCountry { get; set; }

    public int NumberOfScans { get; set; }

    [ForeignKey(nameof(TransactionId))]
    public virtual Transaction? Transaction { get; set; }

    [ForeignKey(nameof(TypeId))]
    public virtual SapResultType? Type { get; set; }
}