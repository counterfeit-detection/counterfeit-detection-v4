﻿using System.ComponentModel.DataAnnotations;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class RepresentationFeature {
    [Key]
    public int Id { get; set; }

    public string? Name { get; set; }
}