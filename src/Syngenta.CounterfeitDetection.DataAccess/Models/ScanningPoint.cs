﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

[Index(nameof(Point), IsUnique = true)]
public class ScanningPoint {
    [Key]
    public int Id { get; set; }

    public string? Point { get; set; }
}