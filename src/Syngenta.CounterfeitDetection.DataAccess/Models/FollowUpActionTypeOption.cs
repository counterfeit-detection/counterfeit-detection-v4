﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

[Index(nameof(TypeId), nameof(Order), IsUnique=true)]
public class FollowUpActionTypeOption {
    [Key]
    public int Id { get; set; }
    
    public int TypeId { get; set; }
    
    public int Order { get; set; }

    public string? Option { get; set; }

    [ForeignKey(nameof(TypeId))]
    public FollowUpActionType? FollowUpActionType { get; set; }
}