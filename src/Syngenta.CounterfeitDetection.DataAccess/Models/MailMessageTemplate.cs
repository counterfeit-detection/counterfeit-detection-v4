﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class MailMessageTemplate {
    [Key]
    public int Id { get; set; }

    public int TypeId { get; set; }

    public string? Name { get; set; }

    public string? Subject { get; set; }

    public string? Body { get; set; }

    [ForeignKey(nameof(TypeId))]
    public MailMessageType? MailMessageType { get; set; }
}