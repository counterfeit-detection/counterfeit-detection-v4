﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class ThemeColorsSetting {
    [Key]
    public int Id { get; set; }

    public int? CountryId { get; set; }

    public int? RoleId { get; set; }

    public int ClientId { get; set; }

    public int ThemeId { get; set; }

    public string? Main { get; set; }

    public string? Dark { get; set; }

    public string? Light { get; set; }
    
    public string? ContrastText { get; set; }

    [ForeignKey(nameof(CountryId))]
    public Country? Country { get; set; }

    [ForeignKey(nameof(RoleId))]
    public Role? Role { get; set; }

    [ForeignKey(nameof(ClientId))]
    public Client? Client { get; set; }

    [ForeignKey(nameof(ThemeId))]
    public Theme? Theme { get; set; }
}