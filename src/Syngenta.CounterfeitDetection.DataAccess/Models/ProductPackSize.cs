﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class ProductPackSize {
    [Key]
    public int Id { get; set; }

    public int ProductId { get; set; }

    public int UnitId { get; set; }

    public string? Size { get; set; }

    [ForeignKey(nameof(ProductId))]
    public virtual ProductBrand? ProductCatalogue { get; set; }

    [ForeignKey(nameof(UnitId))]
    public virtual MeasureUnit? MeasureUnit { get; set; }

    public IEnumerable<ProductPackSizeGTIN>? ProductPackSizeGTINs { get; set; }
}