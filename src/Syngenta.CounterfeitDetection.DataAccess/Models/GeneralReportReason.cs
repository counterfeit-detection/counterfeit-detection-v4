﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

[Index(nameof(Reason), IsUnique = true)]
public class GeneralReportReason {
    [Key]
    public int Id { get; set; }

    public string? Reason { get; set; }
}