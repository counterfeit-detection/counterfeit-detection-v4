﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

[Index(nameof(Type), IsUnique = true)]
public class SapResultType {
    [Key]
    public int Id { get; set; }

    public string? Type { get; set; }
}