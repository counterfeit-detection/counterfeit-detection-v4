﻿using System.ComponentModel.DataAnnotations;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class AppType {
    [Key]
    public int Id { get; set; }

    public string? Name { get; set; }
}