﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class OutgoingMailAttachment {
    [Key]
    public int Id { get; set; }

    public int MailId { get; set; }

    public int AttachmentId { get; set; }

    [ForeignKey(nameof(MailId))]
    public OutgoingMail? OutgoingMail { get; set; }

    [ForeignKey(nameof(AttachmentId))]
    public BlobStorageFile? BlobStorageFile { get; set; }
}