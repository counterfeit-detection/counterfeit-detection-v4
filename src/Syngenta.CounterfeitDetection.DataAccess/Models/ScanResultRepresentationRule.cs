﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

[Index(nameof(Order), IsUnique = true)]
public class ScanResultRepresentationRule {
    [Key]
    public int Id { get; set; }

    public int Order { get; set; }

    public int? CountryId { get; set; }

    public int? ClientId { get; set; }

    public int? RoleId { get; set; }

    public int TransactionResultTypeId { get; set; }

    public string ResultTitleKey { get; set; } = String.Empty;
    
    public string? ResultActionKey { get; set; }

    public int? ResultIconId { get; set; }

    public string? ResultTitleColor { get; set; }

    public string? ResultActionColor { get; set; }
    
    [ForeignKey(nameof(CountryId))]
    public Country? Country { get; set; }

    [ForeignKey(nameof(ClientId))]
    public Client? Client { get; set; }

    [ForeignKey(nameof(RoleId))]
    public Role? Role { get; set; }

    [ForeignKey(nameof(TransactionResultTypeId))]
    public TransactionResultType? TransactionResultType { get; set; }

    [ForeignKey(nameof(ResultIconId))]
    public ScanResultIcon? ResultIcon { get; set; }

    [ForeignKey(nameof(ResultTitleKey))]
    public TranslationMaster? MasterResultTitle { get; set; }

    [ForeignKey(nameof(ResultActionKey))]
    public TranslationMaster? MasterResultAction { get; set; }
}