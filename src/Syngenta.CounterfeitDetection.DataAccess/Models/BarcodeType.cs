﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

[Index(nameof(Value), IsUnique = true)]
public class BarcodeType {
    [Key] 
    public int Id { get; set; }

    [MaxLength(100)]
    public string? Name { get; set; }

    [MaxLength(100)]
    public string? Value { get; set; }
}