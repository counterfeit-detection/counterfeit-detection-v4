﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class ReportReasonSetting {
    [Key]
    public int Id { get; set; }

    public int Order { get; set; }

    public int? CountryId { get; set; }

    public int? RoleId { get; set; }

    public int? ClientId { get; set; }

    public int? GeneralReasonId { get; set; }

    public string? ReasonKey { get; set; }

    public bool RequireCustomReason { get; set; }

    [ForeignKey(nameof(CountryId))]
    public Country? Country { get; set; }

    [ForeignKey(nameof(RoleId))]
    public Role? Role { get; set; }

    [ForeignKey(nameof(ClientId))]
    public Client? Client { get; set; }

    [ForeignKey(nameof(GeneralReasonId))]
    public GeneralReportReason? GeneralReportReason { get; set; }
}