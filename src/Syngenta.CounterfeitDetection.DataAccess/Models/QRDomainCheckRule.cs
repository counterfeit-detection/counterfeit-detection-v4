﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

[Index(nameof(Order), IsUnique = true)]
public class QRDomainCheckRule {
    [Key]
    public int Id { get; set; }

    public int Order { get; set; }

    public int CountryId { get; set; }

    public int FailScanResultId { get; set; }

    public string? AllowedDomain { get; set; }

    [ForeignKey(nameof(CountryId))]
    public Country? Country { get; set; }

    [ForeignKey(nameof(FailScanResultId))]
    public TransactionResultType? FailScanResult { get; set; }
}