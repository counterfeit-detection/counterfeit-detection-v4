﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class ItemDetails {
    [Key]
    public int Id { get; set; }

    public int TransactionId { get; set; }

    public string? GTIN { get; set; }

    public string? SerialNumber { get; set; }

    public string? BatchNumber { get; set; }

    public DateTime? ExpiryDate { get; set; }

    public DateTime? ManufacturedDate { get; set; }

    public string? ManufacturedAt { get; set; }

    public int PackSize { get; set; }

    [ForeignKey(nameof(TransactionId))]
    public Transaction? Transaction { get; set; }
}