﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class Transaction {
    [Key]
    public int Id { get; set; }

    public int StatusId { get; set; }

    public int? UserId { get; set; }

    public int? UserRoleId { get; set; }

    public int? UserCountryId { get; set; }

    public string? GeoLocation { get; set; }

    public DateTime CreationTime { get; set; }

    public DateTime LastUpdateTime { get; set; }

    [ForeignKey(nameof(StatusId))]
    public virtual TransactionStatusType? Status { get; set; }

    [ForeignKey(nameof(UserId))]
    public virtual User? User { get; set; }

    [ForeignKey(nameof(UserRoleId))] 
    public virtual Role? UserRole { get; set; }

    [ForeignKey(nameof(UserCountryId))]
    public virtual Country? UserCountry { get; set; }

    // inverse nav props
    public virtual TransactionResult? Result { get; set; }

    public virtual ScanInputDetails? ScanInputDetails { get; set; }
    
    public virtual ItemDetails? ItemDetails { get; set; }
    
    public virtual TransactionReview? TransactionReview { get; set; }
    
    public virtual Report? TransactionReport { get; set; }

    public virtual SapResult? SapDetails { get; set; }

    public virtual IEnumerable<FollowUpAction>? FollowUpActions { get; set; }
}