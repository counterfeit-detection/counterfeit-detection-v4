﻿using System.ComponentModel.DataAnnotations;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class ScanResultIcon {
    [Key]
    public int Id { get; set; }

    public string? Image { get; set; }
}