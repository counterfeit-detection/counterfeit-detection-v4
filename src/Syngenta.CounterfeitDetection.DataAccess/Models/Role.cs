﻿using System.ComponentModel.DataAnnotations;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class Role {
    [Key]
    public int Id { get; set; }

    public string? Name { get; set; }

    public string? Code { get; set; }
}