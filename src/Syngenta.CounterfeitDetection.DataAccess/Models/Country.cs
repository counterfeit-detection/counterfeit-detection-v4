﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class Country {
    [Key]
    public int Id { get; set; }

    public int RegionId { get; set; }

    public string? Name { get; set; }

    public string? Code { get; set; }

    [ForeignKey(nameof(RegionId))]
    public Region? Region { get; set; }
}