﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class CountryMailSetting {
    [Key]
    public int Id { get; set; }

    public int CountryId { get; set; }

    public int MessageTypeId { get; set; }

    public string? To { get; set; }

    public string? Cc { get; set; }

    public string? Bcc { get; set; }

    public string? From { get; set; }

    public string? FromMask { get; set; }

    [ForeignKey(nameof(CountryId))]
    public Country? Country { get; set; }

    [ForeignKey(nameof(MessageTypeId))]
    public MailMessageType? MessageType { get; set; }
}