﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class TransactionReview {
    [Key]
    public int Id { get; set; }

    public int TransactionId { get; set; }

    public DateTime ReviewTime { get; set; }

    [ForeignKey(nameof(TransactionId))]
    public Transaction? Transaction { get; set; }
}