﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

[Index(nameof(Order), IsUnique = true)]
public class FollowUpActionResultRule {
    [Key]
    public int Id { get; set; }

    public int Order { get; set; }

    public int? CountryId { get; set; }

    public int? ClientId { get; set; }

    public int FollowUpActionTypeId { get; set; }

    public int? FollowUpPickedOptionId { get; set; }

    public int? DesiredTransactionResultTypeId { get; set; }

    [ForeignKey(nameof(CountryId))]
    public Country? Country { get; set; }

    [ForeignKey(nameof(ClientId))]
    public Client? Client { get; set; }

    [ForeignKey(nameof(FollowUpActionTypeId))]
    public FollowUpActionType? FollowUpActionType { get; set; }

    [ForeignKey(nameof(FollowUpPickedOptionId))]
    public FollowUpActionTypeOption? FollowUpPickedOption { get; set; }

    [ForeignKey(nameof(DesiredTransactionResultTypeId))]
    public TransactionResultType? DesiredTransactionResultType { get; set; }

    // inverse nav props
    public virtual IEnumerable<FollowUpResultFollowUpAction>? DesiredFollowUpActions { get; set; }
}