﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class ClientAppType {
    [Key]
    public int Id { get; set; }

    public int ClientId { get; set; }

    public int AppTypeId { get; set; }

    public string? ClientAppName { get; set; }

    [ForeignKey(nameof(ClientId))]
    public Client? Client { get; set; }

    [ForeignKey(nameof(AppTypeId))]
    public AppType? AppType { get; set; }
}