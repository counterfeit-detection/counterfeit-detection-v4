﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class User {
    [Key]
    public int Id { get; set; }

    public int CountryId { get; set; }

    public int? RoleId { get; set; }

    public string? Name { get; set; }

    public string? AuthId { get; set; }

    [ForeignKey(nameof(CountryId))]
    public Country? Country { get; set; }

    [ForeignKey(nameof(RoleId))]
    public Role? Role { get; set; }
}