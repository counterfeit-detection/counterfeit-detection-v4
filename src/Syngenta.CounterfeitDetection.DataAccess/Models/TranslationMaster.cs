﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

[Index(nameof(Key), IsUnique=true)]
public class TranslationMaster {
    [Key]
    public int Id { get; set; }
    
    public string? Key { get; set; }

    public string? DefaultValue { get; set; }

    public IEnumerable<TranslationLanguage>? LanguageTranslations { get; set; }
    
    public IEnumerable<ScanResultRepresentationRule>? ScanResultRepresentationRuleTitle { get; set; }

    public IEnumerable<ScanResultRepresentationRule>? ScanResultRepresentationRuleAction { get; set; }
}