﻿using System.ComponentModel.DataAnnotations;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class BlobStorageFile {
    [Key]
    public int Id { get; set; }

    public string? FileName { get; set; }

    public string? StorageUrl { get; set; }
}