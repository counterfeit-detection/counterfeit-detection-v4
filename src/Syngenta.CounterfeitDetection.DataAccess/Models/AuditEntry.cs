﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class AuditEntry {
    [Key]
    public int Id { get; set; }

    public int UserId { get; set; }

    public string? ObjectType { get; set; }

    public DateTime Time { get; set; }

    public string? Action { get; set; }

    public string? Comment { get; set; }

    [ForeignKey(nameof(UserId))]
    public User? User { get; set; }
}