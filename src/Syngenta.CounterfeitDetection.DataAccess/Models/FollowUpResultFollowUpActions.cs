﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class FollowUpResultFollowUpAction {
    [Key]
    public int Id { get; set; }

    public int FollowUpActionResultRuleId { get; set; }

    public int FollowUpActionTypeId { get; set; }

    [ForeignKey(nameof(FollowUpActionResultRuleId))]
    public FollowUpActionResultRule? FollowUpActionResultRule { get; set; }

    [ForeignKey(nameof(FollowUpActionTypeId))]
    public FollowUpActionType? FollowUpActionType { get; set; }
}