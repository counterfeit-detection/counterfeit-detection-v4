﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class FollowUpAction {
    [Key]
    public int Id { get; set; }

    public int TransactionId { get; set; }

    public int TypeId { get; set; }

    public bool IsCompleted { get; set; }

    public int? PickedOptionId { get; set; }

    public string? Data { get; set; }

    [ForeignKey(nameof(TransactionId))]
    public virtual Transaction? Transaction { get; set; }

    [ForeignKey(nameof(TypeId))]
    public virtual FollowUpActionType? Type { get; set; }

    [ForeignKey(nameof(PickedOptionId))]
    public virtual FollowUpActionTypeOption? PickedOption { get; set; }
}