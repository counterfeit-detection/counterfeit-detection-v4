﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class ProductBrand {
    [Key]
    public int Id { get; set; }

    public int ProductTypeId { get; set; }

    public string? Name { get; set; }

    public bool? IsGPD { get; set; }

    [ForeignKey(nameof(ProductTypeId))]
    public ProductType? ProductType { get; set; }

    public IEnumerable<ProductPackSize>? ProductPackSizes { get; set; }

    public ProductCountry? ProductCountry { get; set; }
}