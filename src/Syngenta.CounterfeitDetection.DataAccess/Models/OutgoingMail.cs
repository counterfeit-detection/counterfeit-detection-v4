﻿using System.ComponentModel.DataAnnotations;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class OutgoingMail {
    [Key]
    public int Id { get; set; }

    public string? To { get; set; }

    public string? Cc { get; set; }

    public string? Bcc { get; set; }

    public string? From { get; set; }

    public string? FromMask { get; set; }

    public string? Subject { get; set; }

    public string? Body { get; set; }

    public DateTime? SendTime { get; set; }
}