﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

public class ExternalEndpoint {
    [Key]
    public int Id { get; set; }

    public int CountryId { get; set; }

    public int EndpointTypeId { get; set; }

    public string? EndpointUrl { get; set; }

    [ForeignKey(nameof(CountryId))]
    public virtual Country? Country { get; set; }

    [ForeignKey(nameof(EndpointTypeId))]
    public virtual ExternalEndpointType? ExternalEndpointType { get; set; }
}