﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

[Index(nameof(Order), IsUnique = true)]
public class TransactionResultDefinitionRule {
    [Key]
    public int Id { get; set; }

    public int Order { get; set; }

    public int? CountryId { get; set; }

    public int? ClientId { get; set; }

    public int? SapResultTypeId { get; set; }

    public int? DesiredTransactionResultTypeId { get; set; }

    public int? SAPScanCount { get; set; }
    
    public string? SAPBU { get; set; }
    
    public string? SAPCountry { get; set; }

    [ForeignKey(nameof(CountryId))]
    public Country? Country { get; set; }

    [ForeignKey(nameof(ClientId))]
    public Client? Client { get; set; }

    [ForeignKey(nameof(SapResultTypeId))]
    public SapResultType? SapResultType { get; set; }

    [ForeignKey(nameof(DesiredTransactionResultTypeId))]
    public TransactionResultType? DesiredTransactionResultType { get; set; }

    public ICollection<TransactionResultFollowUpActions>? FollowUpActions { get; set; }
}