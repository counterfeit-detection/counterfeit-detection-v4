﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Syngenta.CounterfeitDetection.DataAccess.Models; 

[Index(nameof(Fingerprint), IsUnique = true)]
public class ScanInputDetails {
    [Key]
    public int Id { get; set; }

    public string? Fingerprint { get; set; }

    public int TransactionId { get; set; }

    public int? CodeTypeId { get; set; }

    public int? ClientAppTypeId { get; set; }

    public bool IsManualScan { get; set; }
    
    public bool IsOffline { get; set; }

    public string? RawData { get; set; }

    public DateTime ScanTime { get; set; }

    [ForeignKey(nameof(TransactionId))]
    public virtual Transaction? Transaction { get; set; }

    [ForeignKey(nameof(CodeTypeId))]
    public virtual BarcodeType? CodeType { get; set; }

    [ForeignKey(nameof(ClientAppTypeId))]
    public virtual ClientAppType? ClientAppType { get; set; }
}