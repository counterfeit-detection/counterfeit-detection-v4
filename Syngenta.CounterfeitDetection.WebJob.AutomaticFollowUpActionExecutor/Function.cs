﻿using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Syngenta.CounterfeitDetection.WebJob.AutomaticFollowUpActionExecutor.Services.Interfaces;

namespace Syngenta.CounterfeitDetection.WebJob.AutomaticFollowUpActionExecutor
{
    public class Function {
        private readonly IExecutorService _service;
        private readonly ILogger<Function> _logger;

        public Function(IExecutorService service, ILogger<Function> logger) {
            _service = service;
            _logger = logger;
        }
        [FunctionName(nameof(AutomaticFollowUpActionExecutor))] 
        public async Task AutomaticFollowUpActionExecutor([TimerTrigger("%WebJobs:AutomaticFollowUpActionExecutor:Timer%", RunOnStartup = true)] TimerInfo timer)
        {
            _logger.LogInformation($"{nameof(AutomaticFollowUpActionExecutor)} WebJob started.");

            await _service.ExecuteAsync();

            _logger.LogInformation($"{nameof(AutomaticFollowUpActionExecutor)} WebJob finished.");
        }
    }
}
