﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Syngenta.CounterfeitDetection.DataAccess;
using Syngenta.CounterfeitDetection.WebJob.AutomaticFollowUpActionExecutor.Constants;
using Syngenta.CounterfeitDetection.WebJob.AutomaticFollowUpActionExecutor.Services;
using Syngenta.CounterfeitDetection.WebJob.AutomaticFollowUpActionExecutor.Services.FollowUpActionHandlers;
using Syngenta.CounterfeitDetection.WebJob.AutomaticFollowUpActionExecutor.Services.Interfaces;

IConfiguration configuration = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
    .AddEnvironmentVariables()
    .Build();

var builder = new HostBuilder();
builder.UseEnvironment(EnvironmentName.Development);

builder.ConfigureWebJobs(b => {
    b.AddAzureStorageCoreServices();
    b.AddTimers();
});

builder.ConfigureLogging((context, b) => b.AddConsole());

builder.ConfigureServices(services => {
    services.AddSingleton<IConfiguration>(configuration);
    services.RegisterDataAccess(configuration.GetSection(Constants.ConnectionStrings));
    services.AddScoped<IExecutorService, ExecutorService>();
    services.AddScoped<IFollowUpActionHandlersFactory, FollowUpActionHandlersFactory>();
    services.AddScoped<MailNotificationSender>();
    services.AddLogging();
    _ = services.AddScoped<Func<string, IFollowUpActionHandler>>(factory =>
        typeName => true switch {
            _ when String.Equals(Constants.MailNotificationSender, typeName, StringComparison.OrdinalIgnoreCase) => factory.GetService<MailNotificationSender>() 
                ?? throw new InvalidOperationException($"{nameof(MailNotificationSender)} is not registered in DI container"),
            _ => throw new InvalidOperationException($"{typeName} type doesn't configured for followup action handlers factory") 
            });
});

var host = builder.Build();

using (host) {
    await host.RunAsync();
}
