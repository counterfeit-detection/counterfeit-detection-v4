﻿using Syngenta.CounterfeitDetection.DataAccess.Models;
using System;

namespace Syngenta.CounterfeitDetection.WebJob.AutomaticFollowUpActionExecutor.Constants;

internal class Constants {
    public const string MailNotificationSender = "Send mail notification";
    public const string ConnectionStrings = "ConnectionStrings";
    public const string DirectReport = "Direct report";
    public const string ScanReport = "Scan report";
    public const string NonGPDProduct = "Non-GPD Product";
    public const string Invalid = "Invalid";
    public const string Valid = "Valid";
    public const string Blacklisted = "Blacklisted";
    public const string Relabled = "Relabled";
    public const string Unknown = "Unknown";

    internal class ConfigurationKeys {
        public const string SMTP_EmailId = "SMTP:EmailId";
        public const string SMTP_Password = "SMTP:Password";
        public const string SMTP_PortNumber = "SMTP:PortNumber";
        public const string SMTP_Server = "SMTP:Server";
    }

    internal class MailMessageTypes {
        public const string ValidProduct = "Corporate Security Alert: Valid Product";
        public const string InvalidProduct = "Corporate Security Alert: Invalid Product";
        public const string NonGPDProduct = "Corporate Security Alert: Non-GPD Product";
        public const string DirectReportedGPDProduct = "Corporate Security Alert: Direct Reported GPD Product";
        public const string DirectReportedNonGPDProduct = "Corporate Security Alert: Direct Reported Non-GPD Product";
    }
}
