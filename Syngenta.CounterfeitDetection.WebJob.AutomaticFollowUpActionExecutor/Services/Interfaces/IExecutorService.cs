﻿namespace Syngenta.CounterfeitDetection.WebJob.AutomaticFollowUpActionExecutor.Services.Interfaces;

public interface IExecutorService {
    public Task ExecuteAsync();
}
