﻿namespace Syngenta.CounterfeitDetection.WebJob.AutomaticFollowUpActionExecutor.Services.Interfaces;

public interface IFollowUpActionHandlersFactory {
    public IFollowUpActionHandler GetHandler(string typeName);
}
