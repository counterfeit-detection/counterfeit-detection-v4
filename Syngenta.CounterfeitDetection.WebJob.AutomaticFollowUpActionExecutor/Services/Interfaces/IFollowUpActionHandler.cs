﻿namespace Syngenta.CounterfeitDetection.WebJob.AutomaticFollowUpActionExecutor.Services.Interfaces;

public interface IFollowUpActionHandler {
    public Task HandleAsync(int transactionId);
}
