﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Syngenta.CounterfeitDetection.DataAccess.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Models;
using Syngenta.CounterfeitDetection.Infrastructure.Exceptions;
using Syngenta.CounterfeitDetection.WebJob.AutomaticFollowUpActionExecutor.Services.Interfaces;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;

namespace Syngenta.CounterfeitDetection.WebJob.AutomaticFollowUpActionExecutor.Services.FollowUpActionHandlers;

internal class MailNotificationSender : IFollowUpActionHandler {
    private readonly IGenericRepository<OutgoingMail> _outgoingMailRepo;
    private readonly IGenericRepository<OutgoingMailAttachment> _outgoingMailAttachmentRepo;
    private readonly IGenericRepository<MailMessageTemplate> _mailMessageTemplateRepo;
    private readonly IGenericRepository<MailMessageType> _mailMessageTypeRepo;
    private readonly IGenericRepository<CountryMailSetting> _countryMailSettingRepo;
    private readonly IGenericRepository<ReportImage> _reportImageRepo;
    private readonly IGenericRepository<TransactionResult> _transactionResultRepo;
    private readonly IGenericRepository<Report> _reportRepo;
    private readonly IGenericRepository<Transaction> _transactionRepo;
    private readonly IGenericRepository<ProductPackSizeGTIN> _productPackSizeGTINRepo;
    private readonly IConfiguration _configuration;

    public MailNotificationSender(
        IGenericRepository<OutgoingMail> outgoingMailRepo,
        IGenericRepository<OutgoingMailAttachment> outgoingMailAttachmentRepo,
        IGenericRepository<MailMessageTemplate> mailMessageTemplateRepo,
        IGenericRepository<MailMessageType> mailMessageTypeRepo,
        IGenericRepository<CountryMailSetting> countryMailSettingRepo,
        IGenericRepository<ReportImage> reportImageRepo,
        IGenericRepository<TransactionResult> transactionResultRepo,
        IGenericRepository<Report> reportRepo,
        IGenericRepository<Transaction> transactionRepo,
        IGenericRepository<ProductPackSizeGTIN> productPackSizeGTINRepo,
        IConfiguration configuration) {
        _outgoingMailRepo = outgoingMailRepo;
        _outgoingMailAttachmentRepo = outgoingMailAttachmentRepo;
        _mailMessageTemplateRepo = mailMessageTemplateRepo;
        _mailMessageTypeRepo = mailMessageTypeRepo;
        _countryMailSettingRepo = countryMailSettingRepo;
        _reportImageRepo = reportImageRepo;
        _transactionResultRepo = transactionResultRepo;
        _reportRepo = reportRepo;
        _transactionRepo = transactionRepo;
        _productPackSizeGTINRepo = productPackSizeGTINRepo;
        _configuration = configuration;
    }

    public async Task HandleAsync(int transactionId) {
        var transaction = (await _transactionRepo.GetManyAsync(tr => tr.Id == transactionId,
            include: transactions => transactions
                .Include(x => x.ItemDetails!)                        
                .Include(x => x.SapDetails!)
                .Include(x => x.ScanInputDetails!)
                .Include(x => x.User!)
                .Include(x => x.Result!)
                    .ThenInclude(x => x.Type!)))
            .FirstOrDefault();

        var report = (await _reportRepo.GetManyAsync(report => report.TransactionId == transactionId,
            include: reports => reports
                .Include(x => x.Type!)
                .Include(x => x.ReportReason!)
                .Include(x => x.ScanningPoint!)))
            .FirstOrDefault() ?? throw new KeyNotFoundException($"<Report> for <Transaction> with id: <{transactionId}> not found");

        var mailMessageType = await DefineMailMessageType(report, transaction!);

        var mailMessageTemplate = (await _mailMessageTemplateRepo.GetManyAsync(template => template.TypeId == mailMessageType!.Id))
            .FirstOrDefault() ?? throw new DataConsistencyException($"Unable to find mail message template for type: <{mailMessageType!.Id}>");

        var countryMailSetting = (await _countryMailSettingRepo.GetManyAsync(template => template.MessageTypeId == mailMessageType!.Id 
                && template.CountryId == transaction!.UserCountryId))
            .FirstOrDefault();
        var images = (await _reportImageRepo.GetManyAsync(image => image.ReportId == report.Id,
                include: images => images
                .Include(x => x.ImageFile!)))
            .Select(image => image.ImageFile);

        var mailBody = await BuildMailBody(mailMessageTemplate!.Body!, report!, transaction);

        await SendMail(countryMailSetting, mailMessageTemplate!.Subject, mailBody, images!);
    }

    private async Task<string> BuildMailBody(string body, Report report, Transaction? transaction) {
        var productpackSizeGTIN = (await _productPackSizeGTINRepo
            .GetManyAsync(x => x.GTIN!.Equals(transaction!.ItemDetails!.GTIN),
                include: packSizeGTINs => packSizeGTINs
                .Include(x => x.PackSize!)
                    .ThenInclude(x => x.ProductCatalogue!)
                .Include(x => x.PackSize!)
                    .ThenInclude(x => x.MeasureUnit!)))
            .FirstOrDefault();

        if (report?.Type?.Type?.Equals(Constants.Constants.DirectReport) ?? false) {
            var isGPD = productpackSizeGTIN?.IsGPD ?? false;

            body = body.Replace("$userName", transaction!.User!.Name);
            body = body.Replace("$userEmail", transaction.User!.AuthId);
            body = body.Replace("$GTIN", hideIfNotPresented(transaction!.ItemDetails!.GTIN));
            body = body.Replace("$batchNumber", hideIfNotPresented(transaction!.ItemDetails!.BatchNumber));
            body = body.Replace("$serailNumber", hideIfNotPresented(transaction!.ItemDetails!.SerialNumber));
            body = body.Replace("$brandName", productpackSizeGTIN?.PackSize?.ProductCatalogue?.Name);
            body = body.Replace("$packSize", hideIfNotPresented(productpackSizeGTIN?.PackSize?.MeasureUnit?.Unit));
            body = body.Replace("$haveQRCode", isGPD ? "Yes" : "No");
            body = body.Replace("$geoLocation", String.Format("https://www.google.com/maps/search/?api=1&query={0}", transaction.GeoLocation));
            body = body.Replace("$productLocationName", hideIfNotPresented(report.ScanLocationName));
            body = body.Replace("$productLocationAddress", hideIfNotPresented(report.ScanAddress));
        }
        if (report?.Type?.Type?.Equals(Constants.Constants.ScanReport) ?? false) {
            var transactionResult = (await _transactionResultRepo.GetManyAsync(result => result.TransactionId == transaction!.Id,
                    include: tr => tr
                    .Include(x => x.Type!)))
                .FirstOrDefault();

            body = body.Replace("$userEmail", transaction!.User!.AuthId);
            body = body.Replace("$userName", transaction!.User!.Name);
            body = body.Replace("$GTIN", hideIfNotPresented(transaction.ItemDetails!.GTIN));
            body = body.Replace("$serailNumber", hideIfNotPresented(transaction.ItemDetails!.SerialNumber));
            body = body.Replace("$batchNumber", hideIfNotPresented(transaction.ItemDetails!.BatchNumber));
            body = body.Replace("$manufactureDate", hideIfNotPresentedDate(transaction.ItemDetails!.ManufacturedDate));
            body = body.Replace("$scanNumber", transaction.SapDetails?.NumberOfScans.ToString());
            body = body.Replace("$manufactureAt", hideIfNotPresented(transaction.ItemDetails!.ManufacturedAt));
            body = body.Replace("$expiryDate", hideIfNotPresentedDate(transaction.ItemDetails!.ExpiryDate));
            body = body.Replace("$productName", productpackSizeGTIN?.PackSize?.ProductCatalogue?.Name);
            body = body.Replace("$packSize",hideIfNotPresented(productpackSizeGTIN?.PackSize?.MeasureUnit?.Unit));
            body = body.Replace("$scanLocation", hideIfNotPresented(report.ScanningPoint?.Point));
            body = body.Replace("$scanAddress", hideIfNotPresented(report.ScanAddress));
            body = body.Replace("$purchaseLocationName", hideIfNotPresented(report.ScanLocationName));
            body = body.Replace("$responseMesaage", hideIfNotPresented(transactionResult?.Type?.Type));
            body = body.Replace("$geoLocation", String.Format("https://www.google.com/maps/search/?api=1&query={0}", transaction.GeoLocation));
        }
        
        body = body.Replace("$whereWasFound", hideIfNotPresented(report!.ScanningPoint!.Point));
        body = body.Replace("$reportReason", hideIfNotPresented(report.ReportReason!.ReasonKey));
        body = body.Replace("$comments", hideIfNotPresented(report.Comment));
        body = body.Replace("</p><p>", "<br>");
        body = body.Replace("<p>", String.Empty);
        body = body.Replace("</p>", String.Empty);
        body = body.Replace("</ p>", "<br>");
        body = body.Replace("<P>", "<br>");
                
        return body;

        string hideIfNotPresented(string? value)
            => String.IsNullOrEmpty(value) ? "NA" : value;

        string? hideIfNotPresentedDate(DateTime? value)
            => value is null
            ? "NA" 
            : value.ToString();
    }

    private async Task<MailMessageType?> DefineMailMessageType(Report report, Transaction transaction) {
        var mailType = String.Empty;
        var productpackSizeGTIN = (await _productPackSizeGTINRepo
            .GetManyAsync(x => x.GTIN!.Equals(transaction!.ItemDetails!.GTIN)))
            .FirstOrDefault();

        if (report.Type!.Type?.Equals(Constants.Constants.DirectReport) ?? false) {
            var isGPD = productpackSizeGTIN?.IsGPD ?? false;

            mailType = isGPD 
                ? Constants.Constants.MailMessageTypes.DirectReportedGPDProduct
                : Constants.Constants.MailMessageTypes.DirectReportedNonGPDProduct;
        }
        if (report.Type!.Type?.Equals(Constants.Constants.ScanReport) ?? false) {

            mailType = true switch {
                var _ when transaction.Result!.Type!.Type!.Equals(Constants.Constants.NonGPDProduct) => Constants.Constants.MailMessageTypes.NonGPDProduct,
                var _ when transaction.Result!.Type!.Type!.Contains(Constants.Constants.Blacklisted) => Constants.Constants.MailMessageTypes.InvalidProduct,
                var _ when transaction.Result!.Type!.Type!.Contains(Constants.Constants.Relabled) => Constants.Constants.MailMessageTypes.InvalidProduct,
                var _ when transaction.Result!.Type!.Type!.Contains(Constants.Constants.Invalid) => Constants.Constants.MailMessageTypes.InvalidProduct,
                var _ when transaction.Result!.Type!.Type!.Contains(Constants.Constants.Valid) => Constants.Constants.MailMessageTypes.ValidProduct,
                _ => Constants.Constants.Unknown
            };
        }

        return (await _mailMessageTypeRepo.GetAllAsync())
            .FirstOrDefault(type => type.Type!.Contains(mailType)) 
            ?? throw new DataConsistencyException($"Unable to define mailType for report type: <{report.Type!.Type}> " +
                $"and transaction result: <{transaction.Result?.Type?.Type}>");
    }

    private async Task SendMail(CountryMailSetting? mailSettings, string? subject, string? body, IEnumerable<BlobStorageFile>? attachments) {
        var message = new MailMessage() {
            From = new MailAddress(mailSettings!.From 
                    ?? throw new ArgumentNullException(nameof(mailSettings.From), $"{nameof(mailSettings.From)} parameter should be specified for mail send"),
                mailSettings.FromMask, System.Text.Encoding.UTF8),
            IsBodyHtml = true,
            Body = body,
            Subject = subject
        };

        if (!String.IsNullOrEmpty(mailSettings.To)) {
            message.To.Add(mailSettings.To);
        }

        if (!String.IsNullOrEmpty(mailSettings.Cc)) {
            message.CC.Add(mailSettings.Cc);
        }
        
        if (!String.IsNullOrEmpty(mailSettings.Bcc)) {
            message.Bcc.Add(mailSettings.Bcc);
        }

        if (attachments?.Any() ?? false)
        {
            var i = 1;
            foreach (var image in attachments)
            {
                var contentType = new ContentType();
                contentType.MediaType = MediaTypeNames.Image.Jpeg;
                contentType.Name = String.Concat("ProductImage", i, ".jpg");
                message.Attachments.Add(new Attachment(await ReadFile(image), contentType));
                i++;
            }
        }

        var outgoingMail = new OutgoingMail() {
            To = mailSettings.To,
            Cc = mailSettings.Cc,
            Bcc = mailSettings.Bcc,
            From = mailSettings.From,
            FromMask = mailSettings.FromMask,
            Subject = subject,
            Body = body
        };

        outgoingMail = await _outgoingMailRepo.CreateAsync(outgoingMail);

        if (attachments?.Any() ?? false)
        {
            foreach (var attachment in attachments) {
                await _outgoingMailAttachmentRepo.CreateAsync(new OutgoingMailAttachment() {
                    MailId = outgoingMail.Id,
                    AttachmentId = attachment.Id
                });
            } 
        }
        
        //TODO: move into separate worker GPDCE-3859
        using var smtp = new SmtpClient
        {
            Host = _configuration.GetValue<string>(Constants.Constants.ConfigurationKeys.SMTP_Server)
                ?? throw new ConfigurationErrorsException($"{Constants.Constants.ConfigurationKeys.SMTP_Server} not specified in configuration file"),
            Port = _configuration.GetValue<int?>(Constants.Constants.ConfigurationKeys.SMTP_PortNumber)
                ?? throw new ConfigurationErrorsException($"{Constants.Constants.ConfigurationKeys.SMTP_PortNumber} not specified in configuration file"),
            EnableSsl = true,
            Credentials = new NetworkCredential(_configuration.GetValue<string>(Constants.Constants.ConfigurationKeys.SMTP_EmailId),
                _configuration.GetValue<string>(Constants.Constants.ConfigurationKeys.SMTP_Password)),
        };

        smtp.Send(message);    
    }

    private static async Task<Stream> ReadFile(BlobStorageFile file) {
        using var client = new HttpClient();
        using var content = await client.GetStreamAsync(file.StorageUrl);
        return content;
    }
}
