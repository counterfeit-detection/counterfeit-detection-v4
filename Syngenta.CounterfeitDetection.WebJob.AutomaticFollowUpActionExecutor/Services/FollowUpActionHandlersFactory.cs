﻿using Syngenta.CounterfeitDetection.WebJob.AutomaticFollowUpActionExecutor.Services.Interfaces;

namespace Syngenta.CounterfeitDetection.WebJob.AutomaticFollowUpActionExecutor.Services;

internal class FollowUpActionHandlersFactory : IFollowUpActionHandlersFactory {
    private readonly Func<string, IFollowUpActionHandler> _factory;

    public FollowUpActionHandlersFactory(Func<string, IFollowUpActionHandler> factory) => _factory = factory;

    public IFollowUpActionHandler GetHandler(string typeName) => _factory(typeName);
}
