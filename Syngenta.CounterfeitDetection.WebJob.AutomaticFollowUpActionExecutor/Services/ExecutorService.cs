﻿using Microsoft.EntityFrameworkCore;
using Syngenta.CounterfeitDetection.DataAccess.Interfaces;
using Syngenta.CounterfeitDetection.DataAccess.Models;
using Syngenta.CounterfeitDetection.WebJob.AutomaticFollowUpActionExecutor.Services.Interfaces;

namespace Syngenta.CounterfeitDetection.WebJob.AutomaticFollowUpActionExecutor.Services;

internal class ExecutorService : IExecutorService {
	private readonly IFollowUpActionHandlersFactory _factory;
	private readonly IGenericRepository<FollowUpAction> _repository;

	public ExecutorService(IFollowUpActionHandlersFactory factory, IGenericRepository<FollowUpAction> repository) {
		_factory = factory;
		_repository = repository;
    }

    public async Task ExecuteAsync() {
		var unhandledItems = await _repository
			.GetManyAsync(action => !action.IsCompleted && action.Type != null && action.Type.IsAutomatic,
             include: followUps => followUps
                .Include(x => x.Type!));
        var handledItems = new List<FollowUpAction>();

		if (unhandledItems.Any()) {
			foreach (var unhandledItem in unhandledItems) {
				try {
					await _factory.GetHandler(unhandledItem.Type?.Type ?? String.Empty).HandleAsync(unhandledItem.TransactionId);
                    unhandledItem.IsCompleted = true;
                    handledItems.Add(unhandledItem);
                }
				catch (Exception ex) {
					Console.WriteLine($"Item with id:{unhandledItem.Id} failed to handle. {ex.Message}");
                }
            }

            if(handledItems.Any())
                await _repository.UpdateRangeAsync(handledItems);
        }
    }
}
